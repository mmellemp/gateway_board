/**
  ******************************************************************************
  * @file    gateway_board_lin.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   This file contains the common defines and functions prototypes for
  *          the Gateway_Board_lin.c driver.
  ******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_LIN_H
#define __GATEWAY_BOARD_LIN_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "gateway_board.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Gateway_Board
  * @{
  */
    
/** @defgroup Gateway_Board_Ecan
  * @{
  */


/** @defgroup Gateway_Board_ecan_Exported_Types
  * @{
  */

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Exported_Constants
  * @{
  */

//typedef enum
//{
//	LINA = 0,
//	LINB
//}LIN_Typedef;



/** @defgroup Gateway_Board_Ecan_Exported_Functions
  * @{
  */
	

HAL_StatusTypeDef BSP_LIN_Init(LIN_Typedef linType,uint32_t BreakDetectLength);
void 						 BSP_LIN_IO_Write(LIN_Typedef linType,uint8_t *pData,uint16_t size);
void						 BSP_LIN_ReceiveComplete_CallBack(LIN_Typedef linType);
void 						 BSP_LIN_Receive_Error_Callback(LIN_Typedef linType);
void 						 BSP_LIN_Query_Slave(LIN_Typedef lintype,uint8_t slaveID);
void 						 BSP_LIN_TransmitComplete_Callback(LIN_Typedef linType);
void             BSP_LIN_DMA_MSP_Init(UART_HandleTypeDef *huart);
/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_Ecan_H */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
