/**
  ******************************************************************************
  * @file    gateway_board.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   This file provides a set of firmware functions to manage LEDs, 
  *          push-buttons and COM ports available on Gateway Board evaluation 
  *          from Arka IMS.
  ******************************************************************************
  */ 
  
/* File Info: 
-----------------------------------------------------------------------------
                                   User NOTE

------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
#include "gateway_board.h"

#define __Gateway_Board_BSP_VERSION_MAIN   (0x02) /*!< [31:24] main version */
#define __Gateway_Board_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __Gateway_Board_BSP_VERSION_SUB2   (0x01) /*!< [15:8]  sub2 version */
#define __Gateway_Board_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */ 
#define __Gateway_Board_BSP_VERSION         ((__Gateway_Board_BSP_VERSION_MAIN << 24)\
                                             |(__Gateway_Board_BSP_VERSION_SUB1 << 16)\
                                             |(__Gateway_Board_BSP_VERSION_SUB2 << 8 )\
                                             |(__Gateway_Board_BSP_VERSION_RC))
/**
  * @}
  */

/** @defgroup Gateway_Board_LOW_LEVEL_Private_Macros
  * @{
  */
/**
  * @}
  */

/** @defgroup Gateway_Board_LOW_LEVEL_Private_Variables
  * @{
  */
GPIO_TypeDef* GPIO_PORT[LEDn] = {LED1_GPIO_PORT,
                                 LED2_GPIO_PORT,
                                 LED3_GPIO_PORT,
                                 LED4_GPIO_PORT,
																 LED5_GPIO_PORT};

const uint16_t GPIO_PIN[LEDn] = {LED1_PIN,
                                 LED2_PIN,
                                 LED3_PIN,
                                 LED4_PIN,
																 LED5_PIN};

GPIO_TypeDef* BUTTON_PORT[BUTTONn] = {BUTTON_1_GPIO_PORT,
																			BUTTON_2_GPIO_PORT,
																			BUTTON_3_GPIO_PORT,
																			BUTTON_4_GPIO_PORT,
																			BUTTON_5_GPIO_PORT,
																			BUTTON_6_GPIO_PORT};

const uint16_t BUTTON_PIN[BUTTONn] = {BUTTON_1_PIN,
																			BUTTON_2_PIN,
																			BUTTON_3_PIN,
																			BUTTON_4_PIN,
																			BUTTON_5_PIN,
																			BUTTON_6_PIN};


USART_TypeDef* COM_USART[COMn] = {EVAL_COM1,
																	EVAL_COM2,
																	EVAL_COM3,
																	EVAL_COM4,
																	EVAL_COM5};

GPIO_TypeDef* COM_TX_PORT[COMn] = {	EVAL_COM1_TX_GPIO_PORT,
																		EVAL_COM2_TX_GPIO_PORT,
																		EVAL_COM3_TX_GPIO_PORT,
																		EVAL_COM4_TX_GPIO_PORT,
																		EVAL_COM5_TX_GPIO_PORT};

GPIO_TypeDef* COM_RX_PORT[COMn] = {EVAL_COM1_RX_GPIO_PORT,EVAL_COM2_RX_GPIO_PORT,EVAL_COM3_RX_GPIO_PORT,EVAL_COM4_RX_GPIO_PORT,EVAL_COM5_RX_GPIO_PORT};

const uint16_t COM_TX_PIN[COMn] = {EVAL_COM1_TX_PIN,EVAL_COM2_TX_PIN,EVAL_COM3_TX_PIN,EVAL_COM4_TX_PIN,EVAL_COM5_TX_PIN};

const uint16_t COM_RX_PIN[COMn] = {EVAL_COM1_RX_PIN,EVAL_COM2_RX_PIN,EVAL_COM3_RX_PIN,EVAL_COM4_RX_PIN,EVAL_COM5_RX_PIN};

const uint16_t COM_TX_AF[COMn] = {EVAL_COM1_TX_AF,EVAL_COM2_TX_AF,EVAL_COM3_TX_AF,EVAL_COM4_TX_AF,EVAL_COM5_TX_AF};

const uint16_t COM_RX_AF[COMn] = {EVAL_COM1_RX_AF,EVAL_COM2_RX_AF,EVAL_COM3_RX_AF,EVAL_COM4_RX_AF,EVAL_COM5_RX_AF};

const uint16_t COM_RX_IRQ[COMn] = {EVAL_COM1_IRQn,EVAL_COM2_IRQn,EVAL_COM3_IRQn,EVAL_COM4_IRQn,EVAL_COM5_IRQn};

SPI_TypeDef* EVAL_SPIx[SPIn] = {EVAL_SPI1,EVAL_SPI2,EVAL_SPI3,EVAL_SPI4,EVAL_SPI5,EVAL_SPI6};

GPIO_TypeDef* EVAL_SPIx_SCK_PORT[SPIn] = {EVAL_SPI1_SCK_GPIO_PORT,EVAL_SPI2_SCK_GPIO_PORT,EVAL_SPI3_SCK_GPIO_PORT,EVAL_SPI4_SCK_GPIO_PORT,EVAL_SPI5_SCK_GPIO_PORT,EVAL_SPI6_SCK_GPIO_PORT};

GPIO_TypeDef* EVAL_SPIx_MISO_PORT[SPIn] = {EVAL_SPI1_MISO_GPIO_PORT,EVAL_SPI2_MISO_GPIO_PORT,EVAL_SPI3_MISO_GPIO_PORT,EVAL_SPI4_MISO_GPIO_PORT,EVAL_SPI5_MISO_GPIO_PORT,EVAL_SPI6_MISO_GPIO_PORT};

GPIO_TypeDef* EVAL_SPIx_MOSI_PORT[SPIn] = {EVAL_SPI1_MOSI_GPIO_PORT,EVAL_SPI2_MOSI_GPIO_PORT,EVAL_SPI3_MOSI_GPIO_PORT,EVAL_SPI4_MOSI_GPIO_PORT,EVAL_SPI5_MOSI_GPIO_PORT,EVAL_SPI6_MOSI_GPIO_PORT};

GPIO_TypeDef* EVAL_SPIx_NSS_PORT[SPIn] = {EVAL_SPI1_NSS_GPIO_PORT,EVAL_SPI2_NSS_GPIO_PORT,EVAL_SPI3_NSS_GPIO_PORT,EVAL_SPI4_NSS_GPIO_PORT,EVAL_SPI5_NSS_GPIO_PORT,EVAL_SPI6_NSS_GPIO_PORT};

const uint16_t EVAL_SPIx_SCK_PIN[SPIn] = {EVAL_SPI1_SCK_PIN,EVAL_SPI2_SCK_PIN,EVAL_SPI3_SCK_PIN,EVAL_SPI4_SCK_PIN,EVAL_SPI5_SCK_PIN,EVAL_SPI6_SCK_PIN};

const uint16_t EVAL_SPIx_MISO_PIN[SPIn] = {EVAL_SPI1_MISO_PIN,EVAL_SPI2_MISO_PIN,EVAL_SPI3_MISO_PIN,EVAL_SPI4_MISO_PIN,EVAL_SPI5_MISO_PIN,EVAL_SPI6_MISO_PIN};

const uint16_t EVAL_SPIx_MOSI_PIN[SPIn] = {EVAL_SPI1_MOSI_PIN,EVAL_SPI2_MOSI_PIN,EVAL_SPI3_MOSI_PIN,EVAL_SPI4_MOSI_PIN,EVAL_SPI5_MOSI_PIN,EVAL_SPI6_MOSI_PIN};

const uint16_t EVAL_SPIx_NSS_PIN[SPIn] = {EVAL_SPI1_NSS_PIN,EVAL_SPI2_NSS_PIN,EVAL_SPI3_NSS_PIN,EVAL_SPI4_NSS_PIN,EVAL_SPI5_NSS_PIN,EVAL_SPI6_NSS_PIN};

const uint16_t EVAL_SPIx_AF[SPIn] = {EVAL_SPI1_AF,EVAL_SPI2_AF,EVAL_SPI3_AF,EVAL_SPI4_AF,EVAL_SPI5_AF,EVAL_SPI6_AF};

I2C_TypeDef* EVAL_I2Cx[I2Cn] = {EVAL_I2C1,EVAL_I2C2};

GPIO_TypeDef* EVAL_I2Cx_SCL_SDA_GPIO_PORT[I2Cn] = {EVAL_I2C1_SCL_SDA_GPIO_PORT,EVAL_I2C2_SCL_SDA_GPIO_PORT};

const uint16_t EVAL_I2Cx_SCL_SDA_AF[I2Cn] = {EVAL_I2C1_SCL_SDA_AF,EVAL_I2C2_SCL_SDA_AF};

const uint16_t EVAL_I2Cx_SCL_PIN[I2Cn] = {EVAL_I2C1_SCL_PIN,EVAL_I2C2_SCL_PIN};

const uint16_t EVAL_I2Cx_SDA_PIN[I2Cn] = {EVAL_I2C1_SDA_PIN,EVAL_I2C2_SDA_PIN};

const uint16_t EVAL_I2Cx_EV_IRQ[I2Cn] = {EVAL_I2C1_EV_IRQn,EVAL_I2C2_EV_IRQn};

const uint16_t EVAL_I2Cx_ER_IRQ[I2Cn] = {EVAL_I2C1_ER_IRQn,EVAL_I2C2_ER_IRQn};

uint32_t I2C_speed = I2C_SPEED_LOW;

SPI_HandleTypeDef    EVAL_SPIxHandle[SPIn] = {0};				/*separate handle for all SPI*/
I2C_HandleTypeDef		 EVAL_I2CxHandle[I2Cn] = {0};				/*separate handle for all I2C*/

extern volatile uint8_t buff[50];


/**
  * @}
  */

/** @defgroup Gateway_Board_LOW_LEVEL_Private_FunctionPrototypes
  * @{
  */
static void     I2Cx_MspInit(I2C_Port_Typedef I2CPort);
static void     I2Cx_Init(I2C_Port_Typedef I2CPort);
static void     I2Cx_Write(I2C_Port_Typedef I2CPort,uint8_t Addr, uint8_t Reg, uint8_t Value);
static uint8_t  I2Cx_Read(I2C_Port_Typedef I2CPort,uint8_t Addr, uint8_t Reg);
static HAL_StatusTypeDef I2Cx_ReadMultiple(I2C_Port_Typedef I2CPort,uint8_t Addr, uint16_t Reg, uint16_t MemAddSize, uint8_t *Buffer, uint16_t Length);
static HAL_StatusTypeDef I2Cx_WriteMultiple(I2C_Port_Typedef I2CPort,uint8_t Addr, uint16_t Reg, uint16_t MemAddSize, uint8_t *Buffer, uint16_t Length);
static HAL_StatusTypeDef I2Cx_IsDeviceReady(I2C_Port_Typedef I2CPort,uint16_t DevAddress, uint32_t Trials);
static void     I2Cx_Error(I2C_Port_Typedef I2CPort,uint8_t Addr);


static void     SPIx_Init(SPI_Port_TypeDef SPIPort);
static void     SPIx_MspInit(SPI_Port_TypeDef SPIPort);
static void  		SPIx_Write(SPI_Port_TypeDef SPIPort, uint8_t Byte);
static  void    SPIx_Error(SPI_Port_TypeDef SPIPort);

static void 		setSWCANMode(SWCAN_Mode_Typedef canMode);
static void 		setGMLANMode(SWCAN_Mode_Typedef canMode);

/* I2C EEPROM IO function */
void                EEPROM_IO_Init(void);
HAL_StatusTypeDef   EEPROM_IO_WriteData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize);
HAL_StatusTypeDef   EEPROM_IO_ReadData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize);
HAL_StatusTypeDef   EEPROM_IO_IsDeviceReady(uint16_t DevAddress, uint32_t Trials);


extern void BSP_LIN_DMA_MSP_Init(UART_HandleTypeDef *huart);
/**
  * @}
  */

/** @defgroup Gateway_Board_LOW_LEVEL_Private_Functions
  * @{
  */ 

  /**
  * @brief  This method returns the Gateway Board BSP Driver revision
  * @param  None
  * @retval version : 0xXYZR (8bits for each decimal, R for RC)
  */
uint32_t BSP_GetVersion(void)
{
  return __Gateway_Board_BSP_VERSION;
}

/**
  * @brief  Configures LED GPIO.
  * @param  Led: LED to be configured. 
  *          This parameter can be one of the following values:
  *            @arg  LED1
  *            @arg  LED2
  *            @arg  LED3
  *            @arg  LED4
  * @retval None
  */
void BSP_LED_Init(Led_TypeDef Led)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /* Enable the GPIO_LED clock */
  LEDx_GPIO_CLK_ENABLE(Led);

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = GPIO_PIN[Led];
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

  HAL_GPIO_Init(GPIO_PORT[Led], &GPIO_InitStruct);

  //HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_SET);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: LED to be set on 
  *          This parameter can be one of the following values:
  *            @arg  LED1
  *            @arg  LED2
  *            @arg  LED3
  *            @arg  LED4
  * @retval None
  */
void BSP_LED_On(Led_TypeDef Led)
{
  //HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
}

/**
  * @brief  Turns selected LED Off. 
  * @param  Led: LED to be set off
  *          This parameter can be one of the following values:
  *            @arg  LED1
  *            @arg  LED2
  *            @arg  LED3
  *            @arg  LED4
  * @retval None
  */
void BSP_LED_Off(Led_TypeDef Led)
{
  //HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_SET); 
}

/**
  * @brief  Toggles the selected LED.
  * @param  Led: LED to be toggled
  *          This parameter can be one of the following values:
  *            @arg  LED1
  *            @arg  LED2
  *            @arg  LED3
  *            @arg  LED4
  * @retval None
  */
void BSP_LED_Toggle(Led_TypeDef Led)
{
  HAL_GPIO_TogglePin(GPIO_PORT[Led], GPIO_PIN[Led]);
}

/**
  * @brief  Configures button GPIO and EXTI Line.
  * @param  Button: Button to be configured
  *          This parameter can be one of the following values:
  *            @arg  BUTTON_WAKEUP: Wakeup Push Button 
  *            @arg  BUTTON_TAMPER: Tamper Push Button  
  * @param  Button_Mode: Button mode
  *          This parameter can be one of the following values:
  *            @arg  BUTTON_MODE_GPIO: Button will be used as simple IO
  *            @arg  BUTTON_MODE_EXTI: Button will be connected to EXTI line 
  *                                    with interrupt generation capability  
  * @retval None
  */
void BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef Button_Mode)
{
  GPIO_InitTypeDef GPIO_InitStruct;
	
  Button_Mode = BUTTON_MODE_GPIO; 
	
  /* Enable the BUTTON clock */
  BUTTONx_GPIO_CLK_ENABLE(Button);
  __SYSCFG_CLK_ENABLE();
  
  if(Button_Mode == BUTTON_MODE_GPIO)
  {
    /* Configure Button pin as input */
    GPIO_InitStruct.Pin = BUTTON_PIN[Button];
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);
  }
}

/**
  * @brief  Returns the selected button state.
  * @param  Button: Button to be checked
  *          This parameter can be one of the following values:
  *            @arg  BUTTON_WAKEUP: Wakeup Push Button 
  *            @arg  BUTTON_TAMPER: Tamper Push Button 
  *            @arg  BUTTON_KEY: Key Push Button
  * @retval The Button GPIO pin value
  */
uint32_t BSP_PB_GetState(Button_TypeDef Button)
{
  return HAL_GPIO_ReadPin(BUTTON_PORT[Button], BUTTON_PIN[Button]);
}





/**
  * @brief  Configures COM port.
  * @param  COM: COM port to be configured.
  *          This parameter can be one of the following values:
  *            @arg  COM1 
  *            @arg  COM2 
  * @param  huart: Pointer to a UART_HandleTypeDef structure that contains the
  *                configuration information for the specified USART peripheral.
  * @retval None
  */
void BSP_COM_Init(COM_TypeDef COM, uint32_t baud_rate, UART_HandleTypeDef *huart)
{
	
  GPIO_InitTypeDef GPIO_InitStruct;

  /* Enable GPIO clock */
  EVAL_COMx_TX_GPIO_CLK_ENABLE(COM);
  EVAL_COMx_RX_GPIO_CLK_ENABLE(COM);

  /* Enable USART clock */
  EVAL_COMx_CLK_ENABLE(COM);
	  /* Enable DMA1 clock */ 
  BSP_LIN_DMA_MSP_Init(huart);
	
  /* Configure USART Tx as alternate function */
  GPIO_InitStruct.Pin = COM_TX_PIN[COM];
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Alternate = COM_TX_AF[COM];
  HAL_GPIO_Init(COM_TX_PORT[COM], &GPIO_InitStruct);

  /* Configure USART Rx as alternate function */
  GPIO_InitStruct.Pin = COM_RX_PIN[COM];
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Alternate = COM_RX_AF[COM];
  HAL_GPIO_Init(COM_RX_PORT[COM], &GPIO_InitStruct);
	
  /* USART configuration */
  huart->Instance = COM_USART[COM];
	
	huart->Init.BaudRate = baud_rate;
	huart->Init.WordLength = UART_WORDLENGTH_8B;
	huart->Init.StopBits   = UART_STOPBITS_1;
	huart->Init.Parity     = UART_PARITY_NONE;
	huart->Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	huart->Init.Mode       = UART_MODE_TX_RX;
	
	/*##-3- Configure the NVIC for UART ########################################*/   
  /* NVIC for USARTx */
//  HAL_NVIC_SetPriority(COM_RX_IRQ[COM], 0, 1);
//  HAL_NVIC_EnableIRQ(COM_RX_IRQ[COM]);


  HAL_UART_Init(huart);
}



/*******************************************************************************
                            BUS OPERATIONS
*******************************************************************************/
/******************************* SPI Routines**********************************/
/**
  * @brief  Configures SPI interface.
  * @param  None
  * @retval None
  */
static void SPIx_Init(SPI_Port_TypeDef SPIPort)
{
  if(HAL_SPI_GetState(&EVAL_SPIxHandle[SPIPort]) == HAL_SPI_STATE_RESET)
  {
    /* SPI configuration -------------------------------------------------------*/
		EVAL_SPIxHandle[SPIPort].Instance               = EVAL_SPIx[SPIPort];
		EVAL_SPIxHandle[SPIPort].Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
		EVAL_SPIxHandle[SPIPort].Init.Direction         = SPI_DIRECTION_2LINES;
		EVAL_SPIxHandle[SPIPort].Init.CLKPhase          = SPI_PHASE_1EDGE;		// As Per ST7565R Datasheet 4-line SPI Interface SCL polarity HiGH
		EVAL_SPIxHandle[SPIPort].Init.CLKPolarity       = SPI_POLARITY_LOW;	// (SPI_PHASE_2EDGE with SPI_POLARITY_HIGH )OR (SPI_PHASE_1EDGE with SPI_POLARITY_LOW) working  
		EVAL_SPIxHandle[SPIPort].Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLED;
		EVAL_SPIxHandle[SPIPort].Init.CRCPolynomial     = 7;
		EVAL_SPIxHandle[SPIPort].Init.DataSize          = SPI_DATASIZE_8BIT;
		EVAL_SPIxHandle[SPIPort].Init.FirstBit          = SPI_FIRSTBIT_MSB;
		EVAL_SPIxHandle[SPIPort].Init.NSS               = SPI_NSS_SOFT;
		EVAL_SPIxHandle[SPIPort].Init.TIMode            = SPI_TIMODE_DISABLED;
		EVAL_SPIxHandle[SPIPort].Init.Mode 						 = SPI_MODE_MASTER;

    SPIx_MspInit(SPIPort);
    HAL_SPI_Init(&EVAL_SPIxHandle[SPIPort]);
  }
}

/**
  * @brief  Sends a Byte through the SPI interface 
  * @param  Byte : Byte send.
  */
static void SPIx_Write(SPI_Port_TypeDef SPIPort, uint8_t Byte)
{
  /* Send a Byte through the SPI peripheral */
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPIPort], EVAL_SPIx_NSS_PIN[SPIPort], GPIO_PIN_RESET);
	/* Read byte from the SPI bus */
  if(HAL_SPI_Transmit(&EVAL_SPIxHandle[SPIPort], &Byte, 1, 1000) != HAL_OK)
  {
    SPIx_Error(SPIPort);
  }
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPIPort], EVAL_SPIx_NSS_PIN[SPIPort], GPIO_PIN_SET);
}

/**
  * @brief Bus error user callback function
  * @param None
  * @retval None
  */
static  void SPIx_Error(SPI_Port_TypeDef SPIPort)
{
  //BSP_LED_On(LED3);
	/* De-initialize the SPI comunication bus */
  //HAL_SPI_DeInit(&SpiHandle);
  
  /* Re-Initiaize the SPI comunication bus */
  //SPIx_Init();
}

/**
  * @brief SPI MSP Init
  * @param hspi: SPI handle
  * @retval None
  */
static void SPIx_MspInit(SPI_Port_TypeDef SPIPort)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
	EVAL_SPIx_SCK_GPIO_CLK_ENABLE(SPIPort);
  EVAL_SPIx_MISO_GPIO_CLK_ENABLE(SPIPort);
  EVAL_SPIx_MOSI_GPIO_CLK_ENABLE(SPIPort);
  EVAL_SPIx_NSS_GPIO_CLK_ENABLE(SPIPort);
  
	/* Enable SPI clock */
	EVAL_SPIx_CLK_ENABLE(SPIPort);
  
  /*##-2- Configure peripheral GPIO ##########################################*/  
	/* SPI SCK GPIO pin configuration  */
  GPIO_InitStruct.Pin       = EVAL_SPIx_SCK_PIN[SPIPort];
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = EVAL_SPIx_AF[SPIPort];
  
  HAL_GPIO_Init(EVAL_SPIx_SCK_PORT[SPIPort], &GPIO_InitStruct);
    
  /* SPI MISO GPIO pin configuration  */
  GPIO_InitStruct.Pin = EVAL_SPIx_MISO_PIN[SPIPort];
  GPIO_InitStruct.Alternate = EVAL_SPIx_AF[SPIPort];
  
  HAL_GPIO_Init(EVAL_SPIx_MISO_PORT[SPIPort], &GPIO_InitStruct);
  
  /* SPI MOSI GPIO pin configuration  */
  GPIO_InitStruct.Pin = EVAL_SPIx_MOSI_PIN[SPIPort];
  GPIO_InitStruct.Alternate = EVAL_SPIx_AF[SPIPort];
    
  HAL_GPIO_Init(EVAL_SPIx_MOSI_PORT[SPIPort], &GPIO_InitStruct);   

	/* SPI NSS GPIO pin configuration  */
	GPIO_InitStruct.Pin       = EVAL_SPIx_NSS_PIN[SPIPort];
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
 	
  HAL_GPIO_Init(EVAL_SPIx_NSS_PORT[SPIPort], &GPIO_InitStruct);  
}

/******************************* I2C Routines**********************************/
/**
  * @brief  Initializes I2C MSP.
  * @param  None
  * @retval None
  */
static void I2Cx_MspInit(I2C_Port_Typedef I2CPort)
{
  GPIO_InitTypeDef  GPIO_InitStruct;  
  
  /*** Configure the GPIOs ***/  
  /* Enable GPIO clock */
  EVAL_I2Cx_SCL_SDA_GPIO_CLK_ENABLE(I2CPort);
  
  /* Configure I2C Tx as alternate function */
  GPIO_InitStruct.Pin = EVAL_I2Cx_SCL_PIN[I2CPort];
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = EVAL_I2Cx_SCL_SDA_AF[I2CPort];
  HAL_GPIO_Init(EVAL_I2Cx_SCL_SDA_GPIO_PORT[I2CPort], &GPIO_InitStruct);
  
  /* Configure I2C Rx as alternate function */
  GPIO_InitStruct.Pin = EVAL_I2Cx_SDA_PIN[I2CPort];
  HAL_GPIO_Init(EVAL_I2Cx_SCL_SDA_GPIO_PORT[I2CPort], &GPIO_InitStruct);
  
  /*** Configure the I2C peripheral ***/ 
  /* Enable I2C clock */
  EVAL_I2Cx_CLK_ENABLE(I2CPort);
  
  /* Force the I2C peripheral clock reset */  
  EVAL_I2Cx_FORCE_RESET(I2CPort); 
  
  /* Release the I2C peripheral clock reset */  
  EVAL_I2Cx_RELEASE_RESET(I2CPort); 
  
  /* Enable and set I2Cx Interrupt to the highest priority */
  HAL_NVIC_SetPriority(EVAL_I2Cx_EV_IRQ[I2CPort], 0x05, 0);
  HAL_NVIC_EnableIRQ(EVAL_I2Cx_EV_IRQ[I2CPort]);
  
  /* Enable and set I2Cx Interrupt to the highest priority */
  HAL_NVIC_SetPriority(EVAL_I2Cx_ER_IRQ[I2CPort], 0x05, 0);
  HAL_NVIC_EnableIRQ(EVAL_I2Cx_ER_IRQ[I2CPort]);
	
	if(HAL_I2C_Init(&EVAL_I2CxHandle[I2CPort]) != HAL_OK)
	{
		/* Initialization Error */
		//assert(0);    
	}
}

/**
  * @brief  Initializes I2C HAL.
  * @param  None
  * @retval None
  */
static void I2Cx_Init(I2C_Port_Typedef I2CPort)
{
  if(HAL_I2C_GetState(&EVAL_I2CxHandle[I2CPort]) == HAL_I2C_STATE_RESET)
  {
    EVAL_I2CxHandle[I2CPort].Instance = EVAL_I2Cx[I2CPort];
    EVAL_I2CxHandle[I2CPort].Init.ClockSpeed      = I2C_speed;
    EVAL_I2CxHandle[I2CPort].Init.DutyCycle       = I2C_DUTYCYCLE_2;
    EVAL_I2CxHandle[I2CPort].Init.OwnAddress1     = 0;
    EVAL_I2CxHandle[I2CPort].Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
    EVAL_I2CxHandle[I2CPort].Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
    EVAL_I2CxHandle[I2CPort].Init.OwnAddress2     = 0;
    EVAL_I2CxHandle[I2CPort].Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
    EVAL_I2CxHandle[I2CPort].Init.NoStretchMode   = I2C_NOSTRETCH_DISABLED;  
    
    /* Init the I2C */
    I2Cx_MspInit(I2CPort);
    HAL_I2C_Init(&EVAL_I2CxHandle[I2CPort]);    
  }
}

/**
  * @brief  Writes a single data.
  * @param  Addr: I2C address
  * @param  Reg: Register address 
  * @param  Value: Data to be written
  * @retval None
  */
static void I2Cx_Write(I2C_Port_Typedef I2CPort,uint8_t Addr, uint8_t Reg, uint8_t Value)
{
  HAL_StatusTypeDef status = HAL_OK;

  status = HAL_I2C_Mem_Write(&EVAL_I2CxHandle[I2CPort], Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, 1000); 

  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    I2Cx_Error(I2CPort,Addr);
  }
}

/**
  * @brief  Reads a single data.
  * @param  Addr: I2C address
  * @param  Reg: Register address 
  * @retval Read data
  */
static uint8_t I2Cx_Read(I2C_Port_Typedef I2CPort,uint8_t Addr, uint8_t Reg)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t Value = 0;
  
  status = HAL_I2C_Mem_Read(&EVAL_I2CxHandle[I2CPort], Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, 1000);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    I2Cx_Error(I2CPort,Addr);
  }

  return Value;   
}

/**
  * @brief  Reads multiple data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @param  Buffer: Pointer to data buffer
  * @param  Length: Length of the data
  * @retval Number of read data
  */
static HAL_StatusTypeDef I2Cx_ReadMultiple(I2C_Port_Typedef I2CPort,uint8_t Addr, uint16_t Reg, uint16_t MemAddress, uint8_t *Buffer, uint16_t Length)
{
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Mem_Read(&EVAL_I2CxHandle[I2CPort], Addr, (uint16_t)Reg, MemAddress, Buffer, Length, 1000);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* I2C error occured */
    I2Cx_Error(I2CPort,Addr);
  }
  return status;    
}

/**
  * @brief  Write a value in a register of the device through BUS in using DMA mode
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @param  pBuffer: The target register value to be written 
  * @param  Length: buffer size to be written
  * @retval HAL status
  */
static HAL_StatusTypeDef I2Cx_WriteMultiple(I2C_Port_Typedef I2CPort,uint8_t Addr, uint16_t Reg, uint16_t MemAddress, uint8_t *Buffer, uint16_t Length)
{
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Mem_Write(&EVAL_I2CxHandle[I2CPort], Addr, (uint16_t)Reg, MemAddress, Buffer, Length, 1000);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initiaize the I2C Bus */
    I2Cx_Error(I2CPort,Addr);
  }
  return status;
}

// For FM

/**
  * @brief  Receives multiple data in master mode in data in blocking mode.
  * @param  Addr: I2C address
  * @param  Buffer: Pointer to data buffer
  * @param  Length: Length of the data
  */
static HAL_StatusTypeDef I2Cx_Master_Receive(I2C_Port_Typedef I2CPort,uint8_t Addr, uint8_t *Buffer, uint16_t Length) // Added for FM
{
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Master_Receive(&EVAL_I2CxHandle[I2CPort], Addr,Buffer, Length, 1000);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    I2Cx_Error(I2CPort,Addr);
  }
  
  return status;   
}

/**
	* @brief Transmit multiple data in master mode in data in blocking mode.
	* @note added for FM Tuner
  * @param Addr: I2C address
  * @param  Buffer: Pointer to data buffer
  * @param  Length: Length of the data
  * @retval None
  */
static HAL_StatusTypeDef I2Cx_Master_Transmit(I2C_Port_Typedef I2CPort,uint8_t Addr, uint8_t *Buffer, uint16_t Length) // Added for FM
{
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Master_Transmit(&EVAL_I2CxHandle[I2CPort],Addr,Buffer,Length,1000); 
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* I2C error occured */
    I2Cx_Error(I2CPort,Addr);
  }
	return status; 
}

/**
  * @brief  Checks if target device is ready for communication. 
  * @note   This function is used with Memory devices
  * @param  DevAddress: Target device address
  * @param  Trials: Number of trials
  * @retval HAL status
  */
static HAL_StatusTypeDef I2Cx_IsDeviceReady(I2C_Port_Typedef I2CPort,uint16_t DevAddress, uint32_t Trials)
{ 
  return (HAL_I2C_IsDeviceReady(&EVAL_I2CxHandle[I2CPort], DevAddress, Trials, 1000));
}

/**
  * @brief  Manages error callback by re-initializing I2C.
  * @param  Addr: I2C Address
  * @retval None
  */
static void I2Cx_Error(I2C_Port_Typedef I2CPort,uint8_t Addr)
{
  /* De-initialize the I2C comunication bus */
  HAL_I2C_DeInit(&EVAL_I2CxHandle[I2CPort]);
  
  /* Re-Initiaize the I2C comunication bus */
  I2Cx_Init(I2CPort);
}


/********************************* LINK AUDIO *********************************/
/**
  * @brief  Initializes Audio low level.
  * @param  None
  * @retval None
  */
void AUDIO_IO_Init(void) 
{
  I2Cx_Init(I2C_PORT2);
}

/**
  * @brief  Writes a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @param  Value: Data to be written
  * @retval None
  */
void AUDIO_IO_Write(uint8_t Addr, uint16_t Reg, uint16_t Value)
{
	 I2Cx_Write(I2C_PORT2,Addr, Reg, Value);
}

/**
  * @brief  Reads a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @retval Data to be read
  */
uint16_t AUDIO_IO_Read(uint8_t Addr, uint16_t Reg)
{
		return I2Cx_Read(I2C_PORT2,Addr, Reg);
}

/**
  * @brief  AUDIO Codec delay 
  * @param  Delay: Delay in ms
  * @retval None
  */
void AUDIO_IO_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}

/********************************* LINK I2C EEPROM *****************************/
/**
  * @brief Initializes peripherals used by the I2C EEPROM driver.
  * @param None
  * @retval None
  */
void EEPROM_IO_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	 /* Enable Reset GPIO Clock */
	EEPROM_WP_GPIO_CLK_ENABLE();
	
  /* Audio reset pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = EEPROM_WP_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;//GPIO_SPEED_FAST;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init(EEPROM_WP_GPIO, &GPIO_InitStruct); 
	
	/* EEPROM set pin for Write Protected */
	//HAL_GPIO_WritePin(EEPROM_WP_GPIO, EEPROM_WP_PIN, GPIO_PIN_SET);
	
	/* EEPROM Reset to enable Write */
	 HAL_GPIO_WritePin(EEPROM_WP_GPIO, EEPROM_WP_PIN, GPIO_PIN_RESET);
	
	
	I2Cx_Init(I2C_PORT1);
}

/**
  * @brief Write data to I2C EEPROM driver in using DMA channel
  * @param DevAddress: Target device address
  * @param MemAddress: Internal memory address
  * @param pBuffer: Pointer to data buffer
  * @param BufferSize: Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef EEPROM_IO_WriteData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize)
{
  return (I2Cx_WriteMultiple(I2C_PORT1,DevAddress, MemAddress, I2C_MEMADD_SIZE_16BIT, pBuffer, BufferSize));
}

/**
  * @brief Read data from I2C EEPROM driver in using DMA channel
  * @param DevAddress: Target device address
  * @param MemAddress: Internal memory address
  * @param pBuffer: Pointer to data buffer
  * @param BufferSize: Amount of data to be read
  * @retval HAL status
  */
HAL_StatusTypeDef EEPROM_IO_ReadData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize)
{
  return (I2Cx_ReadMultiple(I2C_PORT1,DevAddress, MemAddress, I2C_MEMADD_SIZE_16BIT, pBuffer, BufferSize));
}

/**
  * @brief Checks if target device is ready for communication. 
  * @note This function is used with Memory devices
  * @param DevAddress: Target device address
  * @param Trials: Number of trials
  * @retval HAL status
  */
HAL_StatusTypeDef EEPROM_IO_IsDeviceReady(uint16_t DevAddress, uint32_t Trials)
{ 
  return (I2Cx_IsDeviceReady(I2C_PORT1,DevAddress, Trials));
}


/********************************* LINK I2C MFi_CP *****************************/
/**
  * @brief Initializes peripherals used by the I2C MFI_CP driver.
  * @param None
  * @retval None
  */
void MFI_CP_IO_Init(void)
{
	I2Cx_Init(I2C_PORT1);
}

/**
  * @brief Write data to I2C MFI_CP driver 
  * @param DevAddress: Target device address
  * @param MemAddress: Internal memory address
  * @param pBuffer: Pointer to data buffer
  * @param BufferSize: Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef MFI_CP_IO_WriteData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize)
{
  return (I2Cx_WriteMultiple(I2C_PORT1,DevAddress, MemAddress, I2C_MEMADD_SIZE_8BIT, pBuffer, BufferSize));
}

/**
  * @brief Read data from I2C MFI_CP driver 
  * @param DevAddress: Target device address
  * @param MemAddress: Internal memory address
  * @param pBuffer: Pointer to data buffer
  * @param BufferSize: Amount of data to be read
  * @retval HAL status
  */
HAL_StatusTypeDef MFI_CP_IO_ReadData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize)
{
	return (I2Cx_ReadMultiple(I2C_PORT1,DevAddress, MemAddress, I2C_MEMADD_SIZE_8BIT, pBuffer, BufferSize));
}


/**
  * @brief  Reads a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @retval Data to be read
  */
uint16_t MFI_CP_IO_Read(uint8_t Addr, uint16_t Reg)
{
		return I2Cx_Read(I2C_PORT1,Addr, Reg);
}

uint16_t MFI_CP_IO_ReadMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length)
{
 return I2Cx_Master_Receive(I2C_PORT1,Addr, Buffer, Length);
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{
  /* Turn LED6 on: Transfer in reception process is correct */
}

/**
  * @brief Checks if target device is ready for communication. 
  * @note This function is used with Memory devices
  * @param DevAddress: Target device address
  * @param Trials: Number of trials
  * @retval HAL status
  */
HAL_StatusTypeDef MFI_CP_IO_IsDeviceReady(uint16_t DevAddress, uint32_t Trials)
{ 
  return (I2Cx_IsDeviceReady(I2C_PORT1,DevAddress, Trials));
}



/************************* Link FM I2C *****************************************/
void TUNER_IO_Init(void)
{
	I2Cx_Init(I2C_PORT1);
}

uint16_t TUNER_IO_ReadMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length)
{
 return I2Cx_Master_Receive(I2C_PORT1,Addr, Buffer, Length);
}


void TUNER_IO_WriteMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length)
{	
	I2Cx_Master_Transmit(I2C_PORT1,Addr, Buffer, Length);
}
/**
  * @brief  FM_TUNER delay. 
  * @param  Delay: Delay in ms
  * @retval None
  */
void TUNER_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}

/********************************* LINK LCD SPI ***********************************/

/**
  * @brief  Initializes LCD low level.
  * @param  None
  * @retval None
  */
void LCD_IO_Init(void) 
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	/* Enable Reset GPIO Clock */
	LCD_GPIO_CLK_ENABLE();

	/* LCD reset pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = LCD_RESET_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
	
  HAL_GPIO_Init(LCD_GPIO_PORT, &GPIO_InitStruct); 
	
	/* LCD A0 pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = LCD_A0_PIN; 
  HAL_GPIO_Init(LCD_GPIO_PORT, &GPIO_InitStruct); 
	

	
	GPIO_InitStruct.Pin = LCD_BKLIT_PIN; 
  HAL_GPIO_Init(LCD_GPIO_PORT, &GPIO_InitStruct);
	
	
	/* LCD PS pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = LCD_PS_PIN;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(LCD_GPIO_PORT, &GPIO_InitStruct); 
	
	
	/* Reset PS (low) for Serial SPI comunication  */
	HAL_GPIO_WritePin(LCD_GPIO_PORT, LCD_PS_PIN, GPIO_PIN_RESET);
	
	/* Enable back lit*/
	HAL_GPIO_WritePin(LCD_GPIO_PORT, LCD_BKLIT_PIN, GPIO_PIN_SET);
	
	/* Power Down the codec */
	HAL_GPIO_WritePin(LCD_GPIO_PORT, LCD_RESET_PIN, GPIO_PIN_RESET);

	/* wait for a delay to insure registers erasing */
	HAL_Delay(100); 

	/* Power on the codec */
	HAL_GPIO_WritePin(LCD_GPIO_PORT, LCD_RESET_PIN, GPIO_PIN_SET);
	
	SPIx_Init(SPI_PORT1);
}

/**
  * @brief  Writes data on LCD data register.
  * @param  Data: Data to be written
  * @retval None
  */
void LCD_IO_WriteData(uint8_t Data) 
{
	/*LCD A0 High for data*/
	HAL_GPIO_WritePin(LCD_GPIO_PORT, LCD_A0_PIN, GPIO_PIN_SET);
	SPIx_Write(SPI_PORT1,Data);
}

/**
  * @brief  Writes register on LCD register.
  * @param  Reg: Register to be written
  * @retval None
  */
void LCD_IO_WriteCmd(uint8_t Cmd) 
{
	/*LCD A0 low for command */
	HAL_GPIO_WritePin(LCD_GPIO_PORT, LCD_A0_PIN, GPIO_PIN_RESET);
	SPIx_Write(SPI_PORT1,Cmd);
}
/**
  * @brief  LCD delay. 
  * @param  Delay: Delay in ms
  * @retval None
  */
void LCD_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}


/******************************* CAN B SPI Routines**********************************/
/********************************* LINK CANB SPI ***********************************/

/**
  * @brief  Initializes CAN low level.
  * @param  None
  * @retval None
  */
void CANB_IO_Init(void) 
{
	SPIx_Init(SPI_PORT4);
}

void CANB_Select(void)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT4], EVAL_SPIx_NSS_PIN[SPI_PORT4], GPIO_PIN_RESET);
}

void CANB_Deselect(void)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT4], EVAL_SPIx_NSS_PIN[SPI_PORT4], GPIO_PIN_SET);
}
	
uint32_t CANB_GetTick(void)
{
	return HAL_GetTick();
}
/**
  * @brief  TransmitReceive data on CAN.
  * @param  pTxData: pointer to transmission data buffer
  * @param  pRxData: pointer to reception data buffer to be
  * @param  Size: amount of data to be sent
	* @retval None
  */
void CANB_IO_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT4], EVAL_SPIx_NSS_PIN[SPI_PORT4], GPIO_PIN_RESET);
	
	if(HAL_SPI_TransmitReceive(&EVAL_SPIxHandle[SPI_PORT4], pTxData, pRxData, Size, 1000) != HAL_OK)
	{
		SPIx_Error(SPI_PORT4);
	}
	
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT4], EVAL_SPIx_NSS_PIN[SPI_PORT4], GPIO_PIN_SET);
}

void CANB_IO_Write(uint8_t *pData, uint16_t Size)
{
  if(HAL_SPI_Transmit(&EVAL_SPIxHandle[SPI_PORT4], pData, Size, 1000) != HAL_OK)
  {
    SPIx_Error(SPI_PORT4);
  }
}


uint8_t CANB_IO_Read(void)
{
	uint8_t pData;

  if(HAL_SPI_Receive(&EVAL_SPIxHandle[SPI_PORT4], &pData, 1, 1000) != HAL_OK)
  {
    SPIx_Error(SPI_PORT4);
  }

	return pData;
}

void CANB_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}


/******************************* Single-Wire CAN SPI Routines**********************************/


/********************************* LINK SWCAN SPI ***********************************/

/**
  * @brief  TransmitReceive data on CAN.
  * @param  pTxData: pointer to transmission data buffer
  * @param  pRxData: pointer to reception data buffer to be
  * @param  Size: amount of data to be sent
	* @retval None
  */
void SWCAN_IO_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT3], EVAL_SPIx_NSS_PIN[SPI_PORT3], GPIO_PIN_RESET);
	
	if(HAL_SPI_TransmitReceive(&EVAL_SPIxHandle[SPI_PORT3], pTxData, pRxData, Size, 1000) != HAL_OK)
	{
		SPIx_Error(SPI_PORT3);
	}
	
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT3], EVAL_SPIx_NSS_PIN[SPI_PORT3], GPIO_PIN_SET);
}

void SWCAN_IO_Write(uint8_t *pData, uint16_t Size)
{
  if(HAL_SPI_Transmit(&EVAL_SPIxHandle[SPI_PORT3], pData, Size, 1000) != HAL_OK)
  {
    SPIx_Error(SPI_PORT3);
  }
}


uint8_t SWCAN_IO_Read(void)
{
	uint8_t pData;
  if(HAL_SPI_Receive(&EVAL_SPIxHandle[SPI_PORT3], &pData, 1, 1000) != HAL_OK)
  {
    SPIx_Error(SPI_PORT3);
  }
	return pData;
}

void SWCAN_Select(void)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT3], EVAL_SPIx_NSS_PIN[SPI_PORT3], GPIO_PIN_RESET);
}

void SWCAN_Deselect(void)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT3], EVAL_SPIx_NSS_PIN[SPI_PORT3], GPIO_PIN_SET);
}

void SWCAN_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}

void SWCAN_IO_Init(void) 
{
	setSWCANMode(SWCAN_NORMAL);
	SPIx_Init(SPI_PORT3);
}

uint32_t SWCAN_GetTick(void)
{
	return HAL_GetTick();
}


/******************************* GMLAN SPI Routines**********************************/


/********************************* LINK GMLAN SPI ***********************************/

/**
  * @brief  TransmitReceive data on CAN.
  * @param  pTxData: pointer to transmission data buffer
  * @param  pRxData: pointer to reception data buffer to be
  * @param  Size: amount of data to be sent
	* @retval None
  */
void GMLAN_IO_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT2], EVAL_SPIx_NSS_PIN[SPI_PORT2], GPIO_PIN_RESET);
	
	if(HAL_SPI_TransmitReceive(&EVAL_SPIxHandle[SPI_PORT2], pTxData, pRxData, Size, 1000) != HAL_OK)
	{
		SPIx_Error(SPI_PORT2);
	}
	
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT2], EVAL_SPIx_NSS_PIN[SPI_PORT2], GPIO_PIN_SET);
}

void GMLAN_IO_Write(uint8_t *pData, uint16_t Size)
{
  if(HAL_SPI_Transmit(&EVAL_SPIxHandle[SPI_PORT2], pData, Size, 1000) != HAL_OK)
  {
    SPIx_Error(SPI_PORT2);
  }
}


uint8_t GMLAN_IO_Read(void)
{
	uint8_t pData;
  if(HAL_SPI_Receive(&EVAL_SPIxHandle[SPI_PORT2], &pData, 1, 1000) != HAL_OK)
  {
    SPIx_Error(SPI_PORT2);
  }
	return pData;
}

void GMLAN_Select(void)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT2], EVAL_SPIx_NSS_PIN[SPI_PORT2], GPIO_PIN_RESET);
}

void GMLAN_Deselect(void)
{
	HAL_GPIO_WritePin(EVAL_SPIx_NSS_PORT[SPI_PORT2], EVAL_SPIx_NSS_PIN[SPI_PORT2], GPIO_PIN_SET);
}

void GMLAN_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}

void GMLAN_IO_Init(void) 
{
	setGMLANMode(SWCAN_NORMAL);
	SPIx_Init(SPI_PORT2);
}

uint32_t GMLAN_GetTick(void)
{
	return HAL_GetTick();
}

static void setSWCANMode(SWCAN_Mode_Typedef canMode)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	/* Enable SWCAN Mode0 GPIO Clock */
	EVAL_SWCAN_MODE0_CLK_ENABLE();
	/* SWCAN Mode0 pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = EVAL_SWCAN_MODE0_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
	
  HAL_GPIO_Init(EVAL_SWCAN_MODE0_GPIO_PORT, &GPIO_InitStruct); 
	if(canMode == SWCAN_SLEEP || canMode == SWCAN_HV_WAKE_UP)
		HAL_GPIO_WritePin(EVAL_SWCAN_MODE0_GPIO_PORT, EVAL_SWCAN_MODE0_PIN, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(EVAL_SWCAN_MODE0_GPIO_PORT, EVAL_SWCAN_MODE0_PIN, GPIO_PIN_SET);
	
	/* Enable SWCAN Mode0 GPIO Clock */
	EVAL_SWCAN_MODE1_CLK_ENABLE();
	/* SWCAN Mode0 pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = EVAL_SWCAN_MODE1_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
	
  HAL_GPIO_Init(EVAL_SWCAN_MODE1_GPIO_PORT, &GPIO_InitStruct); 
	if(canMode == SWCAN_SLEEP || canMode == SWCAN_HS)
		HAL_GPIO_WritePin(EVAL_SWCAN_MODE1_GPIO_PORT, EVAL_SWCAN_MODE1_PIN, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(EVAL_SWCAN_MODE1_GPIO_PORT, EVAL_SWCAN_MODE1_PIN, GPIO_PIN_SET);
}

static void setGMLANMode(SWCAN_Mode_Typedef canMode)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	/* Enable SWCAN Mode0 GPIO Clock */
	EVAL_GMLAN_MODE0_CLK_ENABLE();
	/* SWCAN Mode0 pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = EVAL_GMLAN_MODE0_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
	
  HAL_GPIO_Init(EVAL_GMLAN_MODE0_GPIO_PORT, &GPIO_InitStruct); 
	if(canMode == SWCAN_SLEEP || canMode == SWCAN_HV_WAKE_UP)
		HAL_GPIO_WritePin(EVAL_GMLAN_MODE0_GPIO_PORT, EVAL_GMLAN_MODE0_PIN, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(EVAL_GMLAN_MODE0_GPIO_PORT, EVAL_GMLAN_MODE0_PIN, GPIO_PIN_SET);
	
	/* Enable SWCAN Mode0 GPIO Clock */
	EVAL_GMLAN_MODE1_CLK_ENABLE();
	/* SWCAN Mode0 pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = EVAL_GMLAN_MODE1_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
	
  HAL_GPIO_Init(EVAL_GMLAN_MODE1_GPIO_PORT, &GPIO_InitStruct); 
	if(canMode == SWCAN_SLEEP || canMode == SWCAN_HS)
		HAL_GPIO_WritePin(EVAL_GMLAN_MODE1_GPIO_PORT, EVAL_GMLAN_MODE1_PIN, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(EVAL_GMLAN_MODE1_GPIO_PORT, EVAL_GMLAN_MODE1_PIN, GPIO_PIN_SET);
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */    

/**
  * @}
  */ 
    
/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
