/**
  ******************************************************************************
  * @file    gateway_board_Ecan.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   This file contains the common defines and functions prototypes for
  *          the Gateway_Board_Ecan.c driver.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_Ecan_H
#define __GATEWAY_BOARD_Ecan_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* Include Ecan component Driver */
#include "gateway_board.h"
#include "..\Components\mcp2515\mcp2515.h"
#include "..\Components\mcp2515\mcp2515_swcan.h"
#include "..\Components\mcp2515\mcp2515_gmlan.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Gateway_Board
  * @{
  */
    
/** @defgroup Gateway_Board_Ecan
  * @{
  */


/** @defgroup Gateway_Board_ecan_Exported_Types
  * @{
  */

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Exported_Constants
  * @{
  */

/* Ecan status definition */     
#define ECAN_OK                              0
#define ECAN_ERROR                           1
#define ECAN_TIMEOUT                         2

typedef enum
{
	CANA = 0,
	CANB,
	SWCAN,
	GMLAN
}CAN_Typedef;



/** @defgroup Gateway_Board_Ecan_Exported_Functions
  * @{
  */
	

uint8_t BSP_CAN_Init(CAN_Typedef canType);
uint8_t BSP_CAN_Reset(CAN_Typedef canType);
uint8_t BSP_CAN_WriteReg(CAN_Typedef canType, uint8_t Addr, uint8_t Value);
uint8_t BSP_CAN_ReadStatus(CAN_Typedef canType);
uint8_t BSP_CAN_ReadReg(CAN_Typedef canType, uint8_t Addr);
uint8_t BSP_CAN_SetCANBaud(CAN_Typedef canType, uint32_t baudRate);
uint8_t BSP_CAN_SetCANMode(CAN_Typedef canType, uint8_t mode, uint8_t abat,uint8_t singleShot,uint8_t clkout,uint8_t clkpre);
uint8_t BSP_CAN_transmitCANMessage(CAN_Typedef canType, CANMSG msg, uint32_t timeout);
uint8_t BSP_CAN_receiveCANMessage(CAN_Typedef canType, CANMSG* msg);
uint8_t BSP_CAN_Test(CAN_Typedef canType);
/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_Ecan_H */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
