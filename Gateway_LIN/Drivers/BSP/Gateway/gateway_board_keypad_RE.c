/**	
 * |----------------------------------------------------------------------
 * | Copyright (C) Tilen Majerle, 2014
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */
#include "gateway_board_keypad_RE.h"
		 
	uint8_t KeyState[6] = {0};	
	uint8_t REkeyState[2] = {1,1};
	
	int32_t Vol_RE_Count, Sel_RE_Count;
	TM_RE_t Vol_RE,Sel_RE;
	TM_RE_Rotate_t RE_Get(TM_RE_t* data, int32_t RE_Count);
	

static void VolumeRE_Config(void);
static void SelectRE_Config(void);
	
/**
  * @brief  Initialize Keypad  
  * @param  None
  * @retval None
  */
void BSP_KEYPAD_Init(void)
{
	
	BSP_PB_Init(BUTTON_1, BUTTON_MODE_GPIO);
	BSP_PB_Init(BUTTON_2, BUTTON_MODE_GPIO);
	BSP_PB_Init(BUTTON_5, BUTTON_MODE_GPIO);
	BSP_PB_Init(BUTTON_3, BUTTON_MODE_GPIO);
	BSP_PB_Init(BUTTON_4, BUTTON_MODE_GPIO);
  BSP_PB_Init(BUTTON_5, BUTTON_MODE_GPIO);
  BSP_PB_Init(BUTTON_6, BUTTON_MODE_GPIO);
	
}

/**
  * @brief  GetKay pressed on Keypad  
  * @param  None
  * @retval None
  */
uint8_t BSP_KEYPAD_GeykeyPressed(void)
{
	volatile uint8_t buttonSel = 0;
	
	for(buttonSel = 0; buttonSel < BUTTONn ; buttonSel++) 
	{
		if(BSP_PB_GetState(buttonSel) == RESET && KeyState[buttonSel] == 0)
		{
			KeyState[buttonSel] = 1;
			return buttonSel;
		}
		if(BSP_PB_GetState(buttonSel) == SET && KeyState[buttonSel] == 1)
		{
			KeyState[buttonSel] = 0;
		}
	}	
	return 0xFF;
}

/**
  * @brief  Initialize Rotory Encoders
  * @param  None
  * @retval None
  */
void BSP_ENCODER_Init(void)	
{
	VolumeRE_Config();
	SelectRE_Config();	
}

/**
  * @brief  Get RE Event  
  * @param  None
  * @retval None
  */
uint8_t BSP_ENCODER_GetEvent(void)
{
	volatile uint8_t buttonSel = 0;
	
	RE_Get(&Vol_RE,Vol_RE_Count);
	if(Vol_RE.Rotation == TM_RE_Rotate_Increment) //Volume ++
	{
		return RE_VOLUMEUP;
	}
	if(Vol_RE.Rotation == TM_RE_Rotate_Decrement) //Volume --
	{
		return RE_VOLUMEDOWN;
	}
	
	RE_Get(&Sel_RE,Sel_RE_Count);
	if(Sel_RE.Rotation == TM_RE_Rotate_Increment) //Select Next 
	{
		return RE_SELNEXT;
	}
	if(Sel_RE.Rotation == TM_RE_Rotate_Decrement) //Select previous 
	{
		return RE_SELPREVIOUS;
	}
	
	/* Encoder Press Event */
	if(HAL_GPIO_ReadPin(RE_VOL_SW_GPIO_PORT, RE_VOL_SW_PIN) && REkeyState[Key_Vol_Center] == 0)
	{
		REkeyState[Key_Vol_Center] = 1;
		return RE_VOLUMEPRESSED;
	}
	if(!HAL_GPIO_ReadPin(RE_VOL_SW_GPIO_PORT, RE_VOL_SW_PIN) && REkeyState[Key_Vol_Center] == 1)
	{
		REkeyState[Key_Vol_Center] = 0;
	}
	if(HAL_GPIO_ReadPin(RE_SEL_SW_GPIO_PORT, RE_SEL_SW_PIN) && REkeyState[Key_Sel_Center] == 0)
	{
		REkeyState[Key_Sel_Center] = 1;
		return RE_SELPRESSED;
	}
	if(!HAL_GPIO_ReadPin(RE_SEL_SW_GPIO_PORT, RE_SEL_SW_PIN) && REkeyState[Key_Sel_Center] == 1)
	{
		REkeyState[Key_Sel_Center] = 0;
	}
	return 0xFF;
}


/* get direction of rotation */
TM_RE_Rotate_t RE_Get(TM_RE_t* data, int32_t RE_Count)
{
	data->Diff = RE_Count - data->Absolute;
	data->Absolute = RE_Count;
	
	if(data->Diff == 0)
	{
		data->Rotation = TM_RE_Rotate_Nothing;
		return TM_RE_Rotate_Nothing;
	}
	else if (data->Diff < 0)
	{
		data->Rotation = TM_RE_Rotate_Decrement;
		return TM_RE_Rotate_Decrement;
	}
	else if (data->Diff > 0)
	{
		data->Rotation = TM_RE_Rotate_Increment;
		return TM_RE_Rotate_Increment;
	}
	data->Rotation = TM_RE_Rotate_Nothing;
	return TM_RE_Rotate_Nothing;
}

void RE_Process(int32_t* RE_Count,uint8_t opt)
{
	static uint8_t last_a = 1;
	uint8_t now_a;
	uint8_t now_b;
	
	if(opt)
	{
		now_a = HAL_GPIO_ReadPin(RE_VOL_A_GPIO_PORT, RE_VOL_A_PIN);
		now_b = HAL_GPIO_ReadPin(RE_VOL_B_GPIO_PORT, RE_VOL_B_PIN);
	}
	else
	{
		now_a = HAL_GPIO_ReadPin(RE_SEL_A_GPIO_PORT, RE_SEL_A_PIN);
		now_b = HAL_GPIO_ReadPin(RE_SEL_B_GPIO_PORT, RE_SEL_B_PIN);
	}
	if (now_a != last_a) 
		{
			last_a = now_a;
		
			if (last_a == 0) 
			{
#ifdef TM_RE_CHANGE_ROTATION
				if (now_b == 1) 
					(*RE_Count)--;
				else
					(*RE_Count)++;
#else
			if (now_b == 1)
					(*RE_Count)++;
			else
					(*RE_Count)--;
#endif
			}
		}
}


static void VolumeRE_Config(void)
{
  GPIO_InitTypeDef   GPIO_InitStructure;
	
	/* Enable RE_VOL_pin clock */
	RE_VOL_A_GPIO_CLK_ENABLE();
	RE_VOL_B_GPIO_CLK_ENABLE();
	RE_VOL_SW_GPIO_CLK_ENABLE();
	
  /* Configure VOL_A pin as input floating with External Interrupt */
	GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = RE_VOL_A_PIN;
  HAL_GPIO_Init(RE_VOL_A_GPIO_PORT, &GPIO_InitStructure);  
  
	/* Configure VOL_B pin as input floating */
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = RE_VOL_B_PIN;
  HAL_GPIO_Init(RE_VOL_B_GPIO_PORT, &GPIO_InitStructure); 
	
	/* Configure VOL_SW pin as input floating */
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = RE_VOL_SW_PIN;
  HAL_GPIO_Init(RE_VOL_SW_GPIO_PORT, &GPIO_InitStructure); 

  /* Enable and set EXTIfor VOL_A Interrupt to the lowest priority */
  HAL_NVIC_SetPriority(RE_VOL_A_EXTI_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(RE_VOL_A_EXTI_IRQn);
}

static void SelectRE_Config(void)
{
 GPIO_InitTypeDef   GPIO_InitStructure;
	
	/* Enable RE_SEL Pin clock */
	RE_SEL_A_GPIO_CLK_ENABLE();
	RE_SEL_B_GPIO_CLK_ENABLE();
	RE_SEL_SW_GPIO_CLK_ENABLE();
	
  /* Configure SEL_A pin as input floating with External Interrupt */
	GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = RE_SEL_A_PIN;
  HAL_GPIO_Init(RE_SEL_A_GPIO_PORT, &GPIO_InitStructure);  
  
	/* Configure SEL_B pin as input floating */
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = RE_SEL_B_PIN;
  HAL_GPIO_Init(RE_SEL_B_GPIO_PORT, &GPIO_InitStructure); 
	
	/* Configure SEL_SW pin as input floating */
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Pin = RE_SEL_SW_PIN;
  HAL_GPIO_Init(RE_SEL_SW_GPIO_PORT, &GPIO_InitStructure); 

  /* Enable and set EXTIfor SEL_A Interrupt to the lowest priority */
  HAL_NVIC_SetPriority(RE_SEL_A_EXTI_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(RE_SEL_A_EXTI_IRQn);
}
/**
  * @brief  This function handles External interrupt request connected Volune A SEl A.
  * @param  None
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin == RE_VOL_A_PIN)
  {
		RE_Process(&Vol_RE_Count,1);
  }
	if(GPIO_Pin == RE_SEL_A_PIN)
  {
		RE_Process(&Sel_RE_Count,0);
  }
}


