/**
  ******************************************************************************
  * @file    st7565r.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    1-Aug-2014
  * @brief   This file includes the LCD driver for st7565r LCD.
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "st7565r.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup st7565r
  * @brief     This file provides a set of functions needed to drive the 
  *            st7565r LCD.
  * @{
  */

/** @defgroup st7565r_Private_TypesDefinitions
  * @{
  */ 

/**
  * @}
  */ 

/** @defgroup st7565r_Private_Defines
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup st7565r_Private_Macros
  * @{
  */
     
/**
  * @}
  */  

/** @defgroup st7565r_Private_Variables
  * @{
  */ 
	
/* Screen buffer Requires at least one bit for every pixel */
uint8_t glcd_buffer[ST7565R_LCD_WIDTH * ST7565R_LCD_HEIGHT / 8];

/* Keeps track of Display Window of area on LCD which need to be updated next reresh cycle */
Glcd_DisplayWindowTypeDef Glcd_DisplayWindow;

/* Pointer to screen buffer currently in use. */
uint8_t *pGlcd_buffer;

/* Pointer to Display Window currently in use */
Glcd_DisplayWindowTypeDef *pGlcd_DisplayWindow;

LCD_DrvTypeDef st7565r_drv = 
{
		st7565r_Init,
		st7565r_ReadID,
		st7565r_DisplayOn,
		st7565r_DisplayOff,
		st7565r_GetLcdPixelWidth,
		st7565r_GetLcdPixelHeight,
		st7565r_SetPixel,
		st7565r_GetPixel,
		st7565r_InvertPixel,
		st7565r_SetDisplayWindow,
		st7565r_GetScreenBuffer,
		st7565r_WriteDisplayBuffer,
		st7565r_ClearDisplayBuffer,
		st7565r_ClearScreen,
		st7565r_DrawBitmap
};


/** @defgroup ST7564R_Function_Prototypes
  * @{
  */
static void st7565r_SendCommand(uint8_t Cmd);
static void st7565r_SendData(uint8_t Data);
void st7565r_SelectScreen(uint8_t *buffer, Glcd_DisplayWindowTypeDef *DisplayWindow);

/**
  * @brief  Initialise the st7565r LCD Component.
  * @param  None
  * @retval None
  */
void st7565r_Init(void) 
{
		LCD_IO_Init();
		
		/* Power off the LCD */
		st7565r_SendCommand(ST7565RCMD_DISPLAY_OFF);// Off LCD
		LCD_Delay(100);
    // Set LCD bias to 1/9th
    st7565r_SendCommand(ST7565RCMD_BIAS_9);

    // Horizontal output direction (ADC segment driver selection)
    st7565r_SendCommand(ST7565RCMD_HORIZONTAL_NORMAL);

    // Vertical output direction (common output mode selection)
    st7565r_SendCommand(ST7565RCMD_VERTICAL_REVERSE);

    // Power control setting (datasheet step 7)
    // Note: Skipping straight to 0x7 works with my hardware.
		st7565r_SendCommand(ST7565RCMD_POWER_CONTROL | 0x7); 
		LCD_Delay(50); 
		
    // Set internal resistor.  A suitable value is used 
  	st7565r_SendCommand(ST7565RCMD_RESISTOR | 0x6);
		LCD_Delay(50); 
		
	 // Volume set (brightness control).  A suitable  value is used 
		st7565r_SendCommand(ST7565RCMD_VOLUME_MODE);
    st7565r_SendCommand(20);//20
    LCD_Delay(50); 

		// Reset start position to the top
    st7565r_SendCommand(ST7565RCMD_DISPLAY_START);

    // Turn the display on
    st7565r_SendCommand(ST7565RCMD_DISPLAY_ON);
		
		// Set screen
		st7565r_SelectScreen((uint8_t *)&glcd_buffer,&Glcd_DisplayWindow);

}
/**
  * @brief  Get the st7565r ID.
  * @param  None
  * @retval The st7565r ID 
  */
uint16_t st7565r_ReadID(void)
{
  LCD_IO_Init(); 
  return 0;
}
/**
  * @brief  Enables the Display.
  * @param  None
  * @retval None
  */
void st7565r_DisplayOn(void)
{
	
}
/**
  * @brief  Disables the Display.
  * @param  None
  * @retval None
  */
void st7565r_DisplayOff(void)
{
	
}

/**
  * @brief  Sends Command to st7565r.
  * @param  Cmd: Command to be send.
  * @retval None
  */
static void st7565r_SendCommand(uint8_t Cmd)
{
	LCD_IO_WriteCmd(Cmd);
}
/**
  * @brief  Sends data to st7565r.
  * @param  Data: data to be send.
  * @retval None
  */
static void st7565r_SendData(uint8_t Data)
{
	LCD_IO_WriteData(Data);
}

/**
  * @brief  Sets a display Contrast
  * @param  Value:  Contrast can be a 6-bit value (0 to 63).
  * @retval None
  */
void st7565r_SetContrast(uint8_t Value)
{
	/* Must send this command byte before setting the contrast */
	st7565r_SendCommand(ST7565RCMD_VOLUME_MODE);
	/* Set the contrat value ("electronic volumne register") */
	if (Value > 63)
	{
		st7565r_SendCommand(63);
	} 
	else
	{
		st7565r_SendCommand(Value);
	}
}


/**
  * @brief  Get the LCD pixel Width.
  * @param  None
  * @retval The Lcd Pixel Width
  */
uint16_t st7565r_GetLcdPixelWidth(void)
{
 return ST7565R_LCD_WIDTH -1;
}

/**
  * @brief  Get the LCD pixel Height.
  * @param  None
  * @retval The Lcd Pixel Height
  */
uint16_t st7565r_GetLcdPixelHeight(void)
{
 return ST7565R_LCD_HEIGHT-1;
}
/**
  * @brief  Set the display RAM page Address for Y Position.
	* @param  PageAddr : Display page Address 
  * @retval None
  */
static void st7565r_SetYAddress(uint8_t PageAddr)
{
	st7565r_SendCommand(ST7565R_PAGE_ADDRESS_SET | (0x0F & PageAddr));	
}

/**
  * @brief  Set the display RAM Column Address for X Position.
	* @param  ColAddr : Display Column Address 
  * @retval None
  */
static void st7565r_SetXAddress(uint8_t ColAddr)
{
	st7565r_SendCommand(ST7565R_COLUMN_ADDRESS_SET_UPPER | (ColAddr >> 4));
	st7565r_SendCommand(ST7565R_COLUMN_ADDRESS_SET_LOWER | (0x0F & ColAddr));
}

/**
  * @brief  Clear the display immediately, does not buffer
	* @param  None
  * @retval None
  */
void st7565r_ClearNow(void)
{
	uint8_t page;
	uint8_t col;
	
	for (page = 0; page < ST7565R_NUMBER_OF_BANKS; page++)
	{
		st7565r_SetYAddress(page);
		st7565r_SetXAddress(0);
		for (col = 0; col < ST7565R_NUMBER_OF_COLS; col++) 
		{
			st7565r_SendData(0);
		}			
	}
}

/**
  * @brief  Show a black and white line pattern on the display for test
	* @param  None
  * @retval None
  */
void st7565r_Pattern(void)
{
	uint8_t page;
	uint8_t col;
	
	for (page = 0; page < ST7565R_NUMBER_OF_BANKS; page++) 
	{
		st7565r_SetYAddress(page);
		st7565r_SetXAddress(0);
		for (col = 0; col < ST7565R_NUMBER_OF_COLS; col++) 
		{
			st7565r_SendData( (col / 8 + 2) % 2 == 1 ? 0xff : 0x00 );
		}			
	}
}


/******************* GLCD Functions ************************************/
/**
  * @brief  The Display Window defines a rectangle in which needs to be refreshed next time
	* 				st7565r_WriteDisplayBuffer() is called. st7565r_WriteDisplayBuffer() only writes to those pixels inside the 
	*					Display Window plus any surrounding pixels which are required according to the 
	*					bank/column write method of the controller.
	* @param xmin Minimum x value of rectangle
	* @param ymin Minimum y value of rectangle
	* @param xmax Maximum x value of rectangle
	* @param ymax Maximum y value of rectangle
  * @retval None
  */
void st7565r_SetDisplayWindow(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax)
{
	/* Keep and check Display Window within limits of LCD screen dimensions */
	if (xmin > (ST7565R_LCD_WIDTH-1))	xmin = ST7565R_LCD_WIDTH-1;
	if (xmax > (ST7565R_LCD_WIDTH-1)) xmax = ST7565R_LCD_WIDTH-1;
	if (ymin > (ST7565R_LCD_HEIGHT-1)) ymin = ST7565R_LCD_HEIGHT-1;
	if (ymax > (ST7565R_LCD_HEIGHT-1)) ymax = ST7565R_LCD_HEIGHT-1;

	/* Update the Display Window size */
	if (xmin < pGlcd_DisplayWindow->x_min) pGlcd_DisplayWindow->x_min = xmin;			
	if (xmax > pGlcd_DisplayWindow->x_max) pGlcd_DisplayWindow->x_max = xmax;
	if (ymin < pGlcd_DisplayWindow->y_min) pGlcd_DisplayWindow->y_min = ymin;
	if (ymax > pGlcd_DisplayWindow->y_max) pGlcd_DisplayWindow->y_max = ymax;
				
}
/**
  * @brief  Reset the Display Window
	* 				After resetting the Display Window, no pixels are marked as needing refreshing.
	* @param None
  * @retval None
  */
void st7565r_ResetDisplayWindow(void)
{
	/* Used after physically writing to the LCD */
	pGlcd_DisplayWindow->x_min = ST7565R_LCD_WIDTH - 1;
	pGlcd_DisplayWindow->x_max = 0;
	pGlcd_DisplayWindow->y_min = ST7565R_LCD_HEIGHT -1;
	pGlcd_DisplayWindow->y_max = 0;	
}

/**
  * @brief  Marks the entire display for re-writing.
	*					Marks Display Window as entire screen, so on next st7565r_WriteDisplayBuffer(), it writes the entire buffer to the LCD
	* @param 	None
  * @retval None
  */
void st7565r_RefreshDisplayWindow(void)
{
	pGlcd_DisplayWindow->x_min = 0;
	pGlcd_DisplayWindow->x_max = ST7565R_LCD_WIDTH - 1;
	pGlcd_DisplayWindow->y_min = 0;
	pGlcd_DisplayWindow->y_max = ST7565R_LCD_HEIGHT -1;		
}

/**
  * @brief  Clear the display. This will clear the buffer and physically write and commit it to the LCD
	* @param 	None
  * @retval None
  */
void st7565r_ClearScreen(void)
{
	memset(pGlcd_buffer, 0x00, ST7565R_LCD_WIDTH * ST7565R_LCD_HEIGHT / 8);
	st7565r_SetDisplayWindow(0,0,ST7565R_LCD_WIDTH - 1,ST7565R_LCD_HEIGHT - 1);
	st7565r_WriteDisplayBuffer();
}

/**
  * @brief  Clear the display buffer only. This does not physically write the changes to the LCD
	* @param 	None
  * @retval None
  */
void st7565r_ClearDisplayBuffer(void) 
{
	memset(pGlcd_buffer, 0x00, ST7565R_LCD_WIDTH * ST7565R_LCD_HEIGHT / 8);
	st7565r_SetDisplayWindow(0,0,ST7565R_LCD_WIDTH - 1,ST7565R_LCD_HEIGHT - 1);
}

/**
  * @brief  Select screen buffer and Display Window structure.
	*					This should be selected at initialisation. There are future plans to support multiple screen buffers
	* 				but this not yet available.
	* @param 	*buffer : Pointer to screen buffer
	* @param 	*DisplayWindow		: Pointer to Display Window object.
  * @retval None
  */
void st7565r_SelectScreen(uint8_t *buffer, Glcd_DisplayWindowTypeDef *DisplayWindow)
{
	pGlcd_buffer = buffer;
	pGlcd_DisplayWindow = DisplayWindow;
}

/**
  * @brief  Sets a single pixel.   
  * @param  Xpos: specifies the X position.
  * @param  Ypos: specifies the Y position.
  * @param  Color: Color can be BLACK or WHITE
  * @retval None
  */
void st7565r_SetPixel(uint8_t Xpos, uint8_t Ypos, uint8_t Color)
{
	if (Xpos < ST7565R_LCD_WIDTH && Ypos < ST7565R_LCD_HEIGHT) 
	{
		if (Color) /* set Black */
			glcd_buffer[Xpos+ (Ypos/8)*ST7565R_LCD_WIDTH] |= ( 1 << (Ypos%8));
		else  /* set White */
			glcd_buffer[Xpos+ (Ypos/8)*ST7565R_LCD_WIDTH] &= ~ (1 << (Ypos%8));

		st7565r_SetDisplayWindow(Xpos,Ypos,Xpos,Ypos);
	}
}

/**
  * @brief  Read a single pixel.   
  * @param  Xpos: specifies the X position.
  * @param  Ypos: specifies the Y position.
  * @retval Color: Color can be BLACK or WHITE
  */
uint8_t st7565r_GetPixel(uint8_t Xpos, uint8_t Ypos) 
{
	if ((Xpos < ST7565R_LCD_WIDTH) || (Ypos < ST7565R_LCD_HEIGHT)) 
	{
		if (glcd_buffer[Xpos+(Ypos/8)*ST7565R_LCD_WIDTH] & ( 1 << (Ypos%8)) ) 
			return BLACK;
		else 
			return WHITE;
	}
	return 0;
}

/**
  * @brief  Invert Pixcel color
  * @param  Xpos:  Bmp X position in the LCD
  * @param  Ypos:  Bmp Y position in the LCD    
	* @retval None
  */
void st7565r_InvertPixel(uint8_t Xpos, uint8_t Ypos) 
	{
	if ((Xpos >= ST7565R_LCD_WIDTH) || (Ypos >= ST7565R_LCD_HEIGHT)) 
	{
		return;
	}
	st7565r_SetDisplayWindow(Xpos,Ypos,Xpos,Ypos);
	glcd_buffer[Xpos+ (Ypos/8)*ST7565R_LCD_WIDTH] ^= ( 1 << (Ypos%8));
}

/**
  * @brief  Displays a bitmap picture..
  * @param  Xpos:  Bmp X position in the LCD
  * @param  Ypos:  Bmp Y position in the LCD    
	* @param  BmpAddress: Bmp picture address.
  * @param  BmpHeignt:  Bmp Height in pixels 
  * @param  BmpWidth:  Bmp Width in pixels  
	* @retval None
  */
void st7565r_DrawBitmap(uint8_t Xpos,uint8_t Ypos,const uint8_t* BmpAddress,uint8_t BmpHeignt,uint8_t BmpWidth)
{
	for (uint8_t j=0; j<BmpHeignt; j++)
	{
    for (uint8_t i=0; i<BmpWidth; i++ )
		{
      if ((BmpAddress[ i + (j/8)*BmpWidth]) & (1 << (j%8))) 
			{
				st7565r_SetPixel(Xpos+i, Ypos+j, 1);
      }
    }
  }
}



/**
  * @brief  Changes physical containt on screen 
	*					updates display buffer as per pGlcd_DisplayWindow and send data to st7565r 
  * @retval None
  */
void st7565r_WriteDisplayBuffer(void)
{
	uint8_t bank;

	for (bank = 0; bank < ST7565R_NUMBER_OF_BANKS; bank++) {
		/* Each bank is a single row 8 bits tall */
		uint8_t column;		
		
		if (pGlcd_DisplayWindow->y_min >= (bank+1)*8) {
			continue; /* Skip the entire bank */
		}
		
		if (pGlcd_DisplayWindow->y_max < bank*8) {
			break;    /* No more banks need updating */
		}
		
		st7565r_SetYAddress(bank);
		st7565r_SetXAddress(pGlcd_DisplayWindow->x_min);

		for (column = pGlcd_DisplayWindow->x_min; column <= pGlcd_DisplayWindow->x_max; column++)
		{
			st7565r_SendData( pGlcd_buffer[ST7565R_NUMBER_OF_COLS * bank + column] );
		}
	}

	st7565r_ResetDisplayWindow();
}

/**
 * gets pointer to glcd Buffer
 * @retval pointer to glcd Buffer
 */

uint8_t* st7565r_GetScreenBuffer(void)
{
	return pGlcd_buffer;
}


/**
 * Scroll screen buffer up by 8 pixels.
 * This is designed to be used in conjunciton with tiny text functions which are 8 bits high.
 * @see Tiny Text
 */
void st7565r_ScrollLine(void)
{
	uint8_t y;
	uint8_t number_of_rows = ST7565R_LCD_HEIGHT / 8;
	for (y=0; y<number_of_rows; y++) {
		if (y < (number_of_rows - 1)) {
			/* All lines except the last */
			memcpy(pGlcd_buffer + y*ST7565R_LCD_WIDTH, pGlcd_buffer + y*ST7565R_LCD_WIDTH + ST7565R_LCD_WIDTH, ST7565R_LCD_WIDTH);
		} else {
			/* Last line, clear it */
			memset(pGlcd_buffer + (number_of_rows - 1)*ST7565R_LCD_WIDTH, 0x00, ST7565R_LCD_WIDTH);
		}
	}
	st7565r_SetDisplayWindow(0,0,ST7565R_LCD_WIDTH - 1,ST7565R_LCD_HEIGHT - 1);
}

void st7565r_ScrollRight(uint8_t x)
	{

	st7565r_RefreshDisplayWindow();
	for (uint8_t l = 0; l < 8; ++l)
	{
        uint8_t* p = glcd_buffer + (l * 128);
        for (uint8_t b = ST7565R_LCD_WIDTH-1; b >= x; --b)
            *(p+b) = *(p+b-x);
        for (uint8_t b = 0; b < x; ++b)
            *(p+b) = 0;
    }
}

/******************************************************************************/










/*******************************************************************************
															st7565r Functions
*******************************************************************************/


/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */

  
/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
