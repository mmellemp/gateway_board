/**
  ******************************************************************************
  * @file    st7565r.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    1-Aug-2014
  * @brief   This file contains all the functions prototypes for the st7565r.c
  *          driver.
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __st7565r_H
#define __st7565r_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
	#include "..\Common\lcd.h"
	#include <string.h>

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup st7565r
  * @{
  */

/** @defgroup st7565r_Exported_Types
  * @{
  */
   
/**
  * @} GLCD Bounding box typedef for pixels that need to be updated
  */ 
typedef struct
{
	uint8_t x_min;
	uint8_t y_min;
	uint8_t x_max;
	uint8_t y_max;
} Glcd_BoundingBoxTypeDef;



/** @defgroup st7565r_Exported_Constants
  * @{
  */
/** 
  * @brief  st7565r Screen Size  
  */  
#define GLCD_LCD_WIDTH 128
#define GLCD_LCD_HEIGHT 65
#define GLCD_NUMBER_OF_BANKS (GLCD_LCD_WIDTH / 8)
#define GLCD_NUMBER_OF_COLS  GLCD_LCD_WIDTH


#define BLACK 1
#define WHITE 0
/** 
  * @brief  st7565r Registers  
  */ 
/**
  * @}
  */
  
/** @defgroup st7565r_Exported_Functions
  * @{
  */ 
	
void st7564r_UpdateBbox(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax);
void st7564r_ResetBbox(void);
void Glcd_bbox_reset(void);
void st7564r_RefreshBbox(void);
void st7564r_Clear(void);
void st7564r_ClearBuffer(void);
void st7564r_SelectScreen(uint8_t *buffer, Glcd_BoundingBoxTypeDef *bbox);
void Glcd_scroll(int8_t x, int8_t y);
void st7564r_ScrollLine(void);

void glcd_draw_bitmap(const unsigned char *data);

/*******************************************************************************
															st7565 Functions
*******************************************************************************/
/* Commands */
/* Command: turn the display on */
#define GLCD_CMD_DISPLAY_ON 						0xAF //0b10101111
/* Command: turn the display off */
#define GLCD_CMD_DISPLAY_OFF 						0xAE  //0b10101110
/* Command: set all points on the screen to normal. */
#define ST7565R_DISPLAY_NORMAL					0xA4  //0b10100100
/* Command: set all points on the screen to "on", without affecting the internal screen buffer. */
#define ST7565R_DISPLAY_ALL_ON  				0xA5  //0b10100101
/* Command: disable inverse (black pixels on a white background) */
#define GLCD_CMD_DISPLAY_NORMAL					0xA6 //0b10100110
/* Command: inverse the screen (white pixels on a black background) */
#define GLCD_CMD_DISPLAY_REVERSE				0xA7    // 0b10100111
/* Command: set LCD bias to 1/9th */
#define GLCD_CMD_BIAS_9									0xA2 //0b10100010
/* Command: set LCD bias to 1/7th */
#define GLCD_CMD_BIAS_7									0xA3 //0b10100011
/* Command: set ADC output direction to normal. */
#define GLCD_CMD_HORIZONTAL_NORMAL			0xA0 //0b10100000
/* Command: set ADC output direction reverse (horizontally flipped). 
             Note that you should use the glcd_flip_screen function so that
             the width is correctly accounted for. */
#define GLCD_CMD_HORIZONTAL_REVERSE   	0xA1 //0b10100001
/* Command: set common output scan direction to normal. */
#define GLCD_CMD_VERTICAL_NORMAL				0xC0 //0b11000000
/* Command: set common output scan direction to reversed (vertically flipped). */
#define GLCD_CMD_VERTICAL_REVERSE	 			0xC8//0b11001000
/* Command: select the internal power supply operating mode. */
#define GLCD_CMD_POWER_CONTROL					0x28//0b00101000
/* Command: set internal R1/R2 resistor bias (OR with 0..7) */
#define GLCD_CMD_RESISTOR								0x20 //0b00100000
/* Command: enter volume mode, send this then send another command
             byte with the contrast (0..63).  The second command
			 must be sent for the GLCD to exit volume mode. */
#define GLCD_CMD_VOLUME_MODE						0x81//0b10000001

#define GLCD_CMD_DISPLAY_START					0x40 //0b01000000

/* Command: set the least significant 4 bits of the column address. */
#define ST7565R_COLUMN_ADDRESS_SET_LOWER	0x00 //0b00000000
/* Command: set the most significant 4 bits of the column address. */
#define ST7565R_COLUMN_ADDRESS_SET_UPPER 	0x10//0b00010000
/* Command: Set the current page (0..7). */
#define ST7565R_PAGE_ADDRESS_SET					0xB0//0b10110000

/* Command: software reset (note: should be combined with toggling GLCD_RS) */
#define GLCD_CMD_RESET										0xE2//0b11100010

/* Command: no operation (note: the datasheet suggests sending this periodically
             to keep the data connection alive) */
#define	GLCD_CMD_NOP											0xE3//0b11100011

#define ST7565R_DISPLAY_ON									0b10101111	 
#define ST7565R_DISPLAY_OFF									0b10101110	 
//#define ST7565R_PAGE_ADDRESS_SET						0b10110000	 
//#define ST7565R_COLUMN_ADDRESS_SET_LOWER 		0x00 
//#define ST7565R_COLUMN_ADDRESS_SET_UPPER 		0x10 
//#define ST7565R_DISPLAY_NORMAL 							0b10100100
//#define ST7565R_DISPLAY_ALL_ON 							0b10100101
#define ST7565R_NORMAL  										0b10100000
#define ST7565R_REVERSE 										0b10100001
#define ST7565R_RESET   										0b11100010
#define ST7565R_SET_START_LINE 							(1<<6)


/* Private functions */
void st7565r_Init(void);
void st7564r_SendCommand(uint8_t Cmd);
void st7564r_SendData(uint8_t Data);
void st7564r_SetContrast(uint8_t Value);
void st7564r_DisplayOn(void);
void st7564r_DisplayOff(void);
void st7564r_SetYAddress(uint8_t PageAddr);
void st7564r_SetXAddress(uint8_t ColAddr);
void st7564r_ClearNow(void);
void st7564r_Pattern(void);

void st7564r_UpdateBbox(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax);
void st7564r_ResetBbox(void);
void st7564r_RefreshBbox(void);
void st7564r_Clear(void);
void st7564r_ClearBuffer(void);
void st7564r_SelectScreen(uint8_t *buffer, Glcd_BoundingBoxTypeDef *bbox);
void st7564r_SetPixel(uint8_t Xpos, uint8_t Ypos, uint8_t Color);
uint8_t st7564r_GetPixel(uint8_t Xpos, uint8_t Ypos);
void st7564r_DrawBitmap(uint8_t Xpos,uint8_t Ypos,const uint8_t *BmpAddress,uint8_t BmpHeignt,uint8_t BmpWidth);
void st7564r_Write(void);
uint8_t *st7564r_Getbufferselected(void);
void st7564r_InvertPixel(uint8_t Xpos, uint8_t Ypos);

void st7564r_ScrollLine(void);
void st7564r_ScrollRight(uint8_t x);	
uint16_t st7564r_GetLcdPixelHeight(void);
uint16_t st7564r_GetLcdPixelWidth(void);


//_______________________________________________________
//void st7564r_set_column_upper(uint8_t addr);
//void st7564r_set_column_lower(uint8_t addr);
//void st7564r_all_on(void);
//void st7564r_normal(void);
//void st7564r_set_start_line(uint8_t addr);
//void st7564r_clear_now(void);
//void st7564r_pattern(void);
//void st7564r_Write(void);
//uint8_t *st7564r_Getbufferselected(void);
///** Init ST7565R controller / display */

//void st7565r_init(void);
//void st7564r_pattern(void);

//void st7564r_SetPixel(uint8_t x, uint8_t y, uint8_t color);
//uint8_t st7564r_GetPixel(uint8_t x, uint8_t y);
//void glcd_invert_pixel(uint8_t x, uint8_t y); 

//void glcd_draw_image(uint8_t x,uint8_t y,const unsigned char *data,uint8_t h,uint8_t w);


//void glcd_ST7565R_init(void);

//void     st7565r_Init(void);
//uint16_t st7565r_ReadID(void);
//void     st7565r_WriteReg(uint8_t LCD_Reg, uint16_t LCD_RegValue);
//uint16_t st7565r_ReadReg(uint8_t LCD_Reg);

//void     st7565r_DisplayOn(void);
//void     st7565r_DisplayOff(void);
//void     st7565r_SetCursor(uint16_t Xpos, uint16_t Ypos);
//void     st7565r_WritePixel(uint16_t Xpos, uint16_t Ypos, uint16_t RGB_Code);
//uint16_t st7565r_ReadPixel(uint16_t Xpos, uint16_t Ypos);

//void     st7565r_DrawHLine(uint16_t RGB_Code, uint16_t Xpos, uint16_t Ypos, uint16_t Length);
//void     st7565r_DrawVLine(uint16_t RGB_Code, uint16_t Xpos, uint16_t Ypos, uint16_t Length);
//void     st7565r_DrawBitmap(uint16_t Xpos, uint16_t Ypos, uint8_t *pbmp);
//void     st7565r_DrawRGBImage(uint16_t Xpos, uint16_t Ypos, uint16_t Xsize, uint16_t Ysize, uint8_t *pdata);

//void     st7565r_SetDisplayWindow(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height);


//uint16_t st7565r_GetLcdPixelWidth(void);
//uint16_t st7565r_GetLcdPixelHeight(void);

///* LCD driver structure */
//extern LCD_DrvTypeDef   st7565r_drv;

/* LCD IO functions */
void 						LCD_IO_Init(void);
void 						LCD_IO_WriteData(uint8_t Data);
void 						LCD_IO_WriteCmd(uint8_t Cmd);
void 						LCD_Delay(uint32_t Delay);
/**
  * @}
  */ 
      
#ifdef __cplusplus
}
#endif

#endif /* __st7565r_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
