/**
  ******************************************************************************
  * @file    fm_tuner.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    23-July-2014
  * @brief   This header file contains the common defines and functions prototypes
  *          for the FM Tuner driver.  
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM_TUNER_H
#define __FM_TUNER_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Component
  * @{
  */ 
    
/** @addtogroup FM TUNER
  * @{
  */ 


/** @defgroup FM TUNER_Exported_Constants
  * @{
  */


/**
  * @}
  */

/** @defgroup FM TUNER_Exported_Types
  * @{
  */ 
/** 
  * @brief  FM TUNER driver structure definition  
  */ 
typedef struct
{
  uint32_t  (*Init)(uint16_t);
  uint32_t  (*ReadID)(uint16_t);  
  uint32_t  (*Start)(uint16_t, uint8_t);
  uint32_t  (*Stop)(uint16_t);
  uint32_t  (*Tune)(uint16_t, uint16_t, uint8_t);
  uint32_t  (*Seek)(uint16_t, uint8_t);
  uint32_t  (*SetVolume)(uint16_t, uint8_t);
  uint32_t  (*SetMute)(uint16_t, uint32_t);
	void 			(*GetChannelList)(uint16_t,uint8_t*);
  
}TUNER_DrvTypeDef;

/**
  * @}
  */
      
#ifdef __cplusplus
}
#endif

#endif /* __FM_TUNER_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
