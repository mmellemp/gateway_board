/**
  ******************************************************************************
  * @file    lcd.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-February-2014
  * @brief   This file contains all the functions prototypes for the LCD driver.   
  ******************************************************************************/ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LCD_H
#define __LCD_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
   
/** @addtogroup BSP
  * @{
  */

/** @addtogroup Components
  * @{
  */ 

/** @addtogroup LCD
  * @{
  */
    
 
/** @defgroup LCD_Exported_Types
  * @{
  */
/** 
  * @brief  LCD driver structure definition  
  */       
typedef struct
{
  void     (*Init)(void);   
  uint16_t (*ReadID)(void); 
  void     (*DisplayOn)(void);
  void     (*DisplayOff)(void);  
	uint16_t (*GetLcdPixelWidth)(void);
  uint16_t (*GetLcdPixelHeight)(void);
	void 		 (*SetPixel)(uint8_t, uint8_t, uint8_t);
	uint8_t  (*GetPixel)(uint8_t, uint8_t);
	void 		 (*InvertPixel)(uint8_t, uint8_t);
	void 		 (*SetDisplayWindow)(uint8_t, uint8_t, uint8_t, uint8_t);
	uint8_t* (*GetScreenBuffer)(void);
	void 		 (*WriteDisplayBuffer)(void);
	void 		 (*ClearDisplayBuffer)(void);
	void 		 (*ClearScreen)(void);
	void 		 (*DrawBitmap)(uint8_t, uint8_t, const uint8_t*, uint8_t ,uint8_t);
}
LCD_DrvTypeDef;    
      
/**
  * @}
  */ 

/** @defgroup LCD_Exported_Constants
  * @{
  */

/**
  * @}
  */
  
/** @defgroup LCD_Exported_Functions
  * @{
  */ 

/**
  * @}
  */ 

   
#ifdef __cplusplus
}
#endif

#endif /* LCD_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
