/**
  ******************************************************************************
  * @file    audio.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-February-2014
  * @brief   This header file contains the common defines and functions prototypes
  *          for the Audio driver.  
  ******************************************************************************/ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AUDIO_H
#define __AUDIO_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Component
  * @{
  */ 
    
/** @addtogroup AUDIO
  * @{
  */ 


/** @defgroup AUDIO_Exported_Constants
  * @{
  */

/* Codec audio Standards */
#define I2S_STANDARD                  I2S_STANDARD_MSB //Arun

/**
  * @}
  */

/** @defgroup AUDIO_Exported_Types
  * @{
  */ 
/** 
  * @brief  AUDIO driver structure definition  
  */ 
typedef struct
{
  uint32_t  (*Init)(uint16_t, uint16_t, uint8_t, uint32_t);
  uint32_t  (*ReadID)(uint16_t);  
  uint32_t  (*Play)(uint16_t, uint16_t*, uint16_t);
  uint32_t  (*Pause)(uint16_t);
  uint32_t  (*Resume)(uint16_t);
  uint32_t  (*Stop)(uint16_t, uint32_t);
  uint32_t  (*SetFrequency)(uint16_t, uint32_t);
  uint32_t  (*SetVolume)(uint16_t, uint8_t);
  uint32_t  (*SetMute)(uint16_t, uint32_t);
  uint32_t  (*SetOutputMode)(uint16_t, uint8_t);
	uint32_t  (*SetInputMode)(uint16_t, uint8_t);
  uint32_t  (*Reset)(uint16_t);
}AUDIO_DrvTypeDef;

/**
  * @}
  */
      
#ifdef __cplusplus
}
#endif

#endif /* AUDIO_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
