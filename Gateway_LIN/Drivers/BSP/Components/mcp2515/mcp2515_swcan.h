/**
  ******************************************************************************
  * @file    mcp2515.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    28-Aug-2014
  * @brief   This file contains all the functions prototypes for the mcp2515.c driver.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __mcp2515_swcan_H
#define __mcp2515_swcan_H


#include <stdint.h>
#include "..\Common\ecan.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Component
  * @{
  */ 
	


/** @addtogroup mcp2515
  * @{
  */

/** @defgroup mcp2515_Exported_Types
  * @{
  */
	
extern ECAN_DrvTypeDef   mcp2515_swcan_drv;
/* ------------------------- CAN IO Functions ------------------------------------*/


void SWCAN_IO_Init(void); 
void SWCAN_IO_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size);
void SWCAN_IO_Write(uint8_t *pData, uint16_t Size); 
void SWCAN_Delay(uint32_t Delay);
void SWCAN_Deselect(void);
void SWCAN_Select(void);
uint8_t SWCAN_IO_Read(void);
uint32_t SWCAN_GetTick(void);



uint8_t mcp2515_swcan_Init(void);
void mcp2515_swcan_Reset(void);
void mcp2515_swcan_WriteReg(uint8_t Addr, uint8_t Value);
uint8_t mcp2515_swcan_ReadStatus(void);
uint8_t mcp2515_swcan_ReadReg(uint8_t Addr);
uint8_t mcp2515_swcan_SetCANBaud(uint32_t baudRate);
uint8_t mcp2515_swcan_SetCANMode(uint8_t mode, uint8_t abat,uint8_t singleShot,uint8_t clkout,uint8_t clkpre);
uint8_t mcp2515_swcan_transmitCANMessage(CANMSG msg, uint32_t timeout);
uint8_t mcp2515_swcan_Test(void);
uint32_t mcp2515_swcan_readCanID(const uint8_t mcp_addr);
uint8_t mcp2515_swcan_recieveCANmsg( const uint8_t buffer_sidh_addr,CANMSG* msg);
uint8_t mcp2515_swcan_readCANmessage(CANMSG *msg);

/**
  * @}
  */
































#endif /* __mcp2515_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT Arka -IMS *****END OF FILE****/
