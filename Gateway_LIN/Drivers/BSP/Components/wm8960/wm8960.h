/**
  ******************************************************************************
  * @file    wm8960.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-February-2014
  * @brief   This file contains all the functions prototypes for the cs43122.c driver.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WM8960_H
#define __WM8960_H

/* Includes ------------------------------------------------------------------*/
#include "..\Common\audio.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Component
  * @{
  */ 
  
/** @addtogroup CS43122
  * @{
  */

/** @defgroup CS43122_Exported_Types
  * @{
  */

/**
  * @}
  */

/** @defgroup CS43122_Exported_Constants
  * @{
  */ 

/******************************************************************************/
/***************************  Codec User defines ******************************/
/******************************************************************************/
/* Codec Input Mode */
#define INPUT_DAC			         1
#define INPUT_FM				       2
#define INPUT_BLUETOOTH        3
#define INPUT_AUX      		     4

/* Volume Levels values */
#define DEFAULT_VOLMIN                0x00
#define DEFAULT_VOLMAX                0xFF
#define DEFAULT_VOLSTEP               0x04

#define AUDIO_PAUSE                   0
#define AUDIO_RESUME                  1

/* Codec POWER DOWN modes */
#define CODEC_PDWN_HW                 1
#define CODEC_PDWN_SW                 2

/* MUTE commands */
#define AUDIO_MUTE_ON                 1
#define AUDIO_MUTE_OFF                0

/******************************************************************************/
/****************************** REGISTER MAPPING ******************************/
/******************************************************************************/

typedef enum
{
    WM8960_LEFT_INPUT_VOLUME    = 0x00,
    WM8960_RIGHT_INPUT_VOLUME   = 0x01,
    WM8960_LEFT_OUT1_VOLUME     = 0x02 ,
    WM8960_RIGHT_OUT1_VOLUME    = 0x03 ,
    WM8960_CLOCKING_1           = 0x04 ,
    WM8960_ADC_DAC_CTRL1        = 0x05,
    WM8960_ADC_DAC_CTRL2        = 0x06,
    WM8960_AUDIO_INTERFACE_1    = 0x07,
    WM8960_CLOCKING_2           = 0x08,
    WM8960_AUDIO_INTERFACE_2    = 0x09,
    WM8960_LEFT_DAC_VOLUME      = 0x0A,
    WM8960_RIGHT_DAC_VOLUME     = 0x0B,
    WM8960_RESET                = 0x0F,
    WM8960_3D_CTRL              = 0x10,
    WM8960_ALC_1                = 0x11,
    WM8960_ALC_2                = 0x12,
    WM8960_ALC_3                = 0x13,
    WM8960_NOISE_GATE           = 0x14,
    WM8960_LEFT_ADC_VOLUME      = 0x15,
    WM8960_RIGHT_ADC_VOLUME     = 0x16,
    WM8960_ADDITIONAL_CTRL_1    = 0x17,
    WM8960_ADDITIONAL_CTRL_2    = 0x18,
    WM8960_ADDITIONAL_CTRL_3    = 0x1B,
    WM8960_POWER_MGMT_1         = 0x19,
    WM8960_POWER_MGMT_2         = 0x1A,
    WM8960_ANTI_POP_1           = 0x1C,
    WM8960_ANTI_POP_2           = 0x1D,
    WM8960_ADCL_SIGNAL_PATH     = 0x20,
    WM8960_ADCR_SIGNAL_PATH     = 0x21,
    WM8960_LEFT_OUT_MIX         = 0x22,
    WM8960_RIGHT_OUT_MIX        = 0x25,
    WM8960_MONO_OUT_MIX_1       = 0x26,
    WM8960_MONO_OUT_MIX_2       = 0x27,
    WM8960_LEFT_SPK_VOLUME      = 0x28,
    WM8960_RIGHT_SPK_VOLUME     = 0x29,
    WM8960_MONO_OUT_VOLUME      = 0x2A,
    WM8960_INPUT_BOOST_MIXER_1  = 0x2B,
    WM8960_INPUT_BOOST_MIXER_2  = 0x2C,
    WM8960_LEFT_BYPASS          = 0x2D,
    WM8960_RIGHT_BYPASS         = 0x2E,
    WM8960_POWER_MGMT_3         = 0x2F,
    WM8960_ADDITIONAL_CTRL_4    = 0x30,
    WM8960_CLASS_D_CTRL_1       = 0x31,
    WM8960_CLASS_D_CTRL_3       = 0x33,
    WM8960_PLL_N                = 0x34,
    WM8960_PLL_K_1              = 0x35,
    WM8960_PLL_K_2              = 0x36,
    WM8960_PLL_K_3              = 0x37

}WM8960_REGISTER;

#define WM8960_TOTAL_REGISTERS 56

/**********************************
 * Defines the possible operation
 * modes for the codec driver.
 *********************************/
typedef enum
{
    O_RDONLY = 0x1,   /* Open the codec driver for a read operation only */
    O_WRONLY = 0x2,   /* Open the codec driver for write operation only   */
    O_RDWR = 0x4,     /* Open the codec driver for both read and write   */

}O_MODE;


#define		SAMPLE_RATE_48000_HZ		48000
#define 	SAMPLE_RATE_44100_HZ		44100
#define   SAMPLE_RATE_32000_HZ		32000
#define		SAMPLE_RATE_24000_HZ		24000
#define   SAMPLE_RATE_16000_HZ		16000
#define   SAMPLE_RATE_8000_HZ			8000
#define   SAMPLE_RATE_NO_CHANGE		0

/**
  * @}
  */ 

/** @defgroup WM8960_Exported_Macros
  * @{
  */
#define VOLUME_CONVERT(Volume)    (((Volume) > 100)? 100:((uint8_t)(((Volume) * 255) / 100)))
/**
  * @}
  */ 

/** @defgroup WM8960_Exported_Functions
  * @{
  */
    
/*------------------------------------------------------------------------------
                           Audio Codec functions 
------------------------------------------------------------------------------*/
/* High Layer codec functions */
uint32_t wm8960_Init(uint16_t DeviceAddr, uint16_t OutputDevice, uint8_t Volume, uint32_t AudioFreq);
uint32_t wm8960_ReadID(uint16_t DeviceAddr);
uint32_t wm8960_Play(uint16_t DeviceAddr, uint16_t* pBuffer, uint16_t Size);
uint32_t wm8960_Pause(uint16_t DeviceAddr);
uint32_t wm8960_Resume(uint16_t DeviceAddr);
uint32_t wm8960_Stop(uint16_t DeviceAddr, uint32_t Cmd);
uint32_t wm8960_SetVolume(uint16_t DeviceAddr, uint8_t Volume);
uint32_t wm8960_SetFrequency(uint16_t DeviceAddr, uint32_t AudioFreq);
uint32_t wm8960_SetMute(uint16_t DeviceAddr, uint32_t Cmd);
uint32_t wm8960_SetOutputMode(uint16_t DeviceAddr, uint8_t Output);
uint32_t wm8960_SetInputMode(uint16_t DeviceAddr, uint8_t Input);
uint32_t wm8960_Reset(uint16_t DeviceAddr);

/* AUDIO IO functions */
void AUDIO_IO_Init(void);
void AUDIO_IO_Write(uint8_t Addr, uint8_t Reg, uint8_t Value);
uint8_t AUDIO_IO_Read(uint8_t Addr, uint8_t Reg);

/* Audio driver structure */
extern AUDIO_DrvTypeDef   wm8960_drv;

#endif /* __WM8960_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
