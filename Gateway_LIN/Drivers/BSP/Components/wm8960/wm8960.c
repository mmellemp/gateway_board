/**
  ******************************************************************************
  * @file    wm8960.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-February-2014
  * @brief   This file provides the wm8960 Audio Codec driver.   
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "wm8960.h"

/** @addtogroup BSP
  * @{
  */
  
/** @addtogroup Components
  * @{
  */ 

/** @addtogroup wm8960
  * @brief     This file provides a set of functions needed to drive the 
  *            wm8960 audio codec.
  * @{
  */

/** @defgroup wm8960_Private_Types
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup wm8960_Private_Defines
  * @{
  */
/* Uncomment this line to enable verifying data sent to codec after each write 
   operation (for debug purpose) */
#if !defined (VERIFY_WRITTENDATA)  
	//#define VERIFY_WRITTENDATA 
#endif /* VERIFY_WRITTENDATA */
/**
  * @}
  */ 

/** @defgroup wm8960_Private_Macros
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup wm8960_Private_Variables
  * @{
  */

/* Audio codec driver structure initialization */  
AUDIO_DrvTypeDef wm8960_drv = 
{
  wm8960_Init,
  wm8960_ReadID,

  wm8960_Play,
  wm8960_Pause,
  wm8960_Resume,
  wm8960_Stop,  
  
  wm8960_SetFrequency,  
  wm8960_SetVolume,
  wm8960_SetMute,  
  wm8960_SetOutputMode,
	wm8960_SetInputMode,
  wm8960_Reset
};

static uint16_t WM8960Registers[WM8960_TOTAL_REGISTERS];

uint32_t data_index = 0;

static uint8_t Is_wm8960_Stop = 1;

volatile uint8_t InputDev = 0;

volatile uint8_t OutputDev = 0;

/**
  * @}
  */ 

/** @defgroup wm8960_Function_Prototypes
  * @{
  */
static uint8_t CODEC_IO_Write(uint8_t Addr, uint8_t Reg, uint16_t Value);
static void wm8960_CodecSetRegCopyToDefault(void);
/**
  * @}
  */ 

/** @defgroup wm8960_Private_Functions
  * @{
  */ 

/**
  * @brief If the codec driver needs to keep track about what was written to the
	* 				codec registers, the following array is available.
  */

static void wm8960_CodecSetRegCopyToDefault(void)
{

		WM8960Registers[WM8960_LEFT_INPUT_VOLUME   ] = 0x97;
    WM8960Registers[WM8960_RIGHT_INPUT_VOLUME  ] = 0x97;
    WM8960Registers[WM8960_LEFT_OUT1_VOLUME    ] = 0;
    WM8960Registers[WM8960_RIGHT_OUT1_VOLUME   ] = 0;
    WM8960Registers[WM8960_CLOCKING_1          ] = 0;
    WM8960Registers[WM8960_ADC_DAC_CTRL1       ] = 0x8;
    WM8960Registers[WM8960_ADC_DAC_CTRL2       ] = 0;
    WM8960Registers[WM8960_AUDIO_INTERFACE_1   ] = 0xA;
    WM8960Registers[WM8960_CLOCKING_2          ] = 0x1C0;
    WM8960Registers[WM8960_AUDIO_INTERFACE_2   ] = 0x0;
    WM8960Registers[WM8960_LEFT_DAC_VOLUME     ] = 0xFF;
    WM8960Registers[WM8960_RIGHT_DAC_VOLUME    ] = 0xFF;
    WM8960Registers[WM8960_RESET               ] = 0x0;
    WM8960Registers[WM8960_3D_CTRL             ] = 0x0;
    WM8960Registers[WM8960_ALC_1               ] = 0x7B;
    WM8960Registers[WM8960_ALC_2               ] = 0x100;
    WM8960Registers[WM8960_ALC_3               ] = 0x32;
    WM8960Registers[WM8960_NOISE_GATE          ] = 0x0;
    WM8960Registers[WM8960_LEFT_ADC_VOLUME     ] = 0xC3;
    WM8960Registers[WM8960_RIGHT_ADC_VOLUME    ] = 0xC3;
    WM8960Registers[WM8960_ADDITIONAL_CTRL_1   ] = 0x1C0;
    WM8960Registers[WM8960_ADDITIONAL_CTRL_2   ] = 0x0;
    WM8960Registers[WM8960_ADDITIONAL_CTRL_3   ] = 0x0;
    WM8960Registers[WM8960_POWER_MGMT_1        ] = 0x0;
    WM8960Registers[WM8960_POWER_MGMT_2        ] = 0x0;
    WM8960Registers[WM8960_ANTI_POP_1          ] = 0x0;
    WM8960Registers[WM8960_ANTI_POP_2          ] = 0x0;
    WM8960Registers[WM8960_ADCL_SIGNAL_PATH    ] = 0x100;
    WM8960Registers[WM8960_ADCR_SIGNAL_PATH    ] = 0x100;
    WM8960Registers[WM8960_LEFT_OUT_MIX     	 ] = 0x50;
    WM8960Registers[WM8960_RIGHT_OUT_MIX    	 ] = 0x50;
    WM8960Registers[WM8960_MONO_OUT_MIX_1      ] = 0x0;
    WM8960Registers[WM8960_MONO_OUT_MIX_2      ] = 0x0;
    WM8960Registers[WM8960_LEFT_SPK_VOLUME     ] = 0x0;
    WM8960Registers[WM8960_RIGHT_SPK_VOLUME    ] = 0x0;
    WM8960Registers[WM8960_MONO_OUT_VOLUME     ] = 0x40;
    WM8960Registers[WM8960_INPUT_BOOST_MIXER_1 ] = 0x0;
    WM8960Registers[WM8960_INPUT_BOOST_MIXER_2 ] = 0x0;
    WM8960Registers[WM8960_LEFT_BYPASS         ] = 0x50;
    WM8960Registers[WM8960_RIGHT_BYPASS        ] = 0x50;
    WM8960Registers[WM8960_POWER_MGMT_3        ] = 0x0;
    WM8960Registers[WM8960_ADDITIONAL_CTRL_4   ] = 0x2;
    WM8960Registers[WM8960_CLASS_D_CTRL_1      ] = 0x37;
    WM8960Registers[WM8960_CLASS_D_CTRL_3      ] = 0x80;
    WM8960Registers[WM8960_PLL_N               ] = 0x8;
    WM8960Registers[WM8960_PLL_K_1             ] = 0x31;
    WM8960Registers[WM8960_PLL_K_2             ] = 0x26;
    WM8960Registers[WM8960_PLL_K_3             ] = 0xE9;
}


/**
  * @brief Initializes the audio codec and the control interface.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param InputDevice: specifies the audio Input Source: ne INPUT_DAC, INPUT_FM,INPUT_BLUETOOTH ,INPUT_AUX
  * @param Volume: Initial volume level (from 0 (Mute) to 100 (Max))
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_Init(uint16_t DeviceAddr, uint16_t InputDevice, uint8_t Volume, uint32_t AudioFreq)
{
  uint32_t counter = 0;
  
  /* Initialize the Control interface of the Audio Codec */
  AUDIO_IO_Init();  

	/*reset Codec */
	wm8960_Reset(DeviceAddr);
	
	/* Setup default state of codec registers */
  wm8960_CodecSetRegCopyToDefault();
		
	/* Audio Interface set slave mode with DSP (PCM data) format */
  WM8960Registers[WM8960_AUDIO_INTERFACE_1] = 0x01;
  counter += CODEC_IO_Write(DeviceAddr, WM8960_AUDIO_INTERFACE_1, WM8960Registers[WM8960_AUDIO_INTERFACE_1]);
	
//	/* Set Volume */
//	/*HeadPhones*/
//	/* Configure HeadPhone output for the 0dB level. range +6db to -73db. */	
//	WM8960Registers[WM8960_LEFT_OUT1_VOLUME] = 0x172; //0x179
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT1_VOLUME, WM8960Registers[WM8960_LEFT_OUT1_VOLUME]);
//	WM8960Registers[WM8960_RIGHT_OUT1_VOLUME] = 0x172; //0x179
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT1_VOLUME, WM8960Registers[WM8960_RIGHT_OUT1_VOLUME]);
//	
//	/*Speaker */
//	/* Configure speaker volume to 0db Dont set it to very high it will saturated speaker */
//	//28h
//	WM8960Registers[WM8960_LEFT_SPK_VOLUME] = 0x14C; //0x179
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_SPK_VOLUME, WM8960Registers[WM8960_LEFT_SPK_VOLUME]);
//	//29h 
//	WM8960Registers[WM8960_RIGHT_SPK_VOLUME] = 0x14C;//0x179
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_SPK_VOLUME, WM8960Registers[WM8960_RIGHT_SPK_VOLUME]);

	/* 31h enable both speakers	*/
	WM8960Registers[WM8960_CLASS_D_CTRL_1] = 0xF7;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_CLASS_D_CTRL_1, WM8960Registers[WM8960_CLASS_D_CTRL_1]);
	
	/* 33h Class D  Control set gain to 1.00x boost	*/
//	WM8960Registers[WM8960_CLASS_D_CTRL_3] = 0x80;
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_CLASS_D_CTRL_3, WM8960Registers[WM8960_CLASS_D_CTRL_3]);
		
// /* Left DAC volume. Set at -20 db*/
// WM8960Registers[WM8960_LEFT_DAC_VOLUME] = 0x1D7;
// counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_DAC_VOLUME, WM8960Registers[WM8960_LEFT_DAC_VOLUME]);

// /* Right DAC volume. Set at -20 dB */ 
// WM8960Registers[WM8960_RIGHT_DAC_VOLUME] = 0x1D7;
// counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_DAC_VOLUME, WM8960Registers[WM8960_RIGHT_DAC_VOLUME]);
	
	/* Disconnect All lines from PGA */
	WM8960Registers[WM8960_ADCL_SIGNAL_PATH] = 0x00;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCL_SIGNAL_PATH, WM8960Registers[WM8960_ADCL_SIGNAL_PATH]);

	WM8960Registers[WM8960_ADCR_SIGNAL_PATH] =0x00;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCR_SIGNAL_PATH, WM8960Registers[WM8960_ADCR_SIGNAL_PATH]);
	
	/*Unmute Left and Right PGA . Other register bits are default. */
	WM8960Registers[WM8960_LEFT_INPUT_VOLUME] = 0x1B9;
	counter +=  CODEC_IO_Write(DeviceAddr, WM8960_LEFT_INPUT_VOLUME, WM8960Registers[WM8960_LEFT_INPUT_VOLUME]);	

	WM8960Registers[WM8960_RIGHT_INPUT_VOLUME] = 0x1B9;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_INPUT_VOLUME, WM8960Registers[WM8960_RIGHT_INPUT_VOLUME]);

	/* Power up all the modules */
	/*                   8     7      6      5      4      3      2      1       0    */
	/*19h Pwr Mgmt (1)|VMIDSEL[1:0]| VREF | AINL | AINR | ADCL | ADCR | MICB | DIGENB| 1_1100_0000 */
	/* PGA and Input Boost Mixer on Vmid set to 2 * 50K , VREF power up , ADC off */ 
	WM8960Registers[WM8960_POWER_MGMT_1] = 0x0F0; 
	counter += CODEC_IO_Write(DeviceAddr, WM8960_POWER_MGMT_1, WM8960Registers[WM8960_POWER_MGMT_1]);

	/*                 8     7      6      5      4      3      2      1       0   	*/
	/*1Ah Pwr Mgmt(2) DACL | DACR | LOUT1| ROUT1 |SPKL |SPKR 0| OUT3 |PLL_EN| 1_0000_0000*/
	/* DAC HP SPEAKER ON , OUT3 PLL Off */
	WM8960Registers[WM8960_POWER_MGMT_2] = 0x1F8; 
	counter += CODEC_IO_Write(DeviceAddr, WM8960_POWER_MGMT_2, WM8960Registers[WM8960_POWER_MGMT_2]);
	
	/*                   8     7      6      5      4      3      2      1     0    */
	/* 2Fh Pwr Mgmt (3)  0     0      0    LMIC    RMIC  LOMIX  ROMIX    0     0    0_0000_0000*/
	/* PGA and OUT MIXERs ON */
	WM8960Registers[WM8960_POWER_MGMT_3] = 0x03C;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_POWER_MGMT_3, WM8960Registers[WM8960_POWER_MGMT_3]);
	
	
	/* Path Configurations Input*/
	wm8960_SetInputMode(DeviceAddr, InputDevice);
	
	wm8960_SetVolume(DeviceAddr,Volume);
	 			
  /* Return communication control value */
  return counter;  
}

/**
  * @brief  Get the wm8960 ID.
  * @param DeviceAddr: Device address on communication Bus.   
  * @retval The wm8960 ID 
  */
uint32_t wm8960_ReadID(uint16_t DeviceAddr)
{
  /* Initialize the Control interface of the Audio Codec */
  AUDIO_IO_Init(); 
  
  //return ((uint32_t)AUDIO_IO_Read(DeviceAddr, wm8960_CHIPID_ADDR));
	return 0xFFFF;
}

/**
  * @brief Start the audio Codec play feature.
  * @note For this codec no Play options are required.
  * @param DeviceAddr: Device address on communication Bus.   
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_Play(uint16_t DeviceAddr, uint16_t* pBuffer, uint16_t Size)
{
  uint32_t counter = 0;
  
  if(Is_wm8960_Stop == 1)
  {
    /* Enable Output device */  
    counter += wm8960_SetMute(DeviceAddr, AUDIO_MUTE_OFF);
 
    Is_wm8960_Stop = 0;
  }
  
  /* Return communication control value */
  return counter;  
}

/**
  * @brief Pauses playing on the audio codec.
  * @param DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_Pause(uint16_t DeviceAddr)
{  
  uint32_t counter = 0;
 
//  /* Pause the audio file playing */
//  /* Mute the output first */
//  counter += wm8960_SetMute(DeviceAddr, AUDIO_MUTE_ON);
//  
//  /* Put the Codec in Power save mode */    
//  counter += CODEC_IO_Write(DeviceAddr,0x02, 0x01);
 
  return counter;
}

/**
  * @brief Resumes playing on the audio codec.
  * @param DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_Resume(uint16_t DeviceAddr)
{
  uint32_t counter = 0;
//  volatile uint32_t index = 0x00;
//  /* Resumes the audio file playing */  
//  /* Unmute the output first */
//  counter += wm8960_SetMute(DeviceAddr, AUDIO_MUTE_OFF);

//  for(index = 0x00; index < 0xFF; index++);
//  
//  counter += CODEC_IO_Write(DeviceAddr,0x04, OutputDev);

//  /* Exit the Power save mode */
//  counter += CODEC_IO_Write(DeviceAddr,0x02, 0x9E); 
  
  return counter;
}

/**
  * @brief Stops audio Codec playing. It powers down the codec.
  * @param DeviceAddr: Device address on communication Bus. 
  * @param CodecPdwnMode: selects the  power down mode.
  *          - CODEC_PDWN_HW: Physically power down the codec. When resuming from this
  *                           mode, the codec is set to default configuration 
  *                           (user should re-Initialize the codec in order to 
  *                            play again the audio stream).
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_Stop(uint16_t DeviceAddr, uint32_t CodecPdwnMode)
{
  uint32_t counter = 0;
  
//  /* Mute the output first */
//  counter += wm8960_SetMute(DeviceAddr, AUDIO_MUTE_ON);
//  
//  /* Power down the DAC and the speaker (PMDAC and PMSPK bits)*/
//  counter += CODEC_IO_Write(DeviceAddr, 0x02, 0x9F);
//  
//  Is_wm8960_Stop = 1;
  return counter;    
}

/**
  * @brief Sets higher or lower the codec volume level.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param Volume: a byte value from 0 to 100 (refer to codec registers 
  *         description for more details).
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_SetVolume(uint16_t DeviceAddr, uint8_t Volume)
{
	uint32_t counter = 0;
	
	uint8_t volumeD;

	Volume = (Volume > 100) ? 100 : Volume;

	volumeD = 37 + (uint8_t) (0.8 * Volume);


//	/* Left DAC volume. Set at 0 db steps Mute, increase volume and then Unmute*/
//	WM8960Registers[WM8960_LEFT_DAC_VOLUME] = 0x100;
//	counter +=  CODEC_IO_Write(DeviceAddr, WM8960_LEFT_DAC_VOLUME, WM8960Registers[WM8960_LEFT_DAC_VOLUME]);
//	
//	/* Right DAC volume. Set at 0 dB Mute */
//	WM8960Registers[WM8960_RIGHT_DAC_VOLUME] = 0x100;
//	counter +=  CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_DAC_VOLUME, WM8960Registers[WM8960_RIGHT_DAC_VOLUME]);

//	/* Left DAC volume. Set New Volume*/
//	WM8960Registers[WM8960_LEFT_DAC_VOLUME] = 0x100 | volumeD;
//	counter +=  CODEC_IO_Write(DeviceAddr, WM8960_LEFT_DAC_VOLUME, WM8960Registers[WM8960_LEFT_DAC_VOLUME]);
//	
//	/* Right DAC volume. Set New Volume */	
//	WM8960Registers[WM8960_RIGHT_DAC_VOLUME] = 0x100 | volumeD;
//	counter +=  CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_DAC_VOLUME, WM8960Registers[WM8960_RIGHT_DAC_VOLUME]);

	/* Set Volume */
	/*HeadPhones*/
//	WM8960Registers[WM8960_LEFT_OUT1_VOLUME] = 0x100;
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT1_VOLUME, WM8960Registers[WM8960_LEFT_OUT1_VOLUME]);
//	
//	WM8960Registers[WM8960_RIGHT_OUT1_VOLUME] = 0x100;
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT1_VOLUME, WM8960Registers[WM8960_RIGHT_OUT1_VOLUME]);
	
	WM8960Registers[WM8960_LEFT_OUT1_VOLUME] = 0x100 | volumeD;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT1_VOLUME, WM8960Registers[WM8960_LEFT_OUT1_VOLUME]);
	
	WM8960Registers[WM8960_RIGHT_OUT1_VOLUME] = 0x100 | volumeD;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT1_VOLUME, WM8960Registers[WM8960_RIGHT_OUT1_VOLUME]);
	
	/*Speaker*/
//	WM8960Registers[WM8960_LEFT_SPK_VOLUME] = 0x100;
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_SPK_VOLUME, WM8960Registers[WM8960_LEFT_SPK_VOLUME]);
//	//29h 
//	WM8960Registers[WM8960_RIGHT_SPK_VOLUME] = 0x100;
//	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_SPK_VOLUME, WM8960Registers[WM8960_RIGHT_SPK_VOLUME]);
	//28h
	WM8960Registers[WM8960_LEFT_SPK_VOLUME] = 0x100 | volumeD;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_SPK_VOLUME, WM8960Registers[WM8960_LEFT_SPK_VOLUME]);
	//29h 
	WM8960Registers[WM8960_RIGHT_SPK_VOLUME] = 0x100 | volumeD;
	counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_SPK_VOLUME, WM8960Registers[WM8960_RIGHT_SPK_VOLUME]);

	return counter;
}

/**
  * @brief Sets new frequency.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param AudioFreq: Audio frequency used to play the audio stream.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_SetFrequency(uint16_t DeviceAddr, uint32_t AudioFreq)
{
	return 0;
}


/**
  * @brief Enables or disables the mute feature on the audio codec.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param Cmd: AUDIO_MUTE_ON to enable the mute or AUDIO_MUTE_OFF to disable the
  *             mute mode.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_SetMute(uint16_t DeviceAddr, uint32_t Cmd)
{
  uint32_t counter = 0;
  
  /* Set the Mute mode */
  if(Cmd == AUDIO_MUTE_ON)
  {
    counter +=  CODEC_IO_Write(DeviceAddr, WM8960_ADC_DAC_CTRL1,0x08);
  }
  else /* AUDIO_MUTE_OFF Disable the Mute */
  {
    counter +=  CODEC_IO_Write(DeviceAddr, WM8960_ADC_DAC_CTRL1,0x00);
  }

  return counter;
}

/**
  * @brief Switch dynamically Input Mode between FM,AUX,Bluetooth,DAC
  *         (speaker or headphone).
  * @note This function modifies a global variable of the audio codec driver: InputDev.
  * @param DeviceAddr: Device address on communication Bus.
  * @param Input: specifies the audio Input Source: ne INPUT_DAC, INPUT_FM,INPUT_BLUETOOTH ,INPUT_AUX
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_SetOutputMode(uint16_t DeviceAddr, uint8_t Output)
{
	return 0;
}

/**
  * @brief Switch dynamically Input Mode between FM,AUX,Bluetooth,DAC
  * @note This function modifies a global variable of the audio codec driver: InputDev.
  * @param DeviceAddr: Device address on communication Bus.
  * @param Input: specifies the audio Input Source: ne INPUT_DAC, INPUT_FM,INPUT_BLUETOOTH ,INPUT_AUX
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_SetInputMode(uint16_t DeviceAddr, uint8_t Input)
{
  uint32_t counter = 0; 
  
  switch (Input) 
  {
    case INPUT_DAC:
			
			/*2Dh Disconnect input mixer From output mixer */				
			WM8960Registers[WM8960_LEFT_BYPASS] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_BYPASS, WM8960Registers[WM8960_LEFT_BYPASS]);
			
			WM8960Registers[WM8960_RIGHT_BYPASS] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_BYPASS, WM8960Registers[WM8960_RIGHT_BYPASS]);			
			
			/*22h Connect Left DAC to Left Output Mixer 22h */
			WM8960Registers[WM8960_LEFT_OUT_MIX] = 0x100;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT_MIX, WM8960Registers[WM8960_LEFT_OUT_MIX]);
			
			/* Connect Right DAC to Left Output Mixer */
			WM8960Registers[WM8960_RIGHT_OUT_MIX] = 0x100;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT_MIX, WM8960Registers[WM8960_RIGHT_OUT_MIX]);
		
			/*05h Unmute DAC */
			WM8960Registers[WM8960_ADC_DAC_CTRL1] = 0x00;
			counter +=  CODEC_IO_Write(DeviceAddr, WM8960_ADC_DAC_CTRL1,WM8960Registers[WM8960_ADC_DAC_CTRL1]);

			break;
      
    case INPUT_FM: /*INPUT 2*/
      /* FM TUNER is Connected on INPUT2 */
			/*22h Disconnect Left DAC from Left Output Mixer 22h */
			WM8960Registers[WM8960_LEFT_OUT_MIX] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT_MIX, WM8960Registers[WM8960_LEFT_OUT_MIX]);
			
			/* Connect Right DAC to Left Output Mixer */
			WM8960Registers[WM8960_RIGHT_OUT_MIX] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT_MIX, WM8960Registers[WM8960_RIGHT_OUT_MIX]);
	
			/* Disconnect All lines from PGA */
			WM8960Registers[WM8960_ADCL_SIGNAL_PATH] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCL_SIGNAL_PATH, WM8960Registers[WM8960_ADCL_SIGNAL_PATH]);

			WM8960Registers[WM8960_ADCR_SIGNAL_PATH] =0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCR_SIGNAL_PATH, WM8960Registers[WM8960_ADCR_SIGNAL_PATH]);
		
			/* 2Bh INPUT2 gain set to +6 db 1110 and INPUT3 boost mute */
			WM8960Registers[WM8960_INPUT_BOOST_MIXER_1] = 0x0E; 
			counter += CODEC_IO_Write(DeviceAddr, WM8960_INPUT_BOOST_MIXER_1, WM8960Registers[WM8960_INPUT_BOOST_MIXER_1]);	

			WM8960Registers[WM8960_INPUT_BOOST_MIXER_2] = 0x0E; 
			counter += CODEC_IO_Write(DeviceAddr, WM8960_INPUT_BOOST_MIXER_2, WM8960Registers[WM8960_INPUT_BOOST_MIXER_2]);	
		
		
			/*2Dh Connect input mixer to output mixer */				
			WM8960Registers[WM8960_LEFT_BYPASS] = 0x80;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_BYPASS, WM8960Registers[WM8960_LEFT_BYPASS]);
			
			WM8960Registers[WM8960_RIGHT_BYPASS] = 0x80;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_BYPASS, WM8960Registers[WM8960_RIGHT_BYPASS]);	
			break;
      
    case INPUT_BLUETOOTH: /* INPUT 1 */
			/* Bluetooth is Connected on INPUT1 it need PGA */
			/*22h Disconnect Left DAC from Left Output Mixer 22h */
			WM8960Registers[WM8960_LEFT_OUT_MIX] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT_MIX, WM8960Registers[WM8960_LEFT_OUT_MIX]);
			
			/* Connect Right DAC to Left Output Mixer */
			WM8960Registers[WM8960_RIGHT_OUT_MIX] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT_MIX, WM8960Registers[WM8960_RIGHT_OUT_MIX]);
	
			/* 2Bh INPUT2 INPUT 3 set to mute */
			WM8960Registers[WM8960_INPUT_BOOST_MIXER_1] = 0x00; 
			counter += CODEC_IO_Write(DeviceAddr, WM8960_INPUT_BOOST_MIXER_1, WM8960Registers[WM8960_INPUT_BOOST_MIXER_1]);	

			WM8960Registers[WM8960_INPUT_BOOST_MIXER_2] = 0x00; 
			counter += CODEC_IO_Write(DeviceAddr, WM8960_INPUT_BOOST_MIXER_2, WM8960Registers[WM8960_INPUT_BOOST_MIXER_2]);	
		
			/*20h Connect INPUT 1 to PGA  and set MICBOOST to +29db and connect to mixer */
			WM8960Registers[WM8960_ADCL_SIGNAL_PATH] = 0x138;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCL_SIGNAL_PATH, WM8960Registers[WM8960_ADCL_SIGNAL_PATH]);

			WM8960Registers[WM8960_ADCR_SIGNAL_PATH] = 0x138;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCR_SIGNAL_PATH, WM8960Registers[WM8960_ADCR_SIGNAL_PATH]);

		
			/*2Dh Connect input mixer to output mixer */				
			WM8960Registers[WM8960_LEFT_BYPASS] = 0x80;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_BYPASS, WM8960Registers[WM8960_LEFT_BYPASS]);
			
			WM8960Registers[WM8960_RIGHT_BYPASS] = 0x80;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_BYPASS, WM8960Registers[WM8960_RIGHT_BYPASS]);	
      break;
      
    case INPUT_AUX: /* INPUT 3 */
			/* AUX is Connected on INPUT3 */
			/*22h Disconnect Left DAC from Left Output Mixer */
			WM8960Registers[WM8960_LEFT_OUT_MIX] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_OUT_MIX, WM8960Registers[WM8960_LEFT_OUT_MIX]);
			
			/* Connect Right DAC to Left Output Mixer */
			WM8960Registers[WM8960_RIGHT_OUT_MIX] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_OUT_MIX, WM8960Registers[WM8960_RIGHT_OUT_MIX]);
	
			/* Disconnect All lines from PGA */
			WM8960Registers[WM8960_ADCL_SIGNAL_PATH] = 0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCL_SIGNAL_PATH, WM8960Registers[WM8960_ADCL_SIGNAL_PATH]);

			WM8960Registers[WM8960_ADCR_SIGNAL_PATH] =0x00;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_ADCR_SIGNAL_PATH, WM8960Registers[WM8960_ADCR_SIGNAL_PATH]);
		
			/* 2Bh INPUT3 gain set to 0 db 1010 and INPUT2 boost mute */
			WM8960Registers[WM8960_INPUT_BOOST_MIXER_1] = 0x50; 
			counter += CODEC_IO_Write(DeviceAddr, WM8960_INPUT_BOOST_MIXER_1, WM8960Registers[WM8960_INPUT_BOOST_MIXER_1]);	

			WM8960Registers[WM8960_INPUT_BOOST_MIXER_2] = 0x50; 
			counter += CODEC_IO_Write(DeviceAddr, WM8960_INPUT_BOOST_MIXER_2, WM8960Registers[WM8960_INPUT_BOOST_MIXER_2]);	
			
			/*2Dh Connect input mixer to output mixer */				
			WM8960Registers[WM8960_LEFT_BYPASS] = 0x80;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_LEFT_BYPASS, WM8960Registers[WM8960_LEFT_BYPASS]);
			
			WM8960Registers[WM8960_RIGHT_BYPASS] = 0x80;
			counter += CODEC_IO_Write(DeviceAddr, WM8960_RIGHT_BYPASS, WM8960Registers[WM8960_RIGHT_BYPASS]);	    
      break;    
      
    default:
      break;
  }  
  return counter;
}

/**
  * @brief Resets wm8994 registers.
  * @param DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t wm8960_Reset(uint16_t DeviceAddr)
{
  uint32_t counter = 0;
  
  /* Reset Codec by wrinting in 0x0000 address register */
  counter = CODEC_IO_Write(DeviceAddr, WM8960_RESET, 0x00);
 
  return counter;
}


/**
  * @brief  Writes/Read a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @param  Value: Data to be written
  * @retval None
  */
static uint8_t CODEC_IO_Write(uint8_t Addr, uint8_t Reg, uint16_t Value)
{
  uint32_t result = 0;

	/*
		A control word consists of 16 bits. The first 7 bits (B15 to B9) are address bits that select which 
		control register is accessed. The remaining 9 bits (B8 to B0) are data bits, corresponding to the 9 
	  bits in each control register.
	*/
	
	uint8_t Add_plus_onebitControl = (uint8_t)(Reg << 1) | (Value >> 8);

	uint8_t Control = (uint8_t)(Value & 0xFF);
  
  AUDIO_IO_Write(Addr, Add_plus_onebitControl, Control);
  
#ifdef VERIFY_WRITTENDATA
  /* Verify that the data has been correctly written */  
  result = (AUDIO_IO_Read(Addr, Reg) == Value)? 0:1;
#endif /* VERIFY_WRITTENDATA */
  
  return result;
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
