/**
  ******************************************************************************
  * @file    cs4385.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014
  * @brief   This file contains all the functions prototypes for the cs4385.c driver.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __cs4385A_H
#define __cs4385A_H

/* Includes ------------------------------------------------------------------*/
#include "..\Common\audio.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Component
  * @{
  */ 
  
/** @addtogroup cs4385A
  * @{
  */

/** @defgroup cs4385A_Exported_Types
  * @{
  */

/**
  * @}
  */

/** @defgroup CS43122_Exported_Constants
  * @{
  */ 

/******************************************************************************/
/***************************  Codec User defines ******************************/
/******************************************************************************/
/* Codec Volume control Modes */																						
#define INDEPENDENT_VOLUME_CONTROL 		0     /* Channel volume controlled independently */
#define PAIRED_VOLUME_CONTROL 				1			/* Channel volume controlled on paires A1-B1 controlled with A1 Channel Volume */
#define SINGLE_VOLUME_CONTROL					2			/* Control all channels volume with single control  A1 Channel Volume */

/* Channel selection defines */
#define CH_A1							((uint8_t)0x01) /* Channel A1 selected */
#define CH_B1							((uint8_t)0x02)	/* Channel B1 selected */
#define CH_A2							((uint8_t)0x04)	/* Channel A2 selected */
#define CH_B2							((uint8_t)0x08)	/* Channel B2 selected */
#define CH_A3							((uint8_t)0x10)	/* Channel A3 selected */
#define CH_B3							((uint8_t)0x20)	/* Channel B3 selected */
#define CH_A4							((uint8_t)0x40)	/* Channel A4 selected */
#define CH_B4							((uint8_t)0x80)	/* Channel B4 selected */
#define CH_ALL						((uint8_t)0xFF)	/* ALL Channels selected */

/* Codec output DEVICE */
#define OUTPUT_DEVICE_SPEAKER         1
#define OUTPUT_DEVICE_HEADPHONE       2
#define OUTPUT_DEVICE_BOTH            3
#define OUTPUT_DEVICE_AUTO            4

/* Volume Levels values */
#define DEFAULT_VOLMIN                0x00
#define DEFAULT_VOLMAX                0xFF
#define DEFAULT_VOLSTEP               0x04

#define AUDIO_PAUSE                   0
#define AUDIO_RESUME                  1

/* Codec POWER DOWN modes */
#define CODEC_PDWN_HW                 1
#define CODEC_PDWN_SW                 2

/* MUTE commands */
#define AUDIO_MUTE_ON                 1
#define AUDIO_MUTE_OFF                0

/* AUDIO FREQUENCY */
#define AUDIO_FREQUENCY_192K          ((uint32_t)192000)
#define AUDIO_FREQUENCY_96K           ((uint32_t)96000)
#define AUDIO_FREQUENCY_48K           ((uint32_t)48000)
#define AUDIO_FREQUENCY_44K           ((uint32_t)44100)
#define AUDIO_FREQUENCY_32K           ((uint32_t)32000)
#define AUDIO_FREQUENCY_22K           ((uint32_t)22050)
#define AUDIO_FREQUENCY_16K           ((uint32_t)16000)
#define AUDIO_FREQUENCY_11K           ((uint32_t)11025)
#define AUDIO_FREQUENCY_8K            ((uint32_t)8000) 

/******************************************************************************/
/****************************** REGISTER MAPPING ******************************/
/******************************************************************************/
/** 
  * @brief  cs4385A ID  
  */  
#define  CS4385A_ID            0x08
#define  CS4385A_ID_MASK       0xF8
/**
  * @brief Chip ID Register: Chip I.D. and Revision Register
  *  Read only register
  *  Default value: 0x01
  *  [7:3] CHIPID[4:0]: I.D. code for the cs4385A.
  *        Default value: 00001b
  *  [2:0] REVID[2:0]: cs4385A revision level.
  *        Default value: 
  *        000 - Rev A0
  *        001 - Rev B0
  */
#define CS4385A_CHIPID_ADDR    				0x01
#define CS4385A_CHIP_REVISION    			0x01		/* Chip I.D. and Revision Register */
#define CS4385A_MODE_CONTROL					0x02		/* Control Port Enable,Freeze Controls,PCM/DSD Selection,DAC Pair Disable,Power Down */	
#define CS4385A_PCM_CONTROL						0x03		/* Digital Interface Format,Functional Mode	*/
#define CS4385A_DSD_CONTROL						0x04		/* DSD Control setting	*/
#define CS4385A_FILTER_CONTROL				0x05		/* Interpolation Filter Select */
#define CS4385A_INVERT_CONTROL				0x06		/* Invert Signal Polarity */ 
#define CS4385A_GROUP_CONTROL					0x07		/* Mutec Pin Control,Channel A Volume = Channel B Volume, Single Volume Control */
#define CS4385A_RAMP_MUTE							0x08		/* Soft Ramp and Zero Cross CONTROL,RMP_UP,RMP_DN,PCM/DSD Auto-Mute,MUTE Polarity and DETECT */

#define CS4385A_MUTE_CONTROL					0x09		/* Mute individual channel */	

#define CS4385A_MIXING_CONTROL_P1			0x0A		/* Pair 1 De-Emphasis Control,ATAPI Channel Mixing and Muting */
#define CS4385A_VOLUME_CONTROL_A1			0x0B		/* Volume Control A1 channel Range 0-255 == 0 -(-127.5 dB) */
#define CS4385A_VOLUME_CONTROL_B1			0x0C		/* Volume Control B1 channel */

#define CS4385A_MIXING_CONTROL_P2			0x0D		/* Pair 2 De-Emphasis Control,ATAPI Channel Mixing and Muting */
#define CS4385A_VOLUME_CONTROL_A2			0x0E		/* Volume Control A2 channel */
#define CS4385A_VOLUME_CONTROL_B2			0x0F		/* Volume Control B2 channel */

#define CS4385A_MIXING_CONTROL_P3			0x10		/* Pair 3 De-Emphasis Control,ATAPI Channel Mixing and Muting */
#define CS4385A_VOLUME_CONTROL_A3			0x11		/* Volume Control A3 channel */
#define CS4385A_VOLUME_CONTROL_B3			0x12		/* Volume Control B3 channel */

#define CS4385A_MIXING_CONTROL_P4			0x13		/* Pair 4 De-Emphasis Control,ATAPI Channel Mixing and Muting */
#define CS4385A_VOLUME_CONTROL_A4			0x14		/* Volume Control A4 channel */
#define CS4385A_VOLUME_CONTROL_B4			0x15		/* Volume Control B4 channel */

#define CS4385A_PCM_CLOCK_MODE				0x16		/* Master Clock DIVIDE by 2 ENABLE */	


/**
  * @}
  */ 

/** @defgroup CS43122_Exported_Macros
  * @{
  */
#define VOLUME_CONVERT(Volume)    (((Volume) > 100)? 100:((uint8_t)(255-((Volume) * 255) / 100)))
/**
  * @}
  */ 

/** @defgroup CS43122_Exported_Functions
  * @{
  */
    
/*------------------------------------------------------------------------------
                           Audio Codec functions 
------------------------------------------------------------------------------*/
/* High Layer codec functions */
uint32_t cs4385A_Init(uint16_t DeviceAddr, uint16_t OutputDevice, uint8_t Volume, uint32_t AudioFreq);
uint32_t cs4385A_ReadID(uint16_t DeviceAddr);
uint32_t cs4385A_Play(uint16_t DeviceAddr, uint16_t* pBuffer, uint16_t Size);
uint32_t cs4385A_Pause(uint16_t DeviceAddr);
uint32_t cs4385A_Resume(uint16_t DeviceAddr);
uint32_t cs4385A_Stop(uint16_t DeviceAddr, uint32_t Cmd);
uint32_t cs4385A_SetVolume(uint16_t DeviceAddr, uint8_t Volume);
uint32_t cs4385A_SetFrequency(uint16_t DeviceAddr, uint32_t AudioFreq);
uint32_t cs4385A_SetMute(uint16_t DeviceAddr, uint32_t Cmd);
uint32_t cs4385A_SetOutputMode(uint16_t DeviceAddr, uint8_t Output);

//Arun
uint32_t cs4385A_SetVolumeControlMode(uint16_t DeviceAddr, uint8_t VolumeCtrlMode);

/* AUDIO IO functions */
void AUDIO_IO_Init(void);
void AUDIO_IO_Write(uint8_t Addr, uint8_t Reg, uint8_t Value);
uint8_t AUDIO_IO_Read(uint8_t Addr, uint8_t Reg);
void AUDIO_RESET(void);
void AUDIO_IO_Delay(uint32_t Delay);

/* Audio driver structure */
extern AUDIO_DrvTypeDef   cs4385A_drv;

#endif /* __cs4385A_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
