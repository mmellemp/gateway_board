/**
  ******************************************************************************
  * @file    cs4385.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014
  * @brief   This file provides the cs4385 Audio Codec driver.   
  ******************************************************************************
	* July 11			Prototype
	*
	*	July 16			First stable version
	*
	*	July 19 		Addded Delay Function
	*
	*	Sep  8 			Added function SetOutputMode
	*/

/* Includes ------------------------------------------------------------------*/
#include "cs4385A.h"

/** @addtogroup BSP
  * @{
  */
  
/** @addtogroup Components
  * @{
  */ 

/** @addtogroup cs4385A
  * @brief     This file provides a set of functions needed to drive the 
  *            cs4385A audio codec.
  * @{
  */

/** @defgroup cs4385A_Private_Types
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup cs4385A_Private_Defines
  * @{
  */
/* Uncomment this line to enable verifying data sent to codec after each write 
   operation (for debug purpose) */
#if !defined (VERIFY_WRITTENDATA)  
// #define VERIFY_WRITTENDATA 
#endif /* VERIFY_WRITTENDATA */

/**
  * @}
  */ 

/** @defgroup cs4385A_Private_Macros
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup cs4385A_Private_Variables
  * @{
  */

/* Audio codec driver structure initialization */  
AUDIO_DrvTypeDef cs4385A_drv = 
{
  cs4385A_Init,
  cs4385A_ReadID,

  cs4385A_Play,
  cs4385A_Pause,
  cs4385A_Resume,
  cs4385A_Stop,  
  
  cs4385A_SetFrequency,  
  cs4385A_SetVolume,
  cs4385A_SetMute,  
	cs4385A_SetOutputMode,
  0,
};

static uint8_t Is_cs4385A_Stop = 1;

volatile uint8_t OutputDev = 0;

/**
  * @}
  */ 

/** @defgroup cs4385A_Function_Prototypes
  * @{
  */
static uint8_t CODEC_IO_Write(uint8_t Addr, uint8_t Reg, uint8_t Value);

/**
  * @}
  */ 

/** @defgroup cs4385A_Private_Functions
  * @{
  */ 

/**
  * @brief Initializes the audio codec and the control interface.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param ChannelSel: can be Channel 1,channel 2...Channel ALL .
  * @param Volume: Initial volume level (from 0 (Mute) to 100 (Max))
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_Init(uint16_t DeviceAddr, uint16_t ChannelSel, uint8_t Volume, uint32_t AudioFreq)
{
  uint32_t counter = 0;
  
  /* Initialize the Control interface of the Audio Codec */
  AUDIO_IO_Init();     
  
	/* Enable Control Port and keep Powered Down */
  counter += CODEC_IO_Write(DeviceAddr,CS4385A_MODE_CONTROL, 0x81); 

	/* Configure Digital Interface Format as PCM I2S and Functional Mode as Auto */
  counter += CODEC_IO_Write(DeviceAddr,CS4385A_PCM_CONTROL, 0x13);

	/* Set the Master volume */
  counter += cs4385A_SetVolume(DeviceAddr, Volume);

	/* Enable Control Port and Powered Up */
  counter += CODEC_IO_Write(DeviceAddr,CS4385A_MODE_CONTROL, 0x80);  
	  
  counter += cs4385A_SetOutputMode(DeviceAddr,ChannelSel);
	/* Return communication control value */
  return counter;  
}

/**
  * @brief  Get the cs4385A ID.
  * @param DeviceAddr: Device address on communication Bus.   
  * @retval The cs4385A ID 
  */
uint32_t cs4385A_ReadID(uint16_t DeviceAddr)
{
  /* Initialize the Control interface of the Audio Codec */
  AUDIO_IO_Init(); 
  
  return ((uint32_t)AUDIO_IO_Read(DeviceAddr, CS4385A_CHIPID_ADDR));
}

/**
  * @brief Start the audio Codec play feature.
  * @note For this codec no Play options are required.
  * @param DeviceAddr: Device address on communication Bus.   
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_Play(uint16_t DeviceAddr, uint16_t* pBuffer, uint16_t Size)
{
  uint32_t counter = 0;
  
  if(Is_cs4385A_Stop == 1)
  {
    /* Enable Output device */  
    counter += cs4385A_SetMute(DeviceAddr, AUDIO_MUTE_OFF);
    
    /* Power on the Codec */
    counter += CODEC_IO_Write(DeviceAddr, CS4385A_MODE_CONTROL, 0x80);  
    Is_cs4385A_Stop = 0;
  }
  
  /* Return communication control value */
  return counter;  
}

/**
  * @brief Pauses playing on the audio codec.
  * @param DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_Pause(uint16_t DeviceAddr)
{  
  uint32_t counter = 0;
 
  /* Pause the audio file playing */
  /* Mute the output first */
  counter += cs4385A_SetMute(DeviceAddr, AUDIO_MUTE_ON);
  
  /* Put the Codec in Power save mode */    
  counter += CODEC_IO_Write(DeviceAddr,CS4385A_MODE_CONTROL, 0x81);
 
  return counter;
}

/**
  * @brief Resumes playing on the audio codec.
  * @param DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_Resume(uint16_t DeviceAddr)
{
  uint32_t counter = 0;
  volatile uint32_t index = 0x00;
  /* Resumes the audio file playing */  
  /* Unmute the output first */
  counter += cs4385A_SetMute(DeviceAddr, AUDIO_MUTE_OFF);

  /* Exit the Power save mode */
  counter += CODEC_IO_Write(DeviceAddr,CS4385A_MODE_CONTROL, 0x80); 
  
  return counter;
}

/**
  * @brief Stops audio Codec playing. It powers down the codec.
  * @param DeviceAddr: Device address on communication Bus. 
  * @param CodecPdwnMode: selects the  power down mode.
  *          - CODEC_PDWN_HW: Physically power down the codec. When resuming from this
  *                           mode, the codec is set to default configuration 
  *                           (user should re-Initialize the codec in order to 
  *                            play again the audio stream).
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_Stop(uint16_t DeviceAddr, uint32_t CodecPdwnMode)
{
  uint32_t counter = 0;
  
  /* Mute the output first */
  counter += cs4385A_SetMute(DeviceAddr, AUDIO_MUTE_ON);
  
	/* Put the Codec in Power save mode */ 
	counter += CODEC_IO_Write(DeviceAddr, CS4385A_MODE_CONTROL, 0x81);

  
  Is_cs4385A_Stop = 1;
  return counter;    
}

/**
  * @brief Sets higher or lower the codec volume level.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param Volume: a byte value from 0 to 255 (refer to codec registers 
  *         description for more details).
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_SetVolume(uint16_t DeviceAddr, uint8_t Volume)
{
  uint32_t counter = 0;
  uint8_t convertedvol = VOLUME_CONVERT(Volume);
		
	if (Volume == 0)
  {
		/* Mute audio codec */
		cs4385A_SetMute(DeviceAddr, AUDIO_MUTE_ON); 
	}
  else
  {
    /* Unmute audio codec */
    cs4385A_SetMute(DeviceAddr, AUDIO_MUTE_OFF); 
    
    /* Channel A1 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_A1, convertedvol); 
		
		/* Channel B1 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_B1, convertedvol); 
    
		/* Channel A2 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_A2, convertedvol); 
    
		/* Channel B2 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_B2, convertedvol); 
		
		/* Channel A3 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_A3, convertedvol); 
    
		/* Channel B3 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_B3, convertedvol); 

		/* Channel A4 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_A4, convertedvol); 
    
		/* Channel B4 Volume */
    counter += CODEC_IO_Write(DeviceAddr,CS4385A_VOLUME_CONTROL_B4, convertedvol); 

  }
  return counter;
}

/**
  * @brief Sets new frequency.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param AudioFreq: Audio frequency used to play the audio stream.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_SetFrequency(uint16_t DeviceAddr, uint32_t AudioFreq)
{
  return 0;
}

/**
  * @brief Enables or disables the mute feature on the audio codec.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param Cmd: AUDIO_MUTE_ON to enable the mute or AUDIO_MUTE_OFF to disable the
  *             mute mode.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_SetMute(uint16_t DeviceAddr, uint32_t Cmd)
{
  uint32_t counter = 0;
  
  /* Set the Mute mode */
  if(Cmd == AUDIO_MUTE_ON)
  {
    counter += CODEC_IO_Write(DeviceAddr, CS4385A_MUTE_CONTROL, 0xFF);
  }
  else /* AUDIO_MUTE_OFF Disable the Mute */
  {
    counter += CODEC_IO_Write(DeviceAddr, CS4385A_MUTE_CONTROL, 0x00);
  }

  return counter;
}

/**
  * @brief Switch dynamically (while audio file is played) the output target 
  *         (Audio Channels).
  * @note This function modifies a global variable of the audio codec driver: OutputDev.
  * @param DeviceAddr: Device address on communication Bus.
  * @param ChannelSel: can be Channel 1,channel 2...Channel ALL
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t cs4385A_SetOutputMode(uint16_t DeviceAddr, uint8_t ChannelSel)
{
	uint32_t counter = 0;
	
	/* Unmute All selected Channels */
	counter += CODEC_IO_Write(DeviceAddr, CS4385A_MUTE_CONTROL, (~ChannelSel & 0xFF));
  
  return counter;
}


/**
  * @brief  Writes/Read a single data.
  * @param  Addr: I2C address
  * @param  Reg: Reg address 
  * @param  Value: Data to be written
  * @retval None
  */
static uint8_t CODEC_IO_Write(uint8_t Addr, uint8_t Reg, uint8_t Value)
{
  uint32_t result = 0;
  
  AUDIO_IO_Write(Addr, Reg, Value);
  
#ifdef VERIFY_WRITTENDATA
  /* Verify that the data has been correctly written */  
  result = (AUDIO_IO_Read(Addr, Reg) == Value)? 0:1;
#endif /* VERIFY_WRITTENDATA */
  
  return result;
}


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
