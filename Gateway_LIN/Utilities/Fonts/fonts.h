/**
  ******************************************************************************
  * @file    fonts.h
  * @author  Arka Ims Team
  * @version V1.0.0
  * @date    15-Sep-2014
  * @brief   Header for fonts.c file
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FONTS_H
#define __FONTS_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/** @addtogroup Utilities
  * @{
  */
  
/** @addtogroup GATEWAY_BOARD
  * @{
  */ 

/** @addtogroup Common
  * @{
  */

/** @addtogroup FONTS
  * @{
  */ 

/** @defgroup FONTS_Exported_Types
  * @{
  */
typedef enum {
	STANG,
	MIKRO,
	GLCD_UTILS
} FONT_TableTypeDef;

typedef struct {
	const char *font_table;
	uint8_t width;
	uint8_t height;
	char start_char;
	char end_char;
	FONT_TableTypeDef table_type;
} sFONT;


extern sFONT Font5x7;
extern sFONT Arial_Black19x20_full;
extern sFONT Liberation_Sans15x21_Numbers;
extern sFONT Verdana11x11_full;

/**
  * @}
  */ 

/**
  * @}
  */ 

/** @defgroup FONTS_Exported_Macros
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup FONTS_Exported_Functions
  * @{
  */ 
/**
  * @}
  */

#ifdef __cplusplus
}
#endif
  
#endif /* __FONTS_H */
 
/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */      

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
