/**
  @page 

  ******************** (C) COPYRIGHT 2014 ARKA *******************
  * @file    Audio_playback_and_record/readme.txt 
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   Description of the Audio_playback_and_record application.
  ******************************************************************************
  @par Example Description 

This example provides a description how to interface Android phone on Gateway Board. 

At the beginning of the main program the HAL_Init() function is called to reset 
all the peripherals.
Then the SystemClock_Config() function is used to configure the systemclock (SYSCLK) 
to run at 168 MHz.

@note The application need to ensure that the SysTick time base is always set to 1 millisecond
to have correct HAL operation.

@par Directory contents 

 
      
@par Hardware and Software environment  



@par How to use it ? 


 * <h3><center>&copy; COPYRIGHT ARKA IMS</center></h3>
 */