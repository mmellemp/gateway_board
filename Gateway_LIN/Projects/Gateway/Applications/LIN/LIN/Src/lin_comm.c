
 /*****************************************************************************
 * File     : lin_comm.c
 * Datum    : 4.15.2015
 * Version  : 1.1
 * Author   : ARKA-IMS
 * \file
 * \brief LIN Faceplate interface service for Gateway Board .
 *
 * This file contains basic functions for the Gateway LIN, with support for all
 * modes, settings and clock speeds.
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "gateway_board.h"
#include "lin_comm.h"
#include "lin_app.h"




void lin_master_task_ID00(void);
void lin_slave_task_ID00(uint8_t* d_buf);

void lin_master_task_ID04(void);
void lin_slave_task_ID04(uint8_t* d_buf);


void lin_master_task_ID34(void);
void lin_slave_task_ID34(uint8_t* d_buf);

uint8_t  lin_data_out_node0x34[8];
uint8_t  lin_data_out_node0x00[7] = {0x40, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
uint8_t  lin_data_out_node0x04[8];
uint8_t linARxBuff1[100];
volatile int tcnt = 0;

/****************** HVAC STUFF TO GO TO ANOTHER FILE **********/
void processHVACMsg(uint8_t* d_buf);
uint8_t identifyHVACKey(uint8_t* d_buf);
void processHVACKeyEvent(uint8_t key);



/**************************************************************/

/****************** ILIN STUFF TO GO TO ANOTHER FILE **********/
void processFaceplateMsg(uint8_t* d_buf);
uint8_t identifyFaceplateKey(uint8_t* d_buf);
void processFaceplateKeyEvent(uint8_t key);


//**********
#define FACEPLATE_ILIN_STATUS_MSG_KEYS_START_BIT	8
#define FACEPLATE_ILIN_STATUS_MSG_KEYS_END_BIT		64			
//**********

// KEY DEFN
#define FP_ILIN_KEY_POWER_TOGGLE	15
#define FP_ILIN_KEY_HOME_TOGGLE		14
#define FP_ILIN_KEY_VOLUME_DOWN		47
#define FP_ILIN_KEY_VOLUME_UP		40

#define GM_AOA_KEY_PRESS_OFFSET		  0x00
#define GM_AOA_KEY_RELEASE_OFFSET     0x30

#define GM_AOA_KEY_PROTOTYPE_HOME_TOGGLE		0x22
#define GM_AOA_KEY_HOME_TOGGLE					0x23
#define GM_AOA_KEY_POWER_TOGGLE					0x31
#define GM_AOA_KEY_VOLUME_DOWN					0x32	
#define GM_AOA_KEY_VOLUME_UP					0x33


//#define NUMBER_OF_LIN_FRAMES_NODE0 8
/**************************************************************/

volatile int testCount = 0 ;	

void vTaskDelay(uint32_t timems)
{
	for(int i=0;i<timems;i++);
}	

void ILINConfig() {
	
	// Configure lin_descriptor
	//- Init LIN data Node 0
	// Object 0
	lin_descript_list_node0[0].l_id = 0x00;
	lin_descript_list_node0[0].l_dlc = 7;
	lin_descript_list_node0[0].l_cmd = PUBLISH;
	lin_descript_list_node0[0].l_pt_data = lin_data_out_node0x00;
	lin_descript_list_node0[0].l_pt_function = lin_slave_task_ID00;
	
	lin_descript_list_node0[1].l_id = 0x04;
	lin_descript_list_node0[1].l_dlc = 7;
	lin_descript_list_node0[1].l_cmd = SUBSCRIBE;
	lin_descript_list_node0[1].l_pt_data = lin_data_out_node0x04; 
	lin_descript_list_node0[1].l_pt_function = lin_slave_task_ID04;
}


void vSbxILINComm(void) {
	
		HAL_Delay(300);

		while(1) {
				testCount++;

				HAL_Delay(20); // 20 ms

				{
						tcnt++;
						HAL_Delay(20);
						memset(lin_data_out_node0x04, 0x00, sizeof(lin_data_out_node0x04));
						lin_master_task_ID04(); // Faceplate Status Message
					  //printf("slave id : %d\n",lin_descript_list_node0[1].l_id);
						// 20 ms
				}
				// 1000 ms (20ms * 50 ) System command
				//tcnt=40;
				if (tcnt == 40) {
					tcnt = 0;
					HAL_Delay(10);
					lin_master_task_ID00(); 
				}

		}
	
	//vTaskDelete(NULL);
	
}

void  lin_master_task_ID00(void) // ILIN Heartbeat
{
lin_send_cmd (0 ,lin_descript_list_node0[0].l_id, lin_descript_list_node0[0].l_dlc );

}

void  lin_master_task_ID04(void) // ILIN Status
{
	lin_send_cmd (0 ,lin_descript_list_node0[1].l_id, lin_descript_list_node0[1].l_dlc );
}


void lin_slave_task_ID04(uint8_t* d_buf) {
	
	//uint8_t buff[50] = {0};
	//sprintf( buff, "\n\rRX %02x %02x %02x %02x %02x %02x %02x %02x", d_buf[0],d_buf[1],d_buf[2],d_buf[3],d_buf[4],d_buf[5], d_buf[6], d_buf[7]);
	////printf (buff);
	// Hack:
	
	if (d_buf[0] != 0x00) {
		// Junk debug data. Realign all the bytes
		d_buf[5] = 0x00;
		for (int i = 0 ; i < 7 ; i++){
			d_buf[i] = d_buf[i+1];	
		}
	}
	else {
		d_buf[4] = 0x00;
	}
	
	//d_buf[4] = 0x00;
	//d_buf[5] = 0x00;
	
	processFaceplateMsg(d_buf);
}

void lin_slave_task_ID00(uint8_t* d_buf) {
	d_buf[0] = 0x40;
	d_buf[1] = 0xFF;
	d_buf[2] = 0xFF;
	d_buf[3] = 0xFF;
	d_buf[4] = 0xFF;
	d_buf[5] = 0xFF;
	d_buf[6] = 0xFF;
}

void processFaceplateMsg(uint8_t* d_buf) {
	static uint8_t prevILINBuff[8] = {0};
			
	if (0 != memcmp ((char*)&prevILINBuff[0]+1, (char*)d_buf+1, 7 /*Hardcoded*/ )) // BUG BUG BUG, will check later
	{
		// Buffers are different. Handle the KEY here.
		// uint8_t buff[80] = {0};
		// sprintf( buff, "\n\rILIN %02x %02x %02x %02x %02x %02x %02x %02x", d_buf[0],d_buf[1],d_buf[2],d_buf[3],d_buf[4],d_buf[5], d_buf[6], d_buf[7]);
		// //printf (buff);
			
		memcpy((char*)&prevILINBuff[0], (char*)d_buf, 8);
			
		uint8_t key = identifyFaceplateKey(d_buf);
		processFaceplateKeyEvent(key);
			
			
	}
}

uint8_t identifyFaceplateKey(uint8_t* controlMsgStatus){
	uint8_t bitPos = FACEPLATE_ILIN_STATUS_MSG_KEYS_START_BIT;
	
	while( bitPos <= FACEPLATE_ILIN_STATUS_MSG_KEYS_END_BIT ) {
	
		if(controlMsgStatus[bitPos/8] & (0x01 << (bitPos%8))) { //Bit is set 
			
			 //uint8_t buff[10] = {0};
			 //sprintf( buff, "\n\rBIT %d", bitPos);
			// //printf (buff);
			return bitPos;
		}
		{
			// Button not found. Initiate release of prev key
		}
		bitPos++;
	}
}

void processFaceplateKeyEvent(uint8_t key) {

	uint8_t buff[40] = {0};
	uint8_t lastKeyDown = 0;
	static uint8_t tCurrentKey = 0x00;

	switch(key) {

		case FP_ILIN_KEY_POWER_TOGGLE:
		sprintf(buff, "\n\rFP_ILIN_KEY_POWER_TOGGLE");
		{
			// tCurrentKey = GM_AOA_KEY_POWER_TOGGLE;
			// aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
			// usb_hid_send_keyboard_down_event(0x74);
			// lastKeyDown = 0x74;
			
			// [2014/07/18 AG] - Mapping to Android Home Key event, through the USB Accessory
			tCurrentKey = GM_AOA_KEY_HOME_TOGGLE;
			//aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
		}
		break;
				
		case FP_ILIN_KEY_HOME_TOGGLE: // Map to prototype Home toggle
		sprintf( buff, "\n\rFP_ILIN_KEY_HOME_TOGGLE");
		{
			tCurrentKey = GM_AOA_KEY_PROTOTYPE_HOME_TOGGLE;
			//aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
		}
		break;
		
		case FP_ILIN_KEY_VOLUME_DOWN:
		sprintf( buff, "\n\rFP_ILIN_KEY_VOLUME_DOWN");
		{
			 //usb_hid_send_keyboard_down_event(HID_KEY_VOL_DOWN);
			 //lastKeyDown = HID_KEY_VOL_DOWN;
			
			tCurrentKey = GM_AOA_KEY_VOLUME_DOWN;
			//aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
		}
		break;
		
		case FP_ILIN_KEY_VOLUME_UP:
		sprintf( buff, "\n\rFP_ILIN_KEY_VOLUME_UP");
		{
			 //usb_hid_send_keyboard_down_event(HID_KEY_VOL_UP);
			 //lastKeyDown = HID_KEY_VOL_UP;
			
			tCurrentKey = GM_AOA_KEY_VOLUME_UP;
			//aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
		}
		break;
		
		default:
			//usb_hid_send_keyboard_up_event(lastKeyDown);
			sprintf( buff, "\n\rRelease Key %x", (tCurrentKey + GM_AOA_KEY_RELEASE_OFFSET));
			tCurrentKey = tCurrentKey + GM_AOA_KEY_RELEASE_OFFSET;
			//aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
			break;
	}

	 //printf (buff);

}



/*************************** end of file ****************************************/

