/**
  ******************************************************************************
  * @file    Audio_playback_and_record/Src/stm32f4xx_it.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


extern UART_HandleTypeDef  linAHandle,linBHandle,dbghandle;

/*Required for handling iAP timeout callbacks*/
BOOL callback_timeout = FALSE;
uint32_t calledTime;
uint32_t timeout = 0;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*             Cortex-M4 Processor Exceptions Handlers                        */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}



/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  //while (1)
  {
		NVIC_SystemReset();
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  //while (1)
  {
		NVIC_SystemReset();
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}


/**
  * @brief  This function handles SysTick Handler. Callback function called after timeout
  * @param  None
  * @retval None
  */
void SysTick_Handler (void)
{
  HAL_IncTick(); 

}


/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles USB-On-The-Go FS/HS global interrupt request.
  * @param  None
  * @retval None
  */
/**
  * @brief  This function handles TIM4 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM4_IRQHandler(void)
{
 // HAL_TIM_IRQHandler(&hTimLed);
}

/**
  * @brief  This function handles USB-On-The-Go FS global interrupt request.
  * @param  None
  * @retval None
  */
void OTG_FS_IRQHandler(void)
{
//  HAL_HCD_IRQHandler(&hhcd);
}
/**
  * @brief  This function handles USB-On-The-Go HS global interrupt requests.
  * @param  None
  * @retval None
  */

void OTG_HS_IRQHandler(void)
{
  //HAL_HCD_IRQHandler(&hhcd);
}



/**
* @brief  This function handles the audio DMA interrupt request.
* @param  None
* @retval None
*/
//void AUDIO_I2Sx_DMAx_IRQHandler(void)
//{
//  //HAL_DMA_IRQHandler(haudio_i2s.hdmatx);
//}

/**
  * @brief  This function handles External 2 interrupt request connected to Volune A.
  * @param  None
  * @retval None
  */
void EXTI2_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}
/**
  * @brief  This function handles External 3 interrupt request connected to Select A.
  * @param  None
  * @retval None
  */
void EXTI3_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}


/*IRQ haneler for debugging*/
void EVAL_COM1_IRQ_Handler(void)
{
	//__HAL_UART_DISABLE_IT(&linBHandle, UART_IT_RXNE);
//	HAL_UART_IRQHandler(&dbghandle);
}

/*IRQ haneler for LINB*/
void EVAL_COM3_IRQ_Handler(void)
{
	//__HAL_UART_DISABLE_IT(&linBHandle, UART_IT_RXNE);
	HAL_UART_IRQHandler(&linBHandle);
}



/*IRQ haneler for LINA*/
//void EVAL_COM4_IRQ_Handler(void)
//{
//	HAL_UART_IRQHandler(&linAHandle);
//}

void EVAL_COM5_IRQ_Handler(void)
{
//  HAL_UART_IRQHandler(& uart_test);
	//buffIndex++; //(buffIndex-1) - Gives the no of bytes received
	//if(buffIndex==20)
	//	 buffIndex=0;
	//HAL_Delay(1);
}

void LINA_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(linAHandle.hdmarx);
}

void LINA_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(linAHandle.hdmatx);
}


/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
