/*
 * IncFile1.h
 *
 * Created: 3/17/2014 2:17:40 PM
 *  Author: Aravind G
 */ 


#ifndef LIN_COMM_H_
#define LIN_COMM_H_
#include "stdint.h"
#include "gateway_board.h"

void vSbxILINComm(void);
void ILINConfig(void);

#define true 1
#define false 0

typedef uint8_t  U8;
typedef uint16_t  U16;
typedef uint32_t  U32;
typedef uint8_t Bool ;

#endif /* LIN_COMM_H_ */