/**
  ******************************************************************************
  * @file    Audio/Audio_playback/Inc/main.h 
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   Header for main.c module
	******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>

#include "Gateway_Board_audio.h"
#include "gateway_board_tuner.h"
#include "gateway_board_keypad_RE.h"
#include "gateway_board_can.h"
#include "gateway_board_lcd.h"
#include "gateway_board_lin.h"
#include "gateway_board.h"
#include "lin_app.h"
#include "lin_comm.h"

typedef enum
{
  APPLICATION_IDLE = 0,  
  APPLICATION_START,    
  APPLICATION_RUNNING,
}
IAP_ApplicationTypeDef;

typedef enum
{
  APP_IDLE = 0,  
  APP_RECEIVE,    
  APP_SEND,
}
APP_RxTxTypeDef;


typedef enum 
{
	Key_NextSong,
	Key_PreviousSong,
	Key_PlayPause,
	Key_Mute,
	Key_Select,
} KeyPressEvents;

typedef enum 
{
	Key_1 = 0,
	Key_2 = 1,
	Key_4 = 2,
	Key_5	= 3,
	Key_7 = 4,
	Key_8 = 5,
} KeyCode;




#define HID_KEY_PLAYPAUSE					0xE8
#define HID_KEY_PREVIOUSSONG			0xEA
#define HID_KEY_NEXTSONG					0xEB
#define HID_KEY_VOL_UP						0x80
#define HID_KEY_VOL_DOWN					0x81
#define HID_KEYPAD_ENTER     			0x58
#define HID_DOWN             			0x51
#define HID_UP               			0x52


void Toggle_Leds(void);
static void OBD_process(void);
static uint8_t prepare_OBD_response(uint8_t *);
//static void extractLinData(uint8_t* in_data, uint8_t* out_data,uint8_t *checksum);
void BSP_CGI_ReceiveComplete_Calback(void);


//                                                                                    
///* Exported macro ------------------------------------------------------------*/
///* Exported functions ------------------------------------------------------- */
//void Error_Handler(void);
#endif /* __MAIN_H */

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
