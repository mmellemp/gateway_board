/**
  ******************************************************************************
  * @file    aoa_class.h
  * @author  Arka
  * @version V0.0
  * @brief   This file contains all the prototypes for the aoa_class.c
  ******************************************************************************
  */ 

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef __USBH_AOA_CORE_H
#define __USBH_AOA_CORE_H

/* Includes ------------------------------------------------------------------*/
#include "usbh_ioreq.h"
#include "usbh_ctlreq.h"
#include "usbh_core.h"
#include "audio_if.h"

/** @addtogroup USBH_LIB
* @{
*/

/** @addtogroup USBH_CLASS
* @{
*/

/** @addtogroup USBH_AOA_CLASS
* @{
*/

/** @defgroup USBH_AOA_CORE
* @brief This file is the Header file for USBH_AOA_CORE.c
* @{
*/ 
/*AOA Class codes*/
#define USB_AOA_CLASS												0xFF /*vendor specific*/
#define USB_AUDIO_CLASS       							0x01
#define USB_SUBCLASS_AUDIOSTREAMING					0x02
#define USB_ACCESSORY_CLASS       					0xFF
#define USB_SUBCLASS_ACCESSORY							0xFF




/**
  * @}
  */ 

/** @defgroup USBH_AOA_CORE_Exported_Types
* @{
*/ 


/**
* @}
*/ 

/** @defgroup USBH_AOA_CORE_Exported_Defines
* @{
*/ 

//AOA 1.0
#define USB_ACCESSORY_VENDOR_ID         		0x18D1
#define USB_ACCESSORY_PRODUCT_ID        		0x2D00
#define USB_ACCESSORY_ADB_PRODUCT_ID    		0x2D01
//AOA 2.0
#define USB_AUDIO_PRODUCT_ID               	0x2D02
#define USB_AUDIO_ADB_PRODUCT_ID           	0x2D03
#define USB_ACCESSORY_AUDIO_PRODUCT_ID     	0x2D04
#define USB_ACCESSORY_AUDIO_ADB_PRODUCT_ID 	0x2D05

#define ACCESSORY_STRING_MANUFACTURER   0
#define ACCESSORY_STRING_MODEL          1
#define ACCESSORY_STRING_DESCRIPTION    2
#define ACCESSORY_STRING_VERSION        3
#define ACCESSORY_STRING_URI            4
#define ACCESSORY_STRING_SERIAL         5

//AOA 1.0
#define ACCESSORY_GET_PROTOCOL          51
#define ACCESSORY_SEND_STRING           52
#define ACCESSORY_START                 53

//AOA 2.0
#define ACCESSORY_REGISTER_HID          54
#define ACCESSORY_UNREGISTER_HID        55
#define ACCESSORY_SET_HID_REPORT_DESC   56
#define ACCESSORY_SEND_HID_EVENT        57
#define ACCESSORY_SET_AUDIO_MODE        58

#define USBH_AOA_DATA_SIZE	64
#define USBH_AOA_AUDIO_DATA_SIZE	256 //176
#define USBH_AOA_NAK_RETRY_LIMIT 5

#define  USB_AOA_CLASS 	0xFF
/**
* @}
*/ 

/** @defgroup USBH_AOA_CORE_Exported_Macros
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_AOA_CORE_Exported_Variables
* @{
*/ 
extern USBH_ClassTypeDef  USBH_aoa;
#define USBH_AOA_CLASS    &USBH_aoa

#define AUDIO_DEFAULT_VOLUME                          85

#define AUDIO_OUT_PACKET                              (uint32_t)(((USBD_AUDIO_FREQ * 2 * 2) /1000)) 
    
/* Number of sub-packets in the audio transfer buffer. You can modify this value but always make sure
  that it is an even number and higher than 3 */
#define AUDIO_OUT_PACKET_NUM                           20
/* Total size of the audio transfer buffer */
#define AUDIO_TOTAL_BUF_SIZE                           ((uint32_t)(AUDIO_OUT_PACKET * AUDIO_OUT_PACKET_NUM))

/* States for AOA Initialize State Machine */
typedef enum
{
  AOA_INIT_SETUP = 0,
  AOA_INIT_GET_PROTOCOL,
	AOA_INIT_SET_AUDIO_MODE,
  AOA_INIT_SEND_MANUFACTURER,
  AOA_INIT_SEND_MODEL,
  AOA_INIT_SEND_DESCRIPTION,
  AOA_INIT_SEND_VERSION,
  AOA_INIT_SEND_URL,
  AOA_INIT_SEND_SERIAL,
  AOA_INIT_STARTACCESSORY,
  AOA_INIT_GET_DEVDESC,
  AOA_INIT_CONFIGURE_ANDROID,
	AOA_INIT_SET_INTERFACE,
	//test
	AOA_INIT_REG_HID,
	AOA_INIT_SEND_HIDREPORT,
	AOA_INIT_SEND_EVENT,
  AOA_INIT_DONE,
  AOA_INIT_FAILED,
}
AOA_InitState;

/* States for AOA State Machine */
typedef enum
{
  AOA_IDLE= 0,
  AOA_SEND_DATA,
  AOA_SEND_DATA_WAIT,
  AOA_RECEIVE_DATA,
  AOA_RECEIVE_DATA_WAIT,
}
AOA_DataStateTypeDef;

typedef enum
{
  AOA_HID_IDLE= 0,
  AOA_HID_SEND_EVENT,
  AOA_HID_SEND_EVENT_WAIT,
}
AOA_HIDEventStateTypeDef;

typedef enum
{
  AOA_IDLE_STATE= 0,
  AOA_TRANSFER_DATA, 
  AOA_ERROR_STATE,  
}
AOA_StateTypeDef;

typedef enum
{
 AUDIO_DATA_IN = 1,  
 AUDIO_DATA_WAIT,
}
AUDIO_ProcessingTypeDef;

typedef enum
{
  AUDIO_OFFSET_NONE = 0,
  AUDIO_OFFSET_HALF,
  AUDIO_OFFSET_FULL,  
  AUDIO_OFFSET_UNKNOWN,    
}
AUDIO_OffsetTypeDef;

typedef struct
{
	//uint8_t	 		buff[AUDIO_TOTAL_BUF_SIZE];
	uint8_t*		buff_ptr;
	uint16_t    rd_ptr;  
  uint16_t    wr_ptr;
}
AUDIO_BuffTypeDef;

typedef struct
{

  uint8_t              Ep;
  uint16_t             EpSize; 
  uint8_t              interface; 
  uint8_t              AltSettings;
  uint8_t              supported;    
  uint8_t              Pipe;  
	//AUDIO_ControlAttributeTypeDef attribute;  
}
AUDIO_STREAMING_IN_HandleTypeDef;

typedef struct
{

  uint8_t              Ep;
  uint16_t             EpSize; 
	uint8_t              Pipe;
	uint8_t							 *pRxData;
	uint32_t						 RxLen;
	uint8_t              supported; 
	uint8_t              Poll;
}
DATA_IN_HandleTypeDef;

typedef struct
{

  uint8_t              Ep;
  uint16_t             EpSize; 
	uint8_t              Pipe; 	
	uint8_t							 *pTxData;
	uint32_t						 TxLen;
	uint8_t              supported; 
}
DATA_OUT_HandleTypeDef;

/* Structure for AOA process */
typedef struct _AOA_Process
{
  DATA_IN_HandleTypeDef						DataIn;
	DATA_OUT_HandleTypeDef					DataOut;

	uint8_t													audioStarted;
	AUDIO_BuffTypeDef								AudioStream;	
  uint8_t             						rd_enable;
  AUDIO_OffsetTypeDef       			offset;
 
	
	AUDIO_STREAMING_IN_HandleTypeDef StreamIn;
	AUDIO_ProcessingTypeDef					 StreamInState;	
	
  AOA_InitState		   							initstate;

	AOA_StateTypeDef          			state;
  AOA_DataStateTypeDef      			data_tx_state;
  AOA_DataStateTypeDef      			data_rx_state;
  AOA_DataStateTypeDef      			data_rw_state;
	
	AOA_HIDEventStateTypeDef				hidEvent_tx_state;
	uint8_t 												hidEvent;
	uint8_t  												dataSent;
	uint8_t  												dataReceived;
	uint8_t  												hidEventSent;
	uint8_t													streamAudio;
	
	USBH_AUDIO_ItfTypeDef 					*fopsAudioInterface;
	uint8_t		   										protocol;
	uint32_t 												audioRecvTimer;
}
AOA_HandleTypeDef;



/**
* @}
*/ 

AOA_StateTypeDef AOA_GetStatus(USBH_HandleTypeDef *phost);



USBH_StatusTypeDef  USBH_AOA_Transmit(USBH_HandleTypeDef *phost, 
                                      uint8_t *pbuff, 
                                      uint32_t length);

USBH_StatusTypeDef  USBH_AOA_Receive(USBH_HandleTypeDef *phost, 
                                     uint8_t *pbuff, 
                                     uint32_t length);

USBH_StatusTypeDef  USBH_AOA_ReceiveAudio(USBH_HandleTypeDef *phost);
uint16_t USBH_AOA_GetLastReceivedDataSize(USBH_HandleTypeDef *phost);
void  USBH_AOA_AUDIO_Sync(USBH_HandleTypeDef *phost, AUDIO_OffsetTypeDef offset);
AOA_InitState AOA_GetInitStatus(USBH_HandleTypeDef *phost);
USBH_StatusTypeDef  USBH_AOA_SendHIDEvent(USBH_HandleTypeDef *phost,uint8_t keyID);
void AOA_StartAudioStreaming(USBH_HandleTypeDef *phost);
void AOA_StopAudioStreaming(USBH_HandleTypeDef *phost);
uint8_t AOA_getAudioStreamingState(USBH_HandleTypeDef *phost);

/**
* @}
*/ 


#endif /* __USBH_AOA_CORE_H */

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

