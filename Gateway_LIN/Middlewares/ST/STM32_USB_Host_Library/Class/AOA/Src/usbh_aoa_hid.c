/**
  ******************************************************************************
  * @file    usbh_aoa_hid.c 
  * @author  Arka Team
  * @version V3.1.0
  * @date    19-June-2014
  * @brief   This file includes the HID related functions
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "usbh_aoa_hid.h"
#include "usbh_aoa.h"

/** @addtogroup USBH_LIB
* @{
*/

/** @addtogroup USBH_CLASS
* @{
*/

/** @addtogroup USBH_MSC_CLASS
* @{
*/

/** @defgroup USBH_AOA_HID 
* @brief    This file includes the mass storage related functions
* @{
*/ 


/** @defgroup USBH_AOA_HID_Private_TypesDefinitions
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_AOA_HID_Private_Defines
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_AOA_HID_Private_Macros
* @{
*/ 
/**
* @}
*/ 


/** @defgroup USBH_AOA_HID_Private_Variables
* @{
*/ 
uint8_t AOA_HID_ReportDescriptor[] = {
        
									0x05, 0x01,	/* Usage Page (Generic Desktop)      */
									0x09, 0x06,	/* Usage (Keyboard)                  */
									0xA1, 0x01,	/* Collection (Application)          */
									0x05, 0x07,	/* Usage Page (Keyboard)             */
									0x19, 224,	/* Usage Minimum (224)               */
									0x29, 231,	/* Usage Maximum (231)               */
									0x15, 0x00,	/* Logical Minimum (0)               */
									0x25, 0x01,	/* Logical Maximum (1)               */
									0x75, 0x01,	/* Report Size (1)                   */
									0x95, 0x08,	/* Report Count (8)                  */
									0x81, 0x02,	/* Input (Data, Variable, Absolute)  */
									0x81, 0x01,	/* Input (Constant)                  */
									0x19, 0x00,	/* Usage Minimum (0)                 */
									0x29, 255,	/* Usage Maximum (101)               */
									0x15, 0x00,	/* Logical Minimum (0)               */
									0x25, 255,	/* Logical Maximum (101)             */
									0x75, 0x08,	/* Report Size (8)                   */
									0x95, 0x06,	/* Report Count (6)                  */
									0x81, 0x00,	/* Input (Data, Array)               */
									0x05, 0x08,	/* Usage Page (LED)                  */
									0x19, 0x01,	/* Usage Minimum (1)                 */
									0x29, 0x05,	/* Usage Maximum (5)                 */
									0x15, 0x00,	/* Logical Minimum (0)               */
									0x25, 0x01,	/* Logical Maximum (1)               */
									0x75, 0x01,	/* Report Size (1)                   */
									0x95, 0x05,	/* Report Count (5)                  */
									0x91, 0x02,	/* Output (Data, Variable, Absolute) */
									0x95, 0x03,	/* Report Count (3)                  */
									0x91, 0x01,	/* Output (Constant)                 */
									0xC0	/* End Collection                    */
								};
								


#define AOA_HID_KBD_REPORT_SIZE		8						
static uint8_t AOA_HID_kbd_report[AOA_HID_KBD_REPORT_SIZE];
/**
* @}
*/ 


/** @defgroup USBH_AOA_HID_Private_FunctionPrototypes
* @{
*/

/**
* @}
*/ 


/** @defgroup USBH_AOA_HID_Exported_Variables
* @{
*/ 
/**
* @}
*/ 


/** @defgroup USBH_AOA_HID_Private_Functions
* @{
*/ 

/**
  * @brief  USBH_AOA_HID_Register_HID 
  *         The function to Register_HID.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_HID_Register_HID(USBH_HandleTypeDef *phost, uint16_t id)
{
	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_REGISTER_HID; //54
	phost->Control.setup.b.wValue.w = id;
	phost->Control.setup.b.wIndex.w = sizeof(AOA_HID_ReportDescriptor);
	phost->Control.setup.b.wLength.w = 0;

	return USBH_CtlReq(phost, 0 , 0); 
}


/**
  * @brief  USBH_AOA_HID_Unregister_HID 
  *         The function to Unegister_HID.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_HID_Unregister_HID(USBH_HandleTypeDef *phost, uint16_t id)
{
	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_UNREGISTER_HID; //55
	phost->Control.setup.b.wValue.w = id;
	phost->Control.setup.b.wIndex.w = 0;
	phost->Control.setup.b.wLength.w = 0;

	return USBH_CtlReq(phost, 0 , 0); 
}

/**
  * @brief  USBH_AOA_HID_SendHIDReportDesc 
  *         The function to Unegister_HID.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_HID_SendHID_ReportDesc(USBH_HandleTypeDef *phost, uint16_t id)
{
	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_SET_HID_REPORT_DESC; //56
	phost->Control.setup.b.wValue.w = id;
	phost->Control.setup.b.wIndex.w = 0;
	phost->Control.setup.b.wLength.w = sizeof(AOA_HID_ReportDescriptor);

	return USBH_CtlReq(phost, AOA_HID_ReportDescriptor , sizeof(AOA_HID_ReportDescriptor)); 
}
	
/**
  * @brief  USBH_AOA_HID_SendHID_Event 
  *         The function to Unegister_HID.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_HID_SendHID_Event(USBH_HandleTypeDef *phost, uint16_t id,uint8_t keyID)
{
	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_SEND_HID_EVENT;
	phost->Control.setup.b.wValue.w = id;
	phost->Control.setup.b.wIndex.w = 0;
	phost->Control.setup.b.wLength.w = AOA_HID_KBD_REPORT_SIZE;
	
	memset(AOA_HID_kbd_report,0,AOA_HID_KBD_REPORT_SIZE);
	
	AOA_HID_kbd_report[2] = keyID;
	
	return USBH_CtlReq(phost, AOA_HID_kbd_report , AOA_HID_KBD_REPORT_SIZE); 
}


/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/

/**
* @}
*/ 

/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/



