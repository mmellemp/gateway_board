/**
  ******************************************************************************
  * @file    iAP2ControlSession.h
  * @author  Arka
  * @version V0.0
  * @brief   This file contains iAP c.control session message structure
  ******************************************************************************
  */ 

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef __iAP2_CONTROL_SESSION_H
#define __iAP2_CONTROL_SESSION_H

/* Includes  -----------------------------------------------------------------*/
#include <stdint.h>
#include "iAP2Link.h"

typedef const char* utf8_t;

/*Enum for iAP2 session Types*/
enum
{
	IAP_CONTROL_SESSION,
	IAP_FILE_TRANSFER_SESSION,
	IAP_EXTERNAL_ACCESSORY_SESSION
};

#define IAP_CONTROL_SESSION_VERSION					1
#define IAP_EXTERNAL_ACCESSORY_SESSION_VER	1

#define IAP_SESSION1												1
#define IAP_SESSION2												2


#pragma pack(push, 1)
/*Control session message parameter structure*/
typedef struct
{
	uint16_t param_length;
	uint16_t param_id;
	uint8_t *param_data;
}iAPControlSessionMsgParam_st;

/*Control session message structure*/
typedef struct
{
	uint16_t SOM;
	uint16_t msg_length;
	uint16_t msg_id;
	iAPControlSessionMsgParam_st *msg_parameter;
}iAPControlSessionMsg_st;
#pragma pack(pop)

static iAPControlSessionMsg_st* parseSessionMessage(uint8_t* data, uint8_t data_length);

#define CTRL_SOM 0x4040

/*Control session Message IDs other than Identification and Authentication*/
#define IAP_MESSAGE_USBDEVMODE_AUDIO_INFO						  0xDA01											/*From Device*/
#define IAP_MESSAGE_START_USB_DEVICE_MODE_AUDIO				0xDA00		
#define IAP_MESSAGE_STOP_USB_DEVICE_MODE_AUDIO				0xDA02

#define IAP_MESSAGE_START_EXTERNAL_ACCESSORY_SESSION	0xEA00
#define IAP_MESSAGE_STOP_EXTERNAL_ACCESSORY_SESSION   0xEA01
#define IAP_MESSAGE_EXTERNAL_ACCESSORY_SESSION_STAT		0xEA03

#define IAP_MESSAGE_REQUEST_APP_LAUNCH								0xEA02

#define IAP_MESSAGE_START_HID													0x6800
#define IAP_MESSAGE_DEVICE_HID_REPORT									0x6801
#define IAP_MESSAGE_ACCESSORY_HID_REPORT							0x6802
#define IAP_MESSAGE_STOP_HID													0x6803

/**
  * @}
  */ 

/** @defgroup USBH_IAP_CORE_Exported_Types
* @{
*/ 


/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Defines
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Macros
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Variables
* @{
*/ 




/**
* @}
*/ 


#endif /* __iAP2_CONTROL_SESSION_H */

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

