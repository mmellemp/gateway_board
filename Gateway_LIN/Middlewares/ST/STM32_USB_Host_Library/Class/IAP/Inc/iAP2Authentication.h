/**
  ******************************************************************************
  * @file    iAP2AccesseoryAuthentication.h
  * @author  Arka
  * @version V0.0
  * @brief   This file contains all the prototypes for the iAP2AccesseoryAuthentication.c
  ******************************************************************************
  */ 

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef __iAP2_ACCESSORY_AUTHENTICATION_H
#define __iAP2_ACCESSORY_AUTHENTICATION_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "gateway_board.h"
#include "iAP2Session.h"

#define CP_ADDRESS															0x20
 
//Coprocessor registers MAP
#define CP_DEVICE_VERSION 											0x00
#define CP_FIRMWARE_VERSION 										0x01
#define CP_AUTHENTICATION_PROTOCOL_MAJOR_VER		0x02
#define CP_AUTHENTICATION_PROTOCOL_MINOR_VER		0x03
#define CP_Device_ID														0x04
#define CP_ERROR_CODE														0x05
#define CP_AUTH_CONROL_AND_STATUS								0x10
#define CP_SIGNATURE_DATA_LENGTH								0x11
#define CP_SIGNATURE_DATA												0x12
#define CP_CHALLENGE_DATA_LENGTH								0x20
#define CP_CHALLENGE_DATA												0x21
#define CP_ACCESSORY_CERTIFICATE_DATA_LENGTH		0x30
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE1			0x31
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE2			0x32
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE3			0x33
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE4			0x34
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE5			0x35
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE6			0x36
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE7			0x37
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE8			0x38
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE9			0x39
#define CP_ACCESSORY_CERTIFICATE_DATA_PAGE10		0x3A
#define CP_SELF_TEST_CONTROL_STATUS							0x40
#define CP_SYSTEM_EVENT_COUNTER									0x4D

#define CP_DEV_VERSION2_0B											0x03
#define CP_DEV_VERSION2_0C											0x05

//Related to control session layer
//Message IDs of Accessory Authentication control session messages
#define IAP_MESSAGE_AUTHENTICATION_REQUEST_CERTIFICATE				0xAA00							//from device
#define IAP_MESSAGE_AUTHENTICATION_CERTIFICATE								0xAA01
#define IAP_MESSAGE_AUTHENTICATION_REQUEST_CHALLENGE_RESPONSE	0xAA02							//from device
#define IAP_MESSAGE_AUTHENTICATION_CHALLENGE_RESPONSE					0xAA03
#define IAP_MESSAGE_AUTHENTICATION_FAILED											0xAA04							//from device
#define IAP_MESSAGE_AUTHENTICATION_SUCCEEDED									0xAA05							//from device

/*enums------------------------------------------------------*/
//enum for error codes
enum
{
	NO_ERROR = 0,
	INVALID_REG_FOR_READ,
	INVALID_REG_FOR_WRITE,
	INVALID_SIGNATURE_LENGTH,
	INVALID_CHALLENGE_LENGTH,
	INVALID_CERTIFICATE_LENGTH,
	INTERNAL_PROCESS_ERROR_DURING_SIGNATURE_GENERATION,
	INTERNAL_PROCESS_ERROR_DURING_CHALLENGE_GENERATION,
	INTERNAL_PROCESS_ERROR_DURING_SIGNATURE_VERIFICATION,
	INTERNAL_PROCESS_ERROR_DURING_CERTIFICATE_VALIDATION,
	INTERNAL_PROCESS_CONTROL,
	PROCESS_CONTROL_OUT_OF_SEQUENCE
};

//enum for Authentication status
enum
{
	NO_VALID_RESULT=0,
	ACCESSORY_SIGN_GENERATED_OK,
	CHALLENGE_GENERATED_OK,
	DEVICE_SIGN_VERIFIED_OK,
	DEVICE_CERT_VALIDATED_OK
};

//general enum for function success or error
enum
{
	CP_ERROR = 0,
	CP_OK
};

uint8_t iAP_CP_CommInit(void);
uint8_t* iAP_prepareAuthenticationCertificate(uint16_t* session_length);
uint8_t* iAP_respondToChallengeRequest(uint16_t challenge_length,uint8_t *challenge_data, uint16_t* session_length);


#endif /* __iAP2_ACCESSORY_AUTHENTICATION_H */


/************************ (C) COPYRIGHT ARKA *****END OF FILE****/

