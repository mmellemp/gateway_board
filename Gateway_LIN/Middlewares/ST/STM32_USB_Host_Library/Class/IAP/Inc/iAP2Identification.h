/**
  ******************************************************************************
  * @file    iAP2Identification.h
  * @author  Arka
  * @version V0.0
  * @brief   This file contains all the prototypes for the iAP2AccessoryIdentification.c
  ******************************************************************************
  */ 

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef __iAP2_IDENTIFICATION_H
#define __iAP2_IDENTIFICATION_H

/* Includes ------------------------------------------------------------------*/

#include "iAP2Session.h"

/** @addtogroup USBH_LIB
* @{
*/

/** @addtogroup USBH_CLASS
* @{
*/

/** @addtogroup USBH_IAP_CLASS
* @{
*/

/** @defgroup USBH_IAP_CORE
* @brief This file is the Header file for USBH_IAP_CORE.c
* @{
*/ 

/*Defines-------------------------------------------------------------*/
//Message IDs of Accessory Identification control session messages
#define IAP_MESSAGE_IDENTIFICATION_START		 				0x1D00
#define IAP_MESSAGE_IDENTIFICATION_INFO							0x1D01
#define IAP_MESSAGE_IDENTIFICATION_ACCEPTED					0x1D02
#define IAP_MESSAGE_IDENTIFICATION_REJECTED					0x1D03
#define IAP_MESSAGE_IDENTIFICATION_CANCEL						0x1D05
#define IAP_MESSAGE_IDENTIFICATION_INFO_UPDATE			0x1D06


/*Transport Parameters: Groups
*No multiple sub parameters*/
#define IAP_TRANSPORT_SERIAL_ID											"1"
#define IAP_TRANSPORT_SERIAL_NAME										"SERIAL"

#define IAP_TRANSPORT_USBDEV_ID											"1"
#define IAP_TRANSPORT_USBDEV_NAME										"USBDevice"
//supported audio sample rate set in iap_accessory_config

#define IAP_TRANSPORT_USBHOST_ID										"1"
#define IAP_TRANSPORT_USBHOST_NAME									"USBHost"
#define IAP_TRANSPORT_USBHOST_CARPLAY_INUM					"1"

#define IAP_TRANSPORT_BLUETOOTH_ID									"1"
#define IAP_TRANSPORT_BLUETOOTH_NAME								"Bluetooth"
/**/

#define IAP_HID_KEYBOARD_ID													"1"
#define IAP_HID_KEYBOARD_NAME												"Keyboard" 
#define IAP_HID_KEYBOARD_FUNCTION										HID_KEYBOARD

#define IAP_HID_HEADPHONE_ID												"2"
#define IAP_HID_MEDIAPLAYBACK_NAME									"Headphone" 
#define IAP_HID_MEDIAPLAYBACK_FUNCTION							HID_HEADPHONE

#define IAP_VEHICLE_INFO_ID													"1"
//Vehicle name and engine type set in config


/*Enums--------------------------------------------------------------------------------------*/
/*enum for HID functions*/
enum
{
	HID_KEYBOARD=0,
	HID_MEDIA_PLAYBACK_REMOTE,
	HID_ASSISTIVE_TOUCH_POINTER,
	HID_STANDARD_GAMEPAD,
	HID_EXTENDED_GAMEPAD_FORM_FITTING,
	HID_EXTENDED_GAMEPAD_NON_FOEM_FITTING,
	HID_ASSISTIVE_SWITCH_CONTROL,
	HID_HEADPHONE
};

enum
{
	USBDEV_TRANSPORT_COMP_ID=0,
	USBDEV_TRANSPORT_COMP_NAME,
	USBDEV_TRANSPORT_COMP_CONNECTION,
	USBDEV_TRANSPORT_COMP_SUPPORTED_AUDIO_RATE
};

/*enum for Identification information message parameters id*/
typedef enum
{
	PARAM_NAME = 0,
	PARAM_MODELID,
	PARAM_MANUFACTURER,
	PARAM_SERIAL_NUM,
	PARAM_FIRWARE_VERSION,
	PARAM_HARDWARE_VERSION,
	PARAM_MESSAGES_SENT_BY_ACCESSORY,
	PARAM_MESSAGES_RECEIVED_FROM_DEVICE,
	PARAM_POWER_PROVIDING_CAPABILITY,
	PARAM_MAXIMUM_CURRENT_DRAWN_FROM_DEVICE,
	PARAM_SUPPORTED_EXTERNAL_ACCESSORY_PROTOCOL,
	PARAM_APP_MATCH_TEAM_ID,
	PARAM_CURRENT_LANGUAGE,
	PARAM_SUPPORTED_LANGUAGE,
	PARAM_SERIAL_TRANSPORT_COMPONENT,
	PARAM_USB_DEVICE_TRANSPORT_COMPONENT,							
	PARAM_USB_HOST_TRANSPORT_COMPONENT,
	PARAM_BLUETOOTH_TRANSPORT_COMPONENT,
	PARAM_IAP2_HID_COMPONENT,
	PARAM_VEHICLE_INFORMATION_COMPONENT = 20,
	PARAM_VEHICLE_STATUS_COMPONENT = 21,
	PARAM_LOCATION_INFORMATION_COMPONENT = 22,
	PARAM_USB_HOST_HID_COMPONENT = 23
}typedef_iap_identification_param_id;

typedef struct
{
	utf8_t param_name;
	utf8_t param_model_id;
	utf8_t param_manufacturer;
	utf8_t param_serial_num;
	utf8_t param_firmware_version;
	utf8_t param_hardware_version;
	utf8_t param_current_language;				//parameter id=6. Different from param id for Identification information 
}info_updte_param_st;

/*Function declaration-----------------------------------------------------------------------*/
uint8_t* iAP_PrepareIdentificationInformation(uint16_t* session_length);
/**
  * @}
  */ 

/** @defgroup USBH_IAP_CORE_Exported_Types
* @{
*/ 


/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Defines
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Macros
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Variables
* @{
*/ 




/**
* @}
*/ 


#endif /* __iAP2_IDENTIFICATION_H */

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 
/************************ (C) COPYRIGHT ARKA *****END OF FILE****/

