/**
  ******************************************************************************
  * @file    iAP2AccessoryIdentification.c
  * @author  Arka Team
  * @version V0.0
  * @date    JAN-2015
  * @brief   This file handles accessory identification.
  ******************************************************************************
  */
	
	/* Includes */
	#include "iAP2Authentication.h"
	
	#define __chArrToInt(addr) (*(addr)<<8) | (*(addr+1))
	
	char *challenge_resp_data;
	uint8_t certificate_len[2];
	
	/**
  * @brief  iAP_CP_CommInit
  *         Initialise communication between accessory processor and coprocessor
	*					Determine coprocessor device version and determine I2C speed
	*					Run self test to verirify presence of certificate data
	*					Create buffer for certificate data
  * @param  None
  * @retval Status
  */
	uint8_t iAP_CP_CommInit(void)
	{	
		I2C_speed = I2C_SPEED_LOW;
		MFI_CP_IO_Init();
		if(MFI_CP_IO_IsDeviceReady(CP_ADDRESS,3000) != HAL_OK)
			return CP_ERROR;
		
		if(MFI_CP_IO_Read(CP_ADDRESS,CP_DEVICE_VERSION) == CP_DEV_VERSION2_0C)
		{
			I2C_speed = I2C_SPEED_HIGH;
			MFI_CP_IO_Init();
			if(MFI_CP_IO_IsDeviceReady(CP_ADDRESS,3000) != HAL_OK)
				return CP_ERROR;
		}
		
		uint8_t selfTestStart = 0x01;
		MFI_CP_IO_WriteData(CP_ADDRESS,CP_SELF_TEST_CONTROL_STATUS,&selfTestStart,1);
		if((MFI_CP_IO_Read(CP_ADDRESS,CP_SELF_TEST_CONTROL_STATUS) & 0xC0) == 0)						//certificate not found in memory
			return CP_ERROR;
		
		MFI_CP_IO_ReadData(CP_ADDRESS,CP_ACCESSORY_CERTIFICATE_DATA_LENGTH,&certificate_len[0],2);
		return CP_OK;
	}	
	
	/**
  * @brief  prepareAuthenticationCertificate
  *         prepare Authentication Certificate data packet to be sent to over control session layer
  * @param  session_length reference to be updated;
  * @retval pointer to prepared authentication certificate session msg
  */
	uint8_t* iAP_prepareAuthenticationCertificate(uint16_t* session_length)
	{
		//Fill certificate data buffer
		uint16_t certificate_len16 = __chArrToInt(certificate_len);
		uint16_t msg_len = certificate_len16+10;
		
		*session_length = msg_len;
		uint8_t* authenticationCertificate_msg = (uint8_t*)malloc(msg_len);
		uint8_t* msg_baseAddr = authenticationCertificate_msg;
		
		*authenticationCertificate_msg = 0x40;				//SOM1
		*(++authenticationCertificate_msg) = 0x40;		//SOM2
		
		*(++authenticationCertificate_msg) = (msg_len >> 8) & 0x00FF;				//Message length
		*(++authenticationCertificate_msg) = msg_len & 0x00FF;
		
		*(++authenticationCertificate_msg) = (IAP_MESSAGE_AUTHENTICATION_CERTIFICATE >> 8) & 0x00FF;			//Message ID
		*(++authenticationCertificate_msg) = IAP_MESSAGE_AUTHENTICATION_CERTIFICATE & 0x00FF;
		
		msg_len -= 6;
		*(++authenticationCertificate_msg) = (msg_len >> 8) & 0x00FF;				//Message parameter length
		*(++authenticationCertificate_msg) = msg_len & 0x00FF;
		
		*(++authenticationCertificate_msg) = 0;															//Message parameter ID
		*(++authenticationCertificate_msg) = 0;
		
		++authenticationCertificate_msg;
		
		uint8_t num_pages = certificate_len16/128+1;
		uint8_t num_bytes_last_page = certificate_len16 % 128;
		uint8_t pageAddress = CP_ACCESSORY_CERTIFICATE_DATA_PAGE1;
		
		while(num_pages--)
		{
			if(num_pages == 0)
				MFI_CP_IO_ReadData(CP_ADDRESS,pageAddress,authenticationCertificate_msg,num_bytes_last_page);
			else
				MFI_CP_IO_ReadData(CP_ADDRESS,pageAddress,authenticationCertificate_msg,128);
			authenticationCertificate_msg += 128;
			pageAddress++;
		}	
		authenticationCertificate_msg = msg_baseAddr;
		return authenticationCertificate_msg;
	}
	
	/**
  * @brief  iAP_respondToChallengeRequest
  *         Write challenge length and challenge data to CP for it to compute challenge response
	*					Start signature genration process and get challenge response and length when done
	*					Create authengication response packet
  * @param  challenge_length: Length of challenge received from device
	*	@param	challenge_data: pointer to challenge data received from device
	* @param	session_length: session length reference to be updated
  * @retval None
  */
	uint8_t* iAP_respondToChallengeRequest(uint16_t challenge_length,uint8_t *challenge_data, uint16_t* session_length)
	{
		uint8_t auth_Control = 0x01;      
		uint8_t auth_Status = 0;
		auth_Status |= (ACCESSORY_SIGN_GENERATED_OK<<4);
		
		uint16_t challenge_resp_len16;
		uint8_t challenge_len[2];
		uint8_t challenge_resp_len[2];
		
		challenge_len[0] = (challenge_length >> 8) & 0x00FF;
		challenge_len[1] = (challenge_length) & 0x00FF;
		
		MFI_CP_IO_WriteData(CP_ADDRESS,CP_CHALLENGE_DATA_LENGTH,challenge_len,2);
		MFI_CP_IO_WriteData(CP_ADDRESS,CP_CHALLENGE_DATA,challenge_data,challenge_length);	
		MFI_CP_IO_WriteData(CP_ADDRESS,CP_AUTH_CONROL_AND_STATUS,&auth_Control,1);																						/*start new signature process*/
		
		while((MFI_CP_IO_Read(CP_ADDRESS,CP_AUTH_CONROL_AND_STATUS) & auth_Status) != auth_Status);														/*Wait till signature get generated*/
		MFI_CP_IO_ReadData(CP_ADDRESS,CP_SIGNATURE_DATA_LENGTH,challenge_resp_len,2);													
		challenge_resp_len16 = __chArrToInt(challenge_resp_len);
		
		uint8_t* challenge_resp_data = (uint8_t*)malloc(challenge_resp_len16+10);
		uint8_t* msg_baseAddr = challenge_resp_data;
		
		*challenge_resp_data = 0x40;				//SOM1
		*(++challenge_resp_data) = 0x40;		//SOM2
		
		*(++challenge_resp_data) = ((challenge_resp_len16+10) >> 8) & 0x00FF;				//Message length
		*(++challenge_resp_data) = (challenge_resp_len16+10) & 0x00FF;
		
		*(++challenge_resp_data) = (IAP_MESSAGE_AUTHENTICATION_CHALLENGE_RESPONSE >> 8) & 0x00FF;			//Message ID
		*(++challenge_resp_data) = IAP_MESSAGE_AUTHENTICATION_CHALLENGE_RESPONSE & 0x00FF;
		
		*(++challenge_resp_data) = ((challenge_resp_len16+4) >> 8) & 0x00FF;				//Message parameter length
		*(++challenge_resp_data) = (challenge_resp_len16+4) & 0x00FF;
		
		*(++challenge_resp_data) = 0;															//Message parameter ID
		*(++challenge_resp_data) = 0;
		
		++challenge_resp_data;
		
		MFI_CP_IO_ReadData(CP_ADDRESS,CP_SIGNATURE_DATA,challenge_resp_data,challenge_resp_len16);
		
		*session_length = challenge_resp_len16+10;
		
		return msg_baseAddr;
	}
	
	/************************ (C) COPYRIGHT ARKA *****END OF FILE****/