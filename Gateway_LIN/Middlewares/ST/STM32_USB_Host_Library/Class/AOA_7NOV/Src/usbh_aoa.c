/**
  ******************************************************************************
  * @file    aoa_class.c
  * @author  Arka Team
  * @version V0.0
  * @date    Aug-2014
  * @brief   This file is the AOA layer Handlers for USB Host AOA class.
  ******************************************************************************
  */

/* Includes */
#include "usbh_aoa.h"
#include "gateway_board.h"

extern USBH_AUDIO_ItfTypeDef  USBH_AUDIO_fops;

uint8_t audioBuff[256];

uint8_t ccR = 0;
uint8_t ccS = 0;

/** @addtogroup USBH_LIB
* @{
*/

/** @addtogroup USBH_CLASS
* @{
*/

/** @addtogroup USBH_AOA_CLASS
* @{
*/


/** @defgroup USBH_AOA_CORE 
* @brief    This file includes AOA Layer Handlers for USB Host AOA class.
* @{
*/ 

/** @defgroup USBH_AOA_CORE_Private_TypesDefinitions
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_AOA_CORE_Private_Defines
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_AOA_CORE_Private_Macros
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_AOA_CORE_Private_Variables
* @{
*/
/**
* @}
*/ 


/** @defgroup USBH_AOA_CORE_Private_FunctionPrototypes
* @{
*/ 

static USBH_StatusTypeDef USBH_AOA_InterfaceInit(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_AOA_InterfaceDeInit(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_AOA_Process (USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_AOA_ClassRequest (USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_AOA_SOFProcess(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef AOA_startAccessory (USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef AOA_getProtocol(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef AOA_configAndroid (USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_AOA_FindAudioStreamingIN(USBH_HandleTypeDef *phost);
	
static USBH_StatusTypeDef USBH_AOA_FindDataINInterface(USBH_HandleTypeDef *phost);

static void AOA_ProcessTransmission(USBH_HandleTypeDef *phost);

static void AOA_ProcessReception(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_AOA_RdWrProcess(USBH_HandleTypeDef *phost);


static void AOA_ProcessAudioStreamIn(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef AOA_AudioStreamInit(USBH_HandleTypeDef *phost);

void  USBH_AOA_AUDIO_Sync(USBH_HandleTypeDef *phost, AUDIO_OffsetTypeDef offset);

USBH_ClassTypeDef  USBH_aoa = 
{
  "AOA",
  USB_AOA_CLASS,
  USBH_AOA_InterfaceInit,
	USBH_AOA_InterfaceDeInit,
  USBH_AOA_ClassRequest,
  USBH_AOA_Process,
	USBH_AOA_SOFProcess,
	NULL,
};

AOA_HandleTypeDef AOA_Handler;
/** @defgroup USBH_AOA_CORE_Private_Functions
* @{
*/ 

/**
  * @brief  USBH_AOA_InterfaceInit 
  *         The function init the AOA class.
  * @param  phost: Host handle
  * @retval USBH Status
  */

USBH_StatusTypeDef USBH_AOA_InterfaceInit (USBH_HandleTypeDef *phost)
{	
  USBH_StatusTypeDef status = USBH_FAIL;
  
  AOA_HandleTypeDef *AOA_Handle;
	//USBH_dbgLog ("Inside AOA InterfaceInit ");

	phost->pActiveClass->pData = &AOA_Handler;//(AOA_HandleTypeDef *)USBH_malloc(sizeof(AOA_HandleTypeDef));
	AOA_Handle =  phost->pActiveClass->pData; 
	

	AOA_Handle->initstate = AOA_INIT_SETUP;
	AOA_Handle->state     = AOA_IDLE_STATE;
	AOA_Handle->data_rx_state = AOA_IDLE;
	AOA_Handle->data_tx_state = AOA_IDLE;
	AOA_Handle->data_rw_state = AOA_IDLE;
	
	status = USBH_OK;
	return status;

}

/**
  * @brief  USBH_AOA_InterfaceDeInit 
  *         The function DeInit the Pipes used for the AOA class.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_InterfaceDeInit (USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	
	/* close bulk transfer pipe */
	if (AOA_Handle->DataIn.Pipe)
	{
			USBH_LL_ClosePipe(phost, AOA_Handle->DataIn.Pipe);
			USBH_FreePipe  (phost, AOA_Handle->DataIn.Pipe);
	    AOA_Handle->DataIn.Pipe = 0;     /* Reset the Channel as Free */
	}
	if (AOA_Handle->DataOut.Pipe)
	{
	    USBH_LL_ClosePipe(phost, AOA_Handle->DataOut.Pipe);
	    USBH_FreePipe  (phost, AOA_Handle->DataOut.Pipe);
	    AOA_Handle->DataOut.Pipe = 0;     /* Reset the Channel as Free */
	}
  if( AOA_Handle->StreamIn.Pipe != 0x00)
  {   
    USBH_ClosePipe(phost,  AOA_Handle->StreamIn.Pipe);
    USBH_FreePipe  (phost,  AOA_Handle->StreamIn.Pipe);
    AOA_Handle->StreamIn.Pipe = 0;     /* Reset the pipe as Free */  
  }
	if(phost->pActiveClass->pData)
  {
		USBH_free (phost->pActiveClass->pData);
    phost->pActiveClass->pData = 0;
  }
  return USBH_OK;
}

/**
* @}
*/ 
/**
  * @brief  AOA_getProtocol 
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef AOA_getProtocol ( USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;

	phost->Control.setup.b.bmRequestType = USB_D2H | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_GET_PROTOCOL;
	phost->Control.setup.b.wValue.w = 0;
	phost->Control.setup.b.wIndex.w = 0;
	phost->Control.setup.b.wLength.w = 2;

	return USBH_CtlReq(phost, &AOA_Handle->protocol , 2);
}

/**
* @}
*/ 
/**
  * @brief  AOA_getProtocol 
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef AOA_setAudioMode ( USBH_HandleTypeDef *phost)
{
	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_SET_AUDIO_MODE;
	phost->Control.setup.b.wValue.w = 1;
	phost->Control.setup.b.wIndex.w = 0;
	phost->Control.setup.b.wLength.w = 0;

	return USBH_CtlReq(phost, 0 , 0);
}
/**
* @}
*/ 
/**
  * @brief  AOA_sendString
  *         Send identifying string information to the Android device.
  * @param  phost: Selected device
  * @param  index: String ID
  * @param  buff: Identifying string
  * @retval USBH_StatusTypeDef
  */
static USBH_StatusTypeDef AOA_sendString ( USBH_HandleTypeDef *phost, uint16_t index, uint8_t* buff)
{
	uint16_t length;
	length = (uint16_t)strlen((const char*)buff)+1;

	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_SEND_STRING;
	phost->Control.setup.b.wValue.w = 0;
	phost->Control.setup.b.wIndex.w = index;
	phost->Control.setup.b.wLength.w = length;

	return USBH_CtlReq(phost, buff , length );
}

/**
* @}
*/ 
/**
  * @brief  AOA_startAccessory 
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef AOA_startAccessory ( USBH_HandleTypeDef *phost)
{
	phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_TYPE_VENDOR | USB_REQ_RECIPIENT_DEVICE;
	phost->Control.setup.b.bRequest = ACCESSORY_START;
	phost->Control.setup.b.wValue.w = 0;
	phost->Control.setup.b.wIndex.w = 0;
	phost->Control.setup.b.wLength.w = 0;
	return USBH_CtlReq(phost, 0 , 0);
}
/**
* @}
*/

/**
  * @brief  Find AOA DATA IN interface
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_AOA_FindDataINInterface(USBH_HandleTypeDef *phost)
{
	uint8_t interface,endpoint, res;
  USBH_StatusTypeDef status = USBH_FAIL ;
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	
	/* Look For DATA IN interface and Endpints */
  res = 0;
  for (interface = 0;  interface < USBH_MAX_NUM_INTERFACES ; interface ++ )
  {
    if((phost->device.CfgDesc.Itf_Desc[interface].bInterfaceClass == USB_ACCESSORY_CLASS)&&
       (phost->device.CfgDesc.Itf_Desc[interface].bInterfaceSubClass == USB_SUBCLASS_ACCESSORY))
    {
      for (endpoint = 0;  endpoint < USBH_MAX_NUM_ENDPOINTS ; endpoint ++ )
      {
				if((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress & 0x80)&&
					 (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize > 0)&&
					 ((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bmAttributes & USBH_EP_BULK) == USBH_EP_BULK))
				{
					AOA_Handle->DataIn.Ep = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress;
					AOA_Handle->DataIn.EpSize = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize;
					AOA_Handle->DataIn.supported = 1; 
					res++;
				}
			}
    }
  } 
  
  if(res > 0)
  {  
     status = USBH_OK;
  }
  
  return status;
}


/**
  * @brief  Find AOA DATA OUT interface
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_AOA_FindDataOUTInterface(USBH_HandleTypeDef *phost)
{
	uint8_t interface,endpoint, res;
  USBH_StatusTypeDef status = USBH_FAIL ;
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	
	/* Look For DATA OUT interface and Endpints */
  res = 0;
  for (interface = 0;  interface < USBH_MAX_NUM_INTERFACES ; interface ++ )
  {
    if((phost->device.CfgDesc.Itf_Desc[interface].bInterfaceClass == USB_ACCESSORY_CLASS)&&
       (phost->device.CfgDesc.Itf_Desc[interface].bInterfaceSubClass == USB_SUBCLASS_ACCESSORY))
    {
      for (endpoint = 0;  endpoint < USBH_MAX_NUM_ENDPOINTS ; endpoint ++ )
      {
				if(((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress & 0x80) == 0)&&
						(phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize > 0)&&
						((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bmAttributes & USBH_EP_BULK) == USBH_EP_BULK))
				{
					AOA_Handle->DataOut.Ep = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress;
					AOA_Handle->DataOut.EpSize = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize;
					AOA_Handle->DataOut.supported = 1; 
					res++;
				}
			}
    }
  } 
  
  if(res > 0)
  {  
     status = USBH_OK;
  }
  
  return status;

}

/**
  * @brief  Find IN Audio Streaming interfaces and Endpoints
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_AOA_FindAudioStreamingIN(USBH_HandleTypeDef *phost)
{
  uint8_t interface,endpoint, res;
  USBH_StatusTypeDef status = USBH_FAIL ;
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
 
  /* Look For AUDIOSTREAMING IN interface and Endpints */
  res = 0;
  for (interface = 0;  interface < USBH_MAX_NUM_INTERFACES ; interface ++ )
  {
    if((phost->device.CfgDesc.Itf_Desc[interface].bInterfaceClass == USB_AUDIO_CLASS)&&
       (phost->device.CfgDesc.Itf_Desc[interface].bInterfaceSubClass == USB_SUBCLASS_AUDIOSTREAMING))
    {
      for (endpoint = 0;  endpoint < USBH_MAX_NUM_ENDPOINTS ; endpoint ++ )
      {
				if((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress & 0x80)&&
					 (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize > 0)&&
					 ((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bmAttributes & 0x03) == USBH_EP_ISO))
				{
					AOA_Handle->StreamIn.Ep = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress;
					AOA_Handle->StreamIn.EpSize = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize;
					AOA_Handle->StreamIn.interface = phost->device.CfgDesc.Itf_Desc[interface].bInterfaceNumber;        
					AOA_Handle->StreamIn.AltSettings = phost->device.CfgDesc.Itf_Desc[interface].bAlternateSetting;
					AOA_Handle->StreamIn.supported = 1; 
					res++;
				}
			}
    }
  } 
  
  if(res > 0)
  {  
     status = USBH_OK;
  }
  
  return status;
}
/**
* @}
*/
/**
  * @brief  USBH_ADK_configAndroid
  *         Setup bulk transfer endpoint and open channel.
  * @param  phost: Selected device
  * @retval USBH_StatusTypeDef
  */
static USBH_StatusTypeDef AOA_configAndroid (USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	/* Find DATA IN Endpint and Interface */	
	if(USBH_AOA_FindDataINInterface(phost) ==  USBH_OK)
	{
		if(AOA_Handle->DataIn.supported == 1)
    {
       AOA_Handle->DataIn.Pipe  = USBH_AllocPipe(phost, AOA_Handle->DataIn.Pipe);
       /* Open pipe for IN endpoint */
       USBH_OpenPipe  (phost,
                       AOA_Handle->DataIn.Pipe,
                       AOA_Handle->DataIn.Ep,
                       phost->device.address,
                       phost->device.speed,
                       USB_EP_TYPE_BULK,
                       AOA_Handle->DataIn.EpSize); 
       
       USBH_LL_SetToggle (phost, AOA_Handle->DataIn.Pipe, 0);          

    }
	}
	else
	{
		//USBH_dbgLog ("DATA In not Supported");
	}
	/* Find DATA OUT Endpint and Interface */	
	if(USBH_AOA_FindDataOUTInterface(phost) ==  USBH_OK)
	{
		if(AOA_Handle->DataOut.supported == 1)
    {
       AOA_Handle->DataOut.Pipe  = USBH_AllocPipe(phost, AOA_Handle->DataOut.Pipe);
       /* Open pipe for OUT endpoint */
       USBH_OpenPipe  (phost,
                       AOA_Handle->DataOut.Pipe,
                       AOA_Handle->DataOut.Ep,
                       phost->device.address,
                       phost->device.speed,
                       USB_EP_TYPE_BULK,
                       AOA_Handle->DataOut.EpSize); 
       
       USBH_LL_SetToggle (phost, AOA_Handle->DataOut.Pipe, 0);          

    }
	}
	else
	{
		//USBH_dbgLog ("DATA OUT not Supported");
	}
	
	/* Find Audio Streaming IN Endpint and Interface */
	if(USBH_AOA_FindAudioStreamingIN (phost) ==  USBH_OK)
	{
		if(AOA_Handle->StreamIn.supported == 1)
    {
       AOA_Handle->StreamIn.Pipe  = USBH_AllocPipe(phost, AOA_Handle->StreamIn.Ep);
       /* Open pipe for IN endpoint */
       USBH_OpenPipe  (phost,
                       AOA_Handle->StreamIn.Pipe,
                       AOA_Handle->StreamIn.Ep,
                       phost->device.address,
                       phost->device.speed,
                       USB_EP_TYPE_ISOC,
                       AOA_Handle->StreamIn.EpSize); 
       
       USBH_LL_SetToggle (phost,  AOA_Handle->StreamIn.Pipe, 0);          

    }
	}
	else
	{
		//USBH_dbgLog ("Audio Stream In not Supported");
	}
		
	return USBH_OK;
}
/**
  * @brief  AOA_CheckIfAccessory 
  *         The function checks if connected device supports Accessory mode or not.
  *         for AOA class.
  * @param  phost: Host handle
  * @retval 1 if device supports Accessory mode else 0
  */
uint8_t AOA_CheckIfAccessory(USBH_HandleTypeDef *phost)
{
	if(phost->device.DevDesc.idVendor == USB_ACCESSORY_VENDOR_ID &&
		(	phost->device.DevDesc.idProduct == USB_ACCESSORY_PRODUCT_ID ||
			phost->device.DevDesc.idProduct == USB_ACCESSORY_ADB_PRODUCT_ID ||
			phost->device.DevDesc.idProduct == USB_AUDIO_PRODUCT_ID ||
			phost->device.DevDesc.idProduct == USB_AUDIO_ADB_PRODUCT_ID ||
			phost->device.DevDesc.idProduct == USB_ACCESSORY_AUDIO_PRODUCT_ID ||
			phost->device.DevDesc.idProduct == USB_ACCESSORY_AUDIO_ADB_PRODUCT_ID ))
	{
		return 1;
	}
	return 0;
}

/**
  * @brief  USBH_AOA_ClassRequest 
  *         The function is responsible for handling Standard requests
  *         for AOA class.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_ClassRequest (USBH_HandleTypeDef *phost)
{   
	USBH_StatusTypeDef status = USBH_BUSY;
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	
	switch (AOA_Handle->initstate)
	{
	
		case AOA_INIT_SETUP:
				/* Check if connected device is Accessory */
				if(AOA_CheckIfAccessory(phost))
				{
					AOA_Handle->initstate = AOA_INIT_CONFIGURE_ANDROID;
				}				
				else
				{
					AOA_Handle->initstate = AOA_INIT_GET_PROTOCOL;
					AOA_Handle->protocol = 0;
				}
				break;
				
		case AOA_INIT_GET_PROTOCOL:
				/* Get protocol */
				if (AOA_getProtocol(phost) == USBH_OK )
				{
					if (AOA_Handle->protocol >= 2)
					{
							AOA_Handle->initstate = AOA_INIT_SET_AUDIO_MODE;
					}
					else if (AOA_Handle->protocol >= 1)
					{
							AOA_Handle->initstate = AOA_INIT_SEND_MANUFACTURER;
					}
					else
					{
							AOA_Handle->initstate = AOA_INIT_FAILED;
					}
				}
				break;
		case AOA_INIT_SET_AUDIO_MODE:
			/* Set Audio Mode */
			if(AOA_setAudioMode(phost) == USBH_OK)
			{
				AOA_Handle->initstate = AOA_INIT_SEND_MANUFACTURER;
			}
			break;
		case AOA_INIT_SEND_MANUFACTURER:
				/* Send Manufature String */
				if( AOA_sendString ( phost, ACCESSORY_STRING_MANUFACTURER, (uint8_t*)AOA_MANUFACTURE)== USBH_OK )
				{
					AOA_Handle->initstate = AOA_INIT_SEND_MODEL;
				}
				break;
	  case AOA_INIT_SEND_MODEL:
				/* Send Model String */
				if(AOA_sendString (phost, ACCESSORY_STRING_MODEL,  (uint8_t*)AOA_MODEL)== USBH_OK )
				{
					AOA_Handle->initstate = AOA_INIT_SEND_DESCRIPTION;
				}
				break;	
  
		case AOA_INIT_SEND_DESCRIPTION:
				/* Send Decription String */
				if( AOA_sendString (phost, ACCESSORY_STRING_DESCRIPTION, (uint8_t*)AOA_DESCRIPTION)== USBH_OK )
				{
						AOA_Handle->initstate = AOA_INIT_SEND_VERSION;
				}
				break;
		
		case AOA_INIT_SEND_VERSION:													
				/* Send Version String */
				if( AOA_sendString ( phost, ACCESSORY_STRING_VERSION, (uint8_t*)AOA_VERSION)== USBH_OK )
				{
					AOA_Handle->initstate = AOA_INIT_SEND_URL;
				}
				break;
		case AOA_INIT_SEND_URL:
				/* Send URl String */
				if( AOA_sendString (phost, ACCESSORY_STRING_URI, (uint8_t*)AOA_URL)== USBH_OK )
				{
					AOA_Handle->initstate = AOA_INIT_SEND_SERIAL;
				}
 				break;
		case AOA_INIT_SEND_SERIAL:
				/* Send Serial String */
				if( AOA_sendString (phost, ACCESSORY_STRING_SERIAL, (uint8_t*)AOA_SERIAL)== USBH_OK )
					{
						AOA_Handle->initstate = AOA_INIT_STARTACCESSORY;
					}
					break;
		case AOA_INIT_STARTACCESSORY:
				if(AOA_startAccessory(phost) == USBH_OK )
				{
					AOA_Handle->initstate = AOA_INIT_GET_DEVDESC; 
				}
				break;

	  case AOA_INIT_GET_DEVDESC:
			if( USBH_Get_DevDesc(phost, USB_DEVICE_DESC_SIZE)== USBH_OK )
				{
						//check vaild device
						if(AOA_CheckIfAccessory(phost))
						{
								AOA_Handle->initstate = AOA_INIT_CONFIGURE_ANDROID;
								//USBH_dbgLog ("FOUND Accesory"); 
						}
						else
						{
								AOA_Handle->initstate = AOA_INIT_FAILED;
								//USBH_dbgLog ("NOT FOUND Accesory");
						}
				}
				break;

	  case AOA_INIT_CONFIGURE_ANDROID:
				/* Configure Android Accessory */
				AOA_configAndroid(phost);
				AOA_Handle->initstate = AOA_INIT_SET_INTERFACE;
				break;
		case  AOA_INIT_SET_INTERFACE: 
			/* set configuration */
			if(AOA_Handle->StreamIn.supported == 1)
			{
				if(USBH_SetInterface(phost, AOA_Handle->StreamIn.interface, AOA_Handle->StreamIn.AltSettings) == USBH_OK)
				{
					AOA_Handle->initstate = AOA_INIT_DONE;
				
				}
			}
			else
				AOA_Handle->initstate = AOA_INIT_DONE;
						
			break;
	  case AOA_INIT_DONE:
				status = USBH_OK;
				AOA_Handle->state = AOA_TRANSFER_DATA;
				AOA_Handle->data_rx_state = AOA_IDLE;
				AOA_Handle->data_tx_state = AOA_IDLE;
				AOA_AudioStreamInit(phost);
				//USBH_dbgLog ("AOA_INIT_DONE");
				break;

	  case AOA_INIT_FAILED:
				status = USBH_FAIL;
				break;

		default:
				break;
	}
		return status;
}



/**
  * @brief  USBH_AOA_Process 
  *         The function is for managing state machine for AOA data transfers 
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_AOA_Process(USBH_HandleTypeDef *phost)
{
	USBH_StatusTypeDef status = USBH_BUSY;
  USBH_StatusTypeDef req_status = USBH_OK;
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;

	switch(AOA_Handle->state)
  {
		case AOA_IDLE_STATE:
			status = USBH_OK;
			break;
		
    case AOA_TRANSFER_DATA:
			AOA_ProcessReception(phost);		
			AOA_ProcessTransmission(phost);
			//USBH_AOA_RdWrProcess(phost);
			AOA_ProcessAudioStreamIn(phost);
			break;   
    
		case AOA_ERROR_STATE:
			req_status = USBH_ClrFeature(phost, 0x00); 
			if(req_status == USBH_OK )
			{        
				/*Change the state to waiting*/
				AOA_Handle->state = AOA_IDLE_STATE ;
			}    
			break;
    
		default:
			break;
    
  }
  
  return status;
}

/**
  * @brief  This function return last recieved data size
  * @param  None
  * @retval None
  */
uint16_t USBH_AOA_GetLastReceivedDataSize(USBH_HandleTypeDef *phost)
{
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData; 
  
  if(phost->gState == HOST_CLASS)
  {
    return USBH_LL_GetLastXferSize(phost, AOA_Handle->DataIn.Pipe);
  }
  else
  {
    return 0;
  }
}
/**
  * @brief  This function prepares the state before issuing the class specific commands
  * @param  phost: Host handle
  * @retval None
  */
USBH_StatusTypeDef  USBH_AOA_Transmit(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
{
  USBH_StatusTypeDef Status = USBH_BUSY;
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
  
  if(((AOA_Handle->state == AOA_IDLE_STATE) || (AOA_Handle->state == AOA_TRANSFER_DATA)) && 
		(AOA_Handle->data_tx_state == AOA_IDLE ) && (AOA_Handle->dataSent == 0) )
  {
    AOA_Handle->DataOut.pTxData = pbuff;
    AOA_Handle->DataOut.TxLen = length;  
    AOA_Handle->state = AOA_TRANSFER_DATA;
		AOA_Handle->data_tx_state = AOA_SEND_DATA; 
    Status = USBH_BUSY;
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif      
  }
	if(AOA_Handle->dataSent == 1) 
	{
		Status = USBH_OK;
		AOA_Handle->dataSent = 0;
	}
  return Status;    
}
  
  
/**
* @brief  This function prepares the state before issuing the class specific commands
* @param  None
* @retval None
*/
USBH_StatusTypeDef  USBH_AOA_Receive(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
{
  USBH_StatusTypeDef Status = USBH_BUSY;
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
  
  if (((AOA_Handle->state == AOA_IDLE_STATE) || (AOA_Handle->state == AOA_TRANSFER_DATA)) &&
			(AOA_Handle->data_rx_state == AOA_IDLE ) && (AOA_Handle->dataReceived == 0))
  {
    AOA_Handle->DataIn.pRxData = pbuff;
    AOA_Handle->DataIn.RxLen = length;  
    AOA_Handle->state = AOA_TRANSFER_DATA;
    AOA_Handle->data_rx_state = AOA_RECEIVE_DATA; 
    Status = USBH_BUSY;
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif        
  }
	if(AOA_Handle->dataReceived == 1)
	{
		Status = USBH_OK;
		AOA_Handle->dataReceived = 0;
	}
  return Status;    
} 

static USBH_StatusTypeDef USBH_AOA_RdWrProcess(USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;
	uint16_t length;
	/* Handle RW state */
	switch(AOA_Handle->data_rw_state)
  {
		case AOA_IDLE:
				AOA_Handle->data_rw_state = AOA_RECEIVE_DATA;
				break;

		case AOA_RECEIVE_DATA:
				if(AOA_Handle->DataIn.RxLen != 0)
				{
					USBH_BulkReceiveData (phost,
																AOA_Handle->DataIn.pRxData, 
																AOA_Handle->DataIn.EpSize, 
																AOA_Handle->DataIn.Pipe);
			
					AOA_Handle->data_rw_state = AOA_RECEIVE_DATA_WAIT;
				}
				else
				{
					AOA_Handle->data_rw_state = AOA_SEND_DATA;
				}
				break;
  
		case AOA_RECEIVE_DATA_WAIT:
				URB_Status = USBH_LL_GetURBState(phost, AOA_Handle->DataIn.Pipe); 
				/*Check the status done for reception*/
				if(URB_Status == USBH_URB_DONE )
				{  
					length = USBH_LL_GetLastXferSize(phost, AOA_Handle->DataIn.Pipe);
        
					if(((AOA_Handle->DataIn.RxLen - length) > 0) && (length > AOA_Handle->DataIn.EpSize))
					{
						AOA_Handle->DataIn.RxLen -= length ;
						AOA_Handle->DataIn.pRxData += length;
						AOA_Handle->data_rw_state = AOA_RECEIVE_DATA; 
					}
					else
					{
						AOA_Handle->DataIn.RxLen = 0; 
						AOA_Handle->data_rw_state = AOA_SEND_DATA;
						AOA_Handle->dataReceived = 1;
						// USBH_AOA_ReceiveCallback(phost);
					}
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif          
				}//Added for time out
//				else
//				{ 
//					ccR++;
//					if(ccR == 3)
//					{
//						AOA_Handle->DataIn.RxLen = 0; 
//						AOA_Handle->data_rw_state = AOA_SEND_DATA;
//						ccR = 0;
//					}
//				}
				break;	

		case AOA_SEND_DATA:
			if(AOA_Handle->DataOut.TxLen != 0)
			{
				if(AOA_Handle->DataOut.TxLen > AOA_Handle->DataOut.EpSize)
				{
					USBH_BulkSendData (phost,
														 AOA_Handle->DataOut.pTxData, 
														 AOA_Handle->DataOut.EpSize, 
														 AOA_Handle->DataOut.Pipe,
														 1);
				}
				else
				{
					USBH_BulkSendData (phost,
														 AOA_Handle->DataOut.pTxData, 
														 AOA_Handle->DataOut.TxLen, 
														 AOA_Handle->DataOut.Pipe,
														 1);
				}
			
				AOA_Handle->data_rw_state = AOA_SEND_DATA_WAIT;
			}
			else
			{
				AOA_Handle->data_rw_state = AOA_IDLE;
			}
			break;
    
		case AOA_SEND_DATA_WAIT:
    
			URB_Status = USBH_LL_GetURBState(phost, AOA_Handle->DataOut.Pipe); 
			/*Check the status done for transmssion*/
			if(URB_Status == USBH_URB_DONE )
			{         
				if(AOA_Handle->DataOut.TxLen > AOA_Handle->DataOut.EpSize)
				{
					AOA_Handle->DataOut.TxLen -= AOA_Handle->DataOut.EpSize ;
					AOA_Handle->DataOut.pTxData += AOA_Handle->DataOut.EpSize;
				}
				else
				{
					AOA_Handle->DataOut.TxLen = 0;
				}
				
				if( AOA_Handle->DataOut.TxLen > 0)
				{
				 AOA_Handle->data_rw_state = AOA_SEND_DATA; 
				}
				else
				{
					AOA_Handle->data_rw_state = AOA_IDLE; 
					AOA_Handle->dataSent = 1;		
				//  USBH_AOA_TransmitCallback(phost);
				}
#if (USBH_USE_OS == 1)
			osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif    
			}
			else if( URB_Status == USBH_URB_NOTREADY )
			{
				AOA_Handle->data_rw_state = AOA_SEND_DATA; 
#if (USBH_USE_OS == 1)
		osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif          
			}// Added for time out
//				else
//				{ 
//					ccS++;
//					if(ccS == 3)
//					{
//						AOA_Handle->DataOut.TxLen = 0; 
//						AOA_Handle->data_rw_state = AOA_IDLE;
//						ccS = 0;
//					}
//				}
			
			break;
		
		default:
			break;
  }
}

/**
* @brief  The function is responsible for sending data to the device
*  @param  pdev: Selected device
* @retval None
*/
static void AOA_ProcessTransmission(USBH_HandleTypeDef *phost)
{
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;
	
  
  switch(AOA_Handle->data_tx_state)
  {
 
  case AOA_SEND_DATA:
   // AOA_Handle->dataSent = 0;
		if(AOA_Handle->DataOut.TxLen > AOA_Handle->DataOut.EpSize)
    {
      USBH_BulkSendData (phost,
                         AOA_Handle->DataOut.pTxData, 
                         AOA_Handle->DataOut.EpSize, 
                         AOA_Handle->DataOut.Pipe,
                         1);
    }
    else
    {
      USBH_BulkSendData (phost,
                         AOA_Handle->DataOut.pTxData, 
                         AOA_Handle->DataOut.TxLen, 
                         AOA_Handle->DataOut.Pipe,
                         1);
    }
    
    AOA_Handle->data_tx_state = AOA_SEND_DATA_WAIT;
    
    break;
    
  case AOA_SEND_DATA_WAIT:
    
    URB_Status = USBH_LL_GetURBState(phost, AOA_Handle->DataOut.Pipe); 
    /*Check the status done for transmssion*/
    if(URB_Status == USBH_URB_DONE )
    {         
      if(AOA_Handle->DataOut.TxLen > AOA_Handle->DataOut.EpSize)
      {
        AOA_Handle->DataOut.TxLen -= AOA_Handle->DataOut.EpSize ;
        AOA_Handle->DataOut.pTxData += AOA_Handle->DataOut.EpSize;
      }
      else
      {
        AOA_Handle->DataOut.TxLen = 0;
      }
      
      if( AOA_Handle->DataOut.TxLen > 0)
      {
       AOA_Handle->data_tx_state = AOA_SEND_DATA; 
      }
      else
      {
        AOA_Handle->data_tx_state = AOA_IDLE;
				AOA_Handle->dataSent = 1;				
      //  USBH_AOA_TransmitCallback(phost);
      }
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif    
    }
    else if( URB_Status == USBH_URB_NOTREADY )
    {
      AOA_Handle->data_tx_state = AOA_SEND_DATA; 
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif          
    }
//		else
//		{ 
//			ccS++;
//			if(ccS == 2)
//			{
//				AOA_Handle->DataOut.TxLen = 0;
//				AOA_Handle->dataSent = 1;					
//				AOA_Handle->data_tx_state = AOA_IDLE;
//				ccS = 0;
//			}
//		}
		
    break;
  default:
    break;
  }
}
/**
* @brief  This function responsible for reception of data from the device
*  @param  pdev: Selected device
* @retval None
*/

static void AOA_ProcessReception(USBH_HandleTypeDef *phost)
{
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;
  uint16_t length;

  switch(AOA_Handle->data_rx_state)
  {
    
  case AOA_RECEIVE_DATA:
		USBH_BulkReceiveData (phost,
                          AOA_Handle->DataIn.pRxData, 
                          AOA_Handle->DataIn.EpSize, 
                          AOA_Handle->DataIn.Pipe);
    
    AOA_Handle->data_rx_state = AOA_RECEIVE_DATA_WAIT;
    
    break;
    
  case AOA_RECEIVE_DATA_WAIT:
    
    URB_Status = USBH_LL_GetURBState(phost, AOA_Handle->DataIn.Pipe); 

		/*Check the status done for reception*/
    if(URB_Status == USBH_URB_DONE )
    {  
      length = USBH_LL_GetLastXferSize(phost, AOA_Handle->DataIn.Pipe);
        
      if(((AOA_Handle->DataIn.RxLen - length) > 0) && (length > AOA_Handle->DataIn.EpSize))
      {
        AOA_Handle->DataIn.RxLen -= length ;
        AOA_Handle->DataIn.pRxData += length;
        AOA_Handle->data_rx_state = AOA_RECEIVE_DATA; 
      }
      else
      {
        
				AOA_Handle->data_rx_state = AOA_IDLE;
				AOA_Handle->dataReceived = 1;
       // USBH_AOA_ReceiveCallback(phost);
      }
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif          
    }
//		else
//		{ 
//			ccR++;
//			if(ccR == 2)
//			{
//				AOA_Handle->DataIn.RxLen = 0; 
//				AOA_Handle->data_rx_state = AOA_IDLE;
//				AOA_Handle->dataReceived = 1;
//				ccR = 0;
//			}
//		}
    break;
    
  default:
    break;
  }
}


static USBH_StatusTypeDef AOA_AudioStreamInit(USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	if(AOA_Handle->initstate == AOA_INIT_DONE)
	{
		AOA_Handle->offset = AUDIO_OFFSET_UNKNOWN;
		AOA_Handle->AudioStream.wr_ptr = 0;
		AOA_Handle->AudioStream.rd_ptr = 0;
		AOA_Handle->rd_enable = 0;
		AOA_Handle->state = AOA_TRANSFER_DATA;
		AOA_Handle->audioStarted = 1;
		
    AOA_Handle->fopsAudioInterface = &USBH_AUDIO_fops;

		/* Initialize the Audio output Hardware layer */
    if((AOA_Handle->fopsAudioInterface)->Init(USBD_AUDIO_FREQ,AUDIO_DEFAULT_VOLUME,0) != USBH_OK)
		{
      return USBH_FAIL;
    }
		AOA_Handle->StreamInState = AUDIO_DATA_IN;
	}
		return USBH_OK;	
}


void FillAudioStreamBuff(USBH_HandleTypeDef *phost,uint8_t *buff, uint16_t len)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	uint16_t i = 0;
	while(i < len)
	{
		AOA_Handle->AudioStream.buff[AOA_Handle->AudioStream.wr_ptr] = *(buff + i++);
		AOA_Handle->AudioStream.wr_ptr++;
		if(AOA_Handle->AudioStream.wr_ptr == AUDIO_TOTAL_BUF_SIZE)
		{
			/* Roll Back */
			AOA_Handle->AudioStream.wr_ptr = 0;
			/* start Audio DAC this happnes only once*/
			if(AOA_Handle->offset == AUDIO_OFFSET_UNKNOWN)
			{
				(AOA_Handle->fopsAudioInterface)->AudioCmd(&AOA_Handle->AudioStream.buff[0] ,
																										AUDIO_TOTAL_BUF_SIZE/2,
																										AUDIO_CMD_START);
				AOA_Handle->offset = AUDIO_OFFSET_NONE;	
			}
		}
	}
	if(AOA_Handle->rd_enable == 0)
	{
		if (AOA_Handle->AudioStream.wr_ptr >= (AUDIO_TOTAL_BUF_SIZE / 2))
		{
			AOA_Handle->rd_enable = 1; 
		}
	}
}

/**
* @brief  This function responsible for reception of Audio stream data from the device
* @param  phost: Host handle
* @retval None
*/
static void AOA_ProcessAudioStreamIn(USBH_HandleTypeDef *phost)
{
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	uint16_t length = 0;

  switch(AOA_Handle->StreamInState)
  {
		case AUDIO_DATA_IN:
				USBH_IsocReceiveData(	phost, 
															&audioBuff[0],
															AOA_Handle->StreamIn.EpSize,
															AOA_Handle->StreamIn.Pipe);

				AOA_Handle->StreamInState = AUDIO_DATA_WAIT;
				break;
 
		case AUDIO_DATA_WAIT:
			if(USBH_LL_GetURBState(phost, AOA_Handle->StreamIn.Pipe) == USBH_URB_DONE)
			{
				length = USBH_LL_GetLastXferSize(phost, AOA_Handle->StreamIn.Pipe);
	
				if(length != 0)
				{
					FillAudioStreamBuff(phost,audioBuff,length);
					BSP_LED_Toggle(LED1);
				}

				AOA_Handle->StreamInState = AUDIO_DATA_IN;
			}
			break;
	
		default:
			break;
  }
		
}	


/**
  * @brief  AOA_AUDIO_SOF
	* @param  phost: Host handle
  * @retval status
  */
void  USBH_AOA_AUDIO_Sync(USBH_HandleTypeDef *phost, AUDIO_OffsetTypeDef offset)
{
  int8_t shift = 0;
	
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	
	AOA_Handle->offset = offset;
	
//	if(AOA_Handle->rd_enable == 1)
	{
		AOA_Handle->AudioStream.rd_ptr += AUDIO_TOTAL_BUF_SIZE/2;

		if (AOA_Handle->AudioStream.rd_ptr == AUDIO_TOTAL_BUF_SIZE)
		{
				/* roll back */
			AOA_Handle->AudioStream.rd_ptr = 0;
		}
	}

		
	if(AOA_Handle->AudioStream.rd_ptr > AOA_Handle->AudioStream.wr_ptr)
	{
		if((AOA_Handle->AudioStream.rd_ptr - AOA_Handle->AudioStream.wr_ptr) < AUDIO_OUT_PACKET)
		{
			//shift = -1 * AUDIO_OUT_PACKET;
			//BSP_LED_Toggle(LED3);
		}
		else if((AOA_Handle->AudioStream.rd_ptr - AOA_Handle->AudioStream.wr_ptr) > (AUDIO_TOTAL_BUF_SIZE - AUDIO_OUT_PACKET))
		{
			//shift = (int8_t)AUDIO_OUT_PACKET;
		//	BSP_LED_Toggle(LED2);
		}    

	}
	else
	{
		if((AOA_Handle->AudioStream.wr_ptr - AOA_Handle->AudioStream.rd_ptr) < AUDIO_OUT_PACKET*2)
		{
			shift = AUDIO_OUT_PACKET;
			//BSP_LED_Toggle(LED1);
		}
		else if((AOA_Handle->AudioStream.wr_ptr - AOA_Handle->AudioStream.rd_ptr) > (AUDIO_TOTAL_BUF_SIZE - AUDIO_OUT_PACKET))
		{
			//shift = -1* AUDIO_OUT_PACKET;
			//BSP_LED_Toggle(LED2);
		}  
	}


  if(AOA_Handle->offset == AUDIO_OFFSET_FULL)
  {
		
		(AOA_Handle->fopsAudioInterface)->AudioCmd(&AOA_Handle->AudioStream.buff[AOA_Handle->AudioStream.rd_ptr],
																								AUDIO_TOTAL_BUF_SIZE/2 + shift,
																								AUDIO_CMD_PLAY);
		
		AOA_Handle->offset = AUDIO_OFFSET_NONE;           
	}
	else if (AOA_Handle->offset == AUDIO_OFFSET_HALF)
  {
		
		(AOA_Handle->fopsAudioInterface)->AudioCmd(&AOA_Handle->AudioStream.buff[AOA_Handle->AudioStream.rd_ptr/2],
																								AUDIO_TOTAL_BUF_SIZE/2,		
																								AUDIO_CMD_PLAY); 
     AOA_Handle->offset = AUDIO_OFFSET_NONE;  
      
	}  
			
}


/**
  * @brief  USBH_AOA_getStatus
  *         Return ADK_Machine.state
  * @param  None
  * @retval AOA_Handle->state
  */
AOA_StateTypeDef AOA_GetStatus(USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	return AOA_Handle->state;
}

/**
  * @brief  USBH_AOA_getStatus
  *         Return ADK_Machine.state
  * @param  None
  * @retval AOA_Handle->state
  */
AOA_InitState AOA_GetInitStatus(USBH_HandleTypeDef *phost)
{
	AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	return AOA_Handle->initstate;
}

/**
* @brief  USBH_AUDIO_RegisterInterface
* @param  fops: Audio interface callback
* @retval status
*/
uint8_t  USBH_AOA_RegisterAudioInterface(USBH_HandleTypeDef *phost, 
                                      USBH_AUDIO_ItfTypeDef *fops)
{
  AOA_HandleTypeDef *AOA_Handle =  phost->pActiveClass->pData;
	if(fops != NULL)
  {
    AOA_Handle->fopsAudioInterface = fops;
  }
  return 0;
}

/**
  * @brief  USBH_AOA_SOFProcess 
  *         The function is for SOF state
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_AOA_SOFProcess(USBH_HandleTypeDef *phost)
{
	return USBH_OK;
}



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
