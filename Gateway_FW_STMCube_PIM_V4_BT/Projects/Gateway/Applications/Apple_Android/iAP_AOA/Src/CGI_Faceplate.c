#include "CGI_Faceplate.h"
//#include "usb_hid.h"
#include "stdio.h"
#include "gateway_board.h"
#include "gateway_board_cgi.h"

//Rx Msg Temp Buffer

//volatile uint8_t buff[100];
extern volatile uint8_t CGI_buff[50];
volatile uint8_t buffIndex = 0;
uint8_t g_MaxBuffSize = 50;
uint16_t CGI_timeMs = 0;
static uint32_t ProcessSbxFsm(void );
static uint32_t ProcSbxState_Sleep(void );
static uint32_t ProcSbxState_RdWakeDelay(void );
static uint32_t ProcSbxState_Active(void);
static uint32_t ProcSbxState_StatusWait(void);

//static uint32_t ProcSbxState_Retry(void);
//static uint32_t ProcSbxState_Reset(void);
//static uint32_t ProcSbxState_RdfFail(void);
//static uint32_t ProcSbxState_FpResetDelay(void);
//static uint32_t ProcSbxState_DisableNormalCommunication(void);
//static uint32_t SendMsgToRDF(uint8_t* cgipkt , uint16_t size);

static void DispatchRdfEvents(uint8_t *controlMsgStatus);
uint8_t GetReceivedCharCount(void);
void SetMaxReceiveMsgSize(uint8_t size);
uint8_t *GetReceivedChars(uint8_t *size);

void ResetReceiveBuffer(void);

static SbxState_e g_SbxState = SBX_STATE_SLEEP;
static uint8_t Event_buffer_Index=0;
typedef uint32_t (*const fptr_state) (void);

uint8_t Event_buffer[20];
uint8_t *EV_REC_ptr=Event_buffer,*EV_READ_ptr;
uint8_t loop=1,init_done_flag=0,first=1;

uint8_t vSbxRdfComm(uint8_t init){
	int8_t ret;
	uint8_t c=0;
	static uint8_t init_flag=0;	
	loop=1,first=1;

	while(loop)
	{	
		if(init==0)
		{  
			ret = ProcessSbxFsm();	
			if(init_done_flag==1)
			{
				loop=0;
				init_done_flag=0;
			}
			if(-1 == ret)
				loop=0;
		}
		else if(init ==1)
		{
			if(first==1)
			{
				g_SbxState = SBX_STATE_ACTIVE;
				first=0;
			}
			ret = ProcessSbxFsm();
			if(init_done_flag==1)
			{
				loop=0;
				init_done_flag=0;
			}
			if(-1 == ret)
				loop=0;
		} 
	}
	return ret;	
}
static uint32_t ProcessSbxFsm(void) {
	
	static fptr_state sbx_states[SBX_STATE_MAX] =
	{
		&ProcSbxState_Sleep,
		&ProcSbxState_RdWakeDelay,
		&ProcSbxState_Active,
		&ProcSbxState_StatusWait
		/*&ProcSbxState_Retry,											
		&ProcSbxState_Reset,
		&ProcSbxState_RdfFail,
		&ProcSbxState_FpResetDelay,
		&ProcSbxState_DisableNormalCommunication*/
	};
	return (*sbx_states[g_SbxState])();
}

static uint32_t ProcSbxState_Sleep(){
	
	uint8_t initCmd[SBX_CNTR_STATCK_WAKE_MSG_SIZE] = {0x01, 0x00, 0x01};  //Display status request
	ResetReceiveBuffer();
	if(BSP_CGI_Transmit_Data(initCmd,sizeof(initCmd)) == CGI_OK)
	{
		HAL_Delay(4);  //added for the sync
		g_SbxState = SBX_STATE_RDWAKEDELAY;
		return 0;
	}
	return -1;
}

static uint32_t ProcSbxState_RdWakeDelay() {
	uint8_t intResp[SBX_DISPLAY_STATUS_MSG_SIZE]={0,0,0,0,0};
	uint8_t msgSize;
	
	while(((SBX_CNTR_STATCK_WAKE_MSG_SIZE+SBX_DISPLAY_STATUS_MSG_SIZE) != GetReceivedCharCount())&& CGI_timeMs < SBX_WAKEUP_DETECT_TIME_MAX_MS){
		CGI_timeMs++;
		HAL_Delay(1);
	}
	//timeout occurred
	if(CGI_timeMs == SBX_WAKEUP_DETECT_TIME_MAX_MS)//Case Not handled yet
	{
		return -1; //original stmt
	}
	else 
	{ 
		//Received response */
		uint8_t *ptr = GetReceivedChars(&msgSize);
		for ( int i = 0; i < SBX_DISPLAY_STATUS_MSG_SIZE; i ++ ) {
			intResp[i] = *(ptr+SBX_CNTR_STATCK_WAKE_MSG_SIZE+i);
		}
		g_SbxState = SBX_STATE_ACTIVE;
		return 0;
	}
	return -1;
}

static uint32_t ProcSbxState_Active() {
	static uint8_t toggle =0;
	ResetReceiveBuffer();  //added to clear the buffindex
	 
	uint8_t ctrlCmd[SBX_REQUEST_CONTROL_STATUS_MSG_SIZE] = {
		0x02, //ControlStatusReq
		0x00, 0x00, //MainDisplay Brightness
		0xAA, 0xAA,	//0xFF, 0xFF, //LED Backlight Brightness
		0x00, 0x00, //Aux Brightness
		0x00, 0x00, //bit 7 positive/negative 
		0x00, 0x00,
		0x00, 0x00,
		0x00, 0x01, 0x56
	};	
	uint8_t ctrlCmd2[SBX_REQUEST_CONTROL_STATUS_MSG_SIZE] = {
		0x02, //ControlStatusReq
		0x00, 0x00, //MainDisplay Brightness
		0x00, 0x00, //LED Backlight Brightness
		0x00, 0x00, //Aux Brightness
		0x00, 0x00,
		0x00, 0x00,
		0x00, 0x00,
		0x00, 0x00, 0x02
	};

	if(toggle)
	{
		if(BSP_CGI_Transmit_Data(ctrlCmd2,sizeof(ctrlCmd2)) == CGI_OK)
		{
			HAL_Delay(4); 
			g_SbxState = SBX_STATE_STATUSWAIT;
			return 0;
		}
		else 
			return -1;
	}
	else 
	{
		if(BSP_CGI_Transmit_Data(ctrlCmd, sizeof(ctrlCmd)) == CGI_OK) \
		{	
			//g_SbxState = SBX_STATE_RDWAKEDELAY;  //original smt
			HAL_Delay(4); //yet make sure to add or comment
			g_SbxState = SBX_STATE_STATUSWAIT;
			return 0;
		}
		else 
			return -1;
	}
}

static uint32_t ProcSbxState_StatusWait(){
	uint8_t msgResp[SBX_CONTROL_STATUS_MSG_SIZE]={0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	uint16_t timeMs = 0;
	uint8_t msgSize;
	while((SBX_REQUEST_CONTROL_STATUS_MSG_SIZE+SBX_CONTROL_STATUS_MSG_SIZE) != GetReceivedCharCount() && timeMs < SBX_MSG_RESPONSE_TIME_MAX_MS) 
	{
		timeMs++;
		HAL_Delay(1);
	}
	//timeout occurred
	if(timeMs == SBX_MSG_RESPONSE_TIME_MAX_MS)	//Case Not handled yet
	{
		return -1;
	}
	else 
	{ 
		//Received response
		uint8_t *ptr = GetReceivedChars(&msgSize);
		for(int i = 0; i < SBX_CONTROL_STATUS_MSG_SIZE; i ++ ) 
		{
			msgResp[i] = *(ptr+SBX_REQUEST_CONTROL_STATUS_MSG_SIZE+i);  //original stmt
		}
		DispatchRdfEvents(msgResp);
		g_SbxState = SBX_STATE_ACTIVE;
		init_done_flag=1;
		return 0;
	}	
	return -1;  
}

/*Need to implement*/
//static uint32_t ProcSbxState_Retry() {	
//	return 0;
//}

//static uint32_t ProcSbxState_Reset() {	
//	return 0;
//}

//static uint32_t ProcSbxState_RdfFail() {	
//	return 0;
//}

//static uint32_t ProcSbxState_FpResetDelay() {	
//	return 0;
//}

//static uint32_t ProcSbxState_DisableNormalCommunication() {	
//	return 0;
//}

uint8_t *GetKeyName(uint8_t key) {
	
	switch(key) {
		case CGI_FACEPLATE_KEY_VOL_BIT_POS:
			return "KEY_VOL";
		case CGI_FACEPLATE_KEY_KEY_1_BIT_POS:
			return "KEY_KEY_1";
		case CGI_FACEPLATE_KEY_KEY_2_BIT_POS:
			return "KEY_KEY_2";
		case CGI_FACEPLATE_KEY_KEY_3_BIT_POS:
			return "KEY_KEY_3";
		case CGI_FACEPLATE_KEY_KEY_4_BIT_POS:
			return "KEY_KEY_4";
		case CGI_FACEPLATE_KEY_KEY_5_BIT_POS:
			return "KEY_KEY_5";
		case CGI_FACEPLATE_KEY_KEY_6_BIT_POS:
			return "KEY_KEY_6";
		case CGI_FACEPLATE_KEY_MENU_BIT_POS:
			return "KEY_MENU";
		case CGI_FACEPLATE_KEY_FAV_BIT_POS:
			return "KEY_FAV";
		case CGI_FACEPLATE_KEY_SOURCE_BIT_POS:
			return "KEY_SOURCE";
		case CGI_FACEPLATE_KEY_HOME_BIT_POS:
			return "KEY_HOME";
		case CGI_FACEPLATE_KEY_BACK_BIT_POS:
			return "KEY_BACK";
		case CGI_FACEPLATE_KEY_SEEK_PREV_BIT_POS:
			return "KEY_SEEK_PREV";
		case CGI_FACEPLATE_KEY_SEEK_NEXT_BIT_POS:
			return "KEY_SEEK_NEXT";
		case CGI_FACEPLATE_KEY_INFO_BIT_POS:
			return "KEY_INFO";
		case CGI_FACEPLATE_KEY_CONFIG_BIT_POS:
			return "KEY_CONFIG";
		case CGI_FACEPLATE_KEY_DEST_BIT_POS:
			return "KEY_DEST";
		case CGI_FACEPLATE_KEY_TONE_BIT_POS:
			return "KEY_TONE";
		case CGI_FACEPLATE_KEY_EJECT_BIT_POS:
			return "KEY_EJECT";
		case CGI_FACEPLATE_KEY_NAV_BIT_POS:
			return "KEY_NAV";
		case CGI_FACEPLATE_KEY_RPT_BIT_POS:
			return "KEY_RPT";
		case CGI_FACEPLATE_KEY_CALL_BIT_POS:
			return "KEY_CALL";	
		default:
			return "INVALID";
	}
}

FaceplateKey_e GetHidEvent(uint8_t key) {
	 
	 switch(key) {
		 
		case CGI_FACEPLATE_KEY_VOL_BIT_POS:
			return FACEPLATE_KEY_VOL;
		case CGI_FACEPLATE_KEY_KEY_1_BIT_POS:
			return FACEPLATE_KEY_KEY_1;
		case CGI_FACEPLATE_KEY_KEY_2_BIT_POS:
			return FACEPLATE_KEY_KEY_2;
		case CGI_FACEPLATE_KEY_KEY_3_BIT_POS:
			return FACEPLATE_KEY_KEY_3;
		case CGI_FACEPLATE_KEY_KEY_4_BIT_POS:
			return FACEPLATE_KEY_KEY_4;
		case CGI_FACEPLATE_KEY_KEY_5_BIT_POS:
			return FACEPLATE_KEY_KEY_5;
		case CGI_FACEPLATE_KEY_KEY_6_BIT_POS:
			return FACEPLATE_KEY_KEY_6;
		case CGI_FACEPLATE_KEY_MENU_BIT_POS:
			return FACEPLATE_KEY_MENU;
		case CGI_FACEPLATE_KEY_FAV_BIT_POS:
			return FACEPLATE_KEY_FAV;
		case CGI_FACEPLATE_KEY_SOURCE_BIT_POS:
			return FACEPLATE_KEY_SOURCE;
		case CGI_FACEPLATE_KEY_HOME_BIT_POS:
			return FACEPLATE_KEY_HOME;
		case CGI_FACEPLATE_KEY_BACK_BIT_POS:
			return FACEPLATE_KEY_BACK;
		case CGI_FACEPLATE_KEY_SEEK_PREV_BIT_POS:
			return FACEPLATE_KEY_SEEK_PREV;
		case CGI_FACEPLATE_KEY_SEEK_NEXT_BIT_POS:
			return FACEPLATE_KEY_SEEK_NEXT;
		case CGI_FACEPLATE_KEY_INFO_BIT_POS:
			return FACEPLATE_KEY_INFO;
		case CGI_FACEPLATE_KEY_CONFIG_BIT_POS:
			return FACEPLATE_KEY_CONFIG;
		case CGI_FACEPLATE_KEY_DEST_BIT_POS:
			return FACEPLATE_KEY_DEST;
		case CGI_FACEPLATE_KEY_TONE_BIT_POS:
			return FACEPLATE_KEY_TONE;
		case CGI_FACEPLATE_KEY_EJECT_BIT_POS:
			return FACEPLATE_KEY_EJECT;
		case CGI_FACEPLATE_KEY_NAV_BIT_POS:
			return FACEPLATE_KEY_NAV;
		case CGI_FACEPLATE_KEY_RPT_BIT_POS:
			return FACEPLATE_KEY_RPT;
		case CGI_FACEPLATE_KEY_CALL_BIT_POS:
			return FACEPLATE_KEY_CALL;
		default:
			return FACEPLATE_KEY_MAX;
	 }
	
}

static void DispatchRdfEvents(uint8_t *controlMsgStatus) {
	
  uint8_t encoderVal = 0;
	static uint8_t isUpDown = 0x00;
	FaceplateKey_e encKey = FACEPLATE_KEY_MAX;
	uint8_t keyData[3] = {1,2,3};
	static FaceplateKey_e lastKeyPress = FACEPLATE_KEY_MAX;
	
	uint8_t isKeystrokeComplete = 0x01;
	FaceplateKey_e currentKeyPress = FACEPLATE_KEY_MAX;
	FaceplateKey_e currentKeyRelease = FACEPLATE_KEY_MAX;
	uint8_t bitPos = SBX_CONTROL_STATUS_MSG_KEYS_START_BIT;
	
	while( bitPos <= SBX_CONTROL_STATUS_MSG_KEYS_END_BIT )
	{
		if(controlMsgStatus[bitPos/8] & (0x01 << (bitPos%8))) //Bit is set 
		{ 
			currentKeyPress = GetHidEvent(bitPos);
			if(lastKeyPress != currentKeyPress)
			{
				Event_buffer[Event_buffer_Index]=currentKeyPress;
				Event_buffer_Index++;
				EV_REC_ptr++;
				if(Event_buffer_Index == EVENT_buffer_LEN)
				{
					Event_buffer_Index=0;
					EV_REC_ptr = Event_buffer; 
				}
			}
			if(currentKeyPress != FACEPLATE_KEY_MAX) 						//Valid Key
			{ 
				if((currentKeyPress != lastKeyPress) && (isKeystrokeComplete == 0x01)) 
				{
					if(		(currentKeyPress == FACEPLATE_KEY_HOME)
						|| 	(currentKeyPress == FACEPLATE_KEY_EJECT)
						|| 	(currentKeyPress == FACEPLATE_KEY_NAV)
						|| 	(currentKeyPress == FACEPLATE_KEY_CALL)
						|| 	(currentKeyPress == FACEPLATE_KEY_RPT) ){							
							//uint8_t tCurrentKey = currentKeyPress;
							//aoa_send_buffered_data((uint8_t *)&tCurrentKey, sizeof(tCurrentKey));
					} 
					#if 0	  /*Commented this code did know its application essence*/
					else	{
						if(currentKeyPress == FACEPLATE_KEY_KEY_6) {
							isUpDown = ~isUpDown;
						}
						//send_key_event(currentKeyPress);
					}
					#endif 
					isKeystrokeComplete = 0x00;
					lastKeyPress = currentKeyPress;
					currentKeyRelease = FACEPLATE_KEY_MAX;
					
				}	
			}
		}											
		else 																				//Bit is not set
		{
			currentKeyRelease = GetHidEvent(bitPos);  //commented  //original stmt
			if(lastKeyPress != FACEPLATE_KEY_MAX) 		 //Valid Key
			{
				if(lastKeyPress == currentKeyRelease ) 
				{
					lastKeyPress = FACEPLATE_KEY_MAX;
					isKeystrokeComplete = 0x01;
				}
			}
		}	
		bitPos++;
	}
	
	if( controlMsgStatus[CGI_FACEPLATE_KEY_MENU_KNOB_BYTE_POS] != 0x00 ) 					//Menu
	{ 
		if( (controlMsgStatus[CGI_FACEPLATE_KEY_MENU_KNOB_BYTE_POS] & 0x80 ) >> 7)	//Knob Anti Clockwise movement
		{ 
			encoderVal = 0xFF - controlMsgStatus[CGI_FACEPLATE_KEY_MENU_KNOB_BYTE_POS] + 1;
			encKey = FACEPLATE_KEY_MENU_DOWN;
			Event_buffer[Event_buffer_Index]=encKey;
			Event_buffer_Index++;
			EV_REC_ptr++;
			if(Event_buffer_Index == EVENT_buffer_LEN)
			{
				Event_buffer_Index=0;
				EV_REC_ptr = Event_buffer; //
			}
		}
		else 																																			//Knob Clockwise movement
		{  
			encoderVal = controlMsgStatus[CGI_FACEPLATE_KEY_MENU_KNOB_BYTE_POS];
			encKey = FACEPLATE_KEY_MENU_UP;
			Event_buffer[Event_buffer_Index]=encKey;
			Event_buffer_Index++;
			EV_REC_ptr++;
			if(Event_buffer_Index == EVENT_buffer_LEN)
			{
				Event_buffer_Index=0;
				EV_REC_ptr = Event_buffer; //
			}
		}
		#if 0
		if(isUpDown == 0x00) 
		{
			if(encKey == FACEPLATE_KEY_MENU_UP)
				//encKey = HID_LEFT;
			  encKey = 0;
			else if(encKey == FACEPLATE_KEY_MENU_DOWN)
				//encKey = HID_RIGHT;
				encKey = 1;
		}
		else
		{
			/*for(uint8_t i = 0; i < encoderVal; i++) {
			send_key_event(encKey);
			}*/
		}
		#endif
		return;
	}
	
	if( controlMsgStatus[CGI_FACEPLATE_KEY_VOL_KNOB_BYTE_POS] != 0x00 ) 					//Volume
	{
		if( (controlMsgStatus[CGI_FACEPLATE_KEY_VOL_KNOB_BYTE_POS] & 0x80 ) >> 7) 	//Knob Anti Clockwise movement
		{ 
			encoderVal = 0xFF - controlMsgStatus[CGI_FACEPLATE_KEY_VOL_KNOB_BYTE_POS] + 1;
			encKey = FACEPLATE_KEY_VOL_DOWN;
			Event_buffer[Event_buffer_Index]=encKey;
			Event_buffer_Index++;
			EV_REC_ptr++;
			if(Event_buffer_Index == EVENT_buffer_LEN)
			{
				Event_buffer_Index=0;
				EV_REC_ptr = Event_buffer; 
			}
		}
		else 																																			//Knob Clockwise movement
		{  
			encoderVal = controlMsgStatus[CGI_FACEPLATE_KEY_VOL_KNOB_BYTE_POS];
			encKey = FACEPLATE_KEY_VOL_UP;
			Event_buffer[Event_buffer_Index]=encKey;
			Event_buffer_Index++;
			EV_REC_ptr++;
			if(Event_buffer_Index == EVENT_buffer_LEN)
			{
				Event_buffer_Index=0;
				EV_REC_ptr = Event_buffer; 
			}
		}
	}
}

#if 0
static uint32_t SendMsgToRDF(uint8_t* cgipkt, uint16_t size){
	ResetReceiveBuffer();	
	if(HAL_UART_Transmit_IT(&UartHandle, cgipkt, size)!= HAL_OK) {}
	return 0;
}
#endif

void SetMaxReceiveMsgSize(uint8_t size) {
	
	g_MaxBuffSize = size;
}

uint8_t GetReceivedCharCount(void) {
	return buffIndex-1;
}

uint8_t * GetReceivedChars(uint8_t *size) {
	*size = (buffIndex-1);
	return &CGI_buff[0];
	
}

void ResetReceiveBuffer(void) {
	
	buffIndex = 0;
}


uint8_t get_Events_occured(void)
{
	uint8_t event_read;
	if(EV_REC_ptr != EV_READ_ptr)
	{
		 event_read = *EV_READ_ptr;
     EV_READ_ptr++;	
		 return event_read;
	}
	else if( (EV_REC_ptr == EV_READ_ptr))
	{
		Event_buffer_Index=0;
		EV_REC_ptr=Event_buffer;
		EV_READ_ptr=EV_REC_ptr;
	}
  return -1;
		 
}
/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/




