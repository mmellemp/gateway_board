/**
  ******************************************************************************
  * @file    Ecan_app.c 
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   Tuner Demo Application. 
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "obd_app.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t OBD_param_get_status;
int8_t OBD_received_value_param;
/* Private function prototypes -----------------------------------------------*/

void OBD_Init(void)
{
	BSP_CAN_Init(CANB);
	//Set Can Baud to match vehicle CAN baud. Mostly 250Kbps or 500Kbps
	//MCP mode must be changed to config to change baud rate
	BSP_CAN_SetCANMode(CANB,MCP2515_MODE_CONFIG,0,0,0,0);
	BSP_CAN_SetCANBaud(CANB,500000);

	BSP_CAN_SetCANMode(CANB,MCP2515_MODE_NORMAL,0,0,0,0);
}

/**
  * @brief  pidToUserParam
  *         Helper function to convert OBD PIDs to enumerated vehicle parameter to maintain code readabilty 
	* @param  parameter: PID
  * @retval Vehicle parameter number
  */
static int8_t pidToUserParam(uint8_t pid)
{
	int8_t param;
	switch(pid)
	{
		case OBD_MODE1_PID_AMBIENT_TEMP:
			param = AMBIENT_TEMP;
			break;
		case OBD_MODE1_PID_ENGINE_COOLANT_TEMP:
			param = ENGINE_COOLANT_TEMP;
			break;
		case OBD_MODE1_PID_ENGINE_RPM:
			param = ENGINE_RPM;
			break;
		case OBD_MODE1_PID_FUEL_INPUT_LEVEL:
			param = FUEL_LEVEL;
			break;
		case OBD_MODE1_PID_RUNTIME_ENGINE_START:
			param = RUNTIME_SINCE_ENGINE_START;
			break;
		case OBD_MODE1_PID_VEHICLE_SPEED:
			param = VEHICLE_SPEED;
			break;
		case OBD_MODE1_PID_TRIP_DISTANCE:
			param = TRIP_DISTANCE;
			break;
		default:
			param = -1;
			break;
	}
	return param;
}
/**
  * @brief  OBD_Query_Vehicle_Parameter
  *         Prepare CAN message to query passed vehicle parameter
	*					Calls OBD_Decode_Vehicle_Parameter after valid data recived
	* @param  parameter: Vehicle parameter
  * @retval None
  */
void OBD_Query_Vehicle_Parameter(uint8_t parameter)
{
	CANMSG txmsg;
	//Prepare CAN Message for transmission
	txmsg.canID = OBD_QUERY_SID;
	txmsg.isExtendedAdrs = 0;
	txmsg.dataLength = 8;
	txmsg.data[0] = OBD_SAE_NUM_ADDITIONAL_DATA;
	txmsg.data[1] = OBD_SAE_MODE1;
	switch(parameter)
	{
		case ENGINE_COOLANT_TEMP:
			txmsg.data[2] = OBD_MODE1_PID_ENGINE_COOLANT_TEMP;
			break;
		case ENGINE_RPM:
			txmsg.data[2] = OBD_MODE1_PID_ENGINE_RPM;
			break;
		case VEHICLE_SPEED:
			txmsg.data[2] = OBD_MODE1_PID_VEHICLE_SPEED;
			break;
		case RUNTIME_SINCE_ENGINE_START:
			txmsg.data[2] = OBD_MODE1_PID_RUNTIME_ENGINE_START;
			break;
		case TRIP_DISTANCE:
			txmsg.data[2] = OBD_MODE1_PID_TRIP_DISTANCE;
			break;
		case AMBIENT_TEMP:
			txmsg.data[2] = OBD_MODE1_PID_AMBIENT_TEMP;
			break;
		case FUEL_LEVEL:
			txmsg.data[2] = OBD_MODE1_PID_FUEL_INPUT_LEVEL;
			break;
		default:
			txmsg.data[2] = 0xFF;
			break;
	}
	//fill other data bytes with 0x55
	txmsg.data[3] = 0x55;
	txmsg.data[4] = 0x55;
	txmsg.data[5] = 0x55;
	txmsg.data[6] = 0x55;
	txmsg.data[7] = 0x55;
	
	//send prepared message
	BSP_CAN_transmitCANMessage(CANB,txmsg,1000);
}

/**
  * @brief  OBD_Decode_data
  *         Check RX buffer for any OBD data and process it
	*	@param  None
  * @retval Current value of Processed parameter
  */
uint32_t OBD_Decode_data(void)
{
	CANMSG rxmsg;
	uint32_t OBD_reponse_value;
	uint8_t OBD_response_value_length;
	uint8_t *data_ptr = &rxmsg.data[3];
	OBD_received_value_param = -1;								//Invalid parameter value. (parameter not supported or no data yet from OBD)
	//check if there is response from CAN
	if(BSP_CAN_receiveCANMessage(CANB,&rxmsg) == ECAN_OK)
	{
		//rxmsg.data[0] contains number of additional data bytes. Next two bytes are mode+0x40 and PID
		OBD_response_value_length = rxmsg.data[0]-2;
		//can receive data for older parameter
		OBD_received_value_param = pidToUserParam(rxmsg.data[2]);
		OBD_reponse_value = *data_ptr;
		while(--OBD_response_value_length)
		{
			OBD_reponse_value = OBD_reponse_value << 8;
			data_ptr++;
			OBD_reponse_value |= (*data_ptr);
		}
		return OBD_Decode_Vehicle_Parameter(OBD_reponse_value,OBD_received_value_param);
	}
	return 0;
}

/**
  * @brief  OBD_Decode_Vehicle_Parameter
  *         Interpret query parameter reponse
	*	@param  obd_response_value: Value data extracted from response
	* @param  parameter: Vehicle parameter
  * @retval Current value of Passed parameter
  */

static uint32_t OBD_Decode_Vehicle_Parameter(uint32_t obd_response_value, uint8_t parameter)
{
	uint32_t decoded_data = 0;
	OBD_param_get_status = OBD_PARAMETER_GET_OK;
	switch(parameter)
	{
		case ENGINE_COOLANT_TEMP:
		case AMBIENT_TEMP:
			decoded_data = obd_response_value-40;					//deg C
			break;
		case ENGINE_RPM:
			decoded_data = obd_response_value/4;					//rpm
			break;
		case FUEL_LEVEL:
			decoded_data = obd_response_value*100.0/255; 	//%	
			break;
		case VEHICLE_SPEED:															//kph
		case RUNTIME_SINCE_ENGINE_START:								//seconds
		case TRIP_DISTANCE:															//km
			decoded_data = obd_response_value;
			break;
		default:
			OBD_param_get_status = OBD_INVALID_PARAMETER;
			break;
	}
	return decoded_data;
}


/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
