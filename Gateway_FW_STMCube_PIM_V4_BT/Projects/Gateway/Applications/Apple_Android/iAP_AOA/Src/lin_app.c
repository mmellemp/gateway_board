/*****************************************************************************
 * File     : lin_app.c
 * Datum    : 4.15.2015
 * Version  : 1.1
 * Author   : ARKA-IMS
 * \file
 * \brief LIN service for Gateway Board .
 *
 * This file contains basic functions for the Gateway LIN, with support for all
 * modes, settings and clock speeds.
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
 
#include "lin_app.h"


/* CheckSum Method */
#define MODE_CLASSIC    2
#define MODE_ENHANCED   1

__IO ITStatus UartReady = RESET;
extern uint8_t linisready;
volatile st_lin_message   lin_descript_list_node0[NUMBER_OF_LIN_FRAMES_NODE0];
// Array of structure of type:'st_lin_message'
//volatile st_lin_message   lin_descript_list_node0[NUMBER_OF_LIN_FRAMES_NODE0];
static void extractLinData(uint8_t* in_data, uint8_t* out_data,uint8_t *checksum);

uint8_t linARecvData[11];
uint8_t linBRecvData[11];
uint8_t linData[8] ;


extern UART_HandleTypeDef   linAHandle,linBHandle;
BOOL bLinDataAvailable = FALSE;
BOOL bLinDataReceived = FALSE;
extern uint8_t slave_resp_size;
static void linTest(void);
uint8_t receivedChecksum;


/*! \brief  This function commands the sending of the LIN header, MASTER task of MASTER node.
 *
 * \param l_node Node Value
 * \param l_id  LIN identifier value. 
 * \param l_len True length 
 * \return Status PASS / FAIL
 *
 */
uint8_t lin_send_cmd ( uint8_t l_node,uint8_t l_id,uint8_t l_len)
{	
	uint8_t msg_frame[11];	
	uint8_t breakData = 0x00; 
	uint8_t index = 0;
	uint8_t l_handle = 0;
	uint8_t tmp=2;
	/**** Synch Field = 0x55 ****/
	msg_frame[0] = 0x55;
	
	if (l_node == 0)
		{
			// Clear error in case of previous communication
			/******* have to do UART RESET********/
			for(index = 0; index < NUMBER_OF_LIN_FRAMES_NODE0; index ++)
				{
					if(lin_descript_list_node0[index].l_id == l_id)
					{
					l_handle = index;
					break;
					}
				}

			if(l_handle != 0xFF)
			{
       #if USART_LIN_VERSION == LIN_2x
					// Configure Parity
					msg_frame[1] = GetParityValue(lin_descript_list_node0[l_handle].l_id);
					// prepare msg_frame
					for(int i=0;i<l_len;i++){
						msg_frame[tmp]=lin_descript_list_node0[l_handle].l_pt_data[i];
						if(tmp<10)
						tmp++;
					  }
					msg_frame[2+l_len]=LIN_makeChecksum(msg_frame,MODE_ENHANCED);

				#elif USART_LIN_VERSION == LIN_1x
					// Configure Parity
					msg_frame[1] = GetParityValue(lin_descript_list_node0[l_handle].l_id);
					// prepare msg_frame
					for(int i=0;i<=l_len;i++){
					msg_frame[i+2]=lin_descript_list_node0[l_handle].l_pt_data[i];
					}
					// prepare Checksum 
					msg_frame[2+l_len]=LIN_makeChecksum(msg_frame,MODE_CLASSIC);

				#endif

				switch (lin_descript_list_node0[l_handle].l_cmd)
					{
					// In Publish, the USART Send the Header and the response
						case PUBLISH:
								 BSP_LIN_IO_Write(LINA,msg_frame,sizeof(msg_frame));	
								 break;
						// In Subscribe, the USART Receive the response
						case SUBSCRIBE:
						      LIN_Master_Query_Slave(LINA,lin_descript_list_node0[l_handle].l_id);
						      HAL_Delay(100);
									if(bLinDataReceived)
										{
											bLinDataReceived = FALSE;
											for (int m=0;m<8;m++){
											    linData[m]=linARecvData[3+m];
											}
											lin_descript_list_node0[l_handle].l_pt_function(linData);
									 if(receivedChecksum == LIN_makeChecksum(linData,MODE_ENHANCED))
											bLinDataAvailable=TRUE;
										}
						      break;
						default:
						      break;
					}
					return FALSE;
			}
			else
					{
					return TRUE;
					}
		}
		else
			{
			return FALSE;
			}
}


/**
  * @brief  Prepare LIN master frame with slave ID to query particular slave. Transmit frame over LIN
  * @param  linType: LIN to be configured.
  *          This parameter can be one of the following values:
  *            @arg  LINA 
  *            @arg  LINB  
  * @param  slaveID: slave Identifier (0x00 to 0x3F)
  * @retval None
  */
void LIN_Master_Query_Slave(LIN_Typedef lintype,uint8_t slaveID)
{
	uint8_t lin_msg_frame[2];
	uint8_t ID;
	if(slaveID <= 0x3F)
	{
		/*Calculate parity for slave ID*/
		ID = GetParityValue(slaveID);		
		/*Determine slave response size from slave ID*/
		if(ID <= 0x1F)
			slave_resp_size = 2;
		else if(ID <= 0x2F)
			slave_resp_size = 4;
		else
			slave_resp_size = 8;
		
		/*Send LIN master frame*/
		lin_msg_frame[0] = 0x55;
		lin_msg_frame[1] = ID;
		BSP_LIN_IO_Write(lintype,lin_msg_frame,2);	
	}
}
/*---------------------------------------------------------------------------------------------------------*/
/* Compute Parity Value                                                                                    */
/*---------------------------------------------------------------------------------------------------------*/

uint8_t GetParityValue(uint8_t id)
{
    uint8_t Res = 0,ID[6],p_Bit[2],mask =0;
   
    for(mask=0;mask<7;mask++)	   
   	    ID[mask] = (id & (1<<mask))>>mask;

    p_Bit[0] = (ID[0] + ID[1] + ID[2] + ID[4])%2;
    p_Bit[1] = (!((ID[1] + ID[3] + ID[4] + ID[5])%2));

    Res = id + (p_Bit[0] <<6) + (p_Bit[1]<<7);
    return Res;
}


/*---------------------------------------------------------------------------------------------------------*/
/* Compute CheckSum Value , MODE_CLASSIC:(Not Include ID)    MODE_ENHANCED:(Include ID)                    */
/*---------------------------------------------------------------------------------------------------------*/
uint8_t LIN_makeChecksum(uint8_t *linBuf,uint32_t ModeSel)
{
  uint8_t ret_wert=0,n;
  uint16_t dummy;
  
  dummy=0;
  for(n=ModeSel;n<=9;n++) {
    dummy+=linBuf[n];
    if(dummy>0xFF) {
      dummy-=0xFF;
    } 
  }
  ret_wert=(uint8_t)(dummy);
  ret_wert^=0xFF;
  
  return(ret_wert);
}




void	BSP_LIN_ReceiveComplete_CallBack(LIN_Typedef linType)
{
	BSP_LED_Toggle(LED2); 
	bLinDataReceived = TRUE;
  UartReady = SET;
	
}

void BSP_LIN_Receive_Error_Callback(LIN_Typedef linType)
{
  BSP_LED_Toggle(LED3); 
}

void BSP_LIN_TransmitComplete_Callback(LIN_Typedef linType)
{
	switch(linType)
	{
		case LINA:	
	      UartReady = SET;
       if(HAL_UART_Receive_DMA(&linAHandle, linARecvData, slave_resp_size+3)!= HAL_OK) 	/*Synch field(0x55) + ID + Response + checksum*/
       {
        BSP_LED_Toggle(LED1);
        }
  		break;
		case LINB:
			 if(HAL_UART_Receive_DMA(&linBHandle, linBRecvData, slave_resp_size+3)!= HAL_OK) 	/*Synch field(0x55) + ID + Response + checksum*/
       {
        BSP_LED_Toggle(LED1);
        }
			break;
		default:
			break;
	}
  BSP_LED_Toggle(LED5);
}


