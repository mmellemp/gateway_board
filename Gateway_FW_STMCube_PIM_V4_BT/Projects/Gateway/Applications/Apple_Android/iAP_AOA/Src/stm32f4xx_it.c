/**
  ******************************************************************************
  * @file    Audio_playback_and_record/Src/stm32f4xx_it.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern HCD_HandleTypeDef hhcd;
/* SAI handler declared in "Gateway_Board_audio.c" file */
//extern SAI_HandleTypeDef haudio_out_sai;

extern I2S_HandleTypeDef  haudio_i2s;


extern UART_HandleTypeDef  linAHandle,linBHandle;
extern UART_HandleTypeDef  CGI_Handle;
extern UART_HandleTypeDef  BT_Handle;

/*Required for handling iAP timeout callbacks*/
BOOL callback_timeout = FALSE;
uint32_t calledTime;
uint32_t timeout = 0;

iAP2Timer_t* __timer;
iAP2TimeCB_t __callback;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*             Cortex-M4 Processor Exceptions Handlers                        */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}



/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  //while (1)
  {
		NVIC_SystemReset();
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  //while (1)
  {
		NVIC_SystemReset();
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function calls callback asynchronously after specified period.
	* @param  delayms: time after which to call callback
	* @param  callback: callback function to call
  * @retval None
  */
BOOL _iAP2TimeCallbackAfter (iAP2Timer_t* timer,
                             uint32_t     delayMs,
                             iAP2TimeCB_t callback)
{
	timeout = delayMs;
	__timer = timer;
	__callback = callback;
	calledTime = HAL_GetTick();
	if(callback_timeout)
	{
		callback_timeout = FALSE;
		(*callback)(__timer,calledTime);
	}
	return TRUE;
}

/**
  * @brief  This function handles SysTick Handler. Callback function called after timeout
  * @param  None
  * @retval None
  */
void SysTick_Handler (void)
{
  HAL_IncTick(); 
	if(timeout != 0)
	{
		if((HAL_GetTick() - calledTime)>timeout)
		{
			callback_timeout = TRUE;
			_iAP2TimeCallbackAfter(__timer,timeout,__callback);
		}
	}
}


/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles USB-On-The-Go FS/HS global interrupt request.
  * @param  None
  * @retval None
  */
/**
  * @brief  This function handles TIM4 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM4_IRQHandler(void)
{
 // HAL_TIM_IRQHandler(&hTimLed);
}

/**
  * @brief  This function handles USB-On-The-Go FS global interrupt request.
  * @param  None
  * @retval None
  */
void OTG_FS_IRQHandler(void)
{
  HAL_HCD_IRQHandler(&hhcd);
}
/**
  * @brief  This function handles USB-On-The-Go HS global interrupt requests.
  * @param  None
  * @retval None
  */

void OTG_HS_IRQHandler(void)
{
  HAL_HCD_IRQHandler(&hhcd);
}



/**
* @brief  This function handles the audio DMA interrupt request.
* @param  None
* @retval None
*/
void AUDIO_I2Sx_DMAx_IRQHandler(void)
{
  //HAL_DMA_IRQHandler(haudio_i2s.hdmatx);
}

/**
  * @brief  This function handles External 2 interrupt request connected to Volune A.
  * @param  None
  * @retval None
  */
void EXTI2_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}
/**
  * @brief  This function handles External 3 interrupt request connected to Select A.
  * @param  None
  * @retval None
  */
void EXTI3_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

/*IRQ haneler for LINB*/
void LINB_IRQ_Handler(void)
{
	HAL_UART_IRQHandler(&linBHandle);
	//HAL_UART_IRQHandler(&BT_Handle);
}


/*IRQ haneler for LINA*/
void LINA_IRQ_Handler(void)
{
	HAL_UART_IRQHandler(&linAHandle);
}

void LINA_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(linAHandle.hdmarx);
}

void LINA_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(linAHandle.hdmatx);
}


void CGI_IRQ_Handler(void)
{
  HAL_UART_IRQHandler(&CGI_Handle);
	buffIndex++; //(buffIndex-1) - Gives the no of bytes received
}

void Bluetooth_IRQ_Handler(void)
{
	HAL_UART_IRQHandler(&BT_Handle);
}


/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
