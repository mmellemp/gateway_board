/**
  ******************************************************************************
  * @file    Ecan_app.c 
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   Tuner Demo Application. 
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

uint8_t debug_result;
uint32_t debug_var;
CANMSG rxMsg;
uint8_t result [100];

void Ecan_Init(void)
{
	debug_result	=	BSP_ECAN_Init();
	//debug_result	=	BSP_ECAN_SetCANBaud(125000,1,3,3,1);
	debug_result = BSP_ECAN_SetCANBaud(500000);
	debug_result	=	BSP_ECAN_ReadReg(MCP2515_CNF1);
	debug_result	=	BSP_ECAN_ReadReg(MCP2515_CNF2);
	debug_result	=	BSP_ECAN_ReadReg(MCP2515_CNF3);
	debug_result	= BSP_ECAN_SetCANMode(MCP2515_MODE_NORMAL,0,0,0,0);
	debug_result	=	BSP_ECAN_ReadReg(MCP2515_CANSTAT);
}

uint8_t Ecan_RereceiveCANMessage(uint8_t *res)
{ 
	uint8_t cc = 0;
	BSP_ECAN_receiveCANMessage(&rxMsg);
	if(rxMsg.dataLength > 0)
	{
			for(cc = rxMsg.dataLength; cc < 8;cc++)
			{
				rxMsg.data[cc] = 0;
			}
		sprintf((char*)res,"ID:%X Len:%d Data:%02X %02X %02X %02X %02X %02X %02X %02X",rxMsg.canID,
																																					rxMsg.dataLength,
																																					rxMsg.data[0],
																																					rxMsg.data[1],
																																					rxMsg.data[2],
																																					rxMsg.data[3],
																																					rxMsg.data[4],
																																					rxMsg.data[5],
																																					rxMsg.data[6],
																																					rxMsg.data[7]);
		
		
	//res = result;
	return strlen(res);
	}
	return 0;
}

uint8_t Ecan_TransmitCANMessage(uint8_t *res)
{
		CANMSG msg;
		msg.canID = 0x11123456;
		msg.isExtendedAdrs = 1;
		msg.rtr = 0;
		msg.dataLength = 8;
		msg.data[0]=0x11;
		msg.data[1]=0x22;
		msg.data[2]=0x33;
		msg.data[3]=0x44;
		msg.data[4]=0x55;
		msg.data[5]=0x66;
		msg.data[6]=0x77;
		msg.data[7]=0x88;
	
		if(BSP_ECAN_transmitCANMessage(msg,1000) != 0)
		{
			sprintf((char*)res,"TX: %d %d %02X %02X %02X %02X %02X %02X %02X %02X",msg.canID,
																																				msg.dataLength,
																																				msg.data[0],
																																				msg.data[1],
																																				msg.data[2],
																																				msg.data[3],
																																				msg.data[4],
																																				msg.data[5],
																																				msg.data[6],
																																				msg.data[7]);
				res = (uint8_t*)result;
				return strlen(result);
		}
		return 0;

}


/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
