/**
  ******************************************************************************
  * @file    iAP2UserImlementaion.c 
  * @author  Arka Team
  * @version V1.0.0
  * @date    17-JAN-2015
  * @brief   This file implements platform specific funstions for iAP2 link layer 
  ******************************************************************************
**/
	
#include "iAP2LinkRunLoop.h"
#include "iAP2Packet.h"
#include "iAP2Link.h"

BOOL b_LinkRunLoopOnce = FALSE;
extern BOOL b_cleanupDone;

/******************************************************************************
User Implementated functions used by iAP2LinkRunLoop
******************************************************************************/


void iAP2LinkRunLoopInitImplementation (iAP2LinkRunLoop_t* linkRunLoop)
{
	linkRunLoop->link->linkConnectionStatus = FALSE;
	b_cleanupDone = FALSE;
}

void iAP2LinkRunLoopCleanupImplementation (iAP2LinkRunLoop_t* linkRunLoop)
{
	/*Stub function*/
}

BOOL iAP2LinkRunLoopWait (iAP2LinkRunLoop_t* linkRunLoop)
{
	/*No waiting*/
	return TRUE;
}

/*maintain flag to indicate wheteher to run iApLinkRunLoopOnce in main while(1)
	This function is called during diffrent events like timeout, etc*/
void iAP2LinkRunLoopSignal (iAP2LinkRunLoop_t* linkRunLoop, void* arg)
{
	if(iAP2LinkRunLoopWait(linkRunLoop))
	{
		b_LinkRunLoopOnce = TRUE;
	}
}

BOOL iAP2LinkRunLoopProtectedCall (iAP2LinkRunLoop_t* linkRunLoop,
                                   void* arg,
                                   BOOL (*func)(iAP2LinkRunLoop_t* linkRunLoop, void* arg))
																		
{
	/*do nothing (straight callback) used when threads are used
	if using single control loop*/
	return (*func)(linkRunLoop, arg);
}																	 


uint32_t iAP2LinkRunLoopGetResetEventMask (iAP2LinkRunLoop_t* linkRunLoop)
{
	uint32_t event_mask = linkRunLoop->eventMask;
	
	//reset event mask
	linkRunLoop->eventMask = 0;
	iAP2LinkRunLoopSetEventMaskBit(linkRunLoop,kiAP2LinkRunLoopEventMaskNone);
	
	//get original event mask
	return event_mask;
}
																	 
void iAP2LinkRunLoopSetEventMaskBit (iAP2LinkRunLoop_t*         linkRunLoop,
                                     iAP2LinkRunLoopEventMask_t bit)
{
	linkRunLoop->eventMask |= bit;
}

void _iAP2TimeCancelCallback (iAP2Timer_t* timer)
{
	if(timer)
		timer = NULL;
}

/*Done in HAL interrupt*/
//BOOL _iAP2TimeCallbackAfter (iAP2Timer_t* timer,
//                             uint32_t     delayMs,
//                             iAP2TimeCB_t callback)
//{
//
//}

void _iAP2TimeCleanupCallback (iAP2Timer_t* timer)
{
	if(timer)
		timer = NULL;
}
	

