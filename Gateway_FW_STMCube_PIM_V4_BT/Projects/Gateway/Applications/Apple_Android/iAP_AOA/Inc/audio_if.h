/**
  ******************************************************************************
  * @file    USB_Device/AUDIO_Standalone/Inc/usbd_audio_if.h
  * @author  MCD Application Team
  * @version V1.0.1
  * @date    26-February-2014
  * @brief   Header for usbd_audio_if.c file.
  ******************************************************************************/ 



/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USBD_AUDIO_IF_H
#define __USBD_AUDIO_IF_H

#include <stdint.h>

/* Audio Commands enmueration */
typedef enum
{
  AUDIO_CMD_START = 1,
  AUDIO_CMD_PLAY,
  AUDIO_CMD_STOP,
}AUDIO_CMD_TypeDef;

typedef struct
{
    int8_t  (*Init)         (uint32_t  AudioFreq, uint32_t Volume, uint32_t options);
    int8_t  (*DeInit)       (uint32_t options);
    int8_t  (*AudioCmd)     (uint8_t* pbuf, uint32_t size, uint8_t cmd);
    int8_t  (*VolumeCtl)    (uint8_t vol);
    int8_t  (*MuteCtl)      (uint8_t cmd);
    int8_t  (*GetState)     (void);
}USBH_AUDIO_ItfTypeDef;

//extern AUDIO_ItfTypeDef AUDIO_Itf_fops; 
//extern USBH_AUDIO_ItfTypeDef USBH_AUDIO_fops; 

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
#endif /* __USBD_AUDIO_IF_H */

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
