/**
  ******************************************************************************
  * @file    iap_accessory_identification.h
  * @author  Arka
  * @version V0.0
  * @brief   This file contains const data for iAP accessory Identification
  ******************************************************************************
  */ 

/*To prevent recursive inclusion*/
#ifndef __IAP_ACCESSORY_CONFIG_H
#define __IAP_ACCESSORY_CONFIG_H

	/* Includes ------------------------------------------------------------------*/
	#include <string.h>
	#include <stdint.h>

	/*Enums-----------------------------------------------------------------------------*/
	/*enum for accessory power providing capability*/
	
	enum 
	{
		POWER_NONE=0,
		POWER_RESERVED,
		POWER_ADVANCED
	};
	

	/*enum for vehicle engine type*/
	enum
	{
		ENGINE_GASOLINE=0,
		ENGINE_DIESEL,
		ENGINE_ELECTRIC,
		ENGINE_CNG
	};

	
	/*enum for accessory's supported audio sample rate*/
	
	enum
	{
		AUDIO_SAMPLE_RATE_8000=0,
		AUDIO_SAMPLE_RATE_11025,
		AUDIO_SAMPLE_RATE_12000,
		AUDIO_SAMPLE_RATE_16000,
		AUDIO_SAMPLE_RATE_22050,
		AUDIO_SAMPLE_RATE_24000,
		AUDIO_SAMPLE_RATE_32000,
		AUDIO_SAMPLE_RATE_44100,
		AUDIO_SAMPLE_RATE_48000
	};
	
	/*Defines---------------------------------------------------------------------*/
	/*Product specific constants*/
	#define IAP_ACCESSORY_NAME																"GATEWAY"				
	#define IAP_ACCESSORY_MODELIDENTIFIER											"GTV100"
	#define IAP_ACCESSORY_MANUFACTURER												"Arka"
	#define IAP_ACCESSORY_SERIAL_NUMBER												"0123456789"				
	#define IAP_ACCESSORY_FIRMWARE_VERSION										"V1.0"							//static string to use during development
	#define IAP_ACCESSORY_HARDWARE_VERSION										"V1.0"							//static string to use during development
	#define IAP_ACCESSORY_POWER_PROVIDING_CAPABILTY						POWER_ADVANCED			/*enum type*/
	#define IAP_ACCESSORY_MAX_CURRENT_DRAWN_FROM_DEVICE				0
	#define IAP_ACCESSORY_CURRENT_LANGUAGE										"en"
	#define IAP_ACCESSORY_SUPPORTED_LANGUAGE									"en"
	#define IAP_ACCESSORY_TRANSPORT_COMPONENT_USB_ID					1
	#define IAP_ACCESSORY_TRANSPORT_COMPONENT_USB_NAME				"USB Accessory"
	#define IAP_ACCESSORY_TRANSPORT_COMPONENT_USB_AUDIO_RATE	AUDIO_SAMPLE_RATE_8000						/*enum type*/
	

	/*Functions and extern variables------------------------------------------------*/
	/*TBD. Why vehicle info is of group type? How can we get it dynamically*/
	#define VEHICLE_NAME																		"CHEVEROLET"
	#define VEHICLE_ENGINE_TYPE															ENGINE_DIESEL

	const char* getAccessoryFirmwareVersion(void);
	const char* getAccessoryHardwareVersion(void);
	const char* getAccessorySerialNumber(void);

	/*This function will write BT MAC address to array accessory_bluetooth_mac_address[]*/
	void getAccessoryBluetoothMACAddress(void);

	extern uint16_t accessory_messages_to_send[];
	extern uint16_t accessory_messages_to_receive[];
	extern uint8_t accessory_bluetooth_mac_address[6];

	extern uint8_t supported_audio_rate;

#endif

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
