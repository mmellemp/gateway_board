/**
  ******************************************************************************
  * @file    obd.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    14-Jan-2015
  * @brief   This header file contains the common defines and functions prototypes
  *          for the obd driver.  
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __OBD_H
#define __OBD_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Component
  * @{
  */ 
    
/** @addtogroup OBD
  * @{
  */ 

#define OBD_QUERY_SID 0x7DF
#define OBD_SAE_NUM_ADDITIONAL_DATA 0x02
#define OBD_NONSAE_NUM_ADDITIONAL_DATA 0x03
	 
#define OBD_SAE_MODE1 0x01
#define OBD_SAE_MODE2 0x02
#define OBD_SAE_MODE3 0x03
#define OBD_SAE_MODE4 0x04
#define OBD_SAE_MODE5 0x05
#define OBD_SAE_MODE6 0x06
#define OBD_SAE_MODE7 0x07
#define OBD_SAE_MODE9 0x09

#define OBD_NONSAE_MODE21 0x21
#define OBD_NONSAE_MODE22 0x22

#define OBD_MODE1_PID_SUPPORTED_GROUP1			0x00
#define OBD_MODE1_PID_SUPPORTED_GROUP2			0x20
#define OBD_MODE1_PID_SUPPORTED_GROUP3  		0x40
#define OBD_MODE1_PID_SUPPORTED_GROUP4			0x60
#define OBD_MODE1_PID_SUPPORTED_GROUP5			0x80
#define OBD_MODE1_PID_SUPPORTED_GROUP6			0xA0
#define OBD_MODE1_PID_SUPPORTED_GROUP7			0xC0

#define OBD_MODE1_PID_ENGINE_COOLANT_TEMP		0x05
#define OBD_MODE1_PID_ENGINE_RPM						0x0C
#define OBD_MODE1_PID_VEHICLE_SPEED					0x0D
#define OBD_MODE1_PID_RUNTIME_ENGINE_START	0x1F
#define OBD_MODE1_PID_TRIP_DISTANCE					0x31
#define OBD_MODE1_PID_AMBIENT_TEMP					0x46
#define OBD_MODE1_PID_FUEL_INPUT_LEVEL			0x2F



	 
	 

/**
  * @}
  */
      
#ifdef __cplusplus
}
#endif

#endif /* __OBD_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
