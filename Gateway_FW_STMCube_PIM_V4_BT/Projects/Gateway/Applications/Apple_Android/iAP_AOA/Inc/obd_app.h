/**
  ******************************************************************************
  * @file    obd.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    14-Jan-2015
  * @brief   This header file contains the common defines and functions prototypes
  *          for the obd driver.  
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __OBD_APP_H
#define __OBD_APP_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
//#include "gateway_board_ecan.h"
#include "gateway_board_can.h"	 
#include "obd.h"
	 
/** @addtogroup BSP
  * @{
  */

/** @addtogroup Component
  * @{
  */ 
    
/** @addtogroup OBD
  * @{
  */ 

void OBD_Init(void);
void OBD_Query_Vehicle_Parameter(uint8_t parameter);
uint32_t OBD_Decode_data(void);
static uint32_t OBD_Decode_Vehicle_Parameter(uint32_t obd_response_value, uint8_t parameter);
	 
typedef enum
{
	ENGINE_RPM = 0,					
	VEHICLE_SPEED,	
	FUEL_LEVEL,
	AMBIENT_TEMP,
	ENGINE_COOLANT_TEMP,
	RUNTIME_SINCE_ENGINE_START,
	TRIP_DISTANCE,
	num_of_veh_param
}OBD_Vehicle_Param_typedef;

enum
{
	OBD_Idle=0,
	OBD_Query,
	OBD_Decode,
	OBD_Send_Data
};

#define OBD_INVALID_PARAMETER 0xFF
#define OBD_PARAMETER_NOT_SUPPORTED 0xFE
#define OBD_PARAMETER_GET_OK 1

extern int8_t OBD_received_value_param;

/**
  * @}
  */
      
#ifdef __cplusplus
}
#endif

#endif /* __OBD_APP_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
