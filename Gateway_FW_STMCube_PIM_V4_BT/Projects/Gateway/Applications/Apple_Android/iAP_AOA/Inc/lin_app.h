//--------------------------------------------------------------
// File     : lin_app.h
//--------------------------------------------------------------

//--------------------------------------------------------------
#ifndef __LIN_APP_H
#define __LIN_APP_H


//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include "gateway_board.h"
#include "gateway_board_lin.h"

#ifndef NUMBER_OF_LIN_FRAMES_NODE0
#define NUMBER_OF_LIN_FRAMES_NODE0    8
#endif // NUMBER_OF_LIN_FRAMES_NODE0

#define USART_LIN_VERSION                   LIN_2x



typedef enum {
  PUBLISH,            //!< The node sends the response
  SUBSCRIBE,          //!< The node receives the response
  IGNORE              //!< The node is not concerned by the response, it doesn't send or receive
} enum_lin_cmd;



/*! @brief This structure allows to handle a LIN message and, at the end of the
 *        received/transmitted message, allows to call a function associated
 *        to this message, the update or the capture of "signals".
 */
typedef  struct {
    //! LIN message ID 
    uint8_t             l_id;

    //! Length of the LIN message, it is the number of data bytes
    //! of the LIN response
    uint8_t             l_dlc;

    //! Select an action for a specific message
    enum_lin_cmd   l_cmd;

    //! Pointer on the data buffer
    uint8_t*            l_pt_data;
	
	   void         (*l_pt_function)(uint8_t*);

} st_lin_message;

uint8_t LIN_makeChecksum(uint8_t *linBuf,uint32_t u32ModeSel);
uint8_t GetParityValue(uint8_t id);
uint8_t lin_send_cmd ( uint8_t l_node,uint8_t l_id,uint8_t l_len);
void LIN_Master_Query_Slave(LIN_Typedef lintype,uint8_t slaveID);

extern  volatile st_lin_message   lin_descript_list_node0[NUMBER_OF_LIN_FRAMES_NODE0];


//--------------------------------------------------------------
// Global Functions
//--------------------------------------------------------------
void LIN_Master_Init(void);






//--------------------------------------------------------------
#endif // 
