/**
  ******************************************************************************
  * @file    iAP2Identification.c
  * @author  Arka Team
  * @version V0.0
  * @date    JAN-2015
  * @brief   This file handles accessory identification.
  ******************************************************************************
  */
	
	/* Includes */
	#include "iAP2Identification.h"
	#include <string.h>
	
	#define __msb(data) (((data) >> 8) & 0x00FF);
	#define __lsb(data) ((data) & 0x00FF)

	uint8_t iAP2_identificationInformation [] = {
    // Name
    0x00,0x0C,0x00,0x00,'G','A','T','E','W','A','Y',0x00,
    // ModelIdentifier
    0x00,0x0B,0x00,0x01,'G','T','V','1','0','0',0x00,
    // Manufacturer
    0x00,0x09,0x00,0x02,'A','r','k','a',0x00,
    // SerialNumber
    0x00,0x0F,0x00,0x03,'0','1','2','3','4','5','6','7','8','9',0x00,
    // FirmwareVersion
    0x00,0x08,0x00,0x04,'1','.','0',0x00,
    // HardwareVersion
    0x00,0x08,0x00,0x05,'1','.','0',0x00,
    // MessagesSentByAccessory
    0x00,0x10,0x00,0x06,0x68,0x00,0x68,0x02,0x68,0x03,0xDA,0x00,0xDA,0x02,0xEA,0x02,
    // MessagesReceivedFromDevice
    0x00,0x0C,0x00,0x07,0x68,0x01,0xDA,0x01,0xEA,0x00,0xEA,0x01,
    // PowerSourceType
    0x00,0x05,0x00,0x08,2,
    // MaximumCurrentDrawnFromDevice
    0x00,0x06,0x00,0x09,0x00, 0x00,
    // CurrentLanguage
    0x00,0x07,0x00,0x0C,'e','n',0x00,
    // SupportedLanguage
    0x00,0x07,0x00,0x0D,'e','n',0x00,
		
		//Supported External Accessory Protocol	
		0x00,0x23,0x00,0x0A,
			//External accessory protocol ID - ID = 10
			0x00,0x05,0x00,0x00,0x0A,
			//External accessory protocol name
			0x00,0x15,0x00,0x01,'c','o','m','.','a','r','k','a','-','i','m','s','.','p','1','0',0x00,
			//External Aceesory app match action
			0x00,0x05,0x00,0x02,0x01,
			
    // USBDeviceTransportComponent
    0x00,0x26,0x00,0x0F,
        // TransportComponentIdentifier (ID = 1)
        0x00,0x06,0x00,0x00,0x00, 0x01,						
        // TransportComponentName
        0x00,0x09,0x00,0x01,'i','A','P','2',0x00,
        // TransportSupportsiAP2Connection
        0x00,0x04,0x00,0x02,
        // USBDeviceSupportedAudioSampleRate
        0x00,0x05,0x00,0x03,0x06,
        // USBDeviceSupportedAudioSampleRate
        0x00,0x05,0x00,0x03,0x07,
        // USBDeviceSupportedAudioSampleRate
        0x00,0x05,0x00,0x03,0x08,
				
    // iAP2HIDComponent
    0x00,0x1A,0x00,0x12,
				// HIDComponentIdentifier2		(ID = 2)
				0x00,0x06,0x00,0x00,0x00, 0x02,
				// HIDComponentName
        0x00,0x0B,0x00,0x01,'K','e','y','p','a','d',0x00,
				// HIDComponentFunction = Keyboard
        0x00,0x05,0x00,0x02,0,				
//		0x00,0x25,0x00,0x12,
//				// HIDComponentIdentifier2		(ID = 3)
//				0x00,0x06,0x00,0x00,0x00,0x03,
//				// HIDComponentName
//        0x00,0x16,0x00,0x01,'A','s','s','i','s','t','i','v','e',' ','c','o','n','t','r','o','l',0x00,
//				// HIDComponentFunction = Assistive touch control
//        0x00,0x05,0x00,0x02,7,
				
};
	
	
	/**
  * @brief  iAP_PrepareIdentificationInformation
  *         Prepare Identification information message packet in response to 
	*					start identification from device  
  * @param  reference to session length to be updated
  * @retval pointer to identification information data created
  */
	uint8_t* iAP_PrepareIdentificationInformation(uint16_t* session_length)
	{
		uint8_t* identification_info = (uint8_t*)malloc(6+sizeof(iAP2_identificationInformation));
		uint8_t* msg_base_addr = identification_info;														
		
		*identification_info = 0x40;						
		*(++identification_info) = 0x40;
		*(++identification_info) = 0;																						//Message length: reassign after calculation
		*(++identification_info) = 0;
		*(++identification_info) = __msb(IAP_MESSAGE_IDENTIFICATION_INFO);
		*(++identification_info) = __lsb(IAP_MESSAGE_IDENTIFICATION_INFO);
		
		memcpy(++identification_info,iAP2_identificationInformation,sizeof(iAP2_identificationInformation));
		identification_info += sizeof(iAP2_identificationInformation);
		
		/*Update total message length*/
		*(msg_base_addr+2) = __msb((uint16_t)(identification_info - msg_base_addr));
		*(msg_base_addr+3) = __lsb((uint16_t)(identification_info - msg_base_addr));
		
		/*Update session length*/
		*session_length = (uint16_t)(identification_info - msg_base_addr);
		
		identification_info = msg_base_addr;
		
		return identification_info;
	}
	
	
/************************ (C) COPYRIGHT ARKA *****END OF FILE****/