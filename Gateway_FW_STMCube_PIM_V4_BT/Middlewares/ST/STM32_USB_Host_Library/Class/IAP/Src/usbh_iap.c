/**
  ******************************************************************************
  * @file    iap_class.c
  * @author  Arka Team
  * @version V0.0
  * @date    JAN-2015
  * @brief   This file is the iAP Handlers for USB Host iAP class.
  ******************************************************************************
  */

/* Includes */
#include "usbh_iap.h"
#include "gateway_board.h"
#include "assert.h"

uint8_t audioBuff[256] = {0};
uint8_t audioBuffdummy[256] = {0};

extern uint8_t dataToApp[64];
extern uint8_t dataFromApp[64];
extern uint8_t dataToAppLen;

uint8_t ccR = 0;
uint8_t ccS = 0;

uint8_t confVal = 0;

uint8_t reportSizeArr[15]={0};						/*Maintain HID report sizes for report ID as index of array*/
uint8_t reportTypeArr[15]={0};						/*Maintain HID report type(input/output) for report ID as index of array*/

uint8_t recv_buf[64] = {0};
uint8_t send_buf[64] = {0};
uint8_t send_buf_len;

iAP2PacketData_t *packetData;
uint16_t packetDataLen;

USBH_HandleTypeDef *pUSBhost_g;
static iAP2LinkRunLoop_t* iAP_link_loop = NULL;

//uint8_t link_buffer[4096] = {0};					/*Sufficient buffer for implementation of link layer*/
uint8_t link_buffer[12288] = {0};					/*Sufficient buffer for implementation of link layer*/

extern BOOL b_LinkRunLoopOnce;

extern uint8_t* cerificate_data;
extern uint16_t certificate_data_length;

extern hid_event_t hid_event;

#define __chArrToInt(addr) (*(addr)<<8) | (*(addr+1))
#define __msb(data) (((data) >> 8) & 0x00FF)
#define __lsb(data) ((data) & 0x00FF)

extern USBH_AUDIO_ItfTypeDef USBH_AUDIO_fops; 

extern uint8_t startOBD;

extern uint8_t hid_keyboard_key;
extern uint8_t DAC_volume_old;

BOOL bAppRunning = FALSE;
BOOL bAudioStreamInitOnce;
BOOL bAudioBuffAllocated;
static BOOL bAudioStateOK = FALSE;

static uint16_t EASessionID;
extern BOOL bTunerInitDone;


/** @addtogroup USBH_LIB
* @{
*/

/** @addtogroup USBH_CLASS
* @{
*/

/** @addtogroup USBH_IAP_CLASS
* @{
*/


/** @defgroup USBH_IAP_CORE 
* @brief    This file includes IAP Layer Handlers for USB Host IAP class.
* @{
*/ 

/** @defgroup USBH_IAP_CORE_Private_TypesDefinitions
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Private_Defines
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Private_Macros
* @{
*/ 
/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Private_Variables
* @{
*/
/**
* @}
*/ 


/** @defgroup USBH_IAP_CORE_Private_FunctionPrototypes
* @{
*/ 

static USBH_StatusTypeDef USBH_IAP_InterfaceInit(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_IAP_InterfaceDeInit(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_IAP_Process (USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_IAP_ClassRequest (USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_IAP_SOFProcess(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef USBH_IAP_FindAudioStreamingIN(USBH_HandleTypeDef *phost);
	
static USBH_StatusTypeDef USBH_IAP_FindTransport(USBH_HandleTypeDef *phost);

static void IAP_ProcessTransmission(USBH_HandleTypeDef *phost);

static void IAP_ProcessReception(USBH_HandleTypeDef *phost);

static void IAP_ProcessAudioStreamIn(USBH_HandleTypeDef *phost);

static USBH_StatusTypeDef IAP_AudioStreamInit(USBH_HandleTypeDef *phost,uint16_t sample_freq);

void  USBH_IAP_AUDIO_Sync(USBH_HandleTypeDef *phost, IAP_AUDIO_OffsetTypeDef offset);

USBH_StatusTypeDef USBH_IAP_GetHIDReportDescriptor (USBH_HandleTypeDef *phost, uint16_t length);

USBH_StatusTypeDef USBH_IAP_GetHIDDescriptor (USBH_HandleTypeDef *phost, uint16_t length);

static void  USBH_IAP_ParseHIDDesc (HID_DescTypeDef *desc, uint8_t *buf);

USBH_StatusTypeDef USBH_IAP_SetReport (USBH_HandleTypeDef *phost,
                                    uint8_t reportType,
                                    uint8_t reportId,
                                    uint8_t* reportBuff,
                                    uint8_t reportLen);

void USBH_IAP_ParseHIDReportDesc(USBH_HandleTypeDef *phost,uint8_t *buf, uint16_t length);																		

USBH_ClassTypeDef  USBH_iap = 
{
  "IAP",
  USB_IAP_CLASS,
  USBH_IAP_InterfaceInit,
	USBH_IAP_InterfaceDeInit,
  USBH_IAP_ClassRequest,
  USBH_IAP_Process,
	USBH_IAP_SOFProcess,
	NULL,
};

IAP_HandleTypeDef IAP_Handler;
/** @defgroup USBH_IAP_CORE_Private_Functions
* @{
*/ 

/**
  * @brief  USBH_IAP_InterfaceInit 
  *         The function init the IAP class.
  * @param  phost: Host handle
  * @retval USBH Status
  */

USBH_StatusTypeDef USBH_IAP_InterfaceInit (USBH_HandleTypeDef *phost)
{	
  USBH_StatusTypeDef status = USBH_FAIL;
  
  IAP_HandleTypeDef *IAP_Handle;
	uint32_t debug_size = sizeof(IAP_HandleTypeDef);
		
	//phost->pActiveClass->pData = &IAP_Handler;//(IAP_HandleTypeDef *)USBH_malloc(sizeof(IAP_HandleTypeDef));
	phost->pActiveClass->pData = (IAP_HandleTypeDef *)USBH_malloc(sizeof(IAP_HandleTypeDef));
	IAP_Handle =  phost->pActiveClass->pData;

	IAP_Handle->state     = IAP_LINK_INIT;
	
	IAP_Handle->data_rx_state = IAP_IDLE;
	IAP_Handle->data_tx_state = IAP_IDLE;
	IAP_Handle->iDeviceInitialized = 0;
	IAP_Handle->dataReceived = 0;
	IAP_Handle->dataSent = 0;
	IAP_Handle->dataReadyToReceive = FALSE;
	IAP_Handle->dataReadyToProcess = FALSE;
	IAP_Handle->bMoreToSend = FALSE;
	IAP_Handle->debug_time = HAL_GetTick();
	IAP_Handle->audio_play_state = PAUSE;
	IAP_Handle->appDataAvailable = FALSE;
	
	bAudioStreamInitOnce = TRUE;
	bAudioBuffAllocated = FALSE;
	bAudioStateOK = FALSE;
	
	IAP_Handle->reportID = 0;					/*set to 0 when not use*/
	
	BSP_LED_Off(LED1);
	BSP_LED_Off(LED2);
	BSP_LED_Off(LED3);
	
	/* Find endpoints */
	
	
	/* Find transport Endpint and Interface which is HID*/
	if(USBH_IAP_FindTransport (phost) ==  USBH_OK)
	{
		if(IAP_Handle->Transport.supported == 1)
    {
       IAP_Handle->Transport.Pipe  = USBH_AllocPipe(phost, IAP_Handle->Transport.Ep);
        
        /* Open pipe for IN endpoint */
        USBH_OpenPipe  (phost,
                        IAP_Handle->Transport.Pipe,
                        IAP_Handle->Transport.Ep,
                        phost->device.address,
                        phost->device.speed,
                        USB_EP_TYPE_INTR,
                        IAP_Handle->Transport.EpSize); 
        
        USBH_LL_SetToggle (phost, IAP_Handle->Transport.Pipe, 0);          

    }
	}
	else
	{
		//USBH_dbgLog ("Audio Stream In not Supported");
	}
	
/* Find Audio Streaming IN Endpint and Interface */
	if(USBH_IAP_FindAudioStreamingIN (phost) ==  USBH_OK)
	{
		if(IAP_Handle->StreamIn.supported == 1)
    {
       IAP_Handle->StreamIn.Pipe  = USBH_AllocPipe(phost, IAP_Handle->StreamIn.Ep);
       /* Open pipe for IN endpoint */
       USBH_OpenPipe  (phost,
                       IAP_Handle->StreamIn.Pipe,
                       IAP_Handle->StreamIn.Ep,
                       phost->device.address,
                       phost->device.speed,
                       USB_EP_TYPE_ISOC,
                       IAP_Handle->StreamIn.EpSize); 
       
       USBH_LL_SetToggle (phost,  IAP_Handle->StreamIn.Pipe, 0);          

    }
	}
	else
	{
		//USBH_dbgLog ("Audio Stream In not Supported");
	}
	
	
	IAP_Handle->ctl_state = IAP_REQ_INIT;

	status = USBH_OK;
	return status;

}

/**
  * @brief  USBH_IAP_InterfaceDeInit 
  *         The function DeInit the Pipes used for the IAP class.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_IAP_InterfaceDeInit (USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	
	/* close Transport transfer pipe */
	if (IAP_Handle->Transport.Pipe)
	{
			USBH_LL_ClosePipe(phost, IAP_Handle->Transport.Pipe);
			USBH_FreePipe  (phost, IAP_Handle->Transport.Pipe);
	    IAP_Handle->Transport.Pipe = 0;     /* Reset the Channel as Free */
	}
	if( IAP_Handle->StreamIn.Pipe != 0x00)
  {   
    USBH_ClosePipe(phost,  IAP_Handle->StreamIn.Pipe);
    USBH_FreePipe  (phost,  IAP_Handle->StreamIn.Pipe);
    IAP_Handle->StreamIn.Pipe = 0;     /* Reset the pipe as Free */  
  }
	if(phost->pActiveClass->pData)
  {
		USBH_free(phost->pActiveClass->pData);
    phost->pActiveClass->pData = 0;
  }
	
	if(iAP_link_loop)
	{
		free(IAP_Handle->AudioStream.buff_ptr);
		iAP2LinkDetached(iAP_link_loop->link);
		iAP2LinkDelete(iAP_link_loop->link);
	}
	
	BSP_LED_Off(LED1);
	BSP_LED_Off(LED2);
	BSP_LED_Off(LED3);
	
  return USBH_OK;
}

/**
* @}
*/ 

/**
* @}
*/

/**
  * @brief  Find IN Audio Streaming interfaces and Endpoints
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_IAP_FindAudioStreamingIN(USBH_HandleTypeDef *phost)
{
  uint8_t interface,endpoint, res;
  USBH_StatusTypeDef status = USBH_FAIL ;
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
 
  /* Look For AUDIOSTREAMING IN interface and Endpints */
  res = 0;
  for (interface = 0;  interface < USBH_MAX_NUM_INTERFACES ; interface ++ )
  {
    if((phost->device.CfgDesc.Itf_Desc[interface].bInterfaceClass == USB_AUDIO_CLASS)&&
       (phost->device.CfgDesc.Itf_Desc[interface].bInterfaceSubClass == USB_SUBCLASS_AUDIOSTREAMING))
    {
      for (endpoint = 0;  endpoint < USBH_MAX_NUM_ENDPOINTS ; endpoint ++ )
      {
				if((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress & 0x80)&&
					 (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize > 0)&&
					 ((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bmAttributes & 0x03) == USBH_EP_ISO))
				{
					IAP_Handle->StreamIn.Ep = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress;
					IAP_Handle->StreamIn.EpSize = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize;
					IAP_Handle->StreamIn.interface = phost->device.CfgDesc.Itf_Desc[interface].bInterfaceNumber;        
					IAP_Handle->StreamIn.AltSettings = phost->device.CfgDesc.Itf_Desc[interface].bAlternateSetting;
					IAP_Handle->StreamIn.supported = 1; 
					res++;
				}
			}
    }
  } 
  
  if(res > 0)
  {  
     status = USBH_OK;
  }
  
  return status;
}

/**
  * @brief  Find HID interfaces and Endpoints
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_IAP_FindTransport(USBH_HandleTypeDef *phost)
{
  uint8_t interface,endpoint, res;
  USBH_StatusTypeDef status = USBH_FAIL ;
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;

  res = 0;   

  /* Look For AUDIOCONTROL  interface */
	for (interface = 0;  interface < USBH_MAX_NUM_INTERFACES ; interface ++ )
  {
    if((phost->device.CfgDesc.Itf_Desc[interface].bInterfaceClass == 0x03)&& /*HID*/
       (phost->device.CfgDesc.Itf_Desc[interface].bInterfaceSubClass == 0x00))
    {
      for (endpoint = 0;  endpoint < USBH_MAX_NUM_ENDPOINTS ; endpoint ++ )
      {
				if((phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress & 0x80)&&
					 (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize > 0)&&
					 (phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bmAttributes == USBH_EP_INTERRUPT))
				{
				
					IAP_Handle->Transport.Ep = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bEndpointAddress;
          IAP_Handle->Transport.EpSize = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].wMaxPacketSize;
          IAP_Handle->Transport.interface = phost->device.CfgDesc.Itf_Desc[interface].bInterfaceNumber;          
          IAP_Handle->Transport.Poll = phost->device.CfgDesc.Itf_Desc[interface].Ep_Desc[endpoint].bInterval;
          IAP_Handle->Transport.supported = 1;
			
					res++;
				}
			}
    }
  } 
  
  if(res > 0)
  {  
     status = USBH_OK;
  }

  return status;
}

/**
  * @brief  USBH_IAP_ClassRequest 
  *         The function is responsible for handling Standard requests
  *         for IAP class.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_IAP_ClassRequest (USBH_HandleTypeDef *phost)
{   
	USBH_StatusTypeDef status         = USBH_BUSY;
  USBH_StatusTypeDef classReqStatus = USBH_BUSY;
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	iAP2PacketSYNData_t linkPacketSyncData;
	/* Switch HID state machine */
  switch (IAP_Handle->ctl_state)
  {
		  
 
		case IAP_REQ_INIT:	
			if (USBH_GetCfg(phost,&confVal) == USBH_OK)		
			{
				IAP_Handle->ctl_state = IAP_REQ_GET_HID_DESC;
			}
			break;
			
		case IAP_REQ_GET_HID_DESC:
    		/* Get HID Desc */ 
				if (USBH_IAP_GetHIDDescriptor (phost, USB_HID_DESC_SIZE)== USBH_OK)
				{
					USBH_IAP_ParseHIDDesc(&IAP_Handle->HID_Desc, phost->device.Data);
					IAP_Handle->ctl_state = IAP_REQ_GET_REPORT_DESC;
				}
				break;
				
		case IAP_REQ_GET_REPORT_DESC:
				/* Get Report Desc */ 
				if (USBH_IAP_GetHIDReportDescriptor(phost, IAP_Handle->HID_Desc.wItemLength) == USBH_OK) 
				{
				/* The decriptor is available in phost->device.Data */
					IAP_Handle->ctl_state = IAP_REQ_SET_INTERFACE;
					IAP_Handle->iDeviceInitialized = 1;
					USBH_IAP_ParseHIDReportDesc(phost,phost->device.Data,IAP_Handle->HID_Desc.wItemLength);
				}
				break;
		case IAP_REQ_SET_INTERFACE:
			if(IAP_Handle->StreamIn.supported == 1)
			{
				if(USBH_SetInterface(phost, IAP_Handle->StreamIn.interface, IAP_Handle->StreamIn.AltSettings) == USBH_OK)
				{
					IAP_Handle->ctl_state = IAP_REQ_IDLE;
					status = USBH_OK;
				}
			}
			break;
		case IAP_REQ_IDLE:
		default:
			break;
	}
		return status;
}

/**
	* @brief Parse HID Report Decriptor to get Report ID and Report Size
	* @param  phost: Host handle
	* @param  buf: Buffer where the source descriptor is available
	* @param  length: Length of the descriptor
	*/

static void USBH_IAP_ParseHIDReportDesc(USBH_HandleTypeDef *phost,uint8_t *buf, uint16_t length)
{
		uint16_t len = 0;
		while(len <= length)
		{
			if(*(uint8_t  *) (buf + len) == 0x85)
			{
				reportSizeArr[(*(uint8_t  *) (buf + len + 1))] = *(uint8_t  *) (buf + len + 3);
				if(*(buf+len+4) == 0x82 || *(buf+len+4) == 0x80)
				{
					reportTypeArr[*(buf + len + 1)] = REPORT_TYPE_INPUT;
				}
				else if(*(buf+len+4) == 0x92 || *(buf+len+4) == 0x90)
				{
					reportTypeArr[*(buf + len + 1)] = REPORT_TYPE_OUTPUT;
				}
				len += 5;  
			}
			else
			{
				len++;
			}
		}
}

/***************************************************** Link related Callback Functions *************************************************************************/
/**
  * @brief  Callback function to send Link packet over Transport. This function will break IAP packet into appropriate sized HID report and send over transport
	*					If device's proposed max number of outstanding packets value is greater than no. of reports to send, send all reports straightaway.
	* @param  Link: Pointer to initialized link structure
	* @param	packet: Actual link packet 
  * @retval None
  */
static void iAP2LinkSendPacketCB(struct iAP2Link_st* link,iAP2Packet_t* packet)
{
	uint16_t packetLength = packet->packetLen;
	uint8_t num_zero_padding = 1;
	uint8_t* send_buf_ptr = send_buf;
	uint8_t num_reports = 0;
	uint8_t last_packet_num_bytes = 0;
	uint8_t current_packet_num = 1;
	uint16_t packet_base_offset = 0;
	uint32_t enter_time = 0;
	
	USBH_HandleTypeDef *phost = getUSBHost();
	IAP_HandleTypeDef *IAP_Handle = phost->pActiveClass->pData;
	
	IAP_Handle->reportID = requiredHIDReportID(packetLength);
	
	num_reports = (packetLength/(reportSizeArr[IAP_Handle->reportID]))+1;		
	IAP_Handle->bMoreToSend = TRUE;
	
	/*Create HID report with calculated report ID*/
	while(IAP_Handle->bMoreToSend)
	{
		if(reportSizeArr[IAP_Handle->reportID] > packetLength)
		{
			//No need to break IAP packet data
			IAP_Handle->bMoreToSend = FALSE;
			*send_buf_ptr = IAP_Handle->reportID;							/*Set report ID doesn't add report ID*/
			++send_buf_ptr;
			*send_buf_ptr = IAP_REPORT_FIRST_AND_LAST;				/*LCB byte*/
			++send_buf_ptr;
			memcpy(send_buf_ptr,processOutboundDataToPort(packet),packetLength);
			
			send_buf_ptr += packetLength;
			num_zero_padding = reportSizeArr[IAP_Handle->reportID] - packetLength - 1;			/*-LCB*/
			while(num_zero_padding--)
			{
				*send_buf_ptr = 0;
				send_buf_ptr++;
			}
		}
		else
		{
			//break IAP packet appropriately		
			if(current_packet_num == 1)
			{
				*send_buf_ptr = IAP_Handle->reportID;
				*(++send_buf_ptr) = IAP_REPORT_FIRST_BUT_NOT_LAST;
				++send_buf_ptr;
				memcpy(send_buf_ptr,processOutboundDataToPort(packet),reportSizeArr[IAP_Handle->reportID]-1);				/*-LCB*/
			}
			else if((current_packet_num > 1) && (current_packet_num < num_reports))
			{
				send_buf_ptr = send_buf;
				packet_base_offset += reportSizeArr[IAP_Handle->reportID]-1;
				*send_buf_ptr = IAP_Handle->reportID;
				*(++send_buf_ptr) = IAP_REPORT_MIDDLE;
				++send_buf_ptr;
				memcpy(send_buf_ptr,processOutboundDataToPort(packet) + packet_base_offset,reportSizeArr[IAP_Handle->reportID]-1);
			}
			else
			{
				send_buf_ptr = send_buf;
				IAP_Handle->bMoreToSend = FALSE;
				last_packet_num_bytes = packetLength - (num_reports-1)*(reportSizeArr[IAP_Handle->reportID]-1); 
				packet_base_offset += reportSizeArr[IAP_Handle->reportID]-1;
				*send_buf_ptr = IAP_Handle->reportID;
				*(++send_buf_ptr) = IAP_REPORT_LAST;
				++send_buf_ptr;
				memcpy(send_buf_ptr,processOutboundDataToPort(packet) + packet_base_offset,last_packet_num_bytes);
				send_buf_ptr += last_packet_num_bytes;
				num_zero_padding = reportSizeArr[IAP_Handle->reportID] - last_packet_num_bytes - 1;										/*-LCB*/
				while(num_zero_padding--)
				{
					*send_buf_ptr = 0;
					send_buf_ptr++;
				}
			}
			
			current_packet_num++;
		}
		
		send_buf_len = reportSizeArr[IAP_Handle->reportID] + 1;	
		IAP_Handle->state = IAP_IDLE_STATE;
		IAP_Handle->data_tx_state = IAP_IDLE;
		enter_time = HAL_GetTick();
		while(USBH_IAP_Transmit(phost,send_buf,send_buf_len) != USBH_OK)
		{
			IAP_ProcessTransmission(phost);
			if((HAL_GetTick() - enter_time) > 10)				/*Timeout of 10ms. Max cum ack timout*/
				return;
		}
		b_LinkRunLoopOnce = FALSE;
	}		
	IAP_Handle->dataReadyToReceive = TRUE;	
}

/**
  * @brief  Callback function to process session data (Already extracted from HID reports received?)
	* @param  Link: Pointer to initialized link structure
	* @param	Data: pointer to Data received 
	* @param	dataLen: Length Data received
	* @param	session: session number
  * @retval TRUE if data is processed and can be discarded.
	*					FALSE if data cannot be processed at this time.
	*	Note: Should be done asynchronously
  */
static BOOL iAP2LinkDataReadyCB (struct iAP2Link_st* link,uint8_t* data,uint32_t dataLen,uint8_t session)
{
	/*Parse report data*/
	USBH_HandleTypeDef *phost = getUSBHost();
	IAP_HandleTypeDef *IAP_Handle = phost->pActiveClass->pData;
	
	/*Check if it is EA data or control session data*/
	if((__chArrToInt(data)) == CTRL_SOM)
	{
		IAP_Handle->session_msg = parseSessionMessage(data,dataLen);
		IAP_Handle->state = IAP_SESSION_HANDLE; 
		return TRUE;
	}
	else
	{
		if((__chArrToInt(data)) == EASessionID)
		{
			IAP_Handle->appDataAvailable = TRUE;
			memcpy(dataFromApp,data+2,dataLen);
			return TRUE;
		}
	}
	return FALSE;
}

/**
  * @brief  Callback function to process when link is up/down
	* @param  Link: Pointer to initialized link structure
	* @param	bConnected: Link status
  * @retval None
  */
static void iAP2LinkConnectedCB (struct iAP2Link_st* link, BOOL bConnected)
{
	if(bConnected)
	{
		link->linkConnectionStatus = TRUE;
		
		USBH_HandleTypeDef *phost = getUSBHost();
		IAP_HandleTypeDef *IAP_Handle = phost->pActiveClass->pData;
		
		/*Store Cum ack timeout to be used when need to send ACK to device*/
		IAP_Handle->link_cum_ack_timeout = link->param.cumAckTimeout;
	}
	else
	{
		link->linkConnectionStatus = FALSE;
	}
}

/**
  * @brief  Callback function to send detect byte sequence(FF 55 02 00 EE 10) on transport.
	* @param  Link: Pointer to initialized link structure
	* @param	bBad: 
  * @retval 
  */
static void iAP2LinkSendDetectCB(struct iAP2Link_st* link, BOOL bBad)
{
	uint8_t detect_byte_seq[7] = {0x00,0xFF,0x55,0x02,0x00,0xEE,0x10};
	USBH_HandleTypeDef *phost = getUSBHost();
	IAP_HandleTypeDef *IAP_Handle = phost->pActiveClass->pData;
	uint32_t enter_time = 0;
	if(!bBad)
	{
		memcpy((uint8_t*)send_buf,detect_byte_seq,7);
		send_buf_len = 7;	
		IAP_Handle->state = IAP_IDLE_STATE;
		IAP_Handle->data_tx_state = IAP_IDLE;
		enter_time = HAL_GetTick();
		while(USBH_IAP_Transmit(phost,send_buf,send_buf_len) != USBH_OK)
		{
			IAP_ProcessTransmission(phost);
			if((HAL_GetTick() - enter_time) > 50)				/*Timeout of 50ms*/
				return;
		}
		IAP_Handle->dataReadyToReceive = TRUE;
	}
}

/**
  * @brief  Callback function to signal a call back into iAP2LinkProcessSendbuff() (asynchronously) to process outgoing data.
	* @param  Link: Pointer to initialized link structure
  * @retval None
  */
static void iAP2LinkSignalSendBuffCB (struct iAP2Link_st* link)
{
}

/**
  * @brief  callback function when data buffer has been sent.
	* @param  Link: Pointer to initialized link structure
	* @param	context:	context to be used for processing main state machine 
  * @retval None
  */
void iAP2LinkDataSentCB(struct iAP2Link_st* link,void* context)
{
	//identification_end_time = HAL_GetTick();
	USBH_HandleTypeDef *phost = getUSBHost();
	IAP_HandleTypeDef *IAP_Handle = phost->pActiveClass->pData;
	
	/*Done with session data. Free it now*/
	free(IAP_Handle->session_data);
	free(IAP_Handle->session_msg);
}
/***************************************************** Link related Callback Functions ends *********************************************************************/

/**
  * @brief  USBH_IAP_Process 
  *         The function is for managing state machine for IAP data transfers 
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_IAP_Process(USBH_HandleTypeDef *phost)
{
	USBH_StatusTypeDef status = USBH_BUSY;
  USBH_StatusTypeDef req_status = USBH_OK;
	
	static BOOL EARequestPending = FALSE;
	static BOOL fromIAP_EA = FALSE;
	
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	
	/*Update USB host handle data for use in callbacks*/
	setUSBHost(phost);
	
	if(IAP_Handle->dataReadyToReceive)
	{
		if(USBH_IAP_Receive(phost, recv_buf, 64 )== USBH_OK)
		{
			processInboundDataFromPort(iAP_link_loop->link,IAP_Handle->recvBuf,sizeof(recv_buf));
			if(!iAP2PacketIsACKOnly(iAP_link_loop->link->recvPck))
			{
				IAP_Handle->dataReadyToReceive = FALSE;
			}
			else																			/*Received data is just acknownledgement of sent data. Don't turn off dataReadyToRecieve Flag*/
			{
				IAP_Handle->bProcessReception = TRUE;
			}
			IAP_Handle->dataReadyToProcess = TRUE;
			b_LinkRunLoopOnce = TRUE;
		}
	}
	
	if(IAP_Handle->state == IAP_EA && !fromIAP_EA)
		EARequestPending = TRUE;
	
	if(fromIAP_EA)
		fromIAP_EA = FALSE;
	
	if(b_LinkRunLoopOnce)
	{
		if(IAP_Handle->state != IAP_SESSION_HANDLE)
		{
			b_LinkRunLoopOnce = FALSE;
			IAP_Handle->state = IAP_LINK_RUN;
		}
		/*If IAP state is to handle session message,
			Need to run link loop later which will in turn call iAP2LinkProcessSendBuf to process session message. Now handle session message received*/
	}
	
	switch(IAP_Handle->state)
  {  
		case IAP_LINK_INIT:
			iAP_link_loop = IAP_Create_Link();
			iAP2LinkRunLoopAttached(iAP_link_loop);
			IAP_Handle->state = IAP_IDLE_STATE;
			status = USBH_OK;
			break;	
		
		case IAP_LINK_RUN:
			if(iAP_link_loop)
			{
				if(IAP_Handle->dataReadyToProcess == FALSE)
				{
					iAP2LinkRunLoopRunOnce(iAP_link_loop,NULL);
					if(EARequestPending)
					{
						EARequestPending = FALSE;
						IAP_Handle->session_data = prepareExternalAcessorySessionDatagram(&IAP_Handle->session_data_length);
						iAP2LinkRunLoopQueueSendData(iAP_link_loop,IAP_Handle->session_data,IAP_Handle->session_data_length,IAP_SESSION2,NULL,iAP2LinkDataSentCB);
					}
				}
				else /*Data ready to process*/
				{
					IAP_Handle->dataReadyToProcess = FALSE;
					iAP2LinkRunLoopRunOnce(iAP_link_loop,iAP_link_loop->link->recvPck);
				}
				if((IAP_Handle->state != IAP_SESSION_HANDLE))
					IAP_Handle->state = IAP_IDLE_STATE;
				if(IAP_Handle->audio_play_state | IAP_Handle->bProcessReception)
					IAP_Handle->data_rx_state = IAP_IDLE;
				if(IAP_Handle->appDataAvailable)
					IAP_Handle->dataReadyToReceive = TRUE;

				status = USBH_OK;
			}
			break;
			
		case IAP_SESSION_HANDLE:
			IAP_Handle_SessionMsg(phost,IAP_Handle->session_msg);
			iAP2LinkRunLoopQueueSendData(iAP_link_loop,IAP_Handle->session_data,IAP_Handle->session_data_length,IAP_SESSION1,NULL,iAP2LinkDataSentCB);
			IAP_Handle->state = IAP_IDLE_STATE;		
			break;
		
		case IAP_EA:		
			/*Send EA session data*/
			if(EARequestPending)
			{
				EARequestPending = FALSE;
				fromIAP_EA = TRUE;
				IAP_Handle->session_data = prepareExternalAcessorySessionDatagram(&IAP_Handle->session_data_length);
				iAP2LinkRunLoopQueueSendData(iAP_link_loop,IAP_Handle->session_data,IAP_Handle->session_data_length,IAP_SESSION2,NULL,iAP2LinkDataSentCB);
				IAP_Handle->bProcessReception = TRUE;
			}
			break;
		
		case IAP_TRANSFER_DATA:
			IAP_ProcessReception(phost);		
			if(IAP_Handle->audio_play_state == PLAY_READY)
				IAP_ProcessAudioStreamIn(phost);
			break; 

			break;
		case IAP_IDLE_STATE:
			status = USBH_OK;
			break;
		
		case IAP_ERROR_STATE:
			req_status = USBH_ClrFeature(phost, 0x00); 
			if(req_status == USBH_OK )
			{        
				/*Change the state to waiting*/
				IAP_Handle->state = IAP_IDLE_STATE ;
			}    
			break;
    
		default:
			break;
    
  }
  
  return status;
}


/**
  * @brief  This function prepares the state for sending data On HID with 
						setreport using Control 
  * @param  phost: Host handle
  * @param  pbuff: pointer of buffer to be send
  * @param  length: length of data to be send
  * @retval None
  */
USBH_StatusTypeDef  USBH_IAP_Transmit(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
{
  USBH_StatusTypeDef Status = USBH_BUSY;
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
  
  if(((IAP_Handle->state == IAP_IDLE_STATE) || (IAP_Handle->state == IAP_TRANSFER_DATA)) && 
		(IAP_Handle->data_tx_state == IAP_IDLE ) && (IAP_Handle->dataSent == 0) ) 
  {
    IAP_Handle->sendbuf = pbuff;
    IAP_Handle->sendbufLen = length;  
		IAP_Handle->state = IAP_TRANSFER_DATA;
		IAP_Handle->data_tx_state = IAP_SEND_DATA; 
    Status = USBH_BUSY;
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif      
  }
	if(IAP_Handle->dataSent == 1) 
	{
		Status = USBH_OK;
		IAP_Handle->dataSent = 0;
	}
  return Status;    
}

 
/**
* @brief  This function prepares the state before issuing the class specific commands
* @param  None
* @retval None
*/
USBH_StatusTypeDef  USBH_IAP_Receive(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
{
  USBH_StatusTypeDef Status = USBH_BUSY;
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
  
  if (((IAP_Handle->state == IAP_IDLE_STATE) || (IAP_Handle->state == IAP_TRANSFER_DATA)) &&
			(IAP_Handle->data_rx_state == IAP_IDLE ) && (IAP_Handle->dataReceived == 0))
	{
    IAP_Handle->recvBuf = pbuff;
    IAP_Handle->recvbufLen = length;  
    IAP_Handle->state = IAP_TRANSFER_DATA;
    IAP_Handle->data_rx_state = IAP_RECEIVE_DATA; 
    Status = USBH_BUSY;
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif        
  }
	if(IAP_Handle->dataReceived == 1)
	{
		Status = USBH_OK;
		IAP_Handle->dataReceived = 0;
	}
  return Status;    
} 


/**********************************************************
									HID Functions
**********************************************************/
/**
* @brief  USBH_Get_HID_ReportDescriptor
  *         Issue report Descriptor command to the device. Once the response 
  *         received, parse the report descriptor and update the status.
  * @param  phost: Host handle
  * @param  Length : HID Report Descriptor Length
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_IAP_GetHIDReportDescriptor (USBH_HandleTypeDef *phost,
                                                         uint16_t length)
{
  
  USBH_StatusTypeDef status;
  
  status = USBH_GetDescriptor(phost,
                              USB_REQ_RECIPIENT_INTERFACE | USB_REQ_TYPE_STANDARD,                                  
                              USB_DESC_HID_REPORT, 
                              phost->device.Data,
                              length);
  
  /* HID report descriptor is available in phost->device.Data.
  In case of USB Boot Mode devices for In report handling ,
  HID report descriptor parsing is not required.
  In case, for supporting Non-Boot Protocol devices and output reports,
  user may parse the report descriptor*/
   
  return status;
}

/**
  * @brief  USBH_Get_HID_Descriptor
  *         Issue HID Descriptor command to the device. Once the response 
  *         received, parse the report descriptor and update the status.
  * @param  phost: Host handle
  * @param  Length : HID Descriptor Length
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_IAP_GetHIDDescriptor (USBH_HandleTypeDef *phost,
                                            uint16_t length)
{
  
  USBH_StatusTypeDef status;
  
  status = USBH_GetDescriptor( phost,
                              USB_REQ_RECIPIENT_INTERFACE | USB_REQ_TYPE_STANDARD,                                  
                              USB_DESC_HID ,
                              phost->device.Data,
                              length);
 
  return status;
}

/**
  * @brief  USBH_HID_Set_Report
  *         Issues Set Report 
  * @param  phost: Host handle
  * @param  reportType  : Report type to be sent
  * @param  reportId    : Targetted report ID for Set Report request
  * @param  reportBuff  : Report Buffer
  * @param  reportLen   : Length of data report to be send
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_IAP_SetReport (USBH_HandleTypeDef *phost,
                                    uint8_t reportType,
                                    uint8_t reportId,
                                    uint8_t* reportBuff,
                                    uint8_t reportLen)
{
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	
	if(IAP_Handle->reportState == REPORT_SEND)
	{
		phost->Control.setup.b.bmRequestType = USB_H2D | USB_REQ_RECIPIENT_INTERFACE | USB_REQ_TYPE_CLASS;    // changed from USB_REQ_RECIPIENT_INTERFACE not working with iphone
	
		phost->Control.setup.b.bRequest = USB_HID_SET_REPORT;
		
		phost->Control.setup.b.wValue.w = (reportType << 8 ) | reportId;

		phost->Control.setup.b.wIndex.w = IAP_Handle->Transport.interface;
		phost->Control.setup.b.wLength.w = reportLen;
		
		IAP_Handle->reportState = REPORT_WAIT;
	}
  
  return USBH_CtlReq(phost, reportBuff , reportLen );
}
/**
  * @brief  USBH_ParseHIDDesc 
  *         This function Parse the HID descriptor
  * @param  desc: HID Descriptor
  * @param  buf: Buffer where the source descriptor is available
  * @retval None
  */
static void  USBH_IAP_ParseHIDDesc (HID_DescTypeDef *desc, uint8_t *buf)
{
  
  desc->bLength                  = *(uint8_t  *) (buf + 0);
  desc->bDescriptorType          = *(uint8_t  *) (buf + 1);
  desc->bcdHID                   =  LE16  (buf + 2);
  desc->bCountryCode             = *(uint8_t  *) (buf + 4);
  desc->bNumDescriptors          = *(uint8_t  *) (buf + 5);
  desc->bReportDescriptorType    = *(uint8_t  *) (buf + 6);
  desc->wItemLength              =  LE16  (buf + 7);
}

/**
* @brief  The function is responsible for sending data to the device
*  @param  pdev: Selected device
* @retval None
*/
static void IAP_ProcessTransmission(USBH_HandleTypeDef *phost)
{
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;
	USBH_StatusTypeDef res = USBH_BUSY;
	
	if(IAP_Handle->data_tx_state == IAP_SEND_DATA)
	{
		if(IAP_Handle->reportState == REPORT_IDLE)
			IAP_Handle->reportState = REPORT_SEND;
		
		res = USBH_IAP_SetReport(phost,USBH_REPORT_OUTPUT,IAP_Handle->reportID,IAP_Handle->sendbuf,IAP_Handle->sendbufLen);
		
		if( res == USBH_OK)
		{
			IAP_Handle->reportState = REPORT_IDLE;
			IAP_Handle->data_tx_state = IAP_IDLE;
			IAP_Handle->dataSent = 1;
		}
	}

}


/**
* @brief  This function responsible for reception of data from the device
*  @param  pdev: Selected device
* @retval None
*/

static void IAP_ProcessReception(USBH_HandleTypeDef *phost)
{
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
  USBH_URBStateTypeDef URB_Status = USBH_URB_IDLE;
  uint16_t length;

  switch(IAP_Handle->data_rx_state)
  {
    
		case IAP_RECEIVE_DATA:
			USBH_InterruptReceiveData (phost,
																IAP_Handle->recvBuf,  
																IAP_Handle->Transport.EpSize, 
																IAP_Handle->Transport.Pipe);
			
			IAP_Handle->data_rx_state = IAP_RECEIVE_DATA_WAIT;
			IAP_Handle->recvTimer = phost->Timer;
    	break;
    
		case IAP_RECEIVE_DATA_WAIT:
      URB_Status = USBH_LL_GetURBState(phost, IAP_Handle->Transport.Pipe); 
			/*Check the status done for reception*/
			if(URB_Status == USBH_URB_DONE )
			{  
      	IAP_Handle->data_rx_state = IAP_IDLE;
				IAP_Handle->dataReceived = 1;
       // USBH_IAP_ReceiveCallback(phost);
			}
			else if(URB_Status == USBH_URB_STALL)
			{
				 /* Issue Clear Feature on interrupt IN endpoint */ 
				if(USBH_ClrFeature(phost,IAP_Handle->Transport.Ep) == USBH_OK)
				{
					IAP_Handle->data_rx_state = IAP_RECEIVE_DATA;
				}
			}
#if (USBH_USE_OS == 1)
      osMessagePut ( phost->os_event, USBH_CLASS_EVENT, 0);
#endif   
			break;
    
		default:
			break;
  }
}


static USBH_StatusTypeDef IAP_AudioStreamInit(USBH_HandleTypeDef *phost,uint16_t sample_freq)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	//static BOOL bAudioBuffAllocated = FALSE;
	if(IAP_Handle->iDeviceInitialized)
	{
		IAP_Handle->offset = IAP_AUDIO_OFFSET_UNKNOWN;
		IAP_Handle->AudioStream.wr_ptr = 0;
		IAP_Handle->AudioStream.rd_ptr = 0;
		IAP_Handle->rd_enable = 0;
		DAC_volume_old = 0;
		
		/*Shailesh*/
		if(!bAudioBuffAllocated)
		{
			IAP_Handle->AudioStream.buff_ptr = (uint8_t*)malloc(AUDIO_TOTAL_BUF_SIZE);
			bAudioBuffAllocated = TRUE;
		}
		IAP_Handle->audioStarted = 1;
		IAP_Handle->streamAudio = 1;
		
    //IAP_Handle->fopsAudioInterface = &AUDIO_Itf_fops;
		IAP_Handle->fopsAudioInterface = &USBH_AUDIO_fops;

		/* Initialize the Audio output Hardware layer */
		if((IAP_Handle->fopsAudioInterface)->Init(sample_freq,AUDIO_DEFAULT_VOLUME,0) != USBH_OK)
		{
      return USBH_FAIL;
    }
		IAP_Handle->StreamInState = IAP_AUDIO_DATA_IN;
	}
		return USBH_OK;	
}


void FillAudioStreamBuff(USBH_HandleTypeDef *phost,uint8_t *buff, uint16_t len)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	uint16_t i = 0,val = 0;
	
	//BSP_LED_Toggle(LED1);
	if(IAP_Handle->AudioStream.wr_ptr + len > AUDIO_TOTAL_BUF_SIZE)
	{
		val = IAP_Handle->AudioStream.wr_ptr + len - AUDIO_TOTAL_BUF_SIZE;
		USBH_memcpy(IAP_Handle->AudioStream.buff_ptr + IAP_Handle->AudioStream.wr_ptr,buff,len-val);
		IAP_Handle->AudioStream.wr_ptr = 0;
		USBH_memcpy(IAP_Handle->AudioStream.buff_ptr + IAP_Handle->AudioStream.wr_ptr,buff+len-val,val);
		IAP_Handle->AudioStream.wr_ptr += val;
		if(IAP_Handle->AudioStream.wr_ptr >= AUDIO_TOTAL_BUF_SIZE)
		{
			IAP_Handle->AudioStream.wr_ptr = 0;
		}
	}
	else
	{
		USBH_memcpy(IAP_Handle->AudioStream.buff_ptr + IAP_Handle->AudioStream.wr_ptr,buff,len);
		IAP_Handle->AudioStream.wr_ptr += len;
		if(IAP_Handle->AudioStream.wr_ptr >= AUDIO_TOTAL_BUF_SIZE)
		{
			IAP_Handle->AudioStream.wr_ptr = 0;
		}
		
	}
	if(IAP_Handle->rd_enable == 0)
	{
		if (IAP_Handle->AudioStream.wr_ptr >= (AUDIO_TOTAL_BUF_SIZE / 2))
		{
			IAP_Handle->rd_enable = 1; 
			/* start Audio DAC this happnes only once*/
			if(IAP_Handle->offset == IAP_AUDIO_OFFSET_UNKNOWN)
			{
				(IAP_Handle->fopsAudioInterface)->AudioCmd(IAP_Handle->AudioStream.buff_ptr,
																										AUDIO_TOTAL_BUF_SIZE/2,
																										IAP_AUDIO_CMD_START);
				IAP_Handle->offset = IAP_AUDIO_OFFSET_NONE;	
				
			}
		}
	}
}

/**
* @brief  This function responsible for reception of Audio stream data from the device
* @param  phost: Host handle
* @retval None
*/
static void IAP_ProcessAudioStreamIn(USBH_HandleTypeDef *phost)
{
  IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	uint16_t length = 0;
	static uint32_t audio_state_OK_time;

  if(IAP_Handle->streamAudio == 1)
	{
		switch(IAP_Handle->StreamInState)
		{
			case IAP_AUDIO_DATA_IN:
				
					USBH_IsocReceiveData(	phost, 
																&audioBuff[0],
																IAP_Handle->StreamIn.EpSize,
																IAP_Handle->StreamIn.Pipe);

					IAP_Handle->StreamInState = IAP_AUDIO_DATA_WAIT;
					IAP_Handle->audioRecvTimer = phost->Timer;
					break;
	 
			case IAP_AUDIO_DATA_WAIT:
				if(USBH_LL_GetURBState(phost, IAP_Handle->StreamIn.Pipe) == USBH_URB_DONE)
				{
					bAudioStateOK = TRUE;
					audio_state_OK_time = HAL_GetTick();
					length = USBH_LL_GetLastXferSize(phost, IAP_Handle->StreamIn.Pipe);
		
					if(length != 0)
					{
						FillAudioStreamBuff(phost,audioBuff,length);
					}
					IAP_Handle->StreamInState = IAP_AUDIO_DATA_IN;
				}
				else
				{
					if(bAudioStateOK)
					{
						if((HAL_GetTick() - audio_state_OK_time) > 200)
						{
							/*problem with audio streaming beacause of possible USB disconnection event which is not captured in USB core. Handle it here*/
							bAudioStateOK = FALSE;
							//USBH_LL_Disconnect(phost);
							NVIC_SystemReset();
						}
					}
				}
				break;
		
			default:
				break;
		}
	}
	else
	{
		audio_state_OK_time = HAL_GetTick();
//		USBH_memset(audioBuffdummy,0xFF,256);
//		FillAudioStreamBuff(phost,audioBuffdummy,256);// for FM 
	}
		
}	


/**
  * @brief  IAP_AUDIO_SOF
	* @param  phost: Host handle
  * @retval status
  */
void  USBH_IAP_AUDIO_Sync(USBH_HandleTypeDef *phost, IAP_AUDIO_OffsetTypeDef offset)
{
  int8_t shift = 0;
	
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	
	IAP_Handle->offset = offset;
	
//	if(IAP_Handle->rd_enable == 1)
	{
		IAP_Handle->AudioStream.rd_ptr += AUDIO_TOTAL_BUF_SIZE/2;

		if (IAP_Handle->AudioStream.rd_ptr == AUDIO_TOTAL_BUF_SIZE)
		{
				/* roll back */
			IAP_Handle->AudioStream.rd_ptr = 0;
		}
	}

		
	if(IAP_Handle->AudioStream.rd_ptr > IAP_Handle->AudioStream.wr_ptr)
	{
		if((IAP_Handle->AudioStream.rd_ptr - IAP_Handle->AudioStream.wr_ptr) < AUDIO_OUT_PACKET)
		{
			//shift = -1 * AUDIO_OUT_PACKET;
			//BSP_LED_Toggle(LED3);
		}
		else if((IAP_Handle->AudioStream.rd_ptr - IAP_Handle->AudioStream.wr_ptr) > (AUDIO_TOTAL_BUF_SIZE - AUDIO_OUT_PACKET))
		{
			//shift = (int8_t)AUDIO_OUT_PACKET;
		//	BSP_LED_Toggle(LED2);
		}    

	}
	else
	{
		if((IAP_Handle->AudioStream.wr_ptr - IAP_Handle->AudioStream.rd_ptr) < AUDIO_OUT_PACKET*2)
		{
			shift = AUDIO_OUT_PACKET;
			//BSP_LED_Toggle(LED1);
		}
		else if((IAP_Handle->AudioStream.wr_ptr - IAP_Handle->AudioStream.rd_ptr) > (AUDIO_TOTAL_BUF_SIZE - AUDIO_OUT_PACKET))
		{
			//shift = -1* AUDIO_OUT_PACKET;
			//BSP_LED_Toggle(LED2);
		}  
	}


  if(IAP_Handle->offset == IAP_AUDIO_OFFSET_FULL)
  {
		
		(IAP_Handle->fopsAudioInterface)->AudioCmd(IAP_Handle->AudioStream.buff_ptr + IAP_Handle->AudioStream.rd_ptr,
																								AUDIO_TOTAL_BUF_SIZE/2 + shift,
																								IAP_AUDIO_CMD_PLAY);
		
		IAP_Handle->offset = IAP_AUDIO_OFFSET_NONE;           
	}
	else if (IAP_Handle->offset == IAP_AUDIO_OFFSET_HALF)
  { 
		(IAP_Handle->fopsAudioInterface)->AudioCmd(IAP_Handle->AudioStream.buff_ptr + IAP_Handle->AudioStream.rd_ptr/2,
																								AUDIO_TOTAL_BUF_SIZE/2,		
																								IAP_AUDIO_CMD_PLAY); 
     IAP_Handle->offset = IAP_AUDIO_OFFSET_NONE;  
      
	}  
			
}


/**
  * @brief  USBH_IAP_getStatus
  *         Return ADK_Machine.state
  * @param  None
  * @retval IAP_Handle->state
  */
IAP_StateTypeDef IAP_GetStatus(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	return IAP_Handle->state;
}

/**
  * @brief  USBH_IAP_getStatus
  *         Return ADK_Machine.state
  * @param  None
  * @retval IAP_Handle->state
  */
uint8_t IAP_GetInitStatus(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	return IAP_Handle->iDeviceInitialized;
}


/**
  * @brief  USBH_IAP_SOFProcess 
  *         The function is for SOF state
  * @param  phost: Host handle
  * @retval USBH Status
  */
static USBH_StatusTypeDef USBH_IAP_SOFProcess(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	if(IAP_Handle->data_rx_state == IAP_RECEIVE_DATA_WAIT)
	{
		if((phost->Timer - IAP_Handle->recvTimer) >= IAP_Handle->Transport.Poll)
		{
			IAP_Handle->data_rx_state = IAP_RECEIVE_DATA;
		}
	}
	if(IAP_Handle->StreamInState == IAP_AUDIO_DATA_WAIT)
	{
		if((phost->Timer - IAP_Handle->audioRecvTimer) >= (IAP_Handle->Transport.Poll+1))
			IAP_Handle->StreamInState = IAP_AUDIO_DATA_IN;
	}
	return USBH_OK;
}

void IAP_SendEADatagram(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	IAP_Handle->state = IAP_EA;
}

void IAP_StopAudioStreaming(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	IAP_Handle->streamAudio = 0;
}

void IAP_StartAudioStreaming(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	IAP_Handle->streamAudio = 1;
}

uint8_t IAP_getAudioStreamingState(USBH_HandleTypeDef *phost)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	return IAP_Handle->streamAudio;
}

/**
  * @brief  IAP_Create_Link 
  *         Function to start Link creation process with defualt params
  * @param  none
  * @retval Initialized link structure
  */
static iAP2LinkRunLoop_t* IAP_Create_Link(void)
{
	iAP2PacketSYNData_t linkPacketSyncData;
	linkPacketSyncData.version = IAP_LINK_VERSION;
	linkPacketSyncData.maxOutstandingPackets = IAP_LINK_MAX_OUTSTANDING_PACKET;
	linkPacketSyncData.maxPacketSize = IAP_MAX_PACKET_SIZE;
	linkPacketSyncData.retransmitTimeout = IAP_RETRANSMISSION_TIMEOUT;
	linkPacketSyncData.cumAckTimeout = IAP_CUMULATIVE_ACK_TIMEOUT;
	linkPacketSyncData.maxCumAck = IAP_MAX_CUMULATIVE_ACKS;
	linkPacketSyncData.maxRetransmissions = IAP_MAX_NUM_OF_RETRANSMISSION;
	linkPacketSyncData.numSessionInfo = 2;							/*control session and External accessory session*/

	linkPacketSyncData.sessionInfo[0].id = IAP_SESSION1;
	linkPacketSyncData.sessionInfo[0].type = IAP_CONTROL_SESSION;
	linkPacketSyncData.sessionInfo[0].version = IAP_CONTROL_SESSION_VERSION;
	
	linkPacketSyncData.sessionInfo[1].id = IAP_SESSION2;
	linkPacketSyncData.sessionInfo[1].type = IAP_EXTERNAL_ACCESSORY_SESSION;
	linkPacketSyncData.sessionInfo[1].version = IAP_EXTERNAL_ACCESSORY_SESSION_VER;
	
	return iAP2LinkRunLoopCreateAccessory(&linkPacketSyncData,NULL,iAP2LinkSendPacketCB,iAP2LinkDataReadyCB,iAP2LinkConnectedCB,iAP2LinkSendDetectCB,FALSE,2,link_buffer);
}

/**
* @brief  setUSBHost
*					set USB host data to use outside IAP process
* @param  host: USB host handler
* @retval None
*/
static void setUSBHost(USBH_HandleTypeDef *host)
{
	pUSBhost_g = host;
}

/**
* @brief  getUSBHost
*					get USB host handler to use outside IAP process
* @param  None
* @retval None
*/
static USBH_HandleTypeDef* getUSBHost(void)
{
	return pUSBhost_g;
}

/**
** processInboundDataFromPort: To create Link packet with received data
** 1) Create an empty packet object by calling iAP2PacketCreateEmpty()
** 2) Call iAP2PacketParseBuffer() with the incoming data and the empty packet
**      (the packet object keeps the parsing state)
** 3) After calling iAP2PacketParseBuffer(), call iAP2PacketIsComplete() to see
**      if a complete packet has been parsed.
**      If a complete packet has been parsed, process the packet and go back
**          to 1) to create a new packet object for parsing the next packet.
**      If a complete packet has NOT been parsed yet, go back to 2) continuing to
**          use the same packet object.
** Call this when received raw data from USB port
*/
static void processInboundDataFromPort(iAP2Link_t* link, uint8_t* input_buff, uint32_t buff_len)
{
	BOOL *iAP2_Support;
	uint32_t parsed_buf_len;
	uint32_t enter_time = HAL_GetTick();
	iAP2Packet_t* packet = iAP2PacketCreateEmptyPacket(link);
	
	do
	{
		parsed_buf_len = iAP2PacketParseBuffer(input_buff,buff_len,packet,IAP_MAX_PACKET_SIZE,iAP2_Support,NULL,NULL);
	}while(!iAP2PacketIsComplete(packet) && ((HAL_GetTick() - enter_time) < 5));
	
	if(!iAP2PacketIsComplete(packet))		/*unable to parse packet in 5ms*/
	{
		iAP2PacketDelete(packet);
		return;																	
	}
	//update packet
	link->recvPck = packet;
}

/**
**	processOutboundDataToPort: To generate checksums for packet data
**	1) Call iAP2PacketGenerateBuffer(); this will generate a flat buffer containing
**      the encoded packet.
** 	2) Use iAP2PacketGetBuffer(packet) and packet->packetLen process the generated
**      encoded packet buffer. (by sending out the port)
**	Get buffer is called by iAP2PacketGenerateBuffer()
**	Call this function before sending packet to port
**/
static uint8_t* processOutboundDataToPort(iAP2Packet_t* packet)
{
	return iAP2PacketGenerateBuffer(packet);
}

/**
* @brief  requiredHIDReportPacketSize
*					calculate appropriate HID report ID based on length of packet to send
*					Things to check report size and report type (input/output)
* @param  len_msb: packet length msb 
*	@param  len_lsb: packet length lsb 
* @retval Appropriate report ID
*/
static uint8_t requiredHIDReportID(uint16_t packet_len)
{
	uint8_t reportSize = 0;				//invalid size
	uint8_t reportID;
	uint8_t reportIDofMaxSize;
	BOOL bPacketLenWithinRange = FALSE;
	uint8_t max_report_size = 0;
	
	/*Find appropriate report packet with size just greater than packet_len and of type output*/
	for(uint8_t report_index = 1; report_index < sizeof(reportSizeArr);report_index++)
	{
		if(reportTypeArr[report_index] != REPORT_TYPE_OUTPUT)
			continue;
		
		if(reportSizeArr[report_index] > max_report_size)
		{
			max_report_size = reportSizeArr[report_index];
			reportIDofMaxSize = report_index;
		}
		if(reportSizeArr[report_index] >= (packet_len+2))					/*Consider LCB*/
		{
			if(bPacketLenWithinRange)		/*Find minimum*/
			{
				if(reportSizeArr[report_index] < reportSize)
				{
					reportSize = reportSizeArr[report_index];
					reportID = report_index;
				}
				continue;
			}
			bPacketLenWithinRange = TRUE;
			reportSize = reportSizeArr[report_index];
			reportID = report_index;
		}
	}
	if(!reportSize)				/*No report size greater than packet len: assign max report size*/
		reportID = reportIDofMaxSize;
	
	return reportID;
}

/**
  * @brief  prepareUSBDevAudioSessionMsg
	*					Prepare Control session message to start USB Device Audio mode
	* @param  Control: start or stop 
	* @param	session_length: reference to session length to be updated
  * @retval return pointer to created session data
  */
static uint8_t* prepareUSBDevAudioSessionMsg(BOOL control,uint16_t* session_length)
{
		uint8_t* usb_audio_msg_ptr = (uint8_t*)malloc(6);
		assert(usb_audio_msg_ptr);
		*usb_audio_msg_ptr = 0x40;
		*(++usb_audio_msg_ptr) = 0x40;
		*(++usb_audio_msg_ptr) = 0x00;
		*(++usb_audio_msg_ptr) = 0x06;
		if(control)
		{
			*(++usb_audio_msg_ptr) = (IAP_MESSAGE_START_USB_DEVICE_MODE_AUDIO >> 8) & 0x00FF;
			*(++usb_audio_msg_ptr) = (IAP_MESSAGE_START_USB_DEVICE_MODE_AUDIO) & 0x00FF;
		}
		else
		{
			*(++usb_audio_msg_ptr) = (IAP_MESSAGE_STOP_USB_DEVICE_MODE_AUDIO >> 8) & 0x00FF;
			*(++usb_audio_msg_ptr) = (IAP_MESSAGE_STOP_USB_DEVICE_MODE_AUDIO) & 0x00FF;
		}
		*session_length = 0x06;
		return usb_audio_msg_ptr-5;
}

static uint8_t* prepareExternalAcessorySessionDatagram(uint16_t* session_length)
{
	USBH_HandleTypeDef *phost = getUSBHost();
	IAP_HandleTypeDef *IAP_Handle = phost->pActiveClass->pData;
	
	uint8_t* session_datagram_ptr = (uint8_t*)malloc(dataToAppLen + 2);
	assert(session_datagram_ptr);
	*session_datagram_ptr = __msb(EASessionID);
	*(++session_datagram_ptr) = __lsb(EASessionID);
	memcpy(++session_datagram_ptr,dataToApp,dataToAppLen);
	
	*session_length = dataToAppLen + 2;

	return session_datagram_ptr-2;
}

/**
  * @brief  prepareRequestAppLaunchSessionMsg
	*					Prepare App launch request session msg
	* @param	session_length: reference to session length to be updated
  * @retval return pointer to created session data
  */
static uint8_t* prepareRequestAppLaunchSessionMsg(uint16_t* session_length)
{
	//const char* app_bundle_id = "com.arka.GmDemoApp";
	const char* app_bundle_id = "com.arka.GatewayDemo";
	uint8_t* app_request_msg_ptr = (uint8_t*)malloc(16+strlen(app_bundle_id));
	assert(app_request_msg_ptr);
	uint8_t* base_addr = app_request_msg_ptr;
	*app_request_msg_ptr = 0x40;
	*(++app_request_msg_ptr) = 0x40;
	*(++app_request_msg_ptr) = 0x00;
	*(++app_request_msg_ptr) = 16 + strlen(app_bundle_id);
	*(++app_request_msg_ptr) = 0xEA;
	*(++app_request_msg_ptr) = 0x02;
	
	*(++app_request_msg_ptr) = 0x00;
	*(++app_request_msg_ptr) = strlen(app_bundle_id)+5;					//+1 of null terminator
	*(++app_request_msg_ptr) = 0x00;
	*(++app_request_msg_ptr) = 0x00;
	strcpy(++app_request_msg_ptr,app_bundle_id);
	app_request_msg_ptr += strlen(app_bundle_id);
	
	*(++app_request_msg_ptr) = 0x00;
	*(++app_request_msg_ptr) = 0x05;
	*(++app_request_msg_ptr) = 0x00;
	*(++app_request_msg_ptr) = 0x01;
	*(++app_request_msg_ptr) = 1;					/*0:Launch with alert. 1:Luanch without alert*/
	
	*session_length = 16+strlen(app_bundle_id);
	return base_addr;
}

/**
  * @brief  parseSessionMessage
	*					Parse session message received from device Link
	* @param  Packet: received packet
  * @retval return pointer to parsed control session message
  */
static iAPControlSessionMsg_st* parseSessionMessage(uint8_t* data, uint8_t data_length)
{
	iAPControlSessionMsg_st *msg = malloc(sizeof(iAPControlSessionMsg_st));
	assert(msg);
	
	msg->msg_parameter = malloc(sizeof(iAPControlSessionMsgParam_st));
	assert(msg->msg_parameter);
	
	msg->SOM = __chArrToInt(data);
	data += 2;
	msg->msg_length = __chArrToInt(data);
	data += 2;
	msg->msg_id = __chArrToInt(data);
	data += 2;
	data_length -= 6;
	if(data_length)
	{
		/*parse message parameters*/
		msg->msg_parameter->param_length = __chArrToInt(data);
		data += 2;
		msg->msg_parameter->param_id = __chArrToInt(data);
		data += 2;
		msg->msg_parameter->param_data = data;
	}
	return msg;
}

/**
  * @brief  IAP_Handle_SessionMsg
	*					Process Control session message and take appropriate action.
	* @param	phost: USB host handler
	* @param  msg: Control session message
  * @retval None
  */
static void IAP_Handle_SessionMsg(USBH_HandleTypeDef *phost, iAPControlSessionMsg_st* msg)
{
	IAP_HandleTypeDef *IAP_Handle =  phost->pActiveClass->pData;
	uint8_t sample_freq_id;
	uint16_t sample_freq = 0;
	static BOOL bLaunchApp = TRUE;
	//static BOOL bAudioStreamInitOnce = TRUE;
	switch(msg->msg_id)
	{
		case IAP_MESSAGE_AUTHENTICATION_REQUEST_CERTIFICATE:
			if(iAP_CP_CommInit() == CP_OK)
			{
				IAP_Handle->session_data = iAP_prepareAuthenticationCertificate(&certificate_data_length);
				IAP_Handle->session_data_length = certificate_data_length;
			}
			else
				assert(0);
			break;
		
		case IAP_MESSAGE_AUTHENTICATION_REQUEST_CHALLENGE_RESPONSE:
			IAP_Handle->session_data = iAP_respondToChallengeRequest(msg->msg_parameter->param_length - 4,msg->msg_parameter->param_data,&IAP_Handle->session_data_length);			/*Actual challenge data length -4bytes(param length and ID)*/
			break;
		
		case IAP_MESSAGE_AUTHENTICATION_SUCCEEDED:
			/*Send ack to Device. Make deliberate timeout of 'cumulative ack timeout', so that ack send in link loop*/
			IAP_Handle->session_data = NULL;									/*No session data to send*/
			HAL_Delay(IAP_Handle->link_cum_ack_timeout + 1);
			break;
		
		case IAP_MESSAGE_AUTHENTICATION_FAILED:
			/*Notify of error*/
			assert(0);
			break;
		
		case IAP_MESSAGE_IDENTIFICATION_START:
			IAP_Handle->session_data  = iAP_PrepareIdentificationInformation(&IAP_Handle->session_data_length);
			break;
		
		case IAP_MESSAGE_IDENTIFICATION_REJECTED:
			/*Notify of error*/
			assert(0);
			break;
		
		case IAP_MESSAGE_IDENTIFICATION_ACCEPTED:
			/*Audio streaming request*/
			IAP_Handle->session_data = prepareUSBDevAudioSessionMsg(START,&IAP_Handle->session_data_length);
			IAP_Handle->audio_play_state = PLAY_WAIT;
		
			IAP_Handle->debug_time = HAL_GetTick();									/*Debug time to launch app*/
			BSP_LED_On(LED1);
			break;
		
		case IAP_MESSAGE_USBDEVMODE_AUDIO_INFO:
 			sample_freq_id = *(msg->msg_parameter->param_data);
			if(sample_freq_id == 6)
				sample_freq = 32000;
			else if(sample_freq_id == 7)
				sample_freq = 44100;
			else if(sample_freq_id == 8)
				sample_freq = 48000;
			
			/*Known Issue: Need to resolve not playing for 48Kz*/
			//IAP_AudioStreamInit(phost,sample_freq);
			if(bAudioStreamInitOnce)
			{
				bAudioStreamInitOnce = FALSE;
				IAP_AudioStreamInit(phost,44100);
			}
			IAP_Handle->audio_play_state = PLAY_READY;
			
			if(bLaunchApp)
			{
				bLaunchApp = FALSE;		
				IAP_Handle->session_data = prepareRequestAppLaunchSessionMsg(&IAP_Handle->session_data_length);
			}
			else
			{
				/*Send Ack for USB dev audio info*/
				IAP_Handle->session_data = NULL;
				HAL_Delay(IAP_Handle->link_cum_ack_timeout + 1);
			}
			break;
			
		case IAP_MESSAGE_START_EXTERNAL_ACCESSORY_SESSION:
			/*Get Session ID App is using for EA seession. Dynamically generated*/
			EASessionID = __chArrToInt(msg->msg_parameter->param_data + 5);
			bAppRunning = TRUE;
		
			/*Send Ack for this*/
			IAP_Handle->session_data = NULL;
			HAL_Delay(IAP_Handle->link_cum_ack_timeout + 1);
			break;
		
		case IAP_MESSAGE_STOP_EXTERNAL_ACCESSORY_SESSION:
			/*Send Ack for this*/
			IAP_Handle->session_data = NULL;
			HAL_Delay(IAP_Handle->link_cum_ack_timeout + 1);
		
			/*App closed*/
			startOBD = 0;
			bAppRunning = FALSE;
			break;
		
		default:
			break;
	}
}

/*User defined application specific callbacks*/
static void callCB(IAP_Process_ApplicationCB_t cb)
{
	(*cb)();
}

/************************ (C) COPYRIGHT ARKA *****END OF FILE****/
