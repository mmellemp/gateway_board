/**
  ******************************************************************************
  * @file    usbh_iap.h
  * @author  Arka
  * @version V0.0
  * @brief   This file contains all the prototypes for the usbh_iap.c
  ******************************************************************************
  */ 

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef __USBH_IAP_CORE_H
#define __USBH_IAP_CORE_H

/* Includes ------------------------------------------------------------------*/
#include "usbh_ioreq.h"
#include "usbh_ctlreq.h"
#include "usbh_core.h"
#include "audio_if.h"
#include "iAP2Link.h"
#include "iAP2Session.h"
#include "iAP2LinkRunLoop.h"
#include "iAP2Authentication.h"
#include "iAP2Identification.h"

/** @addtogroup USBH_LIB
* @{
*/

/** @addtogroup USBH_CLASS
* @{
*/

/** @addtogroup USBH_IAP_CLASS
* @{
*/

/** @defgroup USBH_IAP_CORE
* @brief This file is the Header file for USBH_IAP_CORE.c
* @{
*/ 
/*IAP Class codes*/
#define USB_IAP_CLASS												0x01 /*vendor specific*/
#define USB_AUDIO_CLASS       							0x01
#define USB_SUBCLASS_AUDIOSTREAMING					0x02
#define USB_HID_CLASS       								0x03
#define USB_ACCESSORY_CLASS       					0xFF
#define USB_SUBCLASS_ACCESSORY							0xFF

#define USB_HID_GET_REPORT           0x01
#define USB_HID_GET_IDLE             0x02
#define USB_HID_GET_PROTOCOL         0x03
#define USB_HID_SET_REPORT           0x09
#define USB_HID_SET_IDLE             0x0A
#define USB_HID_SET_PROTOCOL         0x0B 

/*iAP Link parameters*/
#define IAP_LINK_VERSION						 				1
#define IAP_LINK_MAX_OUTSTANDING_PACKET			5			
#define IAP_MAX_PACKET_SIZE									512					
#define IAP_RETRANSMISSION_TIMEOUT					2000
#define IAP_CUMULATIVE_ACK_TIMEOUT					10
#define IAP_MAX_NUM_OF_RETRANSMISSION				20
#define	IAP_MAX_CUMULATIVE_ACKS							3


/**
  * @}
  */ 

/** @defgroup USBH_IAP_CORE_Exported_Types
* @{
*/ 


/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Defines
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Macros
* @{
*/ 

/**
* @}
*/ 

/** @defgroup USBH_IAP_CORE_Exported_Variables
* @{
*/ 
extern USBH_ClassTypeDef  USBH_iap;
#define USBH_IAP_CLASS    &USBH_iap

extern USBH_ClassTypeDef  USBH_aoa;
#define USBH_AOA_CLASS    &USBH_aoa

#define AUDIO_DEFAULT_VOLUME                          85

#define TUNER_VOL 75

#define AUDIO_OUT_PACKET                              (uint32_t)(((USBD_AUDIO_FREQ * 2 * 2) /1000)) 
    
/* Number of sub-packets in the audio transfer buffer. You can modify this value but always make sure
  that it is an even number and higher than 3 */
#define AUDIO_OUT_PACKET_NUM                           20
/* Total size of the audio transfer buffer */
#define AUDIO_TOTAL_BUF_SIZE                           ((uint32_t)(AUDIO_OUT_PACKET * AUDIO_OUT_PACKET_NUM))

#define START																					1
#define	STOP																					0

typedef void (*IAP_Process_ApplicationCB_t) (void);

/*Enum for audio play state*/
typedef enum
{
	PAUSE = 0,
	PLAY_WAIT,
	PLAY_READY
}audio_play_state_t;

/* States for IAP State Machine */
typedef enum
{
  IAP_IDLE= 0,
  IAP_SEND_DATA,
  IAP_SEND_DATA_WAIT,
  IAP_RECEIVE_DATA,
  IAP_RECEIVE_DATA_WAIT,
}
IAP_DataStateTypeDef;

typedef enum
{
  IAP_HID_IDLE= 0,
  IAP_HID_SEND_EVENT,
  IAP_HID_SEND_EVENT_WAIT,
}
IAP_HIDEventStateTypeDef;

typedef enum
{
	IAP_LINK_INIT = 0,
	IAP_LINK_RUN,								
	IAP_SESSION_HANDLE,
  IAP_IDLE_STATE,
  IAP_TRANSFER_DATA,
	IAP_EA,
	IAP_HID,
	IAP_APP_LAUNCH,
  IAP_ERROR_STATE,  
}
IAP_StateTypeDef;

typedef enum
{
 IAP_AUDIO_DATA_IN = 1,  
 IAP_AUDIO_DATA_WAIT,
}
IAP_AUDIO_ProcessingTypeDef;

typedef enum
{
  IAP_AUDIO_OFFSET_NONE = 0,
  IAP_AUDIO_OFFSET_HALF,
  IAP_AUDIO_OFFSET_FULL,  
  IAP_AUDIO_OFFSET_UNKNOWN,    
}
IAP_AUDIO_OffsetTypeDef;

/* Audio Commands enmueration */
typedef enum
{
  IAP_AUDIO_CMD_START = 1,
  IAP_AUDIO_CMD_PLAY,
  IAP_AUDIO_CMD_STOP,
}IAP_AUDIO_CMD_TypeDef;

typedef struct
{
	//uint8_t	 		buff[AUDIO_TOTAL_BUF_SIZE];
	uint8_t*    buff_ptr;
	uint16_t    rd_ptr;  
  uint16_t    wr_ptr;
}
IAP_AUDIO_BuffTypeDef;

typedef struct
{

  uint8_t              Ep;
  uint16_t             EpSize; 
  uint8_t              interface; 
  uint8_t              AltSettings;
  uint8_t              supported;    
  uint8_t              Pipe;  
	//AUDIO_ControlAttributeTypeDef attribute;  
}
IAP_AUDIO_STREAMING_IN_HandleTypeDef;

typedef struct
{
  uint8_t              Ep;
  uint16_t             EpSize; 
	uint8_t              Pipe;
	uint8_t							 *pRxData;
	uint32_t						 RxLen;
	uint8_t              supported; 
}
IAP_DATA_IN_HandleTypeDef;

typedef struct
{

  uint8_t              Ep;
  uint16_t             EpSize; 
	uint8_t              Pipe; 	
	uint8_t							 *pTxData;
	uint32_t						 TxLen;
	uint8_t              supported; 
}
IAP_DATA_OUT_HandleTypeDef;

typedef struct
{

  uint8_t              Ep;
  uint16_t             EpSize; 
  uint8_t              interface; 
  uint8_t              supported; 
  
  uint8_t              Pipe;  
  uint8_t              Poll; 
  uint32_t             timer ; 
}
IAP_TRANSPORT_HandleTypeDef;

typedef enum 
{
  REPORT_IDLE =0,
  REPORT_SEND,
  REPORT_WAIT
} REPORT_StateTypeDef; 

typedef enum
{
  IAP_REQ_INIT = 0,
  IAP_REQ_IDLE, 
  IAP_REQ_GET_REPORT_DESC,
  IAP_REQ_GET_HID_DESC,
  IAP_REQ_SET_IDLE,
  IAP_REQ_SET_PROTOCOL,
  IAP_REQ_SET_REPORT,
	IAP_REQ_SET_INTERFACE
}
IAP_CtlStateTypeDef;

typedef enum 
{
	REPORT_TYPE_INPUT=1,
	REPORT_TYPE_OUTPUT
}IAP_HID_Report_Type_t;

enum
{
	IAP_REPORT_FIRST_AND_LAST = 0,
	IAP_REPORT_LAST,
	IAP_REPORT_FIRST_BUT_NOT_LAST,
	IAP_REPORT_MIDDLE
};

typedef enum
{
	hid_btn_released = 0,
	hid_menu = 1,
	hid_previous = 2,
	hid_play_pause = 4,
	hid_next = 8,
}hid_event_t;

typedef struct _HIDDescriptor
{
  uint8_t   bLength;
  uint8_t   bDescriptorType;
  uint16_t  bcdHID;               /* indicates what endpoint this descriptor is describing */
  uint8_t   bCountryCode;        /* specifies the transfer type. */
  uint8_t   bNumDescriptors;     /* specifies the transfer type. */
  uint8_t   bReportDescriptorType;    /* Maximum Packet Size this endpoint is capable of sending or receiving */  
  uint16_t  wItemLength;          /* is used to specify the polling interval of certain transfers. */
}
HID_DescTypeDef;

/* Structure for IAP process */
typedef struct _IAP_Process
{
	HID_DescTypeDef      										HID_Desc;

	uint8_t																	audioStarted;
	IAP_AUDIO_BuffTypeDef										AudioStream;	
  uint8_t             										rd_enable;
  IAP_AUDIO_OffsetTypeDef       					offset;
 
	
	IAP_AUDIO_STREAMING_IN_HandleTypeDef 		StreamIn;
	IAP_AUDIO_ProcessingTypeDef					 		StreamInState;	
	
  IAP_CtlStateTypeDef  										ctl_state;

	IAP_StateTypeDef          							state;
  IAP_DataStateTypeDef      							data_tx_state;
  IAP_DataStateTypeDef      							data_rx_state;

	IAP_TRANSPORT_HandleTypeDef							Transport;
	uint32_t 																recvTimer;
	uint32_t 																audioRecvTimer;
	
	uint8_t																	*recvBuf;
	uint8_t																	*sendbuf;
	uint16_t																sendbufLen;
	uint16_t																recvbufLen;
	REPORT_StateTypeDef											reportState;
	
	IAP_HIDEventStateTypeDef								hidEvent_tx_state;
	uint8_t 																hidEvent;
	uint8_t  																dataSent;
	uint8_t  																dataReceived;
	uint8_t  																hidEventSent;
	uint8_t																	streamAudio;
	uint8_t																	iDeviceInitialized;
	
	USBH_AUDIO_ItfTypeDef 									*fopsAudioInterface;
	uint8_t		   														protocol;
	
	BOOL																		dataReadyToReceive;
	BOOL																		dataReadyToProcess;
	BOOL																		bMoreToSend;
	
	BOOL																		bProcessReception;
	
	uint8_t																	reportID;
	
	iAPControlSessionMsg_st*								session_msg;
	uint8_t*																session_data;
	uint16_t																session_data_length;
	
	uint32_t 																debug_time;
	uint32_t 																link_cum_ack_timeout;
	
	audio_play_state_t											audio_play_state;
	
	BOOL 																		appDataAvailable;
}
IAP_HandleTypeDef;




/**
* @}
*/ 

IAP_StateTypeDef IAP_GetStatus(USBH_HandleTypeDef *phost);



USBH_StatusTypeDef  USBH_IAP_Transmit(USBH_HandleTypeDef *phost, 
                                      uint8_t *pbuff, 
                                      uint32_t length);


USBH_StatusTypeDef  USBH_IAP_Receive(USBH_HandleTypeDef *phost, 
                                     uint8_t *pbuff, 
                                     uint32_t length);
																		 
																		 

USBH_StatusTypeDef  USBH_IAP_ReceiveAudio(USBH_HandleTypeDef *phost);
uint16_t USBH_IAP_GetLastReceivedDataSize(USBH_HandleTypeDef *phost);
void  USBH_IAP_AUDIO_Sync(USBH_HandleTypeDef *phost, IAP_AUDIO_OffsetTypeDef offset);
USBH_StatusTypeDef  USBH_IAP_SendHIDEvent(USBH_HandleTypeDef *phost,uint8_t keyID);
void IAP_StartAudioStreaming(USBH_HandleTypeDef *phost);
void IAP_StopAudioStreaming(USBH_HandleTypeDef *phost);
uint8_t IAP_getAudioStreamingState(USBH_HandleTypeDef *phost);
uint8_t IAP_GetInitStatus(USBH_HandleTypeDef *phost);
static void USBH_IAP_ParseHIDReportDesc(USBH_HandleTypeDef *phost,uint8_t *buf, uint16_t length);

static iAP2LinkRunLoop_t* IAP_Create_Link(void);
static void setUSBHost(USBH_HandleTypeDef *host);
static USBH_HandleTypeDef* getUSBHost(void);
static void processInboundDataFromPort(iAP2Link_t* link, uint8_t* input_buff, uint32_t buff_len);
static uint8_t* processOutboundDataToPort(iAP2Packet_t* packet);
static uint8_t requiredHIDReportID(uint16_t packet_len);
static void IAP_Handle_SessionMsg(USBH_HandleTypeDef *phost, iAPControlSessionMsg_st* msg);
static uint8_t* prepareUSBDevAudioSessionMsg(BOOL control,uint16_t* session_length);
static uint8_t* prepareExternalAcessorySessionDatagram(uint16_t* session_length);
static uint8_t* prepareRequestAppLaunchSessionMsg(uint16_t* session_length);
static uint8_t* prepareStartHIDSessionMsg(uint16_t* session_length);
void IAP_SendEADatagram(USBH_HandleTypeDef *phost);
static uint8_t* prepareHIDSessionMsg(uint16_t* session_length,uint8_t hid_event);
static void callCB(IAP_Process_ApplicationCB_t cb);



/**
* @}
*/ 


#endif /* __USBH_IAP_CORE_H */

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 

/**
* @}
*/ 
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

