
/**
  ******************************************************************************
  * @file    si4703.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    22-July-2014
  * @brief   This file provides the si4703 FM Tuner driver.
  ******************************************************************************
	* July 22			Prototype
	*
	*	July 25			First stable version
	*
	*
	*
	*/

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "si4743.h"
#include "stm32f4xx_hal.h"

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Components
  * @{
  */

/** @addtogroup si4703
  * @brief     This file provides a set of functions needed to drive the
  *            si4703 FM Tuner.
  * @{
  */

/** @defgroup si4703_Private_Types
  * @{
  */

/** @defgroup si4703_Private_Defines
  * @{
  */
/* Uncomment this line to enable verifying data sent to codec after each write
   operation (for debug purpose) */
#if !defined (VERIFY_WRITTENDATA)
 #define VERIFY_WRITTENDATA
#endif /* VERIFY_WRITTENDATA */

/** @defgroup si4703_Private_Macros
  * @{
  */


/** @defgroup si4703_Private_Variables
  * @{
 *  
  */


uint8_t   Si4743_rx_buf[10];
uint8_t   Si4743_tx_buf[10];

static uint32_t channels_seek_start_time;

static uint32_t ch_updated[MAX_CHANNELS+1] = {0};

/* FM Tuner driver structure initialization */
TUNER_DrvTypeDef si4743_drv =
{
  si4743_Init,
  0,
  si4743_Start,
  si4743_Stop,
  si4743_Tune,
  si4743_Seek,
  si4743_SetVolume,
  si4743_SetMute, 
  si4743_GetChannelList
};


static uint8_t IsPowerOn = 0;
extern uint32_t channels[MAX_CHANNELS+1], ch_updated[MAX_CHANNELS+1];


/** @defgroup si4703_Function_Prototypes
  * @{
  */
static uint8_t FM_IO_Write(uint8_t Addr, uint8_t Reg, uint16_t Value, uint8_t Set_reset);
static uint8_t FM_IO_Read(uint8_t Addr, uint8_t Cmd);
static void FM_ModifyShadowReg(uint8_t Reg, uint16_t Value, uint8_t Set_reset);

/**
  * @}
  */

/** @defgroup si4703_Private_Functions
  * @{
  */
/**
  * @brief Modifies Shadow Registers.
  * @note Recommended to simplify manipulation of register bits
	*       and to reduce the number of reads/writes to the I/O bus
  * @param Reg: Register to be modified.
	* @param Value: Data to be written.
	* @param Set_Reset: Set or Reset option.
	* @retval none
  */
static void FM_ModifyShadowReg(uint8_t Reg, uint16_t Value, uint8_t Set_Reset)
{

}
/**
  * @brief  Writes Multiple reg.
	* @note 	For Si4703 write always start from Reg address 02h high byte to 0Fh lower byte
	*					then automatically wraps around upper Higher byte of 00h and continues till stop condition(Circular way)
	*					Before Writing the registers is Updated in shadow reg then shadow reg are written
  * @param  Reg: Reg to write
	* @param  Addr: Device Address
  * @param  Value: Data to be written
  * @param  Set_reset: Set or reset value in shadow registers
  * @retval None
  */

static uint8_t FM_IO_Write(uint8_t Addr,uint8_t Reg, uint16_t Value, uint8_t Set_reset)
{
	return 0;
}

/**
  * @brief  Read Multiple reg.
	* @note 	For Si4703 read always start from Reg address 0Ah high byte to 0Fh lower byte
	*					then automatically wraps around upper Higher byte of 00h and continues till stop condition(Circular way)
	* @param  Addr: Device Address
  * @param  Com: FM_ALL_REG_4743 or FM_0A0B_REG
  * @retval None
  */
static uint8_t FM_IO_Read(uint8_t Addr, uint8_t Cmd)
{
  return 0;
}

/**
  * @brief Initializes the FM Tuner and the control interface.
  * @param DeviceAddr: Device address on communication Bus.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4743_Init(uint16_t DeviceAddr)
{
	/* Initialize the Control interface of the FM Tuner */
	TUNER_IO_Init(); 
}

/**
  * @brief Start the FM Tuner after configuring Registers.
  * @note Activate and power Up Si4743.
  * @param DeviceAddr: Device address on communication Bus.
	* @param Volume: Volune in range of 0 to 16 .
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4743_Start(uint16_t DeviceAddr, uint8_t Volume)
{
	clear_buff();
	uint32_t start_time;

	 /* Power-Up mode for bootup the device */
	Si4743_tx_buf[0] = 0x01;    // cmd
	Si4743_tx_buf[1] = 15;      // arg1 -> Query Library ID.
	Si4743_tx_buf[2] = 5;       // arg2 -> Analog audio outputs (LOUT/ROUT).
	TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 3);
	
	start_time = HAL_GetTick();
	do
	{
		TUNER_Delay(TUNER_POLL_TIME);
		TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
		if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
			return failure;
	}while((Si4743_rx_buf[0] & 0xC0) != 0x80);

	clear_buff();
	Si4743_tx_buf[0] = 0x01;                                                    // cmd
	Si4743_tx_buf[1] = 0x00;                                                    // arg1 -> FM receiving Mode
	Si4743_tx_buf[2] = 5;                                                       // arg2 -> Analog audio outputs (LOUT/ROUT).
	TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 3);	
	
	start_time = HAL_GetTick();
	do
	{
		TUNER_Delay(TUNER_POLL_TIME);
		TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
		if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
			return failure;
	}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
	
	Si4743_tx_buf[0] = 0x12;                                            // Set Property
	Si4743_tx_buf[1] = 0x00;
	Si4743_tx_buf[2] = 0x14;                                            // Property  "FM_SEEK_TUNE_SNR_THRESHOLD"
	Si4743_tx_buf[3] = 0x03;
	Si4743_tx_buf[4] = 0x00;                                            // 1db
	Si4743_tx_buf[5] = SEEK_SNR_THRESHOLD;
	
	TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);	
	
	start_time = HAL_GetTick();
	do
	{
		TUNER_Delay(TUNER_POLL_TIME);
		TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
		if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
			return failure;
	}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
	
	
	Si4743_tx_buf[0] = 0x12;                                            // Set Property
	Si4743_tx_buf[1] = 0x00;
	Si4743_tx_buf[2] = 0x14;                                            // Property  "FM_SEEK_TUNE_RSSI_THRESHOLD"
	Si4743_tx_buf[3] = 0x04;
	Si4743_tx_buf[4] = 0x00;                                            // 10
	Si4743_tx_buf[5] = SEEK_RSSI_THRESHOLD;
	
	TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);	
	
	start_time = HAL_GetTick();
	do
	{
		TUNER_Delay(TUNER_POLL_TIME);
		TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
		if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
			return failure;
	}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
	
	/* Update Power on status */
	IsPowerOn = 1;
	
	/* Set Tuner Volume */
	si4743_SetVolume(DeviceAddr, Volume);

	return (success);
}


/**
  * @brief  Stop the FM Tuner.
  * @param  DeviceAddr: Device address on communication Bus.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4743_Stop(uint16_t DeviceAddr)
{
	clear_buff();
	Si4743_tx_buf[0] = 0x11;                                                    // cmd
	TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 1);
	
	uint32_t start_time = HAL_GetTick();
	do
	{
		TUNER_Delay(TUNER_POLL_TIME);
		TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
		if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
			return failure;
	}while((Si4743_rx_buf[0] & 0xC0) != 0x80);

	/* Update Power on status */
	IsPowerOn = 0;
	return (success); 
}

/**
  * @brief Tune FM Tuner to given frequency.
	* @note  This Function checks for valid frequency in band
  * @param DeviceAddr: Device address on communication Bus.
  * @param Frequency: Frequency to tune in in the form of freq(Mhz) => frer*100 (Example 98.3 => 0x2666)
  * @param Direction: direction to tune in. ( Optional)
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4743_Tune(uint16_t DeviceAddr, uint16_t Frequency, uint8_t Direction)
{
	uint32_t start_time;
	clear_buff();
	/* If tune in frequency is valid use it for direct tuingin */
	if ( ( Frequency>6400 ) && (Frequency<10800 ) )                             //if ( ( Frequency > 874 ) && (Frequency < 1081 ) ) Privious version for Si4703
	{
		if (IsPowerOn == 1)
		{
			/* Write modifiled frequency value */
			Si4743_tx_buf[0] = 0x20;                                            // Cmd
			Si4743_tx_buf[1] = 0x02;                                            // 1->Fast , 2->Freeezed
			Si4743_tx_buf[2] = (uint8_t)(Frequency>>8);                         // Frequency -> 98.3(0x2666) => Hi byte-> 26, Low Byte -> 66
			Si4743_tx_buf[3] = (uint8_t)(Frequency);
			Si4743_tx_buf[4] = 0x00;                                            // automatically Antenna tune
			TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 5);
		
			start_time = HAL_GetTick();
			do
			{
				TUNER_Delay(TUNER_POLL_TIME);
				TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
				if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
					return failure;
			}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
			return success;
		}
		else
		{
			return (failure);
		}
	}
	else
		return (failure);   
}

/**
  * @brief Seek FM Tuner to given Direction.
	* @note This Function Seeks for valid frequiencies in band in given  direction
  * @param DeviceAddr: Device address on communication Bus.
  * @param Direction: direction tune in.
  * @retval 0 if correct communication, else wrong communication
  */

uint32_t si4743_Seek(uint16_t DeviceAddr, uint8_t Direction)
{
	clear_buff();
	uint32_t start_time;
	if(Direction == UP_INCREMENT)
	{
		Si4743_tx_buf[0] = 0x21;                                                // cmd for FM_SEEK_START
		Si4743_tx_buf[1] = 0x0C;                                                // Up(3rd bit) -> 8(1) & wrap tune(2nd bit) -> 4     Bits (0000 1100)
		TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 2);
		
		start_time = HAL_GetTick();
		do
		{
			TUNER_Delay(TUNER_POLL_TIME);
			TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
			if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
				break;//return failure;
		}while((Si4743_rx_buf[0] & 0xC0) != 0x80);

		Si4743_tx_buf[0] = 0x22;                                            // cmd for FM_TUNE_STATUS
		Si4743_tx_buf[1] = 0x01;                                            // Arg1 - clear STCINT flag
		TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 2);
	
		start_time = HAL_GetTick();
		do
		{
			TUNER_Delay(TUNER_POLL_TIME);
			TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
			if(HAL_GetTick() - start_time >= TUNER_SEEK_TIMEOUT)
				return failure;
		}while(((Si4743_rx_buf[0] & 0xC0) != 0x80) || (Si4743_rx_buf[1]!=0x01));	//valid frequency found

		return((Si4743_rx_buf[2]<<8)|Si4743_rx_buf[3]);
	}
	else if(Direction == DOWN_DECREMENT)
	{
		Si4743_tx_buf[0] = 0x21;                                                // cmd for FM_SEEK_START
		Si4743_tx_buf[1] = 0x04;                                                // Down(3rd bit) -> 8(0) & wrap tune(2nd bit) -> 4     Bits (0000 0100)
		TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 2);
		
		start_time = HAL_GetTick();
		do
		{
			TUNER_Delay(TUNER_POLL_TIME);
			TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
			if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
				return failure;
		}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
		
		Si4743_tx_buf[0] = 0x22;                                            // cmd for FM_TUNE_STATUS
		Si4743_tx_buf[1] = 0x01;                                            // Arg1
		TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 2);
		
		start_time = HAL_GetTick();
		do
		{
			TUNER_Delay(TUNER_POLL_TIME);
			TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
			if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
				return failure;
		}while(((Si4743_rx_buf[0] & 0xC0) != 0x80) || (Si4743_rx_buf[1]!=0x01));	//valid frequency found
		
		return((Si4743_rx_buf[2]<<8)|Si4743_rx_buf[3]);
	}
	return (failure);
}

/**
  * @brief Set tuner Volume.
  * @param DeviceAddr: Device address on communication Bus.
  * @param Volume: value for Volume to be set
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4743_SetVolume(uint16_t DeviceAddr, uint8_t Volume)
{
	clear_buff();
	if((Volume>=0) && (Volume<64))
	{
		Si4743_tx_buf[0] = 0x12;                                            // Set Property
		Si4743_tx_buf[1] = 0x00;
		Si4743_tx_buf[2] = 0x40;                                            // Cmd "RX_VOLUME"
		Si4743_tx_buf[3] = 0x00;
		Si4743_tx_buf[4] = 0x00;                                            // Volume Range 0-> Min Volume(Mute) to 63 -> Max Volume
		Si4743_tx_buf[5] = Volume;
		TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);

		uint32_t start_time = HAL_GetTick();
		do
		{
			TUNER_Delay(TUNER_POLL_TIME);
			TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
			if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
				return failure;
		}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
		
		return (success); 
	}
}

/**
  * @brief Enables or disables the mute feature on the tuner.
  * @param DeviceAddr: Device address on communication Bus.
  * @param Cmd: TUNER_MUTE_ON to enable the mute or TUNER_MUTE_OFF to disable the
  *             mute mode.
  * @retval 0 if correct communication, else wrong communication
  */

uint32_t si4743_SetMute(uint16_t DeviceAddr, uint32_t Cmd)
{
	clear_buff();

	Si4743_tx_buf[0] = 0x12;                                                    // SET_PROPERTY
	Si4743_tx_buf[1] = 0x00;
	Si4743_tx_buf[2] = 0x40;                                                    // RX_HARD_MUTE
	Si4743_tx_buf[3] = 0x01;
	Si4743_tx_buf[4] = 0x00;

	if(Cmd == MUTE_ALL)
	{
			Si4743_tx_buf[5] = 3;                                                   //   Mutes L Audio Output(1), Mutes R Audio Output(1)
			TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);
	}
	else if(Cmd == MUTE_LEFT)
	{
			Si4743_tx_buf[5] = 2;                                                   //   Mutes L Audio Output(1), Mutes R Audio Output(0)
			TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);
	}
	else if(Cmd == MUTE_RIGHT)
	{
			Si4743_tx_buf[5] = 1;                                                   //   Mutes L Audio Output(0), Mutes R Audio Output(1)
			TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);
	}
	else if(Cmd == UNMUTE)
	{
			Si4743_tx_buf[5] = 0;                                                   //   Mutes L Audio Output(0), Mutes R Audio Output(0)
			TUNER_IO_WriteMultiple(DeviceAddr, Si4743_tx_buf, 6);
	}

	uint32_t start_time = HAL_GetTick();
	do
	{
		TUNER_Delay(TUNER_POLL_TIME);
		TUNER_IO_ReadMultiple(DeviceAddr, Si4743_rx_buf, FM_ALL_REG_4743);
		if(HAL_GetTick() - start_time >= TUNER_MAX_TIMEOUT)
			return failure;
	}while((Si4743_rx_buf[0] & 0xC0) != 0x80);
	
	return (success); 
}

void si4743_GetChannelList(uint16_t DeviceAddr, uint8_t *res)
{
	uint8_t i=0;
	uint8_t ch_updated_index = 0;
	channels_seek_start_time = HAL_GetTick();

	clear_buff();
	for(i = 0;i<MAX_CHANNELS;i++)
	{
		channels[i] = 0;
		ch_updated[i] = 0;
	}

	for(i =0 ; i<MAX_CHANNELS ; i++)
	{
		channels[i] = si4743_Seek(DeviceAddr,UP_INCREMENT);
		if(channels[i] < 8750)
			channels[i] = 0;
		if(HAL_GetTick() - channels_seek_start_time > 10000)             // 10 sec
			break;
	}

	for(i=0; i<MAX_CHANNELS; i++)
	{
		if(!isNewChannelFound(channels[i]))
		{
			ch_updated[ch_updated_index] = channels[i];
			ch_updated_index++;
		}
	}
	sprintf((char*)res,",%d,%d,%d,%d,%d,%d,%d,%d",ch_updated[0]/10,ch_updated[1]/10,ch_updated[2]/10,ch_updated[3]/10,ch_updated[4]/10,ch_updated[5]/10,ch_updated[6]/10,ch_updated[7]/10);
}

static uint8_t isNewChannelFound(uint32_t ch)
{
	uint8_t i;
	for(i = 0;i<MAX_CHANNELS;i++)
	{
		if(ch_updated[i] == ch)
		{
			return 1;
		}			
	} 
	return 0;	
}

void clear_buff(void)
{
    memset(Si4743_tx_buf, 0x00, sizeof(Si4743_tx_buf));
    memset(Si4743_rx_buf, 0x00, sizeof(Si4743_rx_buf));
}

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
