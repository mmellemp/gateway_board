/* 
 * File:   si4743.h
 * Author: ptghyd
 *
 * Created on March 27, 2015, 4:44 PM
 */

#ifndef SI4743_H
#define	SI4743_H

#include "..\Common\tuner.h"
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif


typedef enum
{
  success=0,
	failure=1
}enum_status;

/* Macros For i2c */
#define     FM_ADDRESS              0x63      // 1100011 7-bit FMTunner address
#define     MAX_CHANNELS            8
#define     FM_ALL_REG_4743         9

#define     DEFAULT_VOLUME     			63          // 0-> Minimum   63-> Maximum

#define     UP_INCREMENT        		1
#define     DOWN_DECREMENT      		0

#define     MUTE_ALL        				1
#define     MUTE_LEFT       				2
#define     MUTE_RIGHT      				4
#define     UNMUTE          				8

#define TUNER_POLL_TIME							5				/*in ms*/
#define TUNER_MAX_TIMEOUT						100			/*in ms*/
#define TUNER_SEEK_TIMEOUT					1000		/*in ms*/

#define SEEK_SNR_THRESHOLD					1 			/*in db*/
#define SEEK_RSSI_THRESHOLD					10			/*in dbuV*/


uint32_t si4743_Init(uint16_t );
uint32_t si4743_Start(uint16_t , uint8_t );
uint32_t si4743_Stop(uint16_t );
uint32_t si4743_Tune(uint16_t , uint16_t , uint8_t );
uint32_t si4743_Seek(uint16_t , uint8_t );
uint32_t si4743_SetVolume(uint16_t , uint8_t );
uint32_t si4743_SetMute(uint16_t , uint32_t );
void si4743_GetChannelList(uint16_t , uint8_t *);
void clear_buff(void);
static uint8_t isNewChannelFound(uint32_t );

void TUNER_IO_Init(void);
uint16_t TUNER_IO_ReadMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length);
void TUNER_IO_WriteMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length);
void TUNER_Delay(uint32_t Delay);

/* Audio driver structure */
extern TUNER_DrvTypeDef   si4743_drv;


#ifdef	__cplusplus
}
#endif

#endif	/* SI4743_H */

