/**
  ******************************************************************************
  * @file    mcp2515.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    28-Aug-2014
  * @brief   This file provides the mcp2515 Standalone CAN .   
  ******************************************************************************
	* 	Aug 28			Prototype
	*/

/* Includes ------------------------------------------------------------------*/
#include "mcp2515_gmlan.h"
extern uint32_t debug_var;
/** @addtogroup BSP
  * @{
  */
  
/** @addtogroup Components
  * @{
  */ 

/** @addtogroup mcp2515
  * @brief     This file provides a set of functions needed to drive the 
  *            mcp2515 Standalone CAN.
  * @{
  */

/** @defgroup CS43L22_Function_Prototypes
  * @{
  */

  
/** @defgroup mcp2515_Private_Defines
  * @{
  */
 
/** @defgroup mcp2515_Private_Macros
  * @{
  */
  
/** @defgroup mcp2515_Private_Variables
  * @{
  */
	
typedef enum
{
  TX_IDLE = 0,  
  TX_SEND,    
  TX_SEND_WAIT
}
MCP2515_TX_state;
MCP2515_TX_state data_tx_state_gmlan = TX_SEND;
	
ECAN_DrvTypeDef mcp2515_gmlan_drv = 
{
	mcp2515_gmlan_Init,
	mcp2515_gmlan_Reset,
	mcp2515_gmlan_WriteReg,
	mcp2515_gmlan_ReadStatus,
	mcp2515_gmlan_ReadReg,
	mcp2515_gmlan_SetCANBaud,
	mcp2515_gmlan_SetCANMode,
	mcp2515_gmlan_transmitCANMessage,
	mcp2515_gmlan_readCANmessage,
	mcp2515_gmlan_Test,
};
	
//Reset Mcp2515
void mcp2515_gmlan_Reset(void)
{
	uint8_t cmd = MCP2515_RESET;	
	GMLAN_Select();
	GMLAN_IO_Write(&cmd,sizeof(cmd));
	GMLAN_Deselect();
	GMLAN_Delay(10);
}

//write reg
void mcp2515_gmlan_WriteReg(uint8_t Addr, uint8_t Value)
{
	const uint8_t buf[] __attribute__((aligned(8))) = {
													[0] = MCP2515_WRITE,
													[1] = Addr,	/* address */
													[2] = Value	/* data */
												};
	GMLAN_Select();
	GMLAN_IO_Write((uint8_t*) buf,sizeof(buf));
	GMLAN_Deselect();				
}

void mcp2515_gmlan_WriteRegBit(uint8_t Regno, uint8_t Bitno, uint8_t Val)
{
	const uint8_t buf[]  __attribute__((aligned(8))) = {
													[0] = BIT_MODIFY,
													[1] = Regno,	/* Regno */
													[2] = 1 << Bitno,	/* data */
													[3] = ((Val != 0)? 0xFF : 0x00),
												};
	GMLAN_Select();
	GMLAN_IO_Write((uint8_t*) buf,sizeof(buf));
	GMLAN_Deselect();	
}


// Read reg
uint8_t mcp2515_gmlan_ReadReg(uint8_t Addr)
{
	const uint8_t buf[]  __attribute__((aligned(8))) = {
													[0] = MCP2515_READ,
													[1] = Addr,	/* address */
												};
	
	uint8_t res;
	GMLAN_Select();
	GMLAN_IO_Write((uint8_t*) buf,sizeof(buf));
	res = GMLAN_IO_Read();						
	GMLAN_Deselect();
	return res;
}

//Read status
uint8_t mcp2515_gmlan_ReadStatus(void)
{
	uint8_t res;
	uint8_t cmd = MCP2515_READ_STATUS;
	
	GMLAN_Select();
	GMLAN_IO_Write(&cmd,sizeof(cmd));
	res = GMLAN_IO_Read();	
	GMLAN_Deselect();
	return res;
}

//Init
uint8_t mcp2515_gmlan_Init(void)
{
	uint8_t mode;
	
	GMLAN_IO_Init();
	/* Reset MCP25151 */
	mcp2515_gmlan_Reset();
	return mcp2515_gmlan_ReadReg(CANSTAT);

}

uint8_t mcp2515_gmlan_SetCANBaud(uint32_t baudRate)
{
	uint8_t ubCNF1 = 0;
  uint8_t ubCNF2 = 0x80; 	//activate user define PS2 & 1 sample
  uint8_t ubCNF3 = 0;
  uint8_t ubBRP = 0;
  uint8_t ubnTQ; 					//Total of TQ in a CAN bus bit time
	uint8_t uphSeg1,uphSeg2;
	//set SJW=1TQ
	//set PRSeg=1TQ
	
	
	if ((baudRate != 0) && (baudRate <= 1000000L) )
	{
		 do
		 {
			ubnTQ = (uint8_t) ( MCP_OSC_FREQ/((uint32_t)((ubBRP + 1) << 1) * baudRate) ) ;
			debug_var = ubnTQ;
		 }while ((ubBRP++ <= MCP2515_MAX_BRP) && (ubnTQ > MCP2515_MAX_TQ));
		 ubCNF1 |= (ubBRP-1);
		 
		 //divide ubnTQ appropriately
		 ubnTQ-=2;		//-1syncSeg -1propSeg
		 uphSeg1 = ubnTQ>>1;
		 uphSeg2 = ubnTQ - uphSeg1;

		 ubCNF2 |= (uphSeg1-1)<<3;
		 ubCNF3 |= (uphSeg2-1);
		 
		 debug_var = ubCNF1;
		 debug_var = ubCNF2;
		 debug_var = ubCNF3;
		 
		 mcp2515_gmlan_WriteReg(CNF1,ubCNF1); 		
		 mcp2515_gmlan_WriteReg(CNF2,ubCNF2); 		
		 mcp2515_gmlan_WriteReg(CNF3,ubCNF3); 		
		 
	}
	
	return 1;
}

//set normal Mode
uint8_t mcp2515_gmlan_SetCANMode(uint8_t mode, uint8_t abat,uint8_t singleShot,uint8_t clkout,uint8_t clkpre)
{
  //REQOP2<2:0> = 000 for normal mode
  //ABAT = 0, do not abort pending transmission
  //OSM = 0, not one shot
  //CLKEN = 1, disable output clock
  //CLKPRE = 0b11, clk/8
  uint8_t opmode=0;
	uint8_t ret = 99;
  uint8_t settings = 0;
  settings |=clkpre ;
	settings |=(clkout << 2);
	settings |=(singleShot << 3) ;
	settings |=(abat << 4);
	settings |=(mode << 5);
  mcp2515_gmlan_WriteReg(MCP2515_CANCTRL,settings);
  //Read mode and make sure it is normal
  debug_var=mcp2515_gmlan_ReadReg(CANSTAT);
	opmode= mcp2515_gmlan_ReadReg(CANSTAT) >> 5;
	mcp2515_gmlan_WriteReg(MCP2515_RXB0CTRL,0x60);
	mcp2515_gmlan_WriteReg(MCP2515_RXB1CTRL,0x60);
	mcp2515_gmlan_WriteReg(MCP2515_CANINTE,0x07);
	
  switch(opmode)
	{
    case MCP2515_MODE_NORMAL:
															ret=MCP2515_MODE_NORMAL; 
															break;
		case MCP2515_MODE_SLEEP	: 
															ret=MCP2515_MODE_SLEEP;
															break;
		case MCP2515_MODE_LOOPBACK: 
															ret=MCP2515_MODE_LOOPBACK;
															break;
		case MCP2515_MODE_LISTENONLY:
															ret=MCP2515_MODE_LISTENONLY;
															break;
		case MCP2515_MODE_CONFIG :
															ret=MCP2515_MODE_CONFIG;
															break;
		default :  break;
	}
	return ret;
}

void mcp2515_gmlan_LoadTxBuffer(uint8_t Buffer, uint8_t *Data, uint8_t Len)
{
  const uint8_t buf[] = {	[0] = 0x40 | Buffer,
													};		
  GMLAN_Select();
	GMLAN_IO_Write((uint8_t*) buf,sizeof(buf));
	GMLAN_IO_Write(Data, Len);											
  GMLAN_Deselect();
}


//Transmit message
uint8_t mcp2515_gmlan_transmitCANMessage(CANMSG msg, uint32_t timeout)
{
  uint32_t startTime, endTime;
	uint8_t valD;
  uint8_t sentMessage;
  uint8_t val=0;
	uint8_t x=0;
	uint8_t y=0;
	uint32_t i;
	
	switch (data_tx_state_gmlan)
	{	
		case TX_IDLE:
				break;
				
		case TX_SEND:
			startTime = GMLAN_GetTick();
			endTime = startTime + timeout;
			debug_var=msg.canID;
			sentMessage = 0;
			uint8_t temp=0;
			uint16_t sid_temp = 0;
			
			if(!msg.isExtendedAdrs)
			{
			//Write standard ID registers
				sid_temp = msg.canID;
				sid_temp = sid_temp << 5;
				mcp2515_gmlan_WriteReg(TXB0SIDH,(sid_temp >> 8));
				mcp2515_gmlan_WriteReg(TXB0SIDL,sid_temp);
				
				
			}
			else
			{
			//Write extended ID registers, which use the standard ID registers
				val = msg.canID>>21;
				mcp2515_gmlan_WriteReg(TXB0SIDH,val);
				val = msg.canID >> 16;
				val = val & 0x03; //0b00000011;
				y=msg.canID >> 18;
				y=y<<5;
				x= y&0xE0;
				val = val | y;//((msg.canID >> 21 )& 0xE0);//0b11100000);
				val |= 1 << EXIDE;
				mcp2515_gmlan_WriteReg(TXB0SIDL,val);
				val = msg.canID>> 8;
				mcp2515_gmlan_WriteReg(TXB0EID8,val);
				val = msg.canID;
				mcp2515_gmlan_WriteReg(TXB0EID0,val);
			}
		 val = msg.dataLength & 0x0f;
			if(msg.rtr)
					val |= 1 << TXRTR; 
			
		 mcp2515_gmlan_WriteReg(TXB0DLC,val);
			
			//Load the Tx Buffer 0 Data bytes with the Message bytes
			mcp2515_gmlan_WriteReg(TXB0D0, msg.data[0]);
			mcp2515_gmlan_WriteReg(TXB0D1, msg.data[1]);
			mcp2515_gmlan_WriteReg(TXB0D2, msg.data[2]);
			mcp2515_gmlan_WriteReg(TXB0D3, msg.data[3]);
			mcp2515_gmlan_WriteReg(TXB0D4, msg.data[4]);
			mcp2515_gmlan_WriteReg(TXB0D5, msg.data[5]);
			mcp2515_gmlan_WriteReg(TXB0D6, msg.data[6]);
			mcp2515_gmlan_WriteReg(TXB0D7, msg.data[7]);
			
			val = mcp2515_gmlan_ReadReg(CANINTF);
			debug_var = val;	
		 //Abort the send if failed
			mcp2515_gmlan_WriteRegBit(CANINTE,TX0IE,1);
			//Transmit the message
			mcp2515_gmlan_WriteRegBit(TXB0CTRL,TXREQ,1);
			sentMessage = 0;
			data_tx_state_gmlan = TX_SEND_WAIT;
			break;
		
		case TX_SEND_WAIT:
			  if(GMLAN_GetTick() < endTime)
				{
					val = mcp2515_gmlan_ReadReg(MCP2515_TXRTSCTRL);
					debug_var = val;
					if((val & (1 << TX0IF)) == 1)
					{
						sentMessage = 1;
						//And clear write interrupt
						mcp2515_gmlan_WriteRegBit(CANINTF,TX0IF,0);
					//	break;
					}
					else 
					{
						valD = mcp2515_gmlan_ReadReg(EFLG);
						debug_var=valD;
					}
				}
				else
				{
					
					//Abort the send if failed
					mcp2515_gmlan_WriteRegBit(TXB0CTRL,TXREQ,0);
					
					//And clear write interrupt
					mcp2515_gmlan_WriteRegBit(CANINTF,TX0IF,0);
				}
				data_tx_state_gmlan = TX_SEND;
				break;
			
		}
	
	return sentMessage;
}





uint8_t mcp2515_gmlan_Test(void)
{
	CANMSG msg;
  msg.canID = 0x11123456;
	msg.isExtendedAdrs = 1;
  msg.rtr = 0;
  msg.dataLength = 8;
  msg.data[0]=0x11;
	msg.data[1]=0x22;
	msg.data[2]=0x33;
	msg.data[3]=0x44;
	msg.data[4]=0x55;
	msg.data[5]=0x66;
	msg.data[6]=0x77;
	msg.data[7]=0x88;
  
  if(!mcp2515_gmlan_transmitCANMessage(msg,1000))
    return 0;
	return 1;
}


uint8_t mcp2515_gmlan_readCANmessage(CANMSG *msg)
{
	uint8_t stat, res;

	stat=0;
	res=0;
	
	stat = mcp2515_gmlan_ReadReg(MCP2515_REC);
	debug_var=stat;
	
	stat = mcp2515_gmlan_ReadReg(MCP2515_CANSTAT);
	debug_var=stat;
	stat = mcp2515_gmlan_ReadReg(MCP2515_EFLG);
	debug_var=stat;
	stat = mcp2515_gmlan_ReadReg(MCP2515_CANINTF);
	debug_var=stat;
	if ( stat & 0x01 ) {
		// Msg in Buffer 0
		mcp2515_gmlan_recieveCANmsg( MCP2515_RXB0SIDH, msg);
		//And clear Rx interrupt
		mcp2515_gmlan_WriteRegBit(CANINTF,RX0IF,0);
		res = 1;
	}
	else if ( stat & 0x02 ) {
		// Msg in Buffer 1
		mcp2515_gmlan_recieveCANmsg( MCP2515_RXB1SIDH, msg);
		//And clear write interrupt
		mcp2515_gmlan_WriteRegBit(CANINTF,RX1IF,0);
		res = 1;
	}
	
	else {
		res = 0;
	}	
	
	return res;
}

/*---------------------------------------------------------------
NAME       : mcp2515_gmlan_read_can_msg()
ENTRY  TYPE: 
RETURN TYPE: 
PURPOSE    : 
VERSION    : 1.0.0
DESCRIPTION: 
CREATE DATE: 
LOGS       : 
-----------------------------------------------------------------*/
// Buffer can be MCP2515_RXB0SIDH or MCP2515_RXB1SIDH 
uint8_t mcp2515_gmlan_recieveCANmsg( const uint8_t buffer_sidh_addr,CANMSG* msg)
{
	
	uint8_t i,mcp_addr, ctrl;
	
	mcp_addr = buffer_sidh_addr;
	
	msg->canID=mcp2515_gmlan_readCanID(mcp_addr);
  debug_var=msg->canID;  
	ctrl = mcp2515_gmlan_ReadReg(mcp_addr-1);
  msg->dataLength = (mcp2515_gmlan_ReadReg( mcp_addr+4 )&0x0f);
	
	if ((msg->dataLength & 0x40) || (ctrl & 0x08)) {
    msg->rtr = 1;
  
	} else {
        msg->rtr = 0;
  
	}
	
	msg->dataLength &= 0x0F;	

 	for(i=0;i<msg->dataLength;i++)
		msg->data[i]=mcp2515_gmlan_ReadReg(mcp_addr+5+i);
	
	//mcp2515_read_registers(mcp_addr+5,&(msg->dta[0]),msg->dlc) ;	
	//msg->dta[0]=mcp2515_read_register( mcp_addr+5);
  //msg->dta[1]=mcp2515_read_register( mcp_addr+6);
  //msg->dta[2]=mcp2515_read_register( mcp_addr+7);
  //msg->dta[3]=mcp2515_read_register( mcp_addr+8);
  //msg->dta[4]=mcp2515_read_register( mcp_addr+9);
  //msg->dta[5]=mcp2515_read_register( mcp_addr+10);
  //msg->dta[6]=mcp2515_read_register( mcp_addr+11);
  //msg->dta[7]=mcp2515_read_register( mcp_addr+12);
	return msg->dataLength;
}


uint32_t mcp2515_gmlan_readCanID(const uint8_t mcp_addr)
{
  uint8_t sidh,sidl,eid8,eid0;
	uint32_t rxCanID=0;
	uint32_t temp=0; 
	sidh= mcp2515_gmlan_ReadReg( mcp_addr);
	sidl= mcp2515_gmlan_ReadReg( mcp_addr+1);
	eid8= mcp2515_gmlan_ReadReg( mcp_addr+2);
	eid0= mcp2515_gmlan_ReadReg( mcp_addr+3);
  
	debug_var=sidh;
	debug_var=sidl;

	//Get the Standard 11-bit ID
	temp = sidh;
	rxCanID |=temp<<21;  // copy SID10-SID3 -8bits to bit positions 31-24
	temp=sidl& 0xE0;	
	temp= temp>>5; 
	rxCanID|=temp <<18;	//copy SID2-SID0 -3bits to bitpositions 23,22,21 
	temp =0;

	if(sidl & 0x08)  //extended ID bit is set for the recieved msg
	{
		temp = sidl&0x07;	
		rxCanID|=temp << 16 ;	//copy EID17-EID16 -2bits to bitpositions 20,19  
		temp =0;
		
		temp|=eid8;
		rxCanID|= temp << 8; // copy EID15 - EID8 - 8bits to bitpositions 18-11
		temp=0;

		temp|=eid0;						//copy EID7-EID0 - 8bits to bitpositions 10-4
		rxCanID|= temp;
		temp=0;

	}	

debug_var = rxCanID;
  // have no extend ID;
  return rxCanID;
}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT Arka IMS *****END OF FILE****/
