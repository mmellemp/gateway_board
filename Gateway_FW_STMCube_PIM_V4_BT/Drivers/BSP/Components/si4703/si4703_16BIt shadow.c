/**
  ******************************************************************************
  * @file    si4703.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    22-July-2014
  * @brief   This file provides the si4703 FM Tuner driver.   
  ******************************************************************************
	* July 22			Prototype
	*
	*	July 				First stable version
	*
	*
	*
	*/

/* Includes ------------------------------------------------------------------*/
#include "si4703.h"

/** @addtogroup BSP
  * @{
  */
  
/** @addtogroup Components
  * @{
  */ 

/** @addtogroup si4703
  * @brief     This file provides a set of functions needed to drive the 
  *            si4703 FM Tuner.
  * @{
  */

/** @defgroup si4703_Private_Types
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup si4703_Private_Defines
  * @{
  */
/* Uncomment this line to enable verifying data sent to codec after each write 
   operation (for debug purpose) */
#if !defined (VERIFY_WRITTENDATA)  
 #define VERIFY_WRITTENDATA 
#endif /* VERIFY_WRITTENDATA */

/**
  * @}
  */ 

/** @defgroup si4703_Private_Macros
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup si4703_Private_Variables
  * @{
  */

/* FM Tuner driver structure initialization */  
FM_TUNER_DrvTypeDef si4703_drv = 
{
  si4703_Init,
  si4703_ReadID,

  si4703_Start,
  si4703_Stop,  
  
  si4703_Tune,
	si4703_Seek,
  si4703_SetVolume,
  si4703_SetMute  
};

static uint16_t ShadowReg[16] = {0x11};

static uint8_t WrReg[12];

uint16_t ChipID =0;

uint16_t Volume; 

uint8_t IsPowerOn = 0;
/**
  * @}
  */ 

/** @defgroup si4703_Function_Prototypes
  * @{
  */
static uint8_t FM_IO_Write(uint8_t Addr, uint8_t Reg, uint16_t Value, uint8_t Set_reset);
static uint8_t FM_IO_Read(uint8_t Addr, uint8_t Cmd);
static void FM_ModifyShadowReg(uint8_t Reg, uint16_t Value, uint8_t Set_reset);
/**
  * @}
  */ 

/** @defgroup si4703_Private_Functions
  * @{
  */ 

void Refresh_MHz() {
uint16_t freq;
uint8_t  ST_bit = 0;
  
  //FM_IO_Read(DeviceAddr,FM_0A0B_REG);

}


/**
  * @brief Modifies Shadow Registers.
  * @note Recommended to simplify manipulation of register bits 
	*       and to reduce the number of reads/writes to the I/O bus
  * @param Reg: Register to be modified.
	* @param Value: Data to be written.
	* @param Set_Reset: Set or Reset option.
	* @retval none
  */
static void FM_ModifyShadowReg(uint8_t Reg, uint16_t Value, uint8_t Set_Reset)
{
	if (Set_Reset == SET) 
	{
		/* Sets the desired bits in register */
		ShadowReg[Reg] |= Value;
  }
  else 
	{
		/* Resets the desired bits in register */
		ShadowReg[Reg] &= Value;
  }

}
/**
  * @brief  Writes Multiple reg.
	* @note 	For Si4703 write always start from Reg address 02h high byte to 0Fh lower byte
	*					then automatically wraps around upper Higher byte of 00h and continues till stop condition(Circular way)
	*					Before Writing the registers is Updated in shadow reg then shadow reg are written
  * @param  Reg: Reg to write
	* @param  Addr: Device Address 
  * @param  Value: Data to be written
  * @param  Set_reset: Set or reset value in shadow registers
  * @retval None
  */

static uint8_t FM_IO_Write(uint8_t Addr,uint8_t Reg, uint16_t Value, uint8_t Set_reset)
{
	uint8_t wr_cnt;
	uint8_t i,j;
	uint8_t writeBuffer[12] = {0x00};
	
//	/* Calculate how much bytes need to be written */
//	wr_cnt = ( Reg - 1 ) << 1; 
//	/* Modify Register Byte in shadow Reg */
//	FM_ModifyShadowReg(Reg,Value,Set_reset);
//	/* Writing to register starts from 0x02 register */
	for(i = 0,j=0;; )
	{
		writeBuffer[i] = ShadowReg[]
	}
	
	FM_TUNER_IO_WriteMultiple(Addr,(uint8_t *)ShadowReg[2],wr_cnt);
	
	return 0;	
}

void setAll()
{ 
	uint8_t i = 0;
	for (i=0;i<32;i++)
	{ 
		ShadowReg[i]= 0x00;
	}
}
/**
  * @brief  Read Multiple reg.
	* @note 	For Si4703 read always start from Reg address 0Ah high byte to 0Fh lower byte
	*					then automatically wraps around upper Higher byte of 00h and continues till stop condition(Circular way)
	* @param  Addr: Device Address  
  * @param  Com: FM_ALL_REG or FM_0A0B_REG
  * @retval None
  */
static uint8_t FM_IO_Read(uint8_t Addr, uint8_t Cmd)
{
	uint8_t readBuffer[32] = {0x00};
	uint8_t i,j;
	//setAll();
	if(Cmd == FM_ALL_REG)
	{	/* Writing to register starts from 0x0A register */
		FM_TUNER_IO_ReadMultiple(Addr, (uint8_t*)readBuffer, 32);
		/*Update shadow registers */
		for (i = 12,j = 0; ; )
		{
			if (i == 32) i = 0;
			ShadowReg[j++] = (readBuffer[i] << 8) | readBuffer[i+1];
			i +=2;
			if (i == 10) break;
		}
	}
	else if(Cmd == FM_0A0B_REG)
	{/* Writing to register starts from 0x0A register read only Two */
		FM_TUNER_IO_ReadMultiple(Addr, (uint8_t*)readBuffer,4);
		for (i = 12,j = 0; ; )
		{
			ShadowReg[j++] = (readBuffer[i] << 8) | readBuffer[i+1];
			i +=2;
			if (i == 16) break;
		}
	}
	return 0;	
}

/**
  * @brief Initializes the FM Tuner and the control interface.
  * @param DeviceAddr: Device address on communication Bus.   
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Init(uint16_t DeviceAddr)
{
  uint32_t counter = 0;
  
  /* Initialize the Control interface of the FM Tuner */
  FM_TUNER_IO_Init(); 
	
  FM_TUNER_Delay(100);
	/* Read All registers  in shadow reg */
	FM_IO_Read(DeviceAddr,FM_ALL_REG); 
	
//	/* save CHIP ID */
//	ChipID = ShadowReg[14] << 8;
//	ChipID |= ShadowReg[15];
//	
//	//FM_TUNER_Delay(10);
//	
//	/*Set the XOSCEN bit to power up the crystal Write data 8100h to 07h*/
// // FM_ModifyShadowReg(0x07, 0xFFFF, RESET);
//	
//	FM_IO_Write(DeviceAddr, 0x07, 0xBC04, SET); //0x8100 0xBC04
//	/* provide sufficient delay (minimum 500 ms) for the oscillator to stabilize */
//	FM_TUNER_Delay(1000);
//	
	/* Read All registers  in shadow reg */
	FM_IO_Read(DeviceAddr,FM_ALL_REG); 
	/* Return communication control value */
  return 0;  
}

/**
  * @brief  Get the si4703 ID.
  * @param  DeviceAddr: Device address on communication Bus.   
  * @retval The si4703 ID 
  */
uint32_t si4703_ReadID(uint16_t DeviceAddr)
{
  uint16_t DeviceID = 0;
	DeviceID = (ShadowReg[12] << 8);
	DeviceID |= ShadowReg[13];
	return ((uint32_t)DeviceID);
}

/**
  * @brief Start the FM Tuner play feature.
  * @note Activate and power Up Si4703.
  * @param DeviceAddr: Device address on communication Bus.
	* @param Volume: Volune in range of 0 to 16 .
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Start(uint16_t DeviceAddr, uint8_t Volume)
{
	uint16_t active;
	
	active = ChipID;
	
	//FM_IO_Write(DeviceAddr, 0x0F, 0x0000, SET);// Errarta
	//FM_IO_Write(DeviceAddr, 0x04, 0x0000, SET);//rds diable
	FM_TUNER_Delay(200);
	/* Set ENABLE bit High and DISABLE bit low Disable DMUTE in reg 02h */
  //FM_ModifyShadowReg(0x02, 0xFFFF, RESET);
	FM_IO_Write(DeviceAddr, 0x02, 0xC001, SET);
	/* Add Delay */
	FM_TUNER_Delay(1000);
	
//	/*  Wait for module powerup */	
//  while (active == ChipID)
//	{             
	FM_IO_Read(DeviceAddr,FM_ALL_REG);
//    active = ShadowReg[14] << 8;
//    active |= ShadowReg[15];
//  }
//	
	FM_TUNER_Delay(200);
	
	/* Write default Volume and RSSI Seek Threshold values */
  FM_ModifyShadowReg(0x05, 0xFFFF, RESET);
	
	FM_IO_Write(DeviceAddr, 0x05, 0x1017, SET); //0x1015
	
	Volume = 5 * 23;
	
	FM_TUNER_Delay(60);
	
	/* Update Power on status */
	IsPowerOn = 1;
	FM_IO_Read(DeviceAddr,FM_ALL_REG);

  /* Return communication control value */
  return 0;  
}

/**
  * @brief Stop the FM Tuner.
  * @param DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Stop(uint16_t DeviceAddr)
{  
  uint32_t counter = 0;
	
//	FM_ModifyShadowReg(0x02, 0x4000, RESET);
//	
//	FM_IO_Write(DeviceAddr, 0x02, 0x0041, SET);
//	
//	/* Update Power on status */
//	IsPowerOn = 0;
   
  return counter;
}

/**
  * @brief Tune FM Tuner to given frequency.
	* @note This Function checks for valid reqiuncy in band 
  * @param DeviceAddr: Device address on communication Bus.
  * @param Frequency: Frequency to tune in.
  * @param Direction: direction tune in.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Tune(uint16_t DeviceAddr, uint16_t Frequency, uint8_t Direction)
{
  uint32_t counter = 0;
	
	uint16_t calc;
	
	uint8_t STC_bit= 0;
	//FM_IO_Write(DeviceAddr, 0x02, 0xC000, SET);
	/* If tune in frequency is valid use it for direct tuingin */
	if ( ( Frequency > 874 ) && (Frequency < 1081 ) )
  {
		calc = Frequency;
	}
	else
	{
		// to be Written
	}
	/* Clear CHANNEL register in shadow reg */
	FM_ModifyShadowReg(0x03, 0x83FF, RESET);
	calc -= 875;
	/*// Set TUNE Flag*/
	calc |= 0x8000; 
	if (IsPowerOn == 1) 
	{                  
    /* Write modifiled frequency value */
		FM_IO_Write(DeviceAddr,0x03, calc, SET);        
    FM_TUNER_Delay(60);
    while (STC_bit == 0)
		{
      FM_IO_Read(DeviceAddr,FM_0A0B_REG);
      STC_bit = ShadowReg[0] & 0x40;
    }
    FM_IO_Write(DeviceAddr,0x03, 0x8000, RESET);   // Set TUNE = 0;
    while (STC_bit > 0) {
      FM_IO_Read(DeviceAddr,FM_0A0B_REG);
      STC_bit = ShadowReg[0] & 0x40;
    }
  }
  else
	{
    FM_ModifyShadowReg(0x03, calc, SET); // If power is off, only modify the shadow register
  }
  
  return counter;
}


uint32_t si4703_Seek(uint16_t DeviceAddr, uint8_t Direction)
{
	uint8_t STC_bit = 0;
	uint8_t ST_bit = 0;
	uint8_t SF_BL_bit = 0;

  if (Direction == FM_DIR_UP)
    FM_IO_Write(DeviceAddr,0x02, 0x0300, SET);       // Activate SEEK
  else if (Direction == FM_DIR_DOWN)
	{
    FM_ModifyShadowReg(0x02, 0x0200, RESET); 					// Set seek direction Down
    FM_IO_Write(DeviceAddr,0x02, 0x0100, SET);       // Activate SEEK
  }
  
  while( STC_bit == 0 )
	{
    setAll();
		FM_IO_Read(DeviceAddr,FM_ALL_REG);
		STC_bit = ShadowReg[0] << 1;
    STC_bit >>= 7;
    FM_TUNER_Delay(60);
  }
  SF_BL_bit = ShadowReg[0] & 20;
  
  FM_IO_Write(DeviceAddr,0x02, 0x0100, RESET);       // Deactivate SEEK
  
  while( STC_bit > 0 )
	{
    FM_IO_Read(DeviceAddr,FM_0A0B_REG);
    STC_bit = ShadowReg[0] << 1;
    STC_bit >>= 3;
  }
  
  if (SF_BL_bit == 0) 
	{
    FM_IO_Read(DeviceAddr,FM_0A0B_REG);
  }
	return 0;
}

uint32_t si4703_SetVolume(uint16_t DeviceAddr, uint8_t Volume)
{
	return 0;
}
uint32_t si4703_SetMute(uint16_t DeviceAddr, uint32_t Cmd)
{
	return 0;
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
