/**
  ******************************************************************************
  * @file    si4703.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    22-July-2014
  * @brief   This file contains all the functions prototypes for the si4703 driver.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SI4703_H
#define __SI4703_H

/* Includes ------------------------------------------------------------------*/
#include "..\Common\tuner.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Component
  * @{
  */ 
  
/** @addtogroup si4703
  * @{
  */


/******************************************************************************/
/***************************  Codec User defines ******************************/
/******************************************************************************/

#define REG_SET 						1
#define REG_RESET 					0
#define FM_DIR_UP 			1
#define FM_DIR_DOWN 		0

#define FM_ALL_REG 			0x0F
#define FM_0A0B_REG 		2

/* MUTE commands */
#define TUNER_MUTE_ON                 1
#define TUNER_MUTE_OFF                0

/******************************************************************************/
/****************************** REGISTER MAPPING ******************************/
/******************************************************************************/
/** 
  * @brief  si4703 ID  
  */  
#define  SI4703_ID            0x1242
#define  SI4703_ID_MASK       0xFFFF


#define SI4703_DEVICEID                 0x00 // Device ID
#define SI4703_CHIPID                   0x01 // Chip ID
#define SI4703_POWERCFG                 0x02 // Power configuration
#define SI4703_CHANNEL                  0x03 // Channel
#define SI4703_SYSCONFIG1               0x04 // System configuration #1
#define SI4703_SYSCONFIG2               0x05 // System configuration #2
#define SI4703_SYSCONFIG3               0x06 // System configuration #3
#define SI4703_TEST1                    0x07 // Test 1
#define SI4703_TEST2                    0x08 // Test 2
#define SI4703_BOOT                     0x09 // Boot configuration
#define SI4703_RSSI                     0x0A // Status RSSI
#define SI4703_READCHAN		              0x0B // Read channel
#define SI4703_RDSA                     0x0C // RDSA
#define SI4703_RDSB                     0x0D // RDSB
#define SI4703_RDSC                     0x0E // RDSC
#define SI4703_RDSD                     0x0F // RDSD

/* Power configuration */
#define SI4703_PWR_DSMUTE               15 // Softmute disable (0 = enable (default); 1 = disable)
#define SI4703_PWR_DMUTE                14 // Mute disable (0 = enable (default); 1 = disable)
#define SI4703_PWR_MONO                 13 // Mono select (0 = stereo (default); 1 = force mono)
#define SI4703_PWR_RDSM                 11 // RDS mode (0 = standard (default); 1 = verbose)
#define SI4703_PWR_SKMODE               10 // Seek mode (0 = wrap band limits and continue (default); 1 = stop at band limit)
#define SI4703_PWR_SEEKUP                9 // Seek direction (0 = down (default); 1 = up)
#define SI4703_PWR_SEEK                  8 // Seek (0 = disable (default); 1 = enable)
#define SI4703_PWR_DISABLE               6 // Powerup disable (0 = enable (default))
#define SI4703_PWR_ENABLE                0 // Powerup enable (0 = enable (default))

#define VOLUME_CONVERTFM(Volume)    (((Volume) > 100)? 15:(uint8_t)((Volume * 15 )/ 100))

#define HIBYTE(v)					(uint8_t) (v >> 8)
#define LOWBYTE(v)				(uint8_t) (v & 0x00FF)
/**
  * @}
  */ 
#define MAX_CHANNELS 8
/** @defgroup CS43122_Exported_Functions
  * @{
  */
    
/*------------------------------------------------------------------------------
                           Audio Codec functions 
------------------------------------------------------------------------------*/
/* High Layer codec functions */
uint32_t si4703_Init(uint16_t DeviceAddr);
uint32_t si4703_ReadID(uint16_t DeviceAddr);
uint32_t si4703_Start(uint16_t DeviceAddr, uint8_t Volume);
uint32_t si4703_Stop(uint16_t DeviceAddr);
uint32_t si4703_Tune(uint16_t DeviceAddr, uint16_t Frequency, uint8_t Direction);
uint32_t si4703_Seek(uint16_t DeviceAddr, uint8_t Direction);
uint32_t si4703_SetVolume(uint16_t DeviceAddr, uint8_t Volume);
uint32_t si4703_SetMute(uint16_t DeviceAddr, uint32_t Cmd);
void Si4703_GetChannelList(uint16_t DeviceAddr,uint8_t *res);

/* FM TUNER IO functions */
void TUNER_IO_Init(void);
uint16_t TUNER_IO_ReadMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length);
void TUNER_IO_WriteMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length);
void TUNER_Delay(uint32_t Delay);


/* Audio driver structure */
extern TUNER_DrvTypeDef   si4703_drv;

#endif /* __SI4703_H */

/**
  * @}
  */ 

/**
  * @}
  */ 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
