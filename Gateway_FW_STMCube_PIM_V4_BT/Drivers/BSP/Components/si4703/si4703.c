/**
  ******************************************************************************
  * @file    si4703.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    22-July-2014
  * @brief   This file provides the si4703 FM Tuner driver.   
  ******************************************************************************
	* July 22			Prototype
	*
	*	July 25			First stable version
	*
	*
	*
	*/

/* Includes ------------------------------------------------------------------*/
#include "si4703.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>

/** @addtogroup BSP
  * @{
  */
  
/** @addtogroup Components
  * @{
  */ 

/** @addtogroup si4703
  * @brief     This file provides a set of functions needed to drive the 
  *            si4703 FM Tuner.
  * @{
  */

/** @defgroup si4703_Private_Types
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup si4703_Private_Defines
  * @{
  */
/* Uncomment this line to enable verifying data sent to codec after each write 
   operation (for debug purpose) */
#if !defined (VERIFY_WRITTENDATA)  
 #define VERIFY_WRITTENDATA 
#endif /* VERIFY_WRITTENDATA */

/**
  * @}
  */ 

/** @defgroup si4703_Private_Macros
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup si4703_Private_Variables
  * @{
  */

/* FM Tuner driver structure initialization */  
TUNER_DrvTypeDef si4703_drv = 
{
  si4703_Init,
  si4703_ReadID,

  si4703_Start,
  si4703_Stop,  
  
  si4703_Tune,
	si4703_Seek,
  si4703_SetVolume,
  si4703_SetMute, 
	Si4703_GetChannelList	
};

static uint8_t ShadowReg[32];

uint16_t ChipID =0;

uint16_t Volume; 

uint8_t IsPowerOn = 0;

uint32_t channels[MAX_CHANNELS+1]={0};

static uint32_t channels_seek_start_time;

static uint32_t ch_updated[MAX_CHANNELS+1] = {0};


/**
  * @}
  */ 

/** @defgroup si4703_Function_Prototypes
  * @{
  */
static uint8_t FM_IO_Write(uint8_t Addr, uint8_t Reg, uint16_t Value, uint8_t Set_reset);
static uint8_t FM_IO_Read(uint8_t Addr, uint8_t Cmd);
static void FM_ModifyShadowReg(uint8_t Reg, uint16_t Value, uint8_t Set_reset);

/**
  * @}
  */ 

/** @defgroup si4703_Private_Functions
  * @{
  */ 
/**
  * @brief Modifies Shadow Registers.
  * @note Recommended to simplify manipulation of register bits 
	*       and to reduce the number of reads/writes to the I/O bus
  * @param Reg: Register to be modified.
	* @param Value: Data to be written.
	* @param Set_Reset: Set or Reset option.
	* @retval none
  */
static void FM_ModifyShadowReg(uint8_t Reg, uint16_t Value, uint8_t Set_Reset)
{
	/* Index of 0x00 register in shadow register is 12 so initialize pos to 12 
	*	 Reading of Si4703 registers always start from 0x0A-0x0F and then starts from 0x00-0x09
	*/
	uint8_t Reg_pos	= 12;
	
	/* Calculate position of register which needs to be modified */
	Reg_pos += Reg << 1;
	
	if (Set_Reset == REG_SET) 
	{
    /* Sets the desired bits in register */
    ShadowReg[Reg_pos++] |= HIBYTE(Value);  // Split value to higher
    ShadowReg[Reg_pos]   |= LOWBYTE(Value); // and lower byte
  }
  else 
	{
    /* Resets the desired bits in register */
    ShadowReg[Reg_pos++] &= ( ~ HIBYTE( Value ) );  // Split value to higher
    ShadowReg[Reg_pos]   &= ( ~ LOWBYTE( Value ) );  //   and lower byte
  }
	//updateShadowDebugReg();
}
/**
  * @brief  Writes Multiple reg.
	* @note 	For Si4703 write always start from Reg address 02h high byte to 0Fh lower byte
	*					then automatically wraps around upper Higher byte of 00h and continues till stop condition(Circular way)
	*					Before Writing the registers is Updated in shadow reg then shadow reg are written
  * @param  Reg: Reg to write
	* @param  Addr: Device Address 
  * @param  Value: Data to be written
  * @param  Set_reset: Set or reset value in shadow registers
  * @retval None
  */

static uint8_t FM_IO_Write(uint8_t Addr,uint8_t Reg, uint16_t Value, uint8_t Set_reset)
{
	uint8_t wr_cnt;
	
	/* Calculate how much bytes need to be written */
	wr_cnt = ( Reg - 1 ) << 1; 
	/* Modify Register Byte in shadow Reg */
	FM_ModifyShadowReg(Reg,Value,Set_reset);
	/* Writing to register starts from 02h to 07h register */
	TUNER_IO_WriteMultiple(Addr,&ShadowReg[16],12); 
	
	return 0;	
}

/**
  * @brief  Read Multiple reg.
	* @note 	For Si4703 read always start from Reg address 0Ah high byte to 0Fh lower byte
	*					then automatically wraps around upper Higher byte of 00h and continues till stop condition(Circular way)
	* @param  Addr: Device Address  
  * @param  Com: FM_ALL_REG or FM_0A0B_REG
  * @retval None
  */
static uint8_t FM_IO_Read(uint8_t Addr, uint8_t Cmd)
{
	if(Cmd == FM_ALL_REG)
	{	/* Writing to register starts from 0x0A register */
		TUNER_IO_ReadMultiple(Addr, (uint8_t*)ShadowReg,32);
	}
	else if(Cmd == FM_0A0B_REG)
	{/* Writing to register starts from 0x0A register read only Two */
		TUNER_IO_ReadMultiple(Addr, (uint8_t*)ShadowReg,4);
	}
	return 0;	
}

/**
  * @brief Initializes the FM Tuner and the control interface.
  * @param DeviceAddr: Device address on communication Bus.  
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Init(uint16_t DeviceAddr)
{
  /* Initialize the Control interface of the FM Tuner */
  TUNER_IO_Init(); 
	
  //TUNER_Delay(500);
	/* Read All registers  in shadow reg */
	FM_IO_Read(DeviceAddr,FM_ALL_REG); 
	
	/* save CHIP ID */
	ChipID = ShadowReg[14] << 8;
	ChipID |= ShadowReg[15];
		
	/*Set the XOSCEN bit to power up the crystal Write data 8100h to 07h*/
	FM_IO_Write(DeviceAddr, SI4703_TEST1, 0x8100, REG_SET); 
	
	/* provide sufficient delay (minimum 500 ms) for the oscillator to stabilize */
	TUNER_Delay(600);
	/* Read All registers  in shadow reg */
	FM_IO_Read(DeviceAddr,FM_ALL_REG); 
	/* Return communication control value */
  return 0;  
}

/**
  * @brief  Get the si4703 ID.
  * @param  DeviceAddr: Device address on communication Bus.   
  * @retval The si4703 ID 
  */
uint32_t si4703_ReadID(uint16_t DeviceAddr)
{
  uint16_t DeviceID = 0;
	
	/* Initialize the Control interface of the FM Tuner */
  TUNER_IO_Init();
	
	TUNER_Delay(500);
	/* Read All registers  in shadow reg */
	FM_IO_Read(DeviceAddr,FM_ALL_REG); 
	
	DeviceID = (ShadowReg[12] << 8);
	DeviceID |= ShadowReg[13];
	return ((uint32_t)DeviceID);
}

/**
  * @brief Start the FM Tuner after configuring Registers.
  * @note Activate and power Up Si4703.
  * @param DeviceAddr: Device address on communication Bus.
	* @param Volume: Volune in range of 0 to 16 .
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Start(uint16_t DeviceAddr, uint8_t Volume)
{
	uint16_t active;
	
	active = ChipID;
	FM_IO_Write(DeviceAddr, SI4703_POWERCFG, 0x4001, REG_SET);
	/* Add Delay */
	TUNER_Delay(500);
	/* Wait for module powerup */
	while (active == ChipID) 
	{             
		FM_IO_Read(DeviceAddr,FM_ALL_REG);
    active = ShadowReg[14] << 8;
    active |= ShadowReg[15];
  }
	
	/* SEEKTH set 12 to skip poor channels */ 
	FM_IO_Write(DeviceAddr, SI4703_SYSCONFIG2, 0x0C00 | VOLUME_CONVERTFM(Volume), REG_SET); 
		/* Arun For Skiping poor channels */
	FM_IO_Write(DeviceAddr, SI4703_SYSCONFIG3, 0x0048 | VOLUME_CONVERTFM(Volume), REG_SET); 
	
	//Volume = 7 * 23;
	TUNER_Delay(100);
	
	/* Update Power on status */
	IsPowerOn = 1;
	
	return 0;  
}

/**
  * @brief  Stop the FM Tuner.
  * @param  DeviceAddr: Device address on communication Bus. 
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Stop(uint16_t DeviceAddr)
{  
  uint32_t counter = 0;
	
	FM_ModifyShadowReg(SI4703_POWERCFG, 0x4000, REG_RESET);
	
	FM_IO_Write(DeviceAddr, SI4703_POWERCFG, 0x0041, REG_SET);

	IsPowerOn = 0;
   
  return counter;
}

/**
  * @brief Tune FM Tuner to given frequency.
	* @note  This Function checks for valid frequency in band 
  * @param DeviceAddr: Device address on communication Bus.
  * @param Frequency: Frequency to tune in.
  * @param Direction: direction to tune in.
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_Tune(uint16_t DeviceAddr, uint16_t Frequency, uint8_t Direction)
{
  uint32_t counter = 0;
	
	uint16_t calc;
	
	uint8_t STC_bit= 0;
	
	/* If tune in frequency is valid use it for direct tuingin */
	if ( ( Frequency > 874 ) && (Frequency < 1081 ) )
  {
		calc = Frequency;
	}
	else
	{
		return 1;
	}
	/* Clear CHANNEL register in shadow reg */
	FM_ModifyShadowReg(SI4703_CHANNEL, 0x83FF, REG_RESET);
	/* Freq (MHz) = Spacing (MHz) x Channel + 87.5 MHz 
	 * Channel = (Freq (MHz) - 87.5 MHz) / Spacing (MHz) */ 
	calc -= 875;
	calc = calc / 2;
	calc |= 0x8000; 

	if (IsPowerOn == 1) 
	{                  
    /* Write modifiled frequency value */
		FM_IO_Write(DeviceAddr,SI4703_CHANNEL, calc, REG_SET);        
    TUNER_Delay(100);
		
		while (STC_bit == 0)
		{
      FM_IO_Read(DeviceAddr,FM_ALL_REG);
      STC_bit = ShadowReg[0] & 0x40;
    }
		/* set Tune =  0 */
		FM_IO_Write(DeviceAddr,SI4703_CHANNEL, 0x8000, REG_RESET); 
		
    while (STC_bit > 0) 
		{
      FM_IO_Read(DeviceAddr,FM_ALL_REG);
      STC_bit = ShadowReg[0] & 0x40;
    }
  }
  else
	{
    /* If power is off, only modify the shadow register */
		FM_ModifyShadowReg(SI4703_CHANNEL, calc, REG_SET); 
  }
  
  return counter;
}
/**
  * @brief Seek FM Tuner to given Direction.
	* @note This Function Seeks for valid frequiencies in band in given  direction
  * @param DeviceAddr: Device address on communication Bus.
  * @param Direction: direction tune in.
  * @retval 0 if correct communication, else wrong communication
  */

uint32_t si4703_Seek(uint16_t DeviceAddr, uint8_t Direction)
{
	uint8_t STC_bit = 0;
	uint8_t ST_bit = 0;
	uint8_t SF_BL_bit = 0;
	uint32_t channel = 0;
	uint32_t seek_start_time = HAL_GetTick();
	uint8_t timeout = 0;

  if (Direction == FM_DIR_UP)
    FM_IO_Write(DeviceAddr,SI4703_POWERCFG, 0x0300, REG_SET);       // Activate SEEK
  else if (Direction == FM_DIR_DOWN)
	{
    FM_ModifyShadowReg(SI4703_POWERCFG, 0x0200, REG_RESET); 					// Set seek direction Down
    FM_IO_Write(DeviceAddr,SI4703_POWERCFG, 0x0100, REG_SET);       // Activate SEEK
  }
  
  while( STC_bit == 0 )
	{
		FM_IO_Read(DeviceAddr,FM_ALL_REG);
		STC_bit = ShadowReg[0] << 1;
    STC_bit >>= 7;
    TUNER_Delay(100);
		if(HAL_GetTick() - seek_start_time > 10000)
		{
			timeout = 1;
			break;
		}
  }
	if(timeout)
		return 0;
	
  SF_BL_bit = ShadowReg[0] & 20;
  
  FM_IO_Write(DeviceAddr,0x02, 0x0100, REG_RESET);       // Deactivate SEEK
  
  while( STC_bit > 0 )
	{
    FM_IO_Read(DeviceAddr,FM_0A0B_REG);
    STC_bit = ShadowReg[0] << 1;
    STC_bit >>= 3;
		
		if(HAL_GetTick() - seek_start_time > 10000)
		{
			timeout = 1;
			break;
		}
  }
	
	if(timeout)
		return 0;
  
  if (SF_BL_bit == 0) 
	{
    FM_IO_Read(DeviceAddr,FM_0A0B_REG);
  }
	FM_IO_Read(DeviceAddr,FM_0A0B_REG);
	channel = (ShadowReg[2] & 0x03)<< 10;
	channel |= ShadowReg[3];
	return (channel * 2 + 875);
}

/**
  * @brief Set tuner Volume.
  * @param DeviceAddr: Device address on communication Bus.
  * @param Volume: value for Volume to be set
  * @retval 0 if correct communication, else wrong communication
  */
uint32_t si4703_SetVolume(uint16_t DeviceAddr, uint8_t Volume)
{
	uint8_t vol = VOLUME_CONVERTFM(Volume);
			
	if ( ( vol >= 0 ) && ( vol <= 16) ) 
	{
    vol &= 0x0F;
    FM_ModifyShadowReg(SI4703_SYSCONFIG2, 0x000F,REG_RESET);
		/* Write Volume value to SYSCONFIG2 register */
    FM_IO_Write(DeviceAddr,SI4703_SYSCONFIG2, vol, REG_SET);
	}
	return 0;
}

/**
  * @brief Enables or disables the mute feature on the tuner.
  * @param DeviceAddr: Device address on communication Bus.   
  * @param Cmd: TUNER_MUTE_ON to enable the mute or TUNER_MUTE_OFF to disable the
  *             mute mode.
  * @retval 0 if correct communication, else wrong communication
  */

uint32_t si4703_SetMute(uint16_t DeviceAddr, uint32_t Cmd)
{
	
	return 0;
}

uint8_t isNewChannelFound(uint32_t ch)
{
	uint8_t i;
	for(i = 0;i<MAX_CHANNELS;i++)
	{
		if(ch_updated[i] == ch)
		{
			return 1;
		}			
	} 
	return 0;	
}

void Si4703_GetChannelList(uint16_t DeviceAddr, uint8_t *res)
{
	uint8_t i;
	channels_seek_start_time = HAL_GetTick();
	uint8_t ch_updated_index = 0;
	
	for(i = 0;i<MAX_CHANNELS;i++)
	{
		channels[i] = 00;
		ch_updated[i] = 0;
	}
		
	//si4703_Tune(DeviceAddr,874,FM_DIR_UP);

	for(i =0 ; i<MAX_CHANNELS ; i++)
	{
		channels[i] = si4703_Seek(DeviceAddr,FM_DIR_UP);
		if(HAL_GetTick() - channels_seek_start_time > 20000)
			break;
	}
	
	for(i=0; i<MAX_CHANNELS; i++)
	{
		if(!isNewChannelFound(channels[i]))
		{
			ch_updated[ch_updated_index] = channels[i];
			ch_updated_index++;
		}	
	}
	
	sprintf((char*)res,",%d,%d,%d,%d,%d,%d,%d,%d",ch_updated[0],ch_updated[1],ch_updated[2],ch_updated[3],ch_updated[4],ch_updated[5],ch_updated[6],ch_updated[7]);
}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
