/**
  ******************************************************************************
  * @file    st7565r.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    1-Aug-2014
  * @brief   This file includes the LCD driver for st7565r LCD.
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "st7565r.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup st7565r
  * @brief     This file provides a set of functions needed to drive the 
  *            st7565r LCD.
  * @{
  */

/** @defgroup st7565r_Private_TypesDefinitions
  * @{
  */ 

/**
  * @}
  */ 

/** @defgroup st7565r_Private_Defines
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup st7565r_Private_Macros
  * @{
  */
     
/**
  * @}
  */  

/** @defgroup st7565r_Private_Variables
  * @{
  */ 
	
/* Screen buffer Requires at least one bit for every pixel */
uint8_t glcd_buffer[GLCD_LCD_WIDTH * GLCD_LCD_HEIGHT / 8];

/* Keeps track of bounding box of area on LCD which need to be updated next reresh cycle */
Glcd_BoundingBoxTypeDef Glcd_bbox;

/* Pointer to screen buffer currently in use. */
uint8_t *Glcd_buffer_selected;

/* Pointer to bounding box currently in use */
Glcd_BoundingBoxTypeDef *Glcd_bbox_selected;

//LCD_DrvTypeDef   st7565r_drv = 
//{
//  st7565r_Init,
//  st7565r_ReadID,
//  st7565r_DisplayOn,
//  st7565r_DisplayOff,
//  0,
//  st7565r_WritePixel,
//  st7565r_ReadPixel,
//  st7565r_SetDisplayWindow,
//  0,
//  0,
//  st7565r_GetLcdPixelWidth,
//  st7565r_GetLcdPixelHeight,
//  0,
//  0,  
//};

/**
  * @brief  Initialise the st7565r LCD Component.
  * @param  None
  * @retval None
  */
void st7565r_Init(void) 
{
		LCD_IO_Init();
		
		/* Power off the LCD */
		st7564r_SendCommand(GLCD_CMD_DISPLAY_OFF);// Off LCD
		LCD_Delay(100);
    // Set LCD bias to 1/9th
    st7564r_SendCommand(GLCD_CMD_BIAS_9);

    // Horizontal output direction (ADC segment driver selection)
    st7564r_SendCommand(GLCD_CMD_HORIZONTAL_NORMAL);

    // Vertical output direction (common output mode selection)
    st7564r_SendCommand(GLCD_CMD_VERTICAL_REVERSE);

    // Power control setting (datasheet step 7)
    // Note: Skipping straight to 0x7 works with my hardware.
		st7564r_SendCommand(GLCD_CMD_POWER_CONTROL | 0x7); 
		LCD_Delay(50); 
		
    // Set internal resistor.  A suitable value is used 
  	st7564r_SendCommand(GLCD_CMD_RESISTOR | 0x6);
		LCD_Delay(50); 
		
	 // Volume set (brightness control).  A suitable  value is used 
		st7564r_SendCommand(GLCD_CMD_VOLUME_MODE);
    st7564r_SendCommand(20);//20
    LCD_Delay(50); 

		// Reset start position to the top
    st7564r_SendCommand(GLCD_CMD_DISPLAY_START);

    // Turn the display on
    st7564r_SendCommand(GLCD_CMD_DISPLAY_ON);
		
		// Set screen
		st7564r_SelectScreen((uint8_t *)&glcd_buffer,&Glcd_bbox);

}
/**
  * @brief  Sends Command to st7564r.
  * @param  Cmd: Command to be send.
  * @retval None
  */
void st7564r_SendCommand(uint8_t Cmd)
{
	LCD_IO_WriteCmd(Cmd);
}
/**
  * @brief  Sends data to st7564r.
  * @param  Data: data to be send.
  * @retval None
  */
void st7564r_SendData(uint8_t Data)
{
	LCD_IO_WriteData(Data);
}

/**
  * @brief  Sets a display Contrast
  * @param  Value:  Contrast can be a 6-bit value (0 to 63).
  * @retval None
  */
void st7564r_SetContrast(uint8_t Value)
{
	/* Must send this command byte before setting the contrast */
	st7564r_SendCommand(GLCD_CMD_VOLUME_MODE);
	/* Set the contrat value ("electronic volumne register") */
	if (Value > 63)
	{
		st7564r_SendCommand(63);
	} 
	else
	{
		st7564r_SendCommand(Value);
	}
}

/**
  * @brief  Enables the Display.
  * @param  None
  * @retval None
  */
void st7564r_DisplayOn(void)
{
	
}
/**
  * @brief  Disables the Display.
  * @param  None
  * @retval None
  */
void st7564r_DisplayOff(void)
{
	
}

/**
  * @brief  Get the LCD pixel Width.
  * @param  None
  * @retval The Lcd Pixel Width
  */
uint16_t st7564r_GetLcdPixelWidth(void)
{
 return (uint16_t)GLCD_LCD_WIDTH;
}

/**
  * @brief  Get the LCD pixel Height.
  * @param  None
  * @retval The Lcd Pixel Height
  */
uint16_t st7564r_GetLcdPixelHeight(void)
{
 return (uint16_t)GLCD_LCD_HEIGHT;
}
/**
  * @brief  Set the display RAM page Address for Y Position.
	* @param  PageAddr : Display page Address 
  * @retval None
  */
void st7564r_SetYAddress(uint8_t PageAddr)
{
	st7564r_SendCommand(ST7565R_PAGE_ADDRESS_SET | (0x0F & PageAddr));	
}

/**
  * @brief  Set the display RAM Column Address for X Position.
	* @param  ColAddr : Display Column Address 
  * @retval None
  */
void st7564r_SetXAddress(uint8_t ColAddr)
{
	st7564r_SendCommand(ST7565R_COLUMN_ADDRESS_SET_UPPER | (ColAddr >> 4));
	st7564r_SendCommand(ST7565R_COLUMN_ADDRESS_SET_LOWER | (0x0F & ColAddr));
}

/**
  * @brief  Clear the display immediately, does not buffer
	* @param  None
  * @retval None
  */
void st7564r_ClearNow(void)
{
	uint8_t page;
	uint8_t col;
	
	for (page = 0; page < GLCD_NUMBER_OF_BANKS; page++)
	{
		st7564r_SetYAddress(page);
		st7564r_SetXAddress(0);
		for (col = 0; col < GLCD_NUMBER_OF_COLS; col++) 
		{
			st7564r_SendData(0);
		}			
	}
}

/**
  * @brief  Show a black and white line pattern on the display for test
	* @param  None
  * @retval None
  */
void st7564r_Pattern(void)
{
	uint8_t page;
	uint8_t col;
	
	for (page = 0; page < GLCD_NUMBER_OF_BANKS; page++) 
	{
		st7564r_SetYAddress(page);
		st7564r_SetXAddress(0);
		for (col = 0; col < GLCD_NUMBER_OF_COLS; col++) 
		{
			st7564r_SendData( (col / 8 + 2) % 2 == 1 ? 0xff : 0x00 );
		}			
	}
}


/******************* GLCD Functions ************************************/
/**
  * @brief  The bounding box defines a rectangle in which needs to be refreshed next time
	* 				st7564r_Write() is called. st7564r_Write() only writes to those pixels inside the 
	*					bounding box plus any surrounding pixels which are required according to the 
	*					bank/column write method of the controller.
	* @param xmin Minimum x value of rectangle
	* @param ymin Minimum y value of rectangle
	* @param xmax Maximum x value of rectangle
	* @param ymax Maximum y value of rectangle
  * @retval None
  */
void st7564r_UpdateBbox(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax)
{
	/* Keep and check bounding box within limits of LCD screen dimensions */
	if (xmin > (GLCD_LCD_WIDTH-1))	xmin = GLCD_LCD_WIDTH-1;
	if (xmax > (GLCD_LCD_WIDTH-1)) 	xmax = GLCD_LCD_WIDTH-1;
	if (ymin > (GLCD_LCD_HEIGHT-1)) ymin = GLCD_LCD_HEIGHT-1;
	if (ymax > (GLCD_LCD_HEIGHT-1)) ymax = GLCD_LCD_HEIGHT-1;

	/* Update the bounding box size */
	if (xmin < Glcd_bbox_selected->x_min) Glcd_bbox_selected->x_min = xmin;			
	if (xmax > Glcd_bbox_selected->x_max) Glcd_bbox_selected->x_max = xmax;
	if (ymin < Glcd_bbox_selected->y_min) Glcd_bbox_selected->y_min = ymin;
	if (ymax > Glcd_bbox_selected->y_max) Glcd_bbox_selected->y_max = ymax;
				
}
/**
  * @brief  Reset the bounding box
	* 				After resetting the bounding box, no pixels are marked as needing refreshing.
	* @param None
  * @retval None
  */
void st7564r_ResetBbox(void)
{
	/* Used after physically writing to the LCD */
	Glcd_bbox_selected->x_min = GLCD_LCD_WIDTH - 1;
	Glcd_bbox_selected->x_max = 0;
	Glcd_bbox_selected->y_min = GLCD_LCD_HEIGHT -1;
	Glcd_bbox_selected->y_max = 0;	
}
///**
// * Same as st7564r_ResetBbox()
// */
//void Glcd_bbox_reset() 
//{
//	st7564r_ResetBbox();
//}

/**
  * @brief  Marks the entire display for re-writing.
	*					Marks bounding box as entire screen, so on next st7564r_Write(), it writes the entire buffer to the LCD
	* @param 	None
  * @retval None
  */
void st7564r_RefreshBbox(void)
{
	Glcd_bbox_selected->x_min = 0;
	Glcd_bbox_selected->x_max = GLCD_LCD_WIDTH - 1;
	Glcd_bbox_selected->y_min = 0;
	Glcd_bbox_selected->y_max = GLCD_LCD_HEIGHT -1;		
}

/**
  * @brief  Clear the display. This will clear the buffer and physically write and commit it to the LCD
	* @param 	None
  * @retval None
  */
void st7564r_Clear(void)
{
	memset(Glcd_buffer_selected, 0x00, GLCD_LCD_WIDTH * GLCD_LCD_HEIGHT / 8);
	st7564r_UpdateBbox(0,0,GLCD_LCD_WIDTH - 1,GLCD_LCD_HEIGHT - 1);
	st7564r_Write();
}

/**
  * @brief  Clear the display buffer only. This does not physically write the changes to the LCD
	* @param 	None
  * @retval None
  */
void st7564r_ClearBuffer(void) 
{
	memset(Glcd_buffer_selected, 0x00, GLCD_LCD_WIDTH * GLCD_LCD_HEIGHT / 8);
	st7564r_UpdateBbox(0,0,GLCD_LCD_WIDTH - 1,GLCD_LCD_HEIGHT - 1);
}

/**
  * @brief  Select screen buffer and bounding box structure.
	*					This should be selected at initialisation. There are future plans to support multiple screen buffers
	* 				but this not yet available.
	* @param 	*buffer : Pointer to screen buffer
	* @param 	*bbox		: Pointer to bounding box object.
  * @retval None
  */
void st7564r_SelectScreen(uint8_t *buffer, Glcd_BoundingBoxTypeDef *bbox)
{
	Glcd_buffer_selected = buffer;
	Glcd_bbox_selected = bbox;
}

/**
  * @brief  Sets a single pixel.   
  * @param  Xpos: specifies the X position.
  * @param  Ypos: specifies the Y position.
  * @param  Color: Color can be BLACK or WHITE
  * @retval None
  */
void st7564r_SetPixel(uint8_t Xpos, uint8_t Ypos, uint8_t Color)
{
	if (Xpos < GLCD_LCD_WIDTH || Ypos < GLCD_LCD_HEIGHT) 
	{
		if (Color) /* set Black */
			glcd_buffer[Xpos+ (Ypos/8)*GLCD_LCD_WIDTH] |= ( 1 << (Ypos%8));
		else  /* set White */
			glcd_buffer[Xpos+ (Ypos/8)*GLCD_LCD_WIDTH] &= ~ (1 << (Ypos%8));

		st7564r_UpdateBbox(Xpos,Ypos,Xpos,Ypos);
	}
}

/**
  * @brief  Read a single pixel.   
  * @param  Xpos: specifies the X position.
  * @param  Ypos: specifies the Y position.
  * @retval Color: Color can be BLACK or WHITE
  */
uint8_t st7564r_GetPixel(uint8_t Xpos, uint8_t Ypos) 
{
	if ((Xpos < GLCD_LCD_WIDTH) || (Ypos < GLCD_LCD_HEIGHT)) 
	{
		if (glcd_buffer[Xpos+(Ypos/8)*GLCD_LCD_WIDTH] & ( 1 << (Ypos%8)) ) 
			return BLACK;
		else 
			return WHITE;
	}
}
/**
  * @brief  Displays a bitmap picture..
  * @param  Xpos:  Bmp X position in the LCD
  * @param  Ypos:  Bmp Y position in the LCD    
	* @param  BmpAddress: Bmp picture address.
  * @param  BmpHeignt:  Bmp Height in pixels 
  * @param  BmpWidth:  Bmp Width in pixels  
	* @retval None
  */
void st7564r_DrawBitmap(uint8_t Xpos,uint8_t Ypos,const uint8_t *BmpAddress,uint8_t BmpHeignt,uint8_t BmpWidth)
{
	for (uint8_t j=0; j<BmpHeignt; j++)
	{
    for (uint8_t i=0; i<BmpWidth; i++ )
		{
      if ((BmpAddress[ i + (j/8)*BmpWidth]) & (1 << (j%8))) 
			{
				st7564r_SetPixel(Xpos+i, Ypos+j, 1);
      }
    }
  }
}

/**
  * @brief  Invert Pixcel color
  * @param  Xpos:  Bmp X position in the LCD
  * @param  Ypos:  Bmp Y position in the LCD    
	* @retval None
  */
void st7564r_InvertPixel(uint8_t Xpos, uint8_t Ypos) 
	{
	if ((Xpos >= GLCD_LCD_WIDTH) || (Ypos >= GLCD_LCD_HEIGHT)) 
	{
		return;
	}
	st7564r_UpdateBbox(Xpos,Ypos,Xpos,Ypos);
	glcd_buffer[Xpos+ (Ypos/8)*GLCD_LCD_WIDTH] ^= ( 1 << (Ypos%8));
}

/**
  * @brief  Changes physical containt on screen 
	*					updates display buffer as per Glcd_bbox_selected and send data to st7564r 
  * @retval None
  */
void st7564r_Write(void)
{
	uint8_t bank;

	for (bank = 0; bank < GLCD_NUMBER_OF_BANKS; bank++) {
		/* Each bank is a single row 8 bits tall */
		uint8_t column;		
		
		if (Glcd_bbox_selected->y_min >= (bank+1)*8) {
			continue; /* Skip the entire bank */
		}
		
		if (Glcd_bbox_selected->y_max < bank*8) {
			break;    /* No more banks need updating */
		}
		
		st7564r_SetYAddress(bank);
		st7564r_SetXAddress(Glcd_bbox_selected->x_min);

		for (column = Glcd_bbox_selected->x_min; column <= Glcd_bbox_selected->x_max; column++)
		{
			st7564r_SendData( Glcd_buffer_selected[GLCD_NUMBER_OF_COLS * bank + column] );
		}
	}

	st7564r_ResetBbox();
}

/**
 * gets pointer to glcd Buffer
 * @retval pointer to glcd Buffer
 */

uint8_t* st7564r_Getbufferselected(void)
{
	return Glcd_buffer_selected;
}


/**
 * Scroll screen buffer up by 8 pixels.
 * This is designed to be used in conjunciton with tiny text functions which are 8 bits high.
 * @see Tiny Text
 */
void st7564r_ScrollLine(void)
{
	uint8_t y;
	uint8_t number_of_rows = GLCD_LCD_HEIGHT / 8;
	for (y=0; y<number_of_rows; y++) {
		if (y < (number_of_rows - 1)) {
			/* All lines except the last */
			memcpy(Glcd_buffer_selected + y*GLCD_LCD_WIDTH, Glcd_buffer_selected + y*GLCD_LCD_WIDTH + GLCD_LCD_WIDTH, GLCD_LCD_WIDTH);
		} else {
			/* Last line, clear it */
			memset(Glcd_buffer_selected + (number_of_rows - 1)*GLCD_LCD_WIDTH, 0x00, GLCD_LCD_WIDTH);
		}
	}
	st7564r_UpdateBbox(0,0,GLCD_LCD_WIDTH - 1,GLCD_LCD_HEIGHT - 1);
}

void st7564r_ScrollRight(uint8_t x)
	{

	st7564r_RefreshBbox();
	for (uint8_t l = 0; l < 8; ++l)
	{
        uint8_t* p = glcd_buffer + (l * 128);
        for (uint8_t b = GLCD_LCD_WIDTH-1; b >= x; --b)
            *(p+b) = *(p+b-x);
        for (uint8_t b = 0; b < x; ++b)
            *(p+b) = 0;
    }
}

/******************************************************************************/










/*******************************************************************************
															st7565r Functions
*******************************************************************************/


/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */

  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
