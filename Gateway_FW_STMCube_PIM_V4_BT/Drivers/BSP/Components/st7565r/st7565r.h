/**
  ******************************************************************************
  * @file    st7565r.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    1-Aug-2014
  * @brief   This file contains all the functions prototypes for the st7565r.c
  *          driver.
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __st7565r_H
#define __st7565r_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
	#include "..\Common\lcd.h"
	#include <string.h>

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup st7565r
  * @{
  */

/** @defgroup st7565r_Exported_Types
  * @{
  */
   
/**
  * @} GLCD Display Window typedef for pixels that need to be updated
  */ 
typedef struct
{
	uint8_t x_min;
	uint8_t y_min;
	uint8_t x_max;
	uint8_t y_max;
} Glcd_DisplayWindowTypeDef;

/** @defgroup st7565r_Exported_Constants
  * @{
  */
/******************************************************************************
														LCD User defines 
******************************************************************************/
/** 
  * @brief  st7565r Screen Size  
  */  
#define ST7565R_LCD_WIDTH 128
#define ST7565R_LCD_HEIGHT 65
#define ST7565R_NUMBER_OF_BANKS (ST7565R_LCD_WIDTH / 8)
#define ST7565R_NUMBER_OF_COLS  ST7565R_LCD_WIDTH


#define BLACK 1
#define WHITE 0
/** 
  * @brief  st7565r Registers  
  */ 
/**
  * @}
  */
  
/** @defgroup st7565r_Exported_Functions
  * @{
  */ 

/******************************************************************************
														LCD Command Set 
******************************************************************************/
/* Command: turn the display on */
#define ST7565RCMD_DISPLAY_ON 						0xAF //0b10101111
/* Command: turn the display off */
#define ST7565RCMD_DISPLAY_OFF 						0xAE  //0b10101110
/* Command: set all points on the screen to normal. */
#define ST7565R_DISPLAY_NORMAL					0xA4  //0b10100100
/* Command: set all points on the screen to "on", without affecting the internal screen buffer. */
#define ST7565R_DISPLAY_ALL_ON  				0xA5  //0b10100101
/* Command: disable inverse (black pixels on a white background) */
#define ST7565RCMD_DISPLAY_NORMAL					0xA6 //0b10100110
/* Command: inverse the screen (white pixels on a black background) */
#define ST7565RCMD_DISPLAY_REVERSE				0xA7    // 0b10100111
/* Command: set LCD bias to 1/9th */
#define ST7565RCMD_BIAS_9									0xA2 //0b10100010
/* Command: set LCD bias to 1/7th */
#define ST7565RCMD_BIAS_7									0xA3 //0b10100011
/* Command: set ADC output direction to normal. */
#define ST7565RCMD_HORIZONTAL_NORMAL			0xA0 //0b10100000
/* Command: set ADC output direction reverse (horizontally flipped). 
             Note that you should use the glcd_flip_screen function so that
             the width is correctly accounted for. */
#define ST7565RCMD_HORIZONTAL_REVERSE   	0xA1 //0b10100001
/* Command: set common output scan direction to normal. */
#define ST7565RCMD_VERTICAL_NORMAL				0xC0 //0b11000000
/* Command: set common output scan direction to reversed (vertically flipped). */
#define ST7565RCMD_VERTICAL_REVERSE	 			0xC8//0b11001000
/* Command: select the internal power supply operating mode. */
#define ST7565RCMD_POWER_CONTROL					0x28//0b00101000
/* Command: set internal R1/R2 resistor bias (OR with 0..7) */
#define ST7565RCMD_RESISTOR								0x20 //0b00100000
/* Command: enter volume mode, send this then send another command
             byte with the contrast (0..63).  The second command
			 must be sent for the GLCD to exit volume mode. */
#define ST7565RCMD_VOLUME_MODE						0x81//0b10000001

#define ST7565RCMD_DISPLAY_START					0x40 //0b01000000

/* Command: set the least significant 4 bits of the column address. */
#define ST7565R_COLUMN_ADDRESS_SET_LOWER	0x00 //0b00000000
/* Command: set the most significant 4 bits of the column address. */
#define ST7565R_COLUMN_ADDRESS_SET_UPPER 	0x10//0b00010000
/* Command: Set the current page (0..7). */
#define ST7565R_PAGE_ADDRESS_SET					0xB0//0b10110000

/* Command: software reset (note: should be combined with toggling ST7565RRS) */
#define ST7565RCMD_RESET										0xE2//0b11100010

/* Command: no operation (note: the datasheet suggests sending this periodically
             to keep the data connection alive) */
#define	ST7565RCMD_NOP											0xE3//0b11100011


/******************************************************************************
														LCD Functions 
******************************************************************************/
void st7565r_Init(void);
uint16_t st7565r_ReadID(void);
void st7565r_DisplayOn(void);
void st7565r_DisplayOff(void);
uint16_t st7565r_GetLcdPixelWidth(void);
uint16_t st7565r_GetLcdPixelHeight(void);
void st7565r_SetPixel(uint8_t Xpos, uint8_t Ypos, uint8_t Color);
uint8_t st7565r_GetPixel(uint8_t Xpos, uint8_t Ypos);
void st7565r_InvertPixel(uint8_t Xpos, uint8_t Ypos);
void st7565r_SetDisplayWindow(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax);
uint8_t* st7565r_GetScreenBuffer(void);
void st7565r_WriteDisplayBuffer(void);
void st7565r_ClearDisplayBuffer(void);
void st7565r_ClearScreen(void);
void st7565r_DrawBitmap(uint8_t Xpos,uint8_t Ypos,const uint8_t *BmpAddress,uint8_t BmpHeignt,uint8_t BmpWidth);



/* LCD driver structure */
extern LCD_DrvTypeDef   st7565r_drv;

/* LCD IO functions */
void 						LCD_IO_Init(void);
void 						LCD_IO_WriteData(uint8_t Data);
void 						LCD_IO_WriteCmd(uint8_t Cmd);
void 						LCD_Delay(uint32_t Delay);
/**
  * @}
  */ 
      
#ifdef __cplusplus
}
#endif

#endif /* __st7565r_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
