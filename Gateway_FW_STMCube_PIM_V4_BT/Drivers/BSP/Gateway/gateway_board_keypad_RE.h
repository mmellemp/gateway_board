/**
  ******************************************************************************
  * @file    gateway_board_keypad_RE.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   This file contains definitions for Gateway_Board's Switches and Rotory encoders
  ******************************************************************************
  */ 
	
	/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_KEYPAD_RE_H
#define __GATEWAY_BOARD_KEYPAD_RE_H

#ifdef __cplusplus
 extern "C" {
#endif
	 
/* Includes ------------------------------------------------------------------*/
#include "gateway_board.h"
	 
typedef enum 
{
	Key_Vol_Center = 0,
	Key_Sel_Center = 1,
	NOKEY					 = 0xFF
} REKeyPressCode;	 
	 
typedef enum 
{
	TM_RE_Rotate_Increment,
	TM_RE_Rotate_Decrement,
	TM_RE_Rotate_Nothing
} TM_RE_Rotate_t;

/**
 * Main struct
 *
 * Parameters:
 * 	- int32_t Absolute
 * 		Absolute value of rotation from beginning of program
 *  - int32_t Diff
 *  	Rotary difference from last check
 *  - TM_RE_Rotate_t Rotation
 *  	It allows you to check if rotary was increased, decreased or nothing
 */
typedef struct
{
	int32_t Absolute;				/* Absolute rotation from beginning */
	int32_t Diff; 					/* Rotary difference from last check */
	TM_RE_Rotate_t Rotation;		/* Increment, Decrement or nothing */
} TM_RE_t;


#define	RE_VOLUMEUP					0
#define	RE_VOLUMEDOWN 			1
#define	RE_SELNEXT					2
#define	RE_SELPREVIOUS			3
#define	RE_VOLUMEPRESSED		4
#define	RE_SELPRESSED				5
#define	RE_NOEVENT					0xFF

#define RE_VOL_A_PIN                				GPIO_PIN_2
#define RE_VOL_A_GPIO_PORT          				GPIOI
#define RE_VOL_A_GPIO_CLK_ENABLE()  				__GPIOI_CLK_ENABLE()
#define RE_VOL_A_GPIO_CLK_DISABLE() 				__GPIOI_CLK_DISABLE()
#define RE_VOL_A_EXTI_IRQn          					EXTI2_IRQn 
#define RE_VOL_A_EXTI_IRQHandler     					EXTI2_IRQHandler 

#define RE_VOL_B_PIN                				GPIO_PIN_0
#define RE_VOL_B_GPIO_PORT          				GPIOI
#define RE_VOL_B_GPIO_CLK_ENABLE()  				__GPIOI_CLK_ENABLE()
#define RE_VOL_B_GPIO_CLK_DISABLE() 				__GPIOI_CLK_DISABLE()


#define RE_VOL_SW_PIN                				GPIO_PIN_1
#define RE_VOL_SW_GPIO_PORT          				GPIOI
#define RE_VOL_SW_GPIO_CLK_ENABLE()  				__GPIOI_CLK_ENABLE()
#define RE_VOL_SW_GPIO_CLK_DISABLE() 				__GPIOI_CLK_DISABLE()
 

#define RE_SEL_A_PIN                				GPIO_PIN_3
#define RE_SEL_A_GPIO_PORT          				GPIOI
#define RE_SEL_A_GPIO_CLK_ENABLE()  				__GPIOI_CLK_ENABLE()
#define RE_SEL_A_GPIO_CLK_DISABLE() 				__GPIOI_CLK_DISABLE()
#define RE_SEL_A_EXTI_IRQn          					EXTI3_IRQn
#define RE_SEL_A_EXTI_IRQHandler     					EXTI3_IRQHandler


#define RE_SEL_B_PIN                				GPIO_PIN_11
#define RE_SEL_B_GPIO_PORT          				GPIOB
#define RE_SEL_B_GPIO_CLK_ENABLE()  				__GPIOB_CLK_ENABLE()
#define RE_SEL_B_GPIO_CLK_DISABLE() 				__GPIOB_CLK_DISABLE()


#define RE_SEL_SW_PIN                				GPIO_PIN_11
#define RE_SEL_SW_GPIO_PORT          				GPIOH
#define RE_SEL_SW_GPIO_CLK_ENABLE()  				__GPIOH_CLK_ENABLE()
#define RE_SEL_SW_GPIO_CLK_DISABLE() 				__GPIOH_CLK_DISABLE()


void BSP_KEYPAD_Init(void);
uint8_t BSP_KEYPAD_GeykeyPressed(void);
void BSP_ENCODER_Init(void);
uint8_t BSP_ENCODER_GetEvent(void);
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_KEYPAD_RE_H */	 