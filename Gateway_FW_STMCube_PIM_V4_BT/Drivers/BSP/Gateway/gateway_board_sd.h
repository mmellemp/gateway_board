/**
  ******************************************************************************
  * @file    gateway_board_sd.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    8-Aug-2014
  * @brief   This file contains the common defines and functions prototypes for
  *          the gateway_board_sd.c driver.
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_SD_H
#define __GATEWAY_BOARD_SD_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup GATEWAY_BOARD
  * @{
  */
    
/** @defgroup GATEWAY_BOARD_SD
  * @{
  */    

/* Exported types ------------------------------------------------------------*/

/** @defgroup GATEWAY_BOARD_SD_Exported_Types
  * @{
  */
  

/* Exported constants --------------------------------------------------------*/ 

/** 
  * @brief  SD status structure definition  
  */     
#define   MSD_OK         0x00
#define   MSD_ERROR      0x01
   
/** @defgroup GATEWAY_BOARD_SD_Exported_Constants
  * @{
  */ 
#define SD_PRESENT               ((uint8_t)0x01)
#define SD_NOT_PRESENT           ((uint8_t)0x00)


#define SD_DETECT_PIN                    GPIO_PIN_13
#define SD_DETECT_GPIO_PORT              GPIOH
#define __SD_DETECT_GPIO_CLK_ENABLE()    __GPIOH_CLK_ENABLE()
#define SD_DETECT_IRQn                   EXTI15_10_IRQn


#define SD_DATATIMEOUT           ((uint32_t)100000000)
    
/* DMA definitions for SD DMA transfer */
#define __DMAx_TxRx_CLK_ENABLE            __DMA2_CLK_ENABLE
#define SD_DMAx_Tx_CHANNEL                DMA_CHANNEL_4
#define SD_DMAx_Rx_CHANNEL                DMA_CHANNEL_4
#define SD_DMAx_Tx_STREAM                 DMA2_Stream6  
#define SD_DMAx_Rx_STREAM                 DMA2_Stream3  
#define SD_DMAx_Tx_IRQn                   DMA2_Stream6_IRQn
#define SD_DMAx_Rx_IRQn                   DMA2_Stream3_IRQn
#define SD_DMAx_Tx_IRQHandler             DMA2_Stream6_IRQHandler
#define SD_DMAx_Rx_IRQHandler             DMA2_Stream3_IRQHandler
#define SD_DetectIRQHandler()             HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13)

/**
  * @}
  */
  
/* Exported macro ------------------------------------------------------------*/
  
/** @defgroup GATEWAY_BOARD_SD_Exported_Macro
  * @{
  */ 

/* Exported functions --------------------------------------------------------*/

/** @defgroup GATEWAY_BOARD_SD_Exported_Functions
  * @{
  */   
uint8_t BSP_SD_Init(void);
uint8_t BSP_SD_ITConfig(void);
void BSP_SD_DetectIT(void);
void BSP_SD_DetectCallback(void);
uint8_t BSP_SD_ReadBlocks(uint32_t *pData, uint64_t ReadAddr, uint32_t BlockSize, uint32_t NumOfBlocks);
uint8_t BSP_SD_WriteBlocks(uint32_t *pData, uint64_t WriteAddr, uint32_t BlockSize, uint32_t NumOfBlocks);
uint8_t BSP_SD_ReadBlocks_DMA(uint32_t *pData, uint64_t ReadAddr, uint32_t BlockSize, uint32_t NumOfBlocks);
uint8_t BSP_SD_WriteBlocks_DMA(uint32_t *pData, uint64_t WriteAddr, uint32_t BlockSize, uint32_t NumOfBlocks);
uint8_t BSP_SD_Erase(uint64_t StartAddr, uint64_t EndAddr);
void BSP_SD_IRQHandler(void);
void BSP_SD_DMA_Tx_IRQHandler(void);
void BSP_SD_DMA_Rx_IRQHandler(void);
HAL_SD_TransferStateTypedef BSP_SD_GetStatus(void);
void BSP_SD_GetCardInfo(HAL_SD_CardInfoTypedef *CardInfo);
uint8_t BSP_SD_IsDetected(void);
 
   
#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_SD_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
