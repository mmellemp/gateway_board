/**
  ******************************************************************************
  * @file    gateway_board_Ecan.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   This file contains the common defines and functions prototypes for
  *          the Gateway_Board_Ecan.c driver.
  ******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_CGI_H
#define __GATEWAY_BOARD_CGI_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* Include Ecan component Driver */
#include "gateway_board.h"
#include "CGI_Faceplate.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Gateway_Board
  * @{
  */
    
/** @defgroup Gateway_Board_Ecan
  * @{
  */


/** @defgroup Gateway_Board_ecan_Exported_Types
  * @{
  */

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Exported_Constants
  * @{
  */

typedef enum
{
	CGI_OK = 0,
	CGI_FAIL = -1
}CGI_status_typedef;


CGI_status_typedef BSP_CGI_Init(void);
CGI_status_typedef BSP_CGI_Process(void);
CGI_status_typedef BSP_CGI_getEvent(void);
CGI_status_typedef BSP_CGI_Transmit_Data(const uint8_t* data, uint8_t dataLen);


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_CGI_H */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
