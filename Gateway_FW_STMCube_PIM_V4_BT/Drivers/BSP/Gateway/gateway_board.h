/**
  ******************************************************************************
  * @file    gateway_board.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-2014 
  * @brief   This file contains definitions for Gateway_Board's LEDs, 
  *          push-buttons and COM ports hardware resources.
  ******************************************************************************
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_H
#define __GATEWAY_BOARD_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include <assert.h>	 
   
/** @addtogroup Utilities
  * @{
  */

/** @addtogroup STM32_EVAL
  * @{
  */

/** @addtogroup Gateway_Board
  * @{
  */
      
/** @addtogroup Gateway_Board_LOW_LEVEL
  * @{
  */ 

/** @defgroup Gateway_Board_LOW_LEVEL_Exported_Types
  * @{
  */
typedef enum 
{
  LED1 = 0,
  LED2 = 1,
  LED3 = 2,
  LED4 = 3,
	LED5 = 4

}Led_TypeDef;

;

typedef enum 
{  
  BUTTON_1 	= 0,
  BUTTON_2 	= 1,
	BUTTON_3 	= 2,
  BUTTON_4 	= 3,
  BUTTON_5 	= 4,
	BUTTON_6  = 5

}Button_TypeDef;

//#define NOKEY 0xFF

typedef enum 
{  
  BUTTON_MODE_GPIO = 0,
  BUTTON_MODE_EXTI = 1

}ButtonMode_TypeDef;

typedef enum 
{
  COM1 = 0,
  COM2,
	COM3,
	COM4,
	COM5
}COM_TypeDef;

typedef enum
{
	LINA = 0,
	LINB
}LIN_Typedef;

typedef enum
{
	SPI_PORT1 = 0,	/*LCD can be connected*/
	SPI_PORT2,			/*GMLAN*/
	SPI_PORT3,			/*SWCAN*/
	SPI_PORT4,			/*CANB*/
	SPI_PORT5,			
	SPI_PORT6				
}SPI_Port_TypeDef;

typedef enum
{
	SWCAN_SLEEP = 0,
	SWCAN_HV_WAKE_UP,
	SWCAN_HS,
	SWCAN_NORMAL
}SWCAN_Mode_Typedef;

typedef enum
{
	I2C_PORT1 = 0,
	I2C_PORT2,
}I2C_Port_Typedef;

#define FALSE 0
#define TRUE  1
#if ! defined(OBJC_BOOL_DEFINED)
typedef signed char BOOL;
#endif

/**
  * @}
  */ 

/** @defgroup Gateway_Board_LOW_LEVEL_Exported_Constants
  * @{
  */ 

/** 
  * @brief  Define for Gateway_Board board  
  */ 
#if !defined (USE_GATEWAY_BOARD)
 #define USE_GATEWAY_BOARD
#endif


/* Exported constant IO ------------------------------------------------------*/
#define AUDIO_I2C_ADDRESS									0x34 // A0 connected to Vcc 2.5V
#define EEPROM_I2C_ADDRESS_A01           	0xA0
#define EEPROM_I2C_ADDRESS_A02           	0xA6

#define MUXR_I2C_ADDRESS           			 	0x6A
#define MUXL_I2C_ADDRESS           				0x6E

#define TUNER_I2C_ADDRESS									0xC6


/* I2C clock speed configuration (in Hz) 
   WARNING: 
   Make sure that this define is not already declared in other files (ie. 
   Gateway_Board.h file). It can be used in parallel by other modules. */
extern uint32_t I2C_speed;

#ifndef I2C_SPEED
 #define I2C_SPEED_LOW                        50000
 #define I2C_SPEED_HIGH                       400000
#endif /* I2C_SPEED */


/** @addtogroup Gateway_Board_LOW_LEVEL_LED
  * @{
  */
#define LEDn                             5

#define LED1_PIN                         GPIO_PIN_9
#define LED1_GPIO_PORT                   GPIOG
#define LED1_GPIO_CLK_ENABLE()           __GPIOG_CLK_ENABLE()  
#define LED1_GPIO_CLK_DISABLE()          __GPIOG_CLK_DISABLE()  
    
    
#define LED2_PIN                         GPIO_PIN_10
#define LED2_GPIO_PORT                   GPIOG
#define LED2_GPIO_CLK_ENABLE()           __GPIOG_CLK_ENABLE()   
#define LED2_GPIO_CLK_DISABLE()          __GPIOG_CLK_DISABLE()  
  
#define LED3_PIN                         GPIO_PIN_11
#define LED3_GPIO_PORT                   GPIOG
#define LED3_GPIO_CLK_ENABLE()           __GPIOG_CLK_ENABLE()   
#define LED3_GPIO_CLK_DISABLE()          __GPIOG_CLK_DISABLE()  
  
#define LED4_PIN                         GPIO_PIN_8
#define LED4_GPIO_PORT                   GPIOC
#define LED4_GPIO_CLK_ENABLE()           __GPIOC_CLK_ENABLE()   
#define LED4_GPIO_CLK_DISABLE()          __GPIOC_CLK_DISABLE()  

#define LED5_PIN                         GPIO_PIN_7
#define LED5_GPIO_PORT                   GPIOG
#define LED5_GPIO_CLK_ENABLE()           __GPIOG_CLK_ENABLE()   
#define LED5_GPIO_CLK_DISABLE()          __GPIOG_CLK_DISABLE()  
    
#define LEDx_GPIO_CLK_ENABLE(__INDEX__)   (((__INDEX__) == 0) ? LED1_GPIO_CLK_ENABLE() :\
                                           ((__INDEX__) == 1) ? LED2_GPIO_CLK_ENABLE() :\
                                           ((__INDEX__) == 2) ? LED3_GPIO_CLK_ENABLE() :\
																					 ((__INDEX__) == 3) ? LED4_GPIO_CLK_ENABLE() :LED5_GPIO_CLK_ENABLE())

#define LEDx_GPIO_CLK_DISABLE(__INDEX__)  (((__INDEX__) == 0) ? LED1_GPIO_CLK_DISABLE() :\
                                           ((__INDEX__) == 1) ? LED2_GPIO_CLK_DISABLE() :\
                                           ((__INDEX__) == 2) ? LED3_GPIO_CLK_DISABLE() :\
																					 ((__INDEX__) == 3) ? LED4_GPIO_CLK_DISABLE() :LED5_GPIO_CLK_DISABLE())

/**
  * @}
  */ 
  
/** @addtogroup Gateway_Board_LOW_LEVEL_BUTTON
  * @{
  */ 

#define BUTTONn                          6  

#define BUTTON_1_PIN                				GPIO_PIN_10
#define BUTTON_1_GPIO_PORT          				GPIOH
#define BUTTON_1_GPIO_CLK_ENABLE()  				__GPIOH_CLK_ENABLE()
#define BUTTON_1_GPIO_CLK_DISABLE() 				__GPIOH_CLK_DISABLE()

#define BUTTON_2_PIN                				GPIO_PIN_9
#define BUTTON_2_GPIO_PORT          				GPIOH
#define BUTTON_2_GPIO_CLK_ENABLE()  				__GPIOH_CLK_ENABLE()
#define BUTTON_2_GPIO_CLK_DISABLE() 				__GPIOH_CLK_DISABLE()


#define BUTTON_3_PIN                				GPIO_PIN_8
#define BUTTON_3_GPIO_PORT          				GPIOH
#define BUTTON_3_GPIO_CLK_ENABLE()  				__GPIOH_CLK_ENABLE()
#define BUTTON_3_GPIO_CLK_DISABLE() 				__GPIOH_CLK_DISABLE()
 

#define BUTTON_4_PIN                				GPIO_PIN_7
#define BUTTON_4_GPIO_PORT          				GPIOH
#define BUTTON_4_GPIO_CLK_ENABLE()  				__GPIOH_CLK_ENABLE()
#define BUTTON_4_GPIO_CLK_DISABLE() 				__GPIOH_CLK_DISABLE()


#define BUTTON_5_PIN                				GPIO_PIN_6
#define BUTTON_5_GPIO_PORT          				GPIOH
#define BUTTON_5_GPIO_CLK_ENABLE()  				__GPIOH_CLK_ENABLE()
#define BUTTON_5_GPIO_CLK_DISABLE() 				__GPIOH_CLK_DISABLE()


#define BUTTON_6_PIN                				GPIO_PIN_10
#define BUTTON_6_GPIO_PORT          				GPIOE
#define BUTTON_6_GPIO_CLK_ENABLE()  				__GPIOE_CLK_ENABLE()
#define BUTTON_6_GPIO_CLK_DISABLE() 				__GPIOE_CLK_DISABLE()


#define BUTTONx_GPIO_CLK_ENABLE(__INDEX__)		(((__INDEX__) == 0) ? BUTTON_1_GPIO_CLK_ENABLE() :\
																						   ((__INDEX__) == 1) ? BUTTON_2_GPIO_CLK_ENABLE() :\
																							 ((__INDEX__) == 2) ? BUTTON_3_GPIO_CLK_ENABLE() :\
																							 ((__INDEX__) == 3) ? BUTTON_4_GPIO_CLK_ENABLE() :\
                                               ((__INDEX__) == 4) ? BUTTON_5_GPIO_CLK_ENABLE() : BUTTON_6_GPIO_CLK_ENABLE())

#define BUTTONx_GPIO_CLK_DISABLE(__INDEX__)   (((__INDEX__) == 0) ? BUTTON_1_GPIO_CLK_DISABLE() :\
																						   ((__INDEX__) == 1) ? BUTTON_2_GPIO_CLK_DISABLE() :\
																							 ((__INDEX__) == 2) ? BUTTON_3_GPIO_CLK_DISABLE() :\
																							 ((__INDEX__) == 3) ? BUTTON_4_GPIO_CLK_ENABLE() 	:\
                                               ((__INDEX__) == 4) ? BUTTON_5_GPIO_CLK_DISABLE() : BUTTON_6_GPIO_CLK_DISABLE())

#define KEY_PRESSED     0x00
#define KEY_NOT_PRESSED 0x01

/**************************************************Board Low level COM*****************************************************************************/
#define COMn                             5

/*@brief Definition for COM port1(Bluetooth original), connected to USART2*/
#define EVAL_COM1                          USART2
#define EVAL_COM1_CLK_ENABLE()             __USART2_CLK_ENABLE()   
#define EVAL_COM1_CLK_DISABLE()            __USART2_CLK_DISABLE()

#define EVAL_COM1_TX_PIN                   GPIO_PIN_2
#define EVAL_COM1_TX_GPIO_PORT             GPIOA
#define EVAL_COM1_TX_GPIO_CLK_ENABLE()     __GPIOA_CLK_ENABLE()   
#define EVAL_COM1_TX_GPIO_CLK_DISABLE()    __GPIOA_CLK_DISABLE()  
#define EVAL_COM1_TX_AF                    GPIO_AF7_USART2

#define EVAL_COM1_RX_PIN                   GPIO_PIN_3
#define EVAL_COM1_RX_GPIO_PORT             GPIOA
#define EVAL_COM1_RX_GPIO_CLK_ENABLE()     __GPIOA_CLK_ENABLE()   
#define EVAL_COM1_RX_GPIO_CLK_DISABLE()    __GPIOA_CLK_DISABLE()  
#define EVAL_COM1_RX_AF                    GPIO_AF7_USART2
#define EVAL_COM1_IRQn                     USART2_IRQn
//#define EVAL_COM1_IRQ_Handler							 USART2_IRQHandler


/*@brief Definition for COM port2(Bluetooth Changed), connected to UART5*/
#define EVAL_COM2                          UART5
#define EVAL_COM2_CLK_ENABLE()             __UART5_CLK_ENABLE()   
#define EVAL_COM2_CLK_DISABLE()            __UART5_CLK_DISABLE()

#define EVAL_COM2_TX_PIN                   GPIO_PIN_12
#define EVAL_COM2_TX_GPIO_PORT             GPIOC
#define EVAL_COM2_TX_GPIO_CLK_ENABLE()     __GPIOC_CLK_ENABLE()   
#define EVAL_COM2_TX_GPIO_CLK_DISABLE()    __GPIOC_CLK_DISABLE()  
#define EVAL_COM2_TX_AF                    GPIO_AF8_UART5

#define EVAL_COM2_RX_PIN                   GPIO_PIN_2
#define EVAL_COM2_RX_GPIO_PORT             GPIOD
#define EVAL_COM2_RX_GPIO_CLK_ENABLE()     __GPIOD_CLK_ENABLE()   
#define EVAL_COM2_RX_GPIO_CLK_DISABLE()    __GPIOD_CLK_DISABLE()  
#define EVAL_COM2_RX_AF                    GPIO_AF8_UART5
#define EVAL_COM2_IRQn                     UART5_IRQn
//#define EVAL_COM2_IRQ_Handler							 UART5_IRQHandler
#define Bluetooth_IRQ_Handler							 UART5_IRQHandler


/*@brief Definition for COM port3(LIN-B), connected to USART2*/
#define EVAL_COM3                          USART2
#define EVAL_COM3_CLK_ENABLE()             __USART2_CLK_ENABLE()   
#define EVAL_COM3_CLK_DISABLE()            __USART2_CLK_DISABLE()

#define EVAL_COM3_TX_PIN                   GPIO_PIN_5
#define EVAL_COM3_TX_GPIO_PORT             GPIOD
#define EVAL_COM3_TX_GPIO_CLK_ENABLE()     __GPIOD_CLK_ENABLE()   
#define EVAL_COM3_TX_GPIO_CLK_DISABLE()    __GPIOD_CLK_DISABLE()  
#define EVAL_COM3_TX_AF                    GPIO_AF7_USART2

#define EVAL_COM3_RX_PIN                   GPIO_PIN_6
#define EVAL_COM3_RX_GPIO_PORT             GPIOD
#define EVAL_COM3_RX_GPIO_CLK_ENABLE()     __GPIOD_CLK_ENABLE()   
#define EVAL_COM3_RX_GPIO_CLK_DISABLE()    __GPIOD_CLK_DISABLE()  
#define EVAL_COM3_RX_AF                    GPIO_AF7_USART2
#define EVAL_COM3_IRQn                     USART2_IRQn
//#define EVAL_COM3_IRQ_Handler							 USART2_IRQHandler
#define LINB_IRQ_Handler				 						USART2_IRQHandler												/*Same IRQ handler for both LINB and Bluetooth*/


/*@brief Definition for COM port4(LIN-A), connected to USART7*/
#define EVAL_COM4                          UART4
#define EVAL_COM4_CLK_ENABLE()             __UART4_CLK_ENABLE()   
#define EVAL_COM4_CLK_DISABLE()            __UART4_CLK_DISABLE()

#define EVAL_COM4_TX_PIN                   GPIO_PIN_0
#define EVAL_COM4_TX_GPIO_PORT             GPIOA
#define EVAL_COM4_TX_GPIO_CLK_ENABLE()     __GPIOA_CLK_ENABLE()   
#define EVAL_COM4_TX_GPIO_CLK_DISABLE()    __GPIOA_CLK_DISABLE()  
#define EVAL_COM4_TX_AF                    GPIO_AF8_UART4

#define EVAL_COM4_RX_PIN                   GPIO_PIN_1
#define EVAL_COM4_RX_GPIO_PORT             GPIOA
#define EVAL_COM4_RX_GPIO_CLK_ENABLE()     __GPIOA_CLK_ENABLE()   
#define EVAL_COM4_RX_GPIO_CLK_DISABLE()    __GPIOA_CLK_DISABLE()  
#define EVAL_COM4_RX_AF                    GPIO_AF8_UART4
#define EVAL_COM4_IRQn                     UART4_IRQn
#define EVAL_COM4_IRQ_Handler							 UART4_IRQHandler
#define DMAx_CLK_ENABLE()                __DMA1_CLK_ENABLE()
#define LINA_TX_DMA_CHANNEL             DMA_CHANNEL_4
#define LINA_TX_DMA_STREAM              DMA1_Stream4
#define LINA_RX_DMA_CHANNEL             DMA_CHANNEL_4
#define LINA_RX_DMA_STREAM              DMA1_Stream2
#define LINA_DMA_TX_IRQn                DMA1_Stream4_IRQn
#define LINA_DMA_RX_IRQn                DMA1_Stream2_IRQn
#define LINA_DMA_TX_IRQHandler          DMA1_Stream4_IRQHandler
#define LINA_DMA_RX_IRQHandler          DMA1_Stream2_IRQHandler


/*@brief Definition for COM port5(CGI), connected to USART8*/
#define EVAL_COM5                          UART8
#define EVAL_COM5_CLK_ENABLE()             __UART8_CLK_ENABLE()   
#define EVAL_COM5_CLK_DISABLE()            __UART8_CLK_DISABLE()

#define EVAL_COM5_TX_PIN                   GPIO_PIN_1
#define EVAL_COM5_TX_GPIO_PORT             GPIOE
#define EVAL_COM5_TX_GPIO_CLK_ENABLE()     __GPIOE_CLK_ENABLE()   
#define EVAL_COM5_TX_GPIO_CLK_DISABLE()    __GPIOE_CLK_DISABLE()  
#define EVAL_COM5_TX_AF                    GPIO_AF8_UART8

#define EVAL_COM5_RX_PIN                   GPIO_PIN_0
#define EVAL_COM5_RX_GPIO_PORT             GPIOE
#define EVAL_COM5_RX_GPIO_CLK_ENABLE()     __GPIOE_CLK_ENABLE()   
#define EVAL_COM5_RX_GPIO_CLK_DISABLE()    __GPIOE_CLK_DISABLE()  
#define EVAL_COM5_RX_AF                    GPIO_AF8_UART8
#define EVAL_COM5_IRQn                     UART8_IRQn
#define EVAL_COM5_IRQ_Handler							 UART8_IRQHandler

/*LINA and LINB NLSP and wakeup pins*/
#define LIN_A_NLSP_PIN                		GPIO_PIN_4
#define LIN_A_NLSP_GPIO_PORT          		GPIOF
#define LIN_A_NSLP_GPIO_CLK_ENABLE()  		__GPIOF_CLK_ENABLE()
#define LIN_A_NSLP_GPIO_CLK_DISABLE() 		__GPIOF_CLK_DISABLE()
#define LIN_A_NSLP_EXTI_IRQn          		EXTI4_IRQn 

#define LIN_B_NLSP_PIN                		GPIO_PIN_15
#define LIN_B_NLSP_GPIO_PORT          		GPIOD
#define LIN_B_NSLP_GPIO_CLK_ENABLE()  		__GPIOD_CLK_ENABLE()
#define LIN_B_NSLP_GPIO_CLK_DISABLE() 		__GPIOD_CLK_DISABLE()
#define LIN_B_NSLP_EXTI_IRQn          		EXTI15_10_IRQn 


#define LIN_A_WAKEUP_PIN                		GPIO_PIN_3
#define LIN_A_WAKEUP_GPIO_PORT          		GPIOF
#define LIN_A_WAKEUP_GPIO_CLK_ENABLE()  		__GPIOF_CLK_ENABLE()
#define LIN_A_WAKEUP_GPIO_CLK_DISABLE() 		__GPIOF_CLK_DISABLE()
#define LIN_A_WAKEUP_EXTI_IRQn          		EXTI3_IRQn 

#define LIN_B_WAKEUP_PIN                		GPIO_PIN_14
#define LIN_B_WAKEUP_GPIO_PORT          		GPIOD
#define LIN_B_WAKEUP_GPIO_CLK_ENABLE()  		__GPIOD_CLK_ENABLE()
#define LIN_B_WAKEUP_GPIO_CLK_DISABLE() 		__GPIOD_CLK_DISABLE()
#define LIN_B_WAKEUP_EXTI_IRQn          		EXTI15_10_IRQn 



#define EVAL_COMx_CLK_ENABLE(__INDEX__)            (((__INDEX__) == 0) ? EVAL_COM1_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_COM2_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_COM3_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_COM4_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_COM5_CLK_ENABLE() : 0)

#define EVAL_COMx_CLK_DISABLE(__INDEX__)           (((__INDEX__) == 0) ? EVAL_COM1_CLK_DISABLE() :\
																										((__INDEX__) == 1) ? EVAL_COM2_CLK_DISABLE() :\
																										((__INDEX__) == 2) ? EVAL_COM3_CLK_DISABLE() :\
																										((__INDEX__) == 3) ? EVAL_COM4_CLK_DISABLE() :\
																										((__INDEX__) == 4) ? EVAL_COM5_CLK_DISABLE() : 0)

#define EVAL_COMx_TX_GPIO_CLK_ENABLE(__INDEX__)    (((__INDEX__) == 0) ? EVAL_COM1_TX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_COM2_TX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_COM3_TX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_COM4_TX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_COM5_TX_GPIO_CLK_ENABLE() :0)

#define EVAL_COMx_TX_GPIO_CLK_DISABLE(__INDEX__)   (((__INDEX__) == 0) ? EVAL_COM1_TX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 1) ? EVAL_COM2_TX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 2) ? EVAL_COM3_TX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 3) ? EVAL_COM4_TX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 4) ? EVAL_COM5_TX_GPIO_CLK_DISABLE() :0)
																										
#define EVAL_COMx_RX_GPIO_CLK_ENABLE(__INDEX__)    (((__INDEX__) == 0) ? EVAL_COM1_RX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_COM2_RX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_COM3_RX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_COM4_RX_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_COM5_RX_GPIO_CLK_ENABLE() :0)																										

#define EVAL_COMx_RX_GPIO_CLK_DISABLE(__INDEX__)   (((__INDEX__) == 0) ? EVAL_COM1_RX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 1) ? EVAL_COM2_RX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 2) ? EVAL_COM3_RX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 3) ? EVAL_COM4_RX_GPIO_CLK_DISABLE() :\
																										((__INDEX__) == 4) ? EVAL_COM5_RX_GPIO_CLK_DISABLE() :0)
/*********************************************************************************************************************************************/


/**************************************************Board Low level SPI*****************************************************************************/
#define SPIn															 6

/*@brief Definition for SPI port1, connected to SPI2 (LCD can be connected)*/
#define EVAL_SPI1													 SPI2
#define EVAL_SPI1_CLK_ENABLE()						 __SPI2_CLK_ENABLE()
#define EVAL_SPI1_SCK_GPIO_CLK_ENABLE()		 __GPIOD_CLK_ENABLE()
#define EVAL_SPI1_MISO_GPIO_CLK_ENABLE()	 __GPIOD_CLK_ENABLE()
#define EVAL_SPI1_MOSI_GPIO_CLK_ENABLE()	 __GPIOD_CLK_ENABLE()
#define EVAL_SPI1_NSS_GPIO_CLK_ENABLE()	   __GPIOD_CLK_ENABLE()

#define EVAL_SPI1_FORCE_RESET()						 __SPI2_FORCE_RESET()	
#define EVAL_SPI1_RELEASE_RESET()					 __SPI2_RELEASE_RESET()	

/* Definition for SPIx Pins */
#define EVAL_SPI1_SCK_PIN                  GPIO_PIN_3
#define EVAL_SPI1_SCK_GPIO_PORT            GPIOD
#define EVAL_SPI1_SCK_AF                   GPIO_AF5_SPI2

#define EVAL_SPI1_MISO_PIN                 GPIO_PIN_14
#define EVAL_SPI1_MISO_GPIO_PORT           GPIOB
#define EVAL_SPI1_MISO_AF                  GPIO_AF5_SPI2

#define EVAL_SPI1_MOSI_PIN                 GPIO_PIN_15
#define EVAL_SPI1_MOSI_GPIO_PORT           GPIOB
#define EVAL_SPI1_MOSI_AF                  GPIO_AF5_SPI2

#define EVAL_SPI1_NSS_PIN                  GPIO_PIN_9
#define EVAL_SPI1_NSS_GPIO_PORT            GPIOB
#define EVAL_SPI1_NSS_AF                   GPIO_AF5_SPI2

#define EVAL_SPI1_AF											 GPIO_AF5_SPI2

/* Definition for SPIx's NVIC */
#define EVAL_SPI1_IRQn                     SPI2_IRQn
#define EVAL_SPI1_IRQHandler               SPI2_IRQHandler	


/*@brief Definition for SPI port2, connected to SPI6 (GMLAN)*/
#define EVAL_SPI2													 SPI6
#define EVAL_SPI2_CLK_ENABLE()						 __SPI6_CLK_ENABLE()
#define EVAL_SPI2_SCK_GPIO_CLK_ENABLE()		 __GPIOG_CLK_ENABLE()
#define EVAL_SPI2_MISO_GPIO_CLK_ENABLE()	 __GPIOG_CLK_ENABLE()
#define EVAL_SPI2_MOSI_GPIO_CLK_ENABLE()	 __GPIOG_CLK_ENABLE()
#define EVAL_SPI2_NSS_GPIO_CLK_ENABLE()	   __GPIOG_CLK_ENABLE()

#define EVAL_SPI2_FORCE_RESET()						 __SPI6_FORCE_RESET()	
#define EVAL_SPI2_RELEASE_RESET()					 __SPI6_RELEASE_RESET()	

/* Definition for SPIx Pins */
#define EVAL_SPI2_SCK_PIN                  GPIO_PIN_13
#define EVAL_SPI2_SCK_GPIO_PORT            GPIOG
#define EVAL_SPI2_SCK_AF                   GPIO_AF5_SPI6

#define EVAL_SPI2_MISO_PIN                 GPIO_PIN_12
#define EVAL_SPI2_MISO_GPIO_PORT           GPIOG
#define EVAL_SPI2_MISO_AF                  GPIO_AF5_SPI6

#define EVAL_SPI2_MOSI_PIN                 GPIO_PIN_14
#define EVAL_SPI2_MOSI_GPIO_PORT           GPIOG
#define EVAL_SPI2_MOSI_AF                  GPIO_AF5_SPI6

#define EVAL_SPI2_NSS_PIN                  GPIO_PIN_8
#define EVAL_SPI2_NSS_GPIO_PORT            GPIOG
#define EVAL_SPI2_NSS_AF                   GPIO_AF5_SPI6

#define EVAL_SPI2_AF											 GPIO_AF5_SPI6

/* Definition for SPIx's NVIC */
#define EVAL_SPI2_IRQn                     SPI6_IRQn
#define EVAL_SPI2_IRQHandler               SPI6_IRQHandler


/*@brief Definition for SPI port3, connected to SPI5 (SWCAN)*/
#define EVAL_SPI3													 SPI5
#define EVAL_SPI3_CLK_ENABLE()						 __SPI5_CLK_ENABLE()
#define EVAL_SPI3_SCK_GPIO_CLK_ENABLE()		 __GPIOF_CLK_ENABLE()
#define EVAL_SPI3_MISO_GPIO_CLK_ENABLE()	 __GPIOF_CLK_ENABLE()
#define EVAL_SPI3_MOSI_GPIO_CLK_ENABLE()	 __GPIOF_CLK_ENABLE()
#define EVAL_SPI3_NSS_GPIO_CLK_ENABLE()	   __GPIOF_CLK_ENABLE()

#define EVAL_SPI3_FORCE_RESET()						 __SPI5_FORCE_RESET()	
#define EVAL_SPI3_RELEASE_RESET()					 __SPI5_RELEASE_RESET()	

/* Definition for SPIx Pins */
#define EVAL_SPI3_SCK_PIN                  GPIO_PIN_7
#define EVAL_SPI3_SCK_GPIO_PORT            GPIOF
#define EVAL_SPI3_SCK_AF                   GPIO_AF5_SPI5

#define EVAL_SPI3_MISO_PIN                 GPIO_PIN_8
#define EVAL_SPI3_MISO_GPIO_PORT           GPIOF
#define EVAL_SPI3_MISO_AF                  GPIO_AF5_SPI5

#define EVAL_SPI3_MOSI_PIN                 GPIO_PIN_9
#define EVAL_SPI3_MOSI_GPIO_PORT           GPIOF
#define EVAL_SPI3_MOSI_AF                  GPIO_AF5_SPI5

#define EVAL_SPI3_NSS_PIN                  GPIO_PIN_6
#define EVAL_SPI3_NSS_GPIO_PORT            GPIOF
#define EVAL_SPI3_NSS_AF                   GPIO_AF5_SPI5

#define EVAL_SPI3_AF											 GPIO_AF5_SPI5

/* Definition for SPIx's NVIC */
#define EVAL_SPI3_IRQn                     SPI5_IRQn
#define EVAL_SPI3_IRQHandler               SPI5_IRQHandler


/*@brief Definition for SPI port4, connected to SPI4 (CANB)*/
#define EVAL_SPI4													 SPI4
#define EVAL_SPI4_CLK_ENABLE()						 __SPI4_CLK_ENABLE()
#define EVAL_SPI4_SCK_GPIO_CLK_ENABLE()		 __GPIOE_CLK_ENABLE()
#define EVAL_SPI4_MISO_GPIO_CLK_ENABLE()	 __GPIOE_CLK_ENABLE()
#define EVAL_SPI4_MOSI_GPIO_CLK_ENABLE()	 __GPIOE_CLK_ENABLE()
#define EVAL_SPI4_NSS_GPIO_CLK_ENABLE()	   __GPIOE_CLK_ENABLE()

#define EVAL_SPI4_FORCE_RESET()						 __SPI4_FORCE_RESET()	
#define EVAL_SPI4_RELEASE_RESET()					 __SPI4_RELEASE_RESET()	

/* Definition for SPIx Pins */
#define EVAL_SPI4_SCK_PIN                  GPIO_PIN_12
#define EVAL_SPI4_SCK_GPIO_PORT            GPIOE
#define EVAL_SPI4_SCK_AF                   GPIO_AF5_SPI4

#define EVAL_SPI4_MISO_PIN                 GPIO_PIN_13
#define EVAL_SPI4_MISO_GPIO_PORT           GPIOE
#define EVAL_SPI4_MISO_AF                  GPIO_AF5_SPI4

#define EVAL_SPI4_MOSI_PIN                 GPIO_PIN_14
#define EVAL_SPI4_MOSI_GPIO_PORT           GPIOE
#define EVAL_SPI4_MOSI_AF                  GPIO_AF5_SPI4

#define EVAL_SPI4_NSS_PIN                  GPIO_PIN_11
#define EVAL_SPI4_NSS_GPIO_PORT            GPIOE
#define EVAL_SPI4_NSS_AF                   GPIO_AF5_SPI4

#define EVAL_SPI4_AF											 GPIO_AF5_SPI4

/* Definition for SPIx's NVIC */
#define EVAL_SPI4_IRQn                     SPI4_IRQn
#define EVAL_SPI4_IRQHandler               SPI4_IRQHandler


/*@brief Definition for SPI port5, connected to SPI2*/
#define EVAL_SPI5													 SPI2
#define EVAL_SPI5_CLK_ENABLE()						 __SPI2_CLK_ENABLE()
#define EVAL_SPI5_SCK_GPIO_CLK_ENABLE()		 __GPIOI_CLK_ENABLE()
#define EVAL_SPI5_MISO_GPIO_CLK_ENABLE()	 __GPIOI_CLK_ENABLE()
#define EVAL_SPI5_MOSI_GPIO_CLK_ENABLE()	 __GPIOI_CLK_ENABLE()
#define EVAL_SPI5_NSS_GPIO_CLK_ENABLE()	   __GPIOI_CLK_ENABLE()

#define EVAL_SPI5_FORCE_RESET()						 __SPI2_FORCE_RESET()	
#define EVAL_SPI5_RELEASE_RESET()					 __SPI2_RELEASE_RESET()	

/* Definition for SPIx Pins */
#define EVAL_SPI5_SCK_PIN                  GPIO_PIN_1
#define EVAL_SPI5_SCK_GPIO_PORT            GPIOI
#define EVAL_SPI5_SCK_AF                   GPIO_AF5_SPI2

#define EVAL_SPI5_MISO_PIN                 GPIO_PIN_2
#define EVAL_SPI5_MISO_GPIO_PORT           GPIOI
#define EVAL_SPI5_MISO_AF                  GPIO_AF5_SP2

#define EVAL_SPI5_MOSI_PIN                 GPIO_PIN_3
#define EVAL_SPI5_MOSI_GPIO_PORT           GPIOI
#define EVAL_SPI5_MOSI_AF                  GPIO_AF5_SPI2

#define EVAL_SPI5_NSS_PIN                  GPIO_PIN_0
#define EVAL_SPI5_NSS_GPIO_PORT            GPIOI
#define EVAL_SPI5_NSS_AF                   GPIO_AF5_SPI2

#define EVAL_SPI5_AF											 GPIO_AF5_SPI2

/* Definition for SPIx's NVIC */
#define EVAL_SPI5_IRQn                     SPI2_IRQn
#define EVAL_SPI5_IRQHandler               SPI2_IRQHandler


/*@brief Definition for SPI port6, connected to SPI2*/
#define EVAL_SPI6													 SPI2
#define EVAL_SPI6_CLK_ENABLE()						 __SPI2_CLK_ENABLE()
#define EVAL_SPI6_SCK_GPIO_CLK_ENABLE()		 __GPIOB_CLK_ENABLE()
#define EVAL_SPI6_MISO_GPIO_CLK_ENABLE()	 __GPIOB_CLK_ENABLE()
#define EVAL_SPI6_MOSI_GPIO_CLK_ENABLE()	 __GPIOB_CLK_ENABLE()
#define EVAL_SPI6_NSS_GPIO_CLK_ENABLE()	   __GPIOB_CLK_ENABLE()

#define EVAL_SPI6_FORCE_RESET()						 __SPI2_FORCE_RESET()	
#define EVAL_SPI6_RELEASE_RESET()					 __SPI2_RELEASE_RESET()	

/* Definition for SPIx Pins */
#define EVAL_SPI6_SCK_PIN                  GPIO_PIN_10
#define EVAL_SPI6_SCK_GPIO_PORT            GPIOB
#define EVAL_SPI6_SCK_AF                   GPIO_AF5_SPI2

#define EVAL_SPI6_MISO_PIN                 GPIO_PIN_14
#define EVAL_SPI6_MISO_GPIO_PORT           GPIOB
#define EVAL_SPI6_MISO_AF                  GPIO_AF5_SPI2

#define EVAL_SPI6_MOSI_PIN                 GPIO_PIN_15
#define EVAL_SPI6_MOSI_GPIO_PORT           GPIOB
#define EVAL_SPI6_MOSI_AF                  GPIO_AF5_SPI2

#define EVAL_SPI6_NSS_PIN                  GPIO_PIN_9
#define EVAL_SPI6_NSS_GPIO_PORT            GPIOB
#define EVAL_SPI6_NSS_AF                   GPIO_AF5_SPI2

#define EVAL_SPI6_AF											 GPIO_AF5_SPI2

/* Definition for SPIx's NVIC */
#define EVAL_SPI6_IRQn                     SPI2_IRQn
#define EVAL_SPI6_IRQHandler               SPI2_IRQHandler

/*SWCAN and GMLAN Other PIN Defination*/
#define EVAL_SWCAN_MODE0_PIN							 GPIO_PIN_5
#define EVAL_SWCAN_MODE0_GPIO_PORT				 GPIOA
#define EVAL_SWCAN_MODE0_CLK_ENABLE()			 __GPIOA_CLK_ENABLE()

#define EVAL_SWCAN_MODE1_PIN							 GPIO_PIN_4
#define EVAL_SWCAN_MODE1_GPIO_PORT				 GPIOA
#define EVAL_SWCAN_MODE1_CLK_ENABLE()			 __GPIOA_CLK_ENABLE()

#define EVAL_GMLAN_MODE0_PIN							 GPIO_PIN_4
#define EVAL_GMLAN_MODE0_GPIO_PORT				 GPIOC
#define EVAL_GMLAN_MODE0_CLK_ENABLE()			 __GPIOC_CLK_ENABLE()

#define EVAL_GMLAN_MODE1_PIN							 GPIO_PIN_5
#define EVAL_GMLAN_MODE1_GPIO_PORT				 GPIOC
#define EVAL_GMLAN_MODE1_CLK_ENABLE()			 __GPIOC_CLK_ENABLE()


/* LCD Reset Pin definition */
#define LCD_GPIO_CLK_ENABLE()     			 	 __GPIOI_CLK_ENABLE()
#define LCD_GPIO_PORT              			 	 GPIOI
#define LCD_A0_PIN                       	 GPIO_PIN_7
#define LCD_PS_PIN                       	 GPIO_PIN_9
#define LCD_BKLIT_PIN                    	 GPIO_PIN_6
#define LCD_RESET_PIN                    	 GPIO_PIN_4


#define EVAL_SPIx_CLK_ENABLE(__INDEX__)						 (((__INDEX__) == 0) ? EVAL_SPI1_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_CLK_ENABLE() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_CLK_ENABLE() :0)
																										
#define EVAL_SPIx_SCK_GPIO_CLK_ENABLE(__INDEX__)	 (((__INDEX__) == 0) ? EVAL_SPI1_SCK_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_SCK_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_SCK_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_SCK_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_SCK_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_SCK_GPIO_CLK_ENABLE() :0)
																										
#define EVAL_SPIx_MISO_GPIO_CLK_ENABLE(__INDEX__)	 (((__INDEX__) == 0) ? EVAL_SPI1_MISO_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_MISO_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_MISO_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_MISO_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_MISO_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_MISO_GPIO_CLK_ENABLE() :0)
																										
#define EVAL_SPIx_MOSI_GPIO_CLK_ENABLE(__INDEX__)	 (((__INDEX__) == 0) ? EVAL_SPI1_MOSI_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_MOSI_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_MOSI_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_MOSI_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_MOSI_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_MOSI_GPIO_CLK_ENABLE() :0)
																										
#define EVAL_SPIx_NSS_GPIO_CLK_ENABLE(__INDEX__)	 (((__INDEX__) == 0) ? EVAL_SPI1_NSS_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_NSS_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_NSS_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_NSS_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_NSS_GPIO_CLK_ENABLE() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_NSS_GPIO_CLK_ENABLE() :0)
																										
#define EVAL_SPIx_FORCE_RESET(__INDEX__)					 (((__INDEX__) == 0) ? EVAL_SPI1_FORCE_RESET() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_FORCE_RESET() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_FORCE_RESET() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_FORCE_RESET() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_FORCE_RESET() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_FORCE_RESET() :0)
																										
#define EVAL_SPIx_RELEASE_RESET(__INDEX__)         (((__INDEX__) == 0) ? EVAL_SPI1_RELEASE_RESET() :\
																										((__INDEX__) == 1) ? EVAL_SPI2_RELEASE_RESET() :\
																										((__INDEX__) == 2) ? EVAL_SPI3_RELEASE_RESET() :\
																										((__INDEX__) == 3) ? EVAL_SPI4_RELEASE_RESET() :\
																										((__INDEX__) == 4) ? EVAL_SPI5_RELEASE_RESET() :\
																										((__INDEX__) == 5) ? EVAL_SPI6_RELEASE_RESET() :0)  
																										
/***********************************************************************************************************************************/			

/********************************************Board Level I2C************************************************************************/
#define I2Cn																					2

/*@brief Definition for I2C port1, connected to I2C1 (EEPROM, CP, FM)*/
#define EVAL_I2C1                             				I2C1
#define EVAL_I2C1_CLK_ENABLE()                				__I2C1_CLK_ENABLE()
#define EVAL_I2C1_SCL_SDA_GPIO_CLK_ENABLE()   				__GPIOB_CLK_ENABLE()

#define EVAL_I2C1_FORCE_RESET()               				__I2C1_FORCE_RESET()
#define EVAL_I2C1_RELEASE_RESET()             				__I2C1_RELEASE_RESET()
  
/* Definition for I2Cx Pins */
#define EVAL_I2C1_SCL_PIN                     				GPIO_PIN_8
#define EVAL_I2C1_SDA_PIN                     				GPIO_PIN_7
#define EVAL_I2C1_SCL_SDA_GPIO_PORT           				GPIOB
#define EVAL_I2C1_SCL_SDA_AF                  				GPIO_AF4_I2C1

/* I2C interrupt requests */
#define EVAL_I2C1_EV_IRQn                     				I2C1_EV_IRQn
#define EVAL_I2C1_ER_IRQn                     				I2C1_ER_IRQn


/*@brief Definition for I2C port2, connected to I2C2 (Audio DAC)*/
#define EVAL_I2C2                             				I2C2
#define EVAL_I2C2_CLK_ENABLE()                				__I2C2_CLK_ENABLE()
#define EVAL_I2C2_SCL_SDA_GPIO_CLK_ENABLE()   				__GPIOF_CLK_ENABLE()

#define EVAL_I2C2_FORCE_RESET()               				__I2C2_FORCE_RESET()
#define EVAL_I2C2_RELEASE_RESET()             				__I2C2_RELEASE_RESET()
  
/* Definition for I2Cx Pins */
#define EVAL_I2C2_SCL_PIN                     				GPIO_PIN_1
#define EVAL_I2C2_SDA_PIN                     				GPIO_PIN_0
#define EVAL_I2C2_SCL_SDA_GPIO_PORT           				GPIOF
#define EVAL_I2C2_SCL_SDA_AF                  				GPIO_AF4_I2C2

/* I2C interrupt requests */
#define EVAL_I2C2_EV_IRQn                     				I2C2_EV_IRQn
#define EVAL_I2C2_ER_IRQn                     				I2C2_ER_IRQn

#define EVAL_I2Cx_CLK_ENABLE(__INDEX__)						 		(((__INDEX__) == 0) ? EVAL_I2C1_CLK_ENABLE() :\
																											 ((__INDEX__) == 1) ? EVAL_I2C2_CLK_ENABLE() :0)
																										
#define EVAL_I2Cx_SCL_SDA_GPIO_CLK_ENABLE(__INDEX__)	(((__INDEX__) == 0) ? EVAL_I2C1_SCL_SDA_GPIO_CLK_ENABLE() :\
																											 ((__INDEX__) == 1) ? EVAL_I2C2_SCL_SDA_GPIO_CLK_ENABLE() :0)		

#define EVAL_I2Cx_FORCE_RESET(__INDEX__)							(((__INDEX__) == 0) ? EVAL_I2C1_FORCE_RESET() :\
																											 ((__INDEX__) == 1) ? EVAL_I2C2_FORCE_RESET() :0)	
																											 
#define EVAL_I2Cx_RELEASE_RESET(__INDEX__)						(((__INDEX__) == 0) ? EVAL_I2C1_RELEASE_RESET() :\
																											 ((__INDEX__) == 1) ? EVAL_I2C2_RELEASE_RESET() :0)																												 


/* EEPROM Write Protect Pin definition */
#define EEPROM_WP_GPIO_CLK_ENABLE()         	__GPIOC_CLK_ENABLE()
#define EEPROM_WP_PIN                       	GPIO_PIN_1
#define EEPROM_WP_GPIO                      	GPIOC

/* FM_TUNER Reset Pin definition */
#define TUNER_RESET_GPIO_CLK_ENABLE()         __GPIOD_CLK_ENABLE()
#define TUNER_RESET_PIN                       GPIO_PIN_13
#define TUNER_RESET_GPIO                      GPIOD

/* Audio Reset Pin definition */
#define AUDIO_RESET_GPIO_CLK_ENABLE()         __GPIOC_CLK_ENABLE()
#define AUDIO_RESET_PIN                       GPIO_PIN_6
#define AUDIO_RESET_GPIO                      GPIOC

#define AUDIO_AMP_GPIO_CLK_ENABLE()         	__GPIOE_CLK_ENABLE()
#define AUDIO_AMP_GPIO                      	GPIOE
#define AUDIO_AMP_CH1_PIN                     GPIO_PIN_7
#define AUDIO_AMP_CH2_PIN                     GPIO_PIN_8
#define AUDIO_AMP_CH3_PIN                     GPIO_PIN_9
#define AUDIO_AMP_CH4_PIN                     GPIO_PIN_10


/**
  * @}
  */ 

/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_LOW_LEVEL_Exported_Macros
  * @{
  */  
/**
  * @}
  */ 


/** @defgroup Gateway_Board_LOW_LEVEL_Exported_Functions
  * @{
  */
uint32_t         BSP_GetVersion(void);  
void             BSP_LED_Init(Led_TypeDef Led);
void             BSP_LED_On(Led_TypeDef Led);
void             BSP_LED_Off(Led_TypeDef Led);
void             BSP_LED_Toggle(Led_TypeDef Led);
void             BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef Button_Mode);
uint32_t         BSP_PB_GetState(Button_TypeDef Button);
void             BSP_COM_Init(COM_TypeDef COM, uint32_t baud_rate, UART_HandleTypeDef *husart);


void MFI_CP_IO_Init(void);
HAL_StatusTypeDef MFI_CP_IO_WriteData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize);
HAL_StatusTypeDef MFI_CP_IO_ReadData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize);
HAL_StatusTypeDef MFI_CP_IO_IsDeviceReady(uint16_t DevAddress, uint32_t Trials);
uint16_t MFI_CP_IO_Read(uint8_t Addr, uint16_t Reg);
uint16_t MFI_CP_IO_ReadMultiple(uint8_t Addr, uint8_t *Buffer, uint16_t Length);


void EEPROM_IO_Init(void);
HAL_StatusTypeDef EEPROM_IO_WriteData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize);
HAL_StatusTypeDef EEPROM_IO_ReadData(uint16_t DevAddress, uint16_t MemAddress, uint8_t* pBuffer, uint32_t BufferSize);
HAL_StatusTypeDef EEPROM_IO_IsDeviceReady(uint16_t DevAddress, uint32_t Trials);




/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */
    
#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_H */
  

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
