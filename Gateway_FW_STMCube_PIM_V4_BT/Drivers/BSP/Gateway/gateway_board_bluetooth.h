/**
  ******************************************************************************
  * @file    gateway_board_Ecan.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   This file contains the common defines and functions prototypes for
  *          the Gateway_Board_Ecan.c driver.
  ******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_BLUETOOTH_H
#define __GATEWAY_BOARD_BLUETOOTH_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* Include Bluetooth component Driver */
#include "gateway_board.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Gateway_Board
  * @{
  */
    
/** @defgroup Gateway_Board_Ecan
  * @{
  */


/** @defgroup Gateway_Board_ecan_Exported_Types
  * @{
  */

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Exported_Constants
  * @{
  */

typedef enum
{
	BT_FAIL = 0,
	BT_OK
}BT_status_typedef;

#define BT_BUFF_SIZE 50
#define COMMAND_MODE 

void BSP_Bluetooth_Init(void);
BT_status_typedef BSP_Bluetooth_Transmit_cmd(char* cmd);
BT_status_typedef BSP_Bluetooth_Transmit_data(char* data);
BT_status_typedef BSP_Bluetooth_IO_Write(char* data);
void BSP_Bluetooth_Delay(uint32_t delay);

static void flush_buff(void);
static void waitTillReady(uint32_t timeout);
static void handleBTReceivedData(void);


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_BLUETOOTH_H */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
