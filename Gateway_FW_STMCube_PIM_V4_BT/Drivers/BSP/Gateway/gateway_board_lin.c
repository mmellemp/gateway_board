/**
  ******************************************************************************
  * @file    gateway_board_lin.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    9-sep-2014
  * @brief   This file provides the LIN driver for the Gateway Board evaluation board.
  ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "gateway_board_lin.h"

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Gateway_Board
  * @{
  */ 
  
/** @defgroup Gateway_Board_ecan
  * @brief This file includes the low layer driver for ecan controller 
  *          available on Gateway Board.
  * @{
  */ 

/** @defgroup Gateway_Board_ecan_Private_Types
  * @{
  */ 
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Defines
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Macros
  * @{
  */
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Variables
  * @{
  */
//ECAN_DrvTypeDef	*canA_drv,*canB_drv,*swcan_drv,*gmlan_drv;

UART_HandleTypeDef linAHandle,linBHandle;
uint8_t slave_resp_size;
uint8_t lin_msg_frame[2];

extern BOOL bLinDataAvailable;

extern  __IO ITStatus UartReady;

typedef struct
{
	uint8_t b0 : 1;
	uint8_t b1 : 1;
	uint8_t b2 : 1;
	uint8_t b3 : 1;
	uint8_t b4 : 1;
	uint8_t b5 : 1;
	uint8_t p0 : 1;
	uint8_t p1 : 1;
}slave_bits_t;


typedef union
{
	slave_bits_t ID_bit;
	uint8_t ID;
}slaveID_t;



void BSP_LIN_DMA_MSP_Init(UART_HandleTypeDef *huart)
{

	static DMA_HandleTypeDef hdma_tx;
  static DMA_HandleTypeDef hdma_rx;
	
	  DMAx_CLK_ENABLE(); 
	
/* Configure the DMA streams ##########################################*/
  /* Configure the DMA handler for Transmission process */
  hdma_tx.Instance                 = LINA_TX_DMA_STREAM;
  
  hdma_tx.Init.Channel             = LINA_TX_DMA_CHANNEL;
  hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
  hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
  hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdma_tx.Init.Mode                = DMA_NORMAL;
  hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
  hdma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  hdma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdma_tx.Init.MemBurst            = DMA_MBURST_INC4;
  hdma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
  
  HAL_DMA_Init(&hdma_tx);   
  
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmatx, hdma_tx);
    
  /* Configure the DMA handler for Transmission process */
  hdma_rx.Instance                 = LINA_RX_DMA_STREAM;
  
  hdma_rx.Init.Channel             = LINA_RX_DMA_CHANNEL;
  hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
  hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdma_rx.Init.Mode                = DMA_NORMAL;
  hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
  hdma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
  hdma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdma_rx.Init.MemBurst            = DMA_MBURST_INC4;
  hdma_rx.Init.PeriphBurst         = DMA_PBURST_INC4; 

  HAL_DMA_Init(&hdma_rx);
    
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmarx, hdma_rx);
    
  /* Configure the NVIC for DMA #########################################*/
  /* NVIC configuration for DMA transfer complete interrupt  */
  HAL_NVIC_SetPriority(LINA_DMA_TX_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(LINA_DMA_TX_IRQn);
    
  /* NVIC configuration for DMA transfer complete interrupt  */
  HAL_NVIC_SetPriority(LINA_DMA_RX_IRQn, 0, 0);   
  HAL_NVIC_EnableIRQ(LINA_DMA_RX_IRQn);
}




static void BSP_LIN_TrscvrInit(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /* Enable the GPIO_LED clock */
  LIN_A_NSLP_GPIO_CLK_ENABLE();

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = LIN_A_NLSP_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(LIN_A_NLSP_GPIO_PORT, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(LIN_A_NLSP_GPIO_PORT, LIN_A_NLSP_PIN, GPIO_PIN_SET);
	
	  /* Enable the GPIO_LED clock */
  LIN_B_NSLP_GPIO_CLK_ENABLE();

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = LIN_B_NLSP_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(LIN_B_NLSP_GPIO_PORT, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(LIN_B_NLSP_GPIO_PORT, LIN_B_NLSP_PIN, GPIO_PIN_SET);
	
	
	  /* Enable the GPIO_LED clock */
  LIN_A_WAKEUP_GPIO_CLK_ENABLE(); 

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = LIN_A_WAKEUP_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(LIN_A_WAKEUP_GPIO_PORT, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(LIN_A_WAKEUP_GPIO_PORT, LIN_A_WAKEUP_PIN, GPIO_PIN_RESET);
	
	  /* Enable the GPIO_LED clock */
  LIN_B_WAKEUP_GPIO_CLK_ENABLE();

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = LIN_B_WAKEUP_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(LIN_B_WAKEUP_GPIO_PORT, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(LIN_B_WAKEUP_GPIO_PORT, LIN_B_WAKEUP_PIN, GPIO_PIN_RESET);

}


/**
  * @brief  Initializes the LIN mode according to the specified
  *         parameters .
  * @param  linType: LIN to be configured.
  *          This parameter can be one of the following values:
  *            @arg  LINA 
  *            @arg  LINB 
  * @param  BreakDetectLength: Specifies the LIN break detection length.
  * @retval HAL status
  */
HAL_StatusTypeDef BSP_LIN_Init(LIN_Typedef linType,uint32_t BreakDetectLength)
{
	HAL_StatusTypeDef status = HAL_ERROR;
	BSP_LIN_TrscvrInit();
	switch(linType)
	{
		case LINA:
			BSP_LIN_DMA_MSP_Init(&linAHandle);
			/*COM port attached to LIN intialization*/
			BSP_COM_Init(COM4,10417,&linAHandle);
			status = HAL_LIN_Init(&linAHandle,BreakDetectLength);
			break;
		case LINB:
			BSP_COM_Init(COM3,9600,&linBHandle);
			status = HAL_LIN_Init(&linBHandle,BreakDetectLength);
			break;
		default:
			break;
	}
	return status;
}

/**
  * @brief  Sends data over LIN
  * @param  linType: LIN to be configured.
  *          This parameter can be one of the following values:
  *            @arg  LINA 
  *            @arg  LINB  
  * @param  pData: pointer to data to be sent.
	* @param  size : size of data to be transfered
  * @retval None
  */
void BSP_LIN_IO_Write(LIN_Typedef linType,uint8_t *pData,uint16_t size)
{
	uint8_t breakData = 0x00;
	switch(linType)
	{
		case LINA:			
			/*SynchBreak(13 bits) emulation by changing baud rate*/
			BSP_COM_Init(COM4,6945,&linAHandle);
			HAL_UART_Transmit_DMA(&linAHandle, &breakData, 1);
					  /*Wait for the end of the transfer ###################################*/  
       while (UartReady != SET)
				{
				}		
				/* Reset transmission flag */
					UartReady = RESET;
			BSP_COM_Init(COM4,10417,&linAHandle);

			if(HAL_UART_Transmit_DMA(&linAHandle, pData, size)!= HAL_OK)
			{
			BSP_LED_Toggle(LED1);
			}
			  /*Wait for the end of the transfer ###################################*/  
       while (UartReady != SET)
				{
				}		
				/* Reset transmission flag */
					UartReady = RESET;

			break;
		case LINB:
			//HAL_LIN_SendBreak(&linBHandle);			
			/*SynchBreak(13 bits) emulation by changing baud rate*/
			BSP_COM_Init(COM3,3200,&linBHandle);
			HAL_UART_Transmit(&linBHandle,&breakData,1,100);
			BSP_COM_Init(COM3,4800,&linBHandle);
			HAL_UART_Transmit_IT(&linBHandle,pData,size);
			break;
		default:
			break;
	}
}



//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
//{
//	if(huart->Instance == EVAL_COM4)
//	{
//		BSP_LIN_ReceiveComplete_CallBack(LINA);
//	}
//	else if(huart->Instance == EVAL_COM3)
//	{
//		BSP_LIN_ReceiveComplete_CallBack(LINB);
//	}
//	else if(huart->Instance == EVAL_COM5)	/*CGI*/
//	{
//		//BSP_CGI_ReceiveComplete_Calback();
//	}
//	/*user can add its own function*/
//}

//void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
//{
//	/*user can add its own function*/
//}

//void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
//{
//	if(huart->Instance == EVAL_COM4)
//	{
//		BSP_LIN_Receive_Error_Callback(LINA);
//	}
//	else if(huart->Instance == EVAL_COM3)
//	{
//		BSP_LIN_Receive_Error_Callback(LINB);
//	}
//}

//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//	if(huart->Instance == EVAL_COM4)
//	{
//		BSP_LIN_TransmitComplete_Callback(LINA);
//	}
//	else if(huart->Instance == EVAL_COM3)
//	{
//		BSP_LIN_TransmitComplete_Callback(LINB);
//	}
//}

///*can be implemented by user at application layer if required*/
//__weak void	BSP_LIN_ReceiveComplete_CallBack(LIN_Typedef linType)
//{
//}

///*can be implemented by user at application layer if required*/
//__weak void	BSP_LIN_HalfReceive_CallBack(LIN_Typedef linType)
//{
//}

///*can be implemented by user at application layer if required*/
//__weak void BSP_LIN_Receive_Error_Callback(LIN_Typedef linType)
//{
//}

///*can be implemented by user at application layer if required*/
//__weak void BSP_LIN_TransmitComplete_Callback(LIN_Typedef linType)
//{
//}
	/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
