/**
  ******************************************************************************
  * @file    gateway_board_ecan.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    9-sep-2014
  * @brief   This file provides the Tuner driver for the Gateway Board evaluation board.
  ******************************************************************************
  */

/*==============================================================================
                                 User NOTES
                                 
How To use this driver:
-----------------------
   + This driver supports STM32F4xx devices on Gateway Board.
	 + Call the function BSP_CAN_Init()
      This function configures all the hardware required for the External SPI CAN (SPI,GPIOs, 
			DMA and interrupt if needed). This function returns ECAN_OK if configuration is OK.
      if the returned value is different from ECAN_OK or the function is stuck then the communication with
      the codec has failed (try to un-plug the power or reset device in this case).
      
   Driver architecture:
--------------------
   + This driver provide the High ECAN Layer: consists of the function API exported in the Gateway_Board_ecan.h file
    (BSP_CAN_Init(), BSP_CAN_SetCanBaud() ...)
   
Known Limitations:
------------------
   + Supports only 16-bit tuner data size.
==============================================================================*/
/* Includes ------------------------------------------------------------------*/
#include "gateway_board_can.h"

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Gateway_Board
  * @{
  */ 
  
/** @defgroup Gateway_Board_ecan
  * @brief This file includes the low layer driver for ecan controller 
  *          available on Gateway Board.
  * @{
  */ 

/** @defgroup Gateway_Board_ecan_Private_Types
  * @{
  */ 
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Defines
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Macros
  * @{
  */
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Variables
  * @{
  */
//ECAN_DrvTypeDef	*canA_drv,*canB_drv,*swcan_drv,*gmlan_drv;

ECAN_DrvTypeDef *can_drv[4] = {0};

/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Function_Prototypes
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_out_Private_Functions
  * @{
  */ 
/**
  * @brief Configure the tuner peripherals.
  * @retval TUNER_OK if correct communication, else wrong communication
  */

uint8_t BSP_CAN_Init(CAN_Typedef canType)
{ 
	uint8_t ret = ECAN_ERROR;
	switch(canType)
	{
		case CANA:
			can_drv[canType] = 0;
			break;
		case CANB:
			can_drv[canType] = &mcp2515_can_drv;
			break;
		case SWCAN:
			can_drv[canType] = &mcp2515_swcan_drv;
			break;
		case GMLAN:
			can_drv[canType] = &mcp2515_gmlan_drv;
			break;
		default:
			break;
	}
	can_drv[canType]->mcp2515_Reset();
	ret = can_drv[canType]->mcp2515_Init();
	
	return ret;
}

uint8_t BSP_CAN_Test(CAN_Typedef canType)
{
	uint8_t ret = (can_drv[canType])?(can_drv[canType])->mcp2515_Test():ECAN_ERROR;
	return ret;
}	


uint8_t BSP_CAN_Reset(CAN_Typedef canType)
{
	uint8_t ret = ECAN_OK;
	if(can_drv[canType])
		can_drv[canType]->mcp2515_Reset();
	return ret;
}

uint8_t BSP_CAN_WriteReg(CAN_Typedef canType, uint8_t Addr, uint8_t Value)
{
	uint8_t ret = ECAN_ERROR;
	if(can_drv[canType])
		can_drv[canType]->mcp2515_WriteReg(Addr,Value);
	else
		return ret;
	if(Value == can_drv[canType]->mcp2515_ReadReg(Addr))
		ret = ECAN_OK;
	return ret; 	
	
}

uint8_t BSP_CAN_ReadStatus(CAN_Typedef canType)
{
	uint8_t ret = 0x77;
	if(can_drv[canType])
		ret = can_drv[canType]->mcp2515_ReadStatus();
	return ret;


}
uint8_t BSP_CAN_ReadReg(CAN_Typedef canType, uint8_t Addr)
{
	uint8_t ret = 0x77;
	if(can_drv[canType])
		ret = can_drv[canType]->mcp2515_ReadReg(Addr);
	return ret;
}



uint8_t BSP_CAN_SetCANBaud(CAN_Typedef canType, uint32_t baudRate)
{
	uint8_t ret = 0x77;
	if(can_drv[canType])
		ret = can_drv[canType]->mcp2515_SetCANBaud(baudRate);
	return ret;
}

uint8_t BSP_CAN_SetCANMode(CAN_Typedef canType, uint8_t mode, uint8_t abat,uint8_t singleShot,uint8_t clkout,uint8_t clkpre)
{
	uint8_t ret = 0x77;
	if(can_drv[canType])
		ret = can_drv[canType]->mcp2515_SetCANMode(mode,abat,singleShot,clkout,clkpre);
	
	if(mode!=ret )
		return ECAN_ERROR;
	else 
		return ECAN_OK;
}



uint8_t BSP_CAN_receiveCANMessage(CAN_Typedef canType, CANMSG* msg)
{
	uint8_t ret = 0x77;
	if(can_drv[canType])
		ret = can_drv[canType]->mcp2515_readCANmessage(msg);
	
	if (ret)
		return ECAN_OK ;
	else
		return  ECAN_ERROR;
		
	
}

uint8_t BSP_CAN_transmitCANMessage(CAN_Typedef canType, CANMSG msg, uint32_t timeout)
{
	uint8_t ret = 0x77;
	if(can_drv[canType])
		ret = can_drv[canType]->mcp2515_transmitCANMessage(msg,timeout);
	
	if (ret)
		return ECAN_OK ;
	else
		return  ECAN_ERROR;

}
	/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
