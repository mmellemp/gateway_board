/**
  ******************************************************************************
  * @file    gateway_board_tuner.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    9-sep-2014
  * @brief   This file provides the Tuner driver for the Gateway Board evaluation board.
  ******************************************************************************
  */

/*==============================================================================
                                 User NOTES
                                 
How To use this driver:
-----------------------
   + This driver supports STM32F4xx devices on Gateway Board.
	 + Call the function BSP_TUNER_Init()
      This function configures all the hardware required for the tuner application (I2C,GPIOs, 
			DMA and interrupt if needed). This function returns TUNER_OK if configuration is OK.
      if the returned value is different from TUNER_OK or the function is stuck then the communication with
      the codec has failed (try to un-plug the power or reset device in this case).
      
   + Call the function BSP_TUNER_Start(
                                  pBuffer : pointer to the tuner data file address
                                  Size    : size of the buffer to be sent in Bytes
                                 )
      to start playing (for the first time) from the tuner file/stream.
   + Call the function BSP_TUNER_Pause() to pause playing   
   + Call the function BSP_TUNER_Resume() to resume playing.
       Note. After calling BSP_TUNER_Pause() function for pause, only BSP_TUNER_Resume() should be called
          for resume (it is not allowed to call BSP_TUNER_Play() in this case).
       Note. This function should be called only when the tuner file is played or paused (not stopped).
   + For each mode, you may need to implement the relative callback functions into your code.
      The Callback functions are named tuner_OUT_XXX_CallBack() and only their prototypes are declared in 
      the Gateway_Board_tuner.h file. (refer to the example for more details on the callbacks implementations)
   + To Stop playing, to modify the volume level, the frequency, the tuner frame slot, 
      the device output mode the mute or the stop, use the functions: BSP_TUNER_SetVolume(), 
      tuner_OUT_SetFrequency(), BSP_TUNER_SettunerFrameSlot(), BSP_TUNER_SetOutputMode(),
      BSP_TUNER_SetMute() and BSP_TUNER_Stop().
   + The driver API and the callback functions are at the end of the Gateway_Board_tuner.h file.
 
Driver architecture:
--------------------
   + This driver provide the High tuner Layer: consists of the function API exported in the Gateway_Board_tuner.h file
    (BSP_TUNER_Init(), BSP_TUNER_Play() ...)
   + This driver provide also the Media Access Layer (MAL): which consists of functions allowing to access the media containing/
     providing the tuner file/stream. These functions are also included as local functions into
   the Gateway_Board_tuner_codec.c file (SAIx_MspInit() and SAIx_Init())   

Known Limitations:
------------------
   + Supports only 16-bit tuner data size.
==============================================================================*/
/* Includes ------------------------------------------------------------------*/
#include "gateway_board_tuner.h"

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Gateway_Board
  * @{
  */ 
  
/** @defgroup Gateway_Board_tuner
  * @brief This file includes the low layer driver for tuner Codec
  *          available on Gateway Board.
  * @{
  */ 

/** @defgroup Gateway_Board_tuner_Private_Types
  * @{
  */ 
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_tuner_Private_Defines
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Private_Macros
  * @{
  */
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_tuner_Private_Variables
  * @{
  */
TUNER_DrvTypeDef	*tuner_drv;

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Private_Function_Prototypes
  * @{
  */
static void TUNER_Reset(void);

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_out_Private_Functions
  * @{
  */ 

/**
  * @brief Configure the tuner peripherals.
  * @retval TUNER_OK if correct communication, else wrong communication
  */
uint8_t BSP_TUNER_Init(void)
{ 
  uint8_t ret = TUNER_ERROR;
	tuner_drv = &si4743_drv; 

	/* Initialize the codec internal registers */
	tuner_drv->Init(TUNER_I2C_ADDRESS);
	
	TUNER_Reset();
	
  return ret;
}

/**
  * @brief Starts playing tuner stream from a data buffer for a determined size. 
	* @param Volume: Volune in range of 0 to 16 . 
  * @retval TUNER_OK if correct communication, else wrong communication
  */
uint8_t BSP_TUNER_Start(uint8_t Volume)
{
	
  /* Call the tuner Start function */
  if(tuner_drv->Start(TUNER_I2C_ADDRESS, Volume) != 0)
  {  
    return TUNER_ERROR;
  }
  return TUNER_OK;
}


/**
  * @brief  Stops tuner and Power down the tuner . 
  * @retval TUNER_OK if correct communication, else wrong communication
  */
uint8_t BSP_TUNER_Stop(void)
{
  
  /* Call tuner Stop function */
  if(tuner_drv->Stop(TUNER_I2C_ADDRESS) != 0)
  {
    return TUNER_ERROR;
  }
  /* Return tuner_OK when all operations are correctly done */
  return TUNER_OK;

}

/**
  * @brief  Controls the current tuner volume level. 
  * @param  Volume: Volume level to be set in percentage from 0% to 100% (0 for 
  *         Mute and 100 for Max volume level).
  * @retval tuner_OK if correct communication, else wrong communication
  */
uint8_t BSP_TUNER_SetVolume(uint8_t Volume)
{
  /* Call the Tuner volume control function with converted volume value */
	if(tuner_drv)
	{
		if(tuner_drv->SetVolume(TUNER_I2C_ADDRESS, Volume) != 0)
		{
			return TUNER_ERROR;
		}
		else
		{
			/* Return tuner_OK when all operations are correctly done */
			return TUNER_OK;
		}
	}
	return TUNER_ERROR;	
}

/**
  * @brief  Enables or disables the MUTE mode by software 
  * @param  Command: could be TUNER_MUTE_ON to mute sound or TUNER_MUTE_OFF to 
  *         unmute the tuner and restore previous volume level.
  * @retval TUNER_OK if correct communication, else wrong communication
  */
uint8_t BSP_TUNER_SetMute(uint32_t Cmd)
{ 
  /* Call the Tuner Mute function */
  if(tuner_drv->SetMute(TUNER_I2C_ADDRESS, Cmd) != 0)
  {
    return TUNER_ERROR;
  }
  else
  {
    /* Return tuner_OK when all operations are correctly done */
    return TUNER_OK;
  }
}

/**
  * @brief Tune Tuner to given frequency.
  * @param Frequency: Frequency to tune in.
  * @param Direction: direction to tune in.
  * @retval TUNER_OK if correct communication, else wrong communication
  * @note This API should be called after the BSP_TUNER_Init() and BSP_TUNER_Start() 
	*	to set the tuner frequency. 
  */
uint8_t BSP_TUNER_Tune(uint16_t Frequency, uint8_t Direction)
{ 
	/* Call the Tuner Tune function */
	if(tuner_drv)
	{
		if(tuner_drv->Tune(TUNER_I2C_ADDRESS, Frequency,Direction) != 0)
		{
			return TUNER_ERROR;
		}
		else
		{
			/* Return tuner_OK when all operations are correctly done */
			return TUNER_OK;
		}
	}
	return TUNER_ERROR;
}

/**
  * @brief Seek Tuner for valid frequencies in given Direction.
  * @param Direction: direction to tune in.
  * @retval New seeked station
  * @note This API should be called after the BSP_TUNER_Init() and BSP_TUNER_Start() 
	*	to set the tuner frequency. 
  */
uint16_t BSP_TUNER_Seek(uint8_t Direction)
{ 
	/* Call the Tuner Tune function */
  return tuner_drv->Seek(TUNER_I2C_ADDRESS, Direction);
    
}

void BSP_TUNER_GetChannelList(uint8_t *res)
{
	tuner_drv->GetChannelList(TUNER_I2C_ADDRESS, res);
}

/******************************************************************************
                            Static Function
*******************************************************************************/
/**
  * @brief  Resets the tuner. It restores the default configuration of the 
  *         Tuner (this function shall be called before initializing the tuner).
  * @param  None
  * @retval None
  */
static void TUNER_Reset(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable Reset GPIO Clock */
  TUNER_RESET_GPIO_CLK_ENABLE();

  /* tuner reset pin configuration -------------------------------------------------*/
  GPIO_InitStruct.Pin = TUNER_RESET_PIN; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init(TUNER_RESET_GPIO, &GPIO_InitStruct); 
	
	/* Reset tuner Reset Pin  */
  HAL_GPIO_WritePin(TUNER_RESET_GPIO, TUNER_RESET_PIN, GPIO_PIN_RESET);

	/* wait for a delay to insure registers erasing */
  HAL_Delay(500); 

  /* Set tuner Reset Pin */
  HAL_GPIO_WritePin(TUNER_RESET_GPIO, TUNER_RESET_PIN, GPIO_PIN_SET);

	//HAL_Delay(500); 
}


/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
