/**
  ******************************************************************************
  * @file    gateway_board_tuner.h
  * @author  Arka Team
  * @version V1.0.0
  * @date    21-July-20
  * @brief   This file contains the common defines and functions prototypes for
  *          the Gateway_Board_tuner.c driver.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GATEWAY_BOARD_tuner_H
#define __GATEWAY_BOARD_tuner_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* Include tuner component Driver */
#include "gateway_board.h"
#include "..\Components\si4703\si4703.h"
#include "..\Components\si4743\si4743.h"


/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Gateway_Board
  * @{
  */
    
/** @defgroup Gateway_Board_tuner
  * @{
  */


/** @defgroup Gateway_Board_tuner_Exported_Types
  * @{
  */

/**
  * @}
  */ 

/** @defgroup Gateway_Board_tuner_Exported_Constants
  * @{
  */

/* tuner status definition */     
#define TUNER_OK                              0
#define TUNER_ERROR                           1
#define TUNER_TIMEOUT                         2
/* tuner Tune Direction */  
#define TUNE_DIR_UP 			1
#define TUNE_DIR_DOWN 		0



/** @defgroup Gateway_Board_tuner_Exported_Functions
  * @{
  */
uint8_t BSP_TUNER_Init(void);
uint8_t BSP_TUNER_Start(uint8_t Volume);
uint8_t BSP_TUNER_Stop(void);
uint8_t BSP_TUNER_SetVolume(uint8_t Volume);
uint8_t BSP_TUNER_SetMute(uint32_t Cmd);
uint8_t BSP_TUNER_Tune(uint16_t Frequency, uint8_t Direction);
uint16_t BSP_TUNER_Seek(uint8_t Direction);
void BSP_TUNER_GetChannelList(uint8_t *res);

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __GATEWAY_BOARD_tuner_H */

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
