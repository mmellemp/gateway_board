/**
  ******************************************************************************
  * @file    gateway_board_cgi.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    9-sep-2014
  * @brief   This file provides the CGI driver for the Gateway Board evaluation board.
  ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "gateway_board_cgi.h"
UART_HandleTypeDef CGI_Handle;
volatile uint8_t CGI_buff[50] = {0};
//uint8_t Event_buffer[20];
extern uint8_t *EV_REC_ptr,*EV_READ_ptr;	


/** @addtogroup BSP
  * @{
  */

/** @addtogroup Gateway_Board
  * @{
  */ 
  
/** @defgroup Gateway_Board_ecan
  * @brief This file includes the low layer driver for ecan controller 
  *          available on Gateway Board.
  * @{
  */ 

/** @defgroup Gateway_Board_ecan_Private_Types
  * @{
  */ 
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Defines
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Macros
  * @{
  */
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Variables
  * @{
  */

CGI_status_typedef BSP_CGI_Init(void)
{
	CGI_status_typedef status = CGI_FAIL;
	BSP_COM_Init(COM5,115200,&CGI_Handle);
	if(HAL_UART_Receive_IT(&CGI_Handle, (uint8_t *)CGI_buff, SBX_CONTROL_STATUS_MSG_KEYS_START_BIT)!= HAL_OK) 
	{
		assert(0); 
	}	

	EV_READ_ptr = EV_REC_ptr;	
	status = vSbxRdfComm(0);
	return status;
}
	
CGI_status_typedef BSP_CGI_Process(void)
{
	CGI_status_typedef status = CGI_FAIL;
	status = vSbxRdfComm(1);
	return status;
}

CGI_status_typedef BSP_CGI_getEvent(void)
{
	return get_Events_occured();
}

CGI_status_typedef BSP_CGI_Transmit_Data(const uint8_t* data, uint8_t dataLen)
{
	CGI_status_typedef status = CGI_FAIL;
	if(HAL_UART_Transmit_IT(&CGI_Handle, (uint8_t*)data, dataLen) == HAL_OK)
		status = CGI_OK;
	return status;
}

void BSP_CGI_ReceiveComplete_Calback(void)
{
	if(HAL_UART_Receive_IT(&CGI_Handle, (uint8_t *)CGI_buff,((SBX_REQUEST_CONTROL_STATUS_MSG_SIZE+SBX_REQUEST_CONTROL_STATUS_MSG_SIZE)-1))!= HAL_OK)  
	{
		BSP_LED_Toggle(LED3);     
	}
}


/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Function_Prototypes
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_out_Private_Functions
  * @{
  */ 
/**
  * @brief Configure the tuner peripherals.
  * @retval TUNER_OK if correct communication, else wrong communication
  */


	/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
