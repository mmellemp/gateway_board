/**
  ******************************************************************************
  * @file    gateway_board_bluetooth.c
  * @author  Arka Team
  * @version V1.0.0
  * @date    9-sep-2014
  * @brief   This file provides the Bluetooth driver for the Gateway Board evaluation board.
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "gateway_board_bluetooth.h"
#include "string.h"

UART_HandleTypeDef BT_Handle;
uint8_t bt_buff[BT_BUFF_SIZE] = {0};
uint8_t *bt_buff_base_ptr = bt_buff;
uint8_t *bt_buff_wr_ptr = bt_buff;
uint8_t bt_recv_char = 0;

char *dataToWrite;
extern uint8_t dataFromApp[64];

/** @addtogroup BSP
  * @{
  */

/** @addtogroup Gateway_Board
  * @{
  */ 
  
/** @defgroup Gateway_Board_Bluetooth
  * @brief This file includes the low layer driver for bluetooth module
  *          available on Gateway Board.
  * @{
  */ 

/** @defgroup Gateway_Board_ecan_Private_Types
  * @{
  */ 
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Defines
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Macros
  * @{
  */
/**
  * @}
  */ 
  
/** @defgroup Gateway_Board_ecan_Private_Variables
  * @{
  */


void BSP_Bluetooth_Init(void)
{
	BSP_COM_Init(COM2,9600,&BT_Handle);
	HAL_UART_Receive_IT(&BT_Handle,&bt_recv_char,1);
	
	/*BT initializations*/
	/*Arka config*/
	
	BSP_Bluetooth_Transmit_cmd("SET ENABLE_SPP=ON");	
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("SET ENABLE_IAP=ON");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("SET ENABLE_IAP_V2=ON");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP ACCESSORY_NAME=GATEWAY");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP MANUFACTURER_NAME=ARKA");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP MODEL_NAME=GTV100");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP SERIAL_NO=V1.1");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP SEED_ID=1234567890");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP PROTOCOL_STRING=com.arka-ims.bt");
	//BSP_Bluetooth_Transmit_cmd("IAP PROTOCOL_STRING=com.blue-creation.Melody");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP HARDWARE_VER=2 0 1");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("IAP FIRMWARE_VER=4 0 1");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("SET DEVICE_ID=0101 0202 0303 0404 0505 0606 0707 0808");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("WRITE");
	waitTillReady(100);
	BSP_Bluetooth_Transmit_cmd("RESET");
	
	BSP_Bluetooth_Delay(500);
	flush_buff();
}

void BSP_Bluetooth_Delay(uint32_t delay)
{
	HAL_Delay(delay);
}

BT_status_typedef BSP_Bluetooth_IO_Write(char* data)
{
	BT_status_typedef status = BT_FAIL;
	uint8_t debug = strlen(data);
	if(HAL_UART_Transmit_IT(&BT_Handle,data,strlen(data)) != HAL_OK)
	{
		BSP_LED_Toggle(LED3);
	}
	else
		status = BT_OK;
	return status;
}

BT_status_typedef BSP_Bluetooth_Transmit_cmd(char* cmd)
{
	BT_status_typedef status = BT_FAIL;
	
	dataToWrite = (char*)malloc(strlen(cmd) + 1);
	strcpy(dataToWrite,cmd);
	strcat(dataToWrite,"\r");
	if(BSP_Bluetooth_IO_Write(dataToWrite) != BT_OK)
	{
		BSP_LED_Toggle(LED3);
	}
	else
		status = BT_OK;
	
	return status;
}

BT_status_typedef BSP_Bluetooth_Transmit_data(char* data)
{
	BT_status_typedef status = BT_FAIL;
	
	dataToWrite = (char*)malloc(strlen(data) + 6);
	strcpy(dataToWrite,"SEND ");
	strcat(dataToWrite,data);
	strcat(dataToWrite,"\r");
	
	if(BSP_Bluetooth_IO_Write(dataToWrite) != BT_OK)
	{
		BSP_LED_Toggle(LED3);
	}
	else
		status = BT_OK;
	
	return status;
}

void BSP_Bluetooth_ReceiveComplete_Calback(void)
{
	if((bt_buff_wr_ptr - bt_buff_base_ptr) < BT_BUFF_SIZE)
	{
		*(bt_buff_wr_ptr) = bt_recv_char;
		bt_buff_wr_ptr++;
	}
	else
	{
		//overflow
		bt_buff_wr_ptr = bt_buff;
		*(bt_buff_wr_ptr) = bt_recv_char;
	}
	HAL_UART_Receive_IT(&BT_Handle,&bt_recv_char,1);
	handleBTReceivedData();
	BSP_LED_Toggle(LED3);
}

void BSP_Bluetooth_TransmitComplete_Calback(void)
{
	free(dataToWrite);
	flush_buff();
	BSP_LED_Toggle(LED5);
}

void BSP_Bluetooth_Receive_Error_Callback(void)
{
	BSP_LED_Toggle(LED4);
}

static void flush_buff(void)
{
	uint8_t loop_cnt = 0;
	while(loop_cnt < BT_BUFF_SIZE)
	{
		bt_buff[loop_cnt++] = 0;
	}
	
	//reinitialize buffer pointer
	bt_buff_wr_ptr = bt_buff;
}

static void waitTillReady(uint32_t timeout)
{
	uint32_t enter_time = HAL_GetTick();
	while(1)
	{
		if((HAL_GetTick() - enter_time) > timeout)
			break;
		if(HAL_UART_GetState(&BT_Handle) == HAL_UART_STATE_READY)
			break;
	}
}

static void handleBTReceivedData(void)
{
	char* startPtr,*endPtr;
	uint8_t appDataIndex = 0;
	if(endPtr == strstr(bt_buff,"\n\r"))
	{
		startPtr = strstr(bt_buff,"RECV SPP ");
		startPtr = (!startPtr)?(strstr(bt_buff,"RECV IAP ")):startPtr;
		if((startPtr != 0) && (startPtr < endPtr))
		{
			/*Received data from android or iPhone*/
			startPtr += 9;					//Next charecter after "RECV SPP "
			while(startPtr != endPtr)
			{
				dataFromApp[appDataIndex] = *startPtr;
				startPtr++;
				appDataIndex++;
			}
			flush_buff();
		}
	}
}



/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_Private_Function_Prototypes
  * @{
  */
/**
  * @}
  */ 

/** @defgroup Gateway_Board_ecan_out_Private_Functions
  * @{
  */ 
/**
  * @brief Configure the tuner peripherals.
  * @retval TUNER_OK if correct communication, else wrong communication
  */


	/**
  * @}
  */ 
  
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */ 

/************************ (C) COPYRIGHT ARKA-IMS *****END OF FILE****/
