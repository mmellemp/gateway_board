/**
  ******************************************************************************
  * @file    stm32f4_discovery_lcd.h
  * @author  MCD Application Team
  * @version V2.0.0
  * @date    18-February-2014
  * @brief   This file contains the common defines and functions prototypes for
  *          stm32f4_discovery_audio_codec.c driver.
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F4_DISCOVERY_LCD_H
#define __STM32F4_DISCOVERY_LCD_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* Include LCD component Driver */
#include "gateway_board.h"
#include "..\Components\st7565r\st7565r.h"
#include "..\..\..\Utilities\Fonts\fonts.h"

//#include <stdlib.h>

/** @addtogroup BSP
  * @{
  */

/** @addtogroup GATEWAY_BOARD
  * @{
  */ 
    
/** @defgroup GATEWAY_BOARD_LCD
  * @{
  */ 

/** @defgroup GATEWAY_BOARD_LCD_Exported_Types
  * @{
  */
	 
typedef enum 
{
  LCD_OK = 0,
  LCD_ERROR = 1,
  LCD_TIMEOUT = 2
}LCD_StatusTypeDef;	 
 

uint8_t BSP_LCD_Init(void);
uint32_t BSP_LCD_GetXSize(void);
uint32_t BSP_LCD_GetYSize(void);
void BSP_LCD_ClearScreen(void);
void BSP_LCD_ClearDisplayBuffer(void);
void BSP_LCD_SetDisplayWindow(uint8_t xmin, uint8_t ymin, uint8_t xmax, uint8_t ymax);
void BSP_LCD_ClearScreenArea(uint8_t Xmin, uint8_t Ymin, uint8_t Xmax, uint8_t Ymax);
void BSP_LCD_Paint(void);
void BSP_LCD_InvertArea(uint8_t Xpos, uint8_t Ypos, uint8_t Width, uint8_t Height);
void BSP_LCD_SetPixel(uint8_t Xpos, uint8_t Ypos, uint8_t Color);
void BSP_LCD_DrawLine(uint8_t X0, uint8_t Y0, uint8_t X1, uint8_t Y1, uint8_t Color);
void BSP_LCD_DrawRect(uint8_t Xpos, uint8_t Ypos, uint8_t Width, uint8_t Height, uint8_t Color);
void BSP_LCD_FillRect(uint8_t Xpos, uint8_t Ypos, uint8_t Width, uint8_t Height, uint8_t Color);
void BSP_LCD_DrawThickRect(uint8_t Xpos, uint8_t Ypos, uint8_t Width, uint8_t Height, uint8_t Xthk, uint8_t Ythk, uint8_t Color);
void BSP_LCD_DrawRectWithShadow(uint8_t Xpos, uint8_t Ypos, uint8_t Width, uint8_t Height, uint8_t Color);
void BSP_LCD_DrawCircle(uint8_t X0, uint8_t Y0, uint8_t Radius, uint8_t Color);
void BSP_LCD_FillCircle(uint8_t X0, uint8_t Y0, uint8_t Radius, uint8_t Color);

void BSP_LCD_SetTinyFont(sFONT *fonts);
void BSP_LCD_DrawTinyChar(uint8_t Xpos, uint8_t line, char c);
void BSP_LCD_DrawTinyString(uint8_t Xpos, uint8_t line, char *str);
void BSP_LCD_DrawTinyString_ammend(char *str);
void BSP_LCD_InvertTinyLine(uint8_t line);
void BSP_LCD_DrawTinyCharXY(uint8_t Xpos, uint8_t Ypos, char c);

void BSP_LCD_SetFont(sFONT *fonts);
sFONT* BSP_LCD_GetFont(void);
uint8_t BSP_LCD_DrawCharXY(uint8_t Xpos, uint8_t Ypos, char c);
void BSP_LCD_DrawStringXY(uint8_t Xpos, uint8_t Ypos, char *c);
void BSP_LCD_DrawBitmap(uint8_t Xpos,uint8_t Ypos,const uint8_t* BmpAddress,uint8_t BmpHeignt,uint8_t BmpWidth);

void BSP_LCD_VerticalGraph(uint8_t Xpos, uint8_t Ypos, uint8_t Width, uint8_t Height, uint8_t Val);
void BSP_LCD_HorizontalGraph(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint8_t val);

void BSP_LCD_DrawScreenBorder(void);
void BSP_LCD_DrawTital(char* Tital,uint8_t len);


//void BSP_LCD_Demo(void);
	 
#ifdef __cplusplus
}
#endif
   
#endif /* __STM32F4_DISCOVERY_LCD_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
